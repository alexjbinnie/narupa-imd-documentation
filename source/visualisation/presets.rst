Presets
=======

.. |clearfloat|  raw:: html

    <div style="clear: both;"></div>

.. sidebar:: Ball and Stick

    Standard ball and stick renderer using Narupa color scheme. It can be specified with the string:

    .. code-block:: json

        "ball and stick"

    This is shorthand for:

    .. code-block:: json

        {
          "color": {
            "type": "element",
            "scheme": "narupa element"
          },
          "render": "ball and stick"
        }

.. figure:: preset/ball_and_stick.png
  :width: 55%



|clearfloat|



.. sidebar:: Liquorice

    Standard liquorice renderer using Narupa color scheme. It can be specified with the string:

    .. code-block:: json

        "liquorice"

    This is shorthand for:

    .. code-block:: json

        {
          "color": {
            "type": "element",
            "scheme": "narupa element"
          },
          "render": "liquorice"
        }

.. figure:: preset/liquorice.png
  :width: 55%



|clearfloat|



.. sidebar:: Cycles

    Similar to liquorice, but fills in cycles of 6 atoms or fewer as a convex hull. The color scheme is the per amino acid color scheme defined at the RCSB, with green indicating non amino acids. It can be specified with the string:

    .. code-block:: json

        "cycles"

    This is shorthand for:

    .. code-block:: json

        {
          "color": "residue name",
          "render": "cycles"
        }

.. figure:: preset/cycles.png
  :width: 55%



|clearfloat|



.. sidebar:: Noodles

    Similar to liquorice, but curving the bonds based upon directions towards neighbours. It can be specified with the string:

    .. code-block:: json

        "noodles"

    This is shorthand for:

    .. code-block:: json

        {
          "color": "residue index in entity",
          "render": "noodles"
        }

.. figure:: preset/noodles.png
  :width: 55%



|clearfloat|



.. sidebar:: Goodsell

    Goodsell style rendering, mainly aimed at proteins. Outline indicates parts which are not nearby in residue index. It can be specified with the string:

    .. code-block:: json

        "goodsell"

    This is shorthand for:

    .. code-block:: json

        {
          "color": "goodsell",
          "render": "goodsell"
        }

.. figure:: preset/goodsell.png
  :width: 55%



|clearfloat|



.. sidebar:: Cartoon

    Protein cartoon rendering, using DSSP to calculate secondary structure. It can be specified with the string:

    .. code-block:: json

        "cartoon"

    This is shorthand for:

    .. code-block:: json

        {
          "sequence": "polypeptide",
          "color": "secondary structure",
          "width": "secondary structure",
          "render": "elliptic spline"
        }

.. figure:: preset/cartoon.png
  :width: 55%
