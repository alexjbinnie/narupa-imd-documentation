Examples
========

.. |clearfloat|  raw:: html

    <div style="clear: both;"></div>

.. sidebar:: Hodgkin's Penicillin

    Ball and stick representation based upon Dorothy Hodgkins model of penicillin. This is a good example of a completely custom color scheme.

    .. code-block:: json

        {
          "color": {
            "type": "element",
            "scheme": {
              "C": "8E5234",
              "O": "E08B14",
              "N": "5E7242",
              "S": "4D5377",
              "P": "8E3D73",
              "H": "E2E0E2"
            }
          },
          "scale": {
            "type": "vdw",
            "scale": 0.25
          },
          "render": {
            "type": "ball and stick",
            "bond.scale": 0.01,
            "bond.color": "564632"
          }
        }

.. figure:: example/hodgkin.png
  :width: 55%



|clearfloat|



.. sidebar:: Geometric Cartoon

    An example of a stark geometric cartoon of a protein. This shows an example of a secondary structure based scheme (here controlling the width of the curve), include using the default keyword.

    .. code-block:: json

        {
          "sequence": "polypeptide",
          "width": {
            "type": "secondary structure",
            "scheme": {
              "H": 0.4,
              "G": 0.4,
              "I": 0.4,
              "B": 0.3,
              "T": 0.3,
              "E": 0.2,
              "S": 0.2,
              "$default": 0.1
            }
          },
          "color": "secondary structure",
          "render": {
            "type": "geometric spline",
            "scale": 0.01
          }
        }

.. figure:: example/geometric_cartoon.png
  :width: 55%



|clearfloat|



.. sidebar:: Coloring by Arbitrary Field

    An example of coloring by an arbitrary field (here, the server has provided an array of floats with the key "particle.bfactors")

    .. code-block:: json

        {
          "color": {
            "type": "float field",
            "key": "$particle.bfactors",
            "min": 15,
            "max": 50,
            "gradient": [
              "red",
              "green",
              "blue"
            ]
          },
          "render": "ball and stick"
        }

.. figure:: example/bfactor.png
  :width: 55%


