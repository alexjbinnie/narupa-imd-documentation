Development
###########

.. graphviz::

    digraph D {

      node [shape=plaintext fontname="Monospace" fontsize="8"];

      master [ label="master"];
      narupa2_ui [ label="narupa2-ui"];
      shared_state_window [ label="shared-state-window" ]
      documentation [ label="docfx-documentation"];
      purge_narupaxr [ label="purge-narupaxr"];
      remote_multiplayer [ label="remote-multiplayer"];
      teleport [ label="teleport"];
      vis_refactor [ label="vis-refactor"];
      state_service [ label="state-service"];
      hyperballs [ label="hyperballs"];
      color_by_velocity [ label="color-by-velocity" ]

      master -> narupa2_ui;
      narupa2_ui -> purge_narupaxr
      purge_narupaxr -> documentation
      purge_narupaxr -> remote_multiplayer
      remote_multiplayer -> teleport
      documentation -> vis_refactor
      master -> state_service
      master -> hyperballs
      master -> color_by_velocity
      master -> shared_state_window
      shared_state_window -> remote_multiplayer
    }