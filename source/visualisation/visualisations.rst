Visualisations
==============

.. default-domain:: cs
.. default-role:: any

Visualiser Node
---------------

A visualiser node is a single class who has one or more fields which are visualisation properties. These properties are normally either marked with `UnityEngine.SerializeFieldAttribute` and represent inputs, or are private fields which indicate outputs. All nodes implement the interface `IVisualisationNode`, which defines the refresh method `IVisualisationNode.Refresh`. Refreshing a node tells it to

* Check if any of its inputs are dirty. This uses the `Property.IsDirty` field of its properties it considers 'inputs'.
* If any inputs are dirty, the outputs are updated. This is when the logic of the node occurs. The inputs are then set to not be dirty.

These nodes form the building blocks of the logic that makes up visualisers. Example nodes include the `ElementColorMappingNode`, which takes a list of atomic `Element` values and converts them into a list of colors based upon some defined mapping, or the `ParticleSphereRendererNode` which renders a set of spheres given lists of positions, radii and colors.

Adaptor Nodes
-------------

There are an important group of nodes known as adaptor nodes. Unlike most nodes, which have a few inputs and outputs with defined names, an Adaptor node can dynamically provide whatever property is asked of it.

Visualiser Subgraph
-------------------

Each node defines a logical step, however it does not define where its inputs and outputs have to be. This is why the logical blocks in which you build a visualiser from are actually scriptable objects which are an instance of `VisualisationSubgraph`. A subgraph consists of

* One or more nodes to perform logic.
* Zero or more input nodes that derive from `IInputNode`
* Zero or more output nodes that derive from `IOutputNode`

The important thing about these inputs and outputs are that they are labelled with well defined keys. For example, the *element* color subgraph consists of the aforementioned `ElementColorMappingNode`, but also has an element input named *particle.elements* and an output called *particle.colors*. These names are important as they allow nodes to indicate exactly what their inputs and outputs represent. As described next, it facilitates chaining subgraphs together and having them automatically link together.


Visualiser Construction
-----------------------

Visualisers are defined using JSON, but nominally can be defined in any language that can be considered to be a combination of lists, dictionaries and values. After the data has been deserialized into either a `~Dictionary{string,object}` or a `~IReadOnlyList{object}`, it can be passed to `VisualiserFactory.ConstructVisualiser`. This static methods creates a new `~UnityEngine.GameObject` which has a `VisualisationSceneGraph` component. This scene graph contains a list of all the nodes that make up the visualiser.

Consider the following

.. code-block:: json

    {
        "color": "element",
        "render": "ball and stick"
    }

When the Visualiser Factory receives this information, the following steps occur

#. A `ParentedAdaptorNode` is created. This is the entry point to the visualiser, and can be be parented to a source of information to render (normally a `FrameAdaptorNode`).

#. A `ParticleFilterNode` is created. This sits after the root adaptor created above. It has an optional filter input, and if provided will automatically filter the values provided by the aforementioned `ParentedAdaptorNode`. All other nodes will eventually be asking this node for a value such as an element array, particle count etc.

#. The provided dictionary is looked through, and subgraphs are found

    * If there is a *filter* key, look up the provided name to see if it is the name of a filter subgraph. These subgraphs filter the particles at the visualiser level, such as selecting only DNA or amino acid residues.

    * If there is a *sequence* key, look up the name to find a sequence subgraph. These subgraphs find sequences of values and filter the data, as well as providing a list of int's which define the various sequence lengths

    * If there is a *color* key, try finding a color subgraph

    * If there is a *width* key, try finding a width subgraph

    * If there is a *scale* key, try finding a scale subgraph

    * If there is a *render* key, try finding a render subgraph

        * If no render subgraph is provided, this defaults to *ball and stick*

    * If any of the subgraphs have an input labelled `sequence.lengths` (such as the *spline* render subgraph), then go back and inserted in a default sequence subgraph.

#. Check if any of the subgraphs require the key *residue.secondarystructure*. If so, then a node is inserted between the `ParentedAdaptorNode` and the `ParticleFilterNode` which does the DSSP calculation and provides a *residue.secondarystructure* key.

#. The subgraphs are instantiated. This gives copies of all the nodes, which will be used for our visualiser.

#. For each subgraph

    * If a subgraph has an adaptor input, look for the previous adaptor in the chain and assign it.

#. For each subgraph

    * For each **input** in this subgraph

        #. See if a value is provided for it directly in the visualisation JSON. If so, set value and return.

        #. If not, is it defined in the root visualisation JSON? If so, set value and return.

        #. Go back through the previous subgraphs, check if they have any ouputs which match up. If so, link together and return.

        #. If there was a default value for this input, then just use that and return.

        #. Go back through previous subgraphs. If any had an adaptor, use a dynamic property provided by that and return.

        #. Finally, if all else fails, use a dynamic property provided by the root `ParticleFilterNode`.

#. A `~UnityEngine.GameObject` with a `VisualisationSceneGraph` is created, the nodes are placed here, and the finished visualiser is returned.