Properties
==========

.. default-domain:: cs
.. default-role:: any

A core part of the visualisation system is the idea of a property (not to be confused with C# properties). At the heart of it, a property is a wrapper around a value. However, they also provide useful tools for decoupling links between values and form the basis of the graph approach for molecular rendering.

Read Only Properties
--------------------

The root of all properties is the `IReadOnlyProperty` interface. This is a wrapper around a value, **which may not be present**. There is a **difference** between a property having a value which is null, and a property not having a value at all. A property can be queried whether it has a value using the `IReadOnlyProperty.HasValue` property. If this is true, then a call to `IReadOnlyProperty.Value` will give the current value of the property. The ungeneric `IReadOnlyProperty` returns an `System.Object`, but the type can be obtained from `IReadOnlyProperty.PropertyType`.

.. warning::

    Calling `IReadOnlyProperty.Value` when the property does not have a value will throw an exception. Always check first with `IReadOnlyProperty.HasValue` before attempting to access the value, unless you are sure it exists.

    .. code-block: c#

        if(property.HasValue) {
            // do something with property.Value
        }

Finally, `IReadOnlyProperty` also exposes the event `IReadOnlyProperty.ValueChanged`, which is triggered when the value is altered.

.. note::

    It is important to note that a property only knows its value has changed if it is told so. If the value is mutable and altered elsewhere, then the change is not detected. This is nominally overcome by setting the value of a property to its current value to trigger an update.

There is also the generic version `IReadOnlyProperty{TValue}`, who represents a read only property with the given type. This is the more common interface to work with, with the ungeneric one mainly required for internal calculations involving reflection.

Writable Properties
-------------------

From this pair of read only properties, two other interfaces `IProperty` and `IProperty{TValue}` define properties which can have the value altered. The generic version has its value changed by simply assigning `IProperty{TValue}.Value`. The value can also be removed, by calling `IProperty.UndefineValue()`.

Writable properties also leverage the ability to be **linked**. If `IProperty.LinkedProperty` is set to another property, then it now mirrors the linked property:

.. code-block:: c#

    // A float property with no value provided
    property1 = new FloatProperty();

    // A float property with a value of 0.5
    property2 = new FloatProperty() {
        Value = 0.5f
    }


    property1.HasValue
    >>> false


    // Link property1 so it points to property2
    property1.LinkedProperty = property2


    property1.HasValue
    >>> true

    property1.Value
    >>> 0.5


    // Update property2's value
    property2.Value = 2.0


    // Property 1 points to the updated value
    property1.Value
    >>> 2.0


Implementation Details of Properties
------------------------------------

The above sections describe the interfaces which define properties in an abstract sense. For using properties, see the following section. Here, we will describe the implementation details of properties.

The base class for concreate properties is the ungeneric abstract class `Property`, which inherits from `IProperty`. It mostly just maps the interface implementation to abstract methods, but it also also has a method `Property.IsDirty`, which is described in the section on nodes.

From `Property` we get the abstract generic `LinkableProperty{TValue}` which is an implementation of a property with a type. It stores the linked property, and handles checking if it has a linked property and delegating `Property.HasValue` and `Property.Value` if so. It also performs a form of caching. It internally stores a reference to its current value, even if the current value is provided by a linked property. This means that for a long chain of properties chained together, repeated calls to `Property.Value` do not have to propagate down the chain every time. This cache is automatically updated thanks to the `Property.ValueChanged` event.

There are two implementations of `LinkableProperty{TValue}` that all the concrete property types you will use derive from. The first is a `SerializableProperty<TValue>`. This serializes a bool indicating if a value is provided and the current value using Unity's serialization system. This means that if you have a property on an object in your Unity scene and sets its value in the Editor, then it will persist.

.. warning::

    All implementations of properties do **not** serialize the linked property. This means that by themselves, properties cannot store who they are linked to outside of the running application. This is dealt with by Visualisation Components.

For properties which should point at interfaces, the `InterfaceProperty{TValue}` class exists. As Unity cannot serialize interfaces, instead this property stores a reference to a `UnityEngine.Object`, such as a component or a scriptable object. This property checks if this object implements said interface, and if so returns the object. If the object is the wrong type, the value of this property is null.

Using Properties
----------------

Properties are commonly dealt with using pre defined classes for each type. As Unity cannot serialize types such as `~LinkableProperty{System.Float}`, instead we use classes such as `~FloatProperty`. A list of current implemented properties in Narupa are given below:

.. list-table:: Currently implemented properties
    :header-rows: 1

    * - `{TValue}`
      - `IProperty{{TValue}}`
      - `IProperty{{TValue}[]}`
    * - `~System.Boolean`
      - `BoolProperty`
      -
    * - `System.Float`
      - `FloatProperty`
      - `FloatArrayProperty`
    * - `System.Int`
      - `IntProperty`
      - `IntArrayProperty`
    * - `System.String`
      - `StringProperty`
      - `StringArrayProperty`
    * - `~UnityEngine.Vector3`
      - `Vector3Property`
      - `Vector3ArrayProperty`
    * - `~UnityEngine.Color`
      - `ColorProperty`
      - `ColorArrayProperty`
    * - `~UnityEngine.Material`
      - `MaterialProperty`
      -
    * - `~UnityEngine.Mesh`
      - `MeshProperty`
      -
    * - `~UnityEngine.Gradient`
      - `GradientProperty`
      -
    * - `~FrameAdaptor`
      - `FrameAdaptorProperty`
      -
    * - `~IMapping{Science.Element, UnityEngine.Color}`
      - `ElementColorMappingProperty`
      -
    * - `BondPair`
      -
      - `BondArrayProperty`
    * - `~Science.Element`
      -
      - `ElementArrayProperty`
    * - `~SecondaryStructureAssignment`
      -
      - `SecondaryStructureArrayProperty`
    * - `~IReadOnlyList{System.Int32}`
      -
      - `SelectionArrayProperty`
    * - `SplineSegment`
      -
      - `SplineArrayProperty`

Implementing a Property for a New Type
--------------------------------------

To implement a property for a new type, the following must be done:

* Decide on whether the value should be serialized directly by deriving from `LinkableProperty{{TValue}}` (in the case that Unity can serialize your type) or via an interface (using `InterfaceProperty{{TValue}}`).
* Implement the class. No methods should have to be overriden. However, in order to allow Unity to serialize your property it is **vital** that you decorate your class with the `System.SerializableAttribute` attribute.

For example, to add a property that contains a rectangle (as defined by `UnityEngine.Rect`):

.. code-block:: csharp

    [Serializable]
    public class RectProperty : SerializableProperty<Rect>
    {
    }
