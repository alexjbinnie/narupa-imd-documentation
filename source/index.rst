.. Narupa iMD documentation master file, created by
   sphinx-quickstart on Fri May  1 01:25:12 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Narupa iMD's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   structure/vr-ui

   visualisation/toc

   narupa/toc
   narupa-imd/toc



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
