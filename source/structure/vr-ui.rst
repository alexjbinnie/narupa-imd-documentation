User Interface and Virtual Reality
==================================

.. default-domain:: cs
.. default-role:: any

This page covers the components and code which handle the user interface and virtual reality aspects of Narupa iMD. It will assume basic familiarity with Unity concepts such as components, game objects, scenes, prefabs and instantiation.

SteamVR
-------

Narupa iMD uses the SteamVR plugin to provide a consistent experience across several VR devices including the HTV Vive, Value Index and Oculus Rift (S). This plugin does the following:

* Allows us to define a set of standard actions, such as clicking, interacting and resizing the system. The various inputs of different VR controllers can be mapped to these actions within the SteamVR interface. From the perspective of the application, this abstracts which actual buttons are being pressed.
* Ensures the camera is tracked to the user's head location

SteamVR also provides two components for each controller, which do the following:

* The two controllers are positioned by the `~Valve.VR.SteamVR_Behaviour_Pose` component, which takes the current position and rotation of the user's controllers and positions them in 3D space.
* There is also a `~Valve.VR.SteamVR_RenderModel` component, which loads the 3D model of the given controller from the SteamVR library. SteamVR also automatically animates this controller base on inputs such as the amount a trigger is being squeezed.


Controllers
-----------

Narupa adds a `~Controllers.VrController` component, which should be treated as the **canonical** representation of the left or right hand. The scene will have **two and only two** instances of *VrController*, which will be situated on the same game object as the corresponding `~Valve.VR.SteamVR_Behaviour_Pose`.

Each of the three systems we support has the pivot point of the controller defined in a slightly different way. This means that the location at which to draw a *Gizmo*, such as a cursor, depends on the system in question. This is accounted for by:

* Having a different prefab for each controller type. These prefabs have a `~Controllers.VrControllerPrefab` component to indicate what they represent. Each prefab also includes transform to represent various pivot points of the controllers, including the *cursor* (positioned off the end), *grip* (positioned approximately where the hand is grasped) and *head* (positioned where the bulk of a controllers buttons are situated). Note there may be different prefabs for the left and right controllers, depending on if the controller is symmetric.
* For each prefab, there is a `~Controllers.SteamVrControllerDefinition` in the Resources/ folder, which provides a link between the *SteamVR Controller Type* and the two prefabs representing the left and right prefabs.
* A `~Controllers.SteamVrDynamicController` component on each of the two controllers in the scene listens to the `~Valve.VR.SteamVR_Behaviour_Pose.onDeviceIndexChanged` event, which occurs when a controller is first turned on. SteamVR is queried about the current *SteamVR Controller Type*, and then the applications looks for any `~Controllers.SteamVrControllerDefinition` in Resources that correspond. If a match is found, that prefab is instantiated in the scene.

Finally, there is the `~Controllers.ControllerManager`, which is a singleton in the scene who contains references to the left and right `~Controllers.VrController`. Other parts of the scene should only have to interact with the *ControllerManager* and its members.

.. figure:: controller_pivots.png
  :width: 100%
  :alt: Controller Pivot Locations

  Indication of the various pivot points of the Valve Index, Oculus Rift and HTC Vive as defined in Narupa. The **grey** point is the location that SteamVR provides for the controller. SteamVR also provides the models of the controllers, correctly offset relative to the grey point. As each controller is positioned differently, we need to define the cursor (**yellow**), grip (**green**) and head (**blue**) positions for each controller seperately, hence the need for `~Controllers.VrControllerPrefab`.

Input Modes
-----------

Depending on the current state of the application, different actions may be possible. If a main menu is open, then only UI actions such as clicking make sense. When connected to a server, actions such as resizing the box and interacting with particles becomes appropriate. This is managed by the Input Mode System.

A `~Controllers.ControllerInputMode` is a component in the scene which represents the current input state, and defines both how the controllers should appear to the user and what actions should currently be possible. Only one such mode can be in use at once. This is managed by the `~Controllers.ControllerManager`. When a `~Controllers.ControllerInputMode` becomes active and enabled, it registers itself using `~ControllerManager.AddInputMode`. There, it is added to the list of possible input modes. Each input mode has a *Priority*, with higher priority taking precedent. When a new input mode is added, it is inserted into the list in order of priority. If this means it would be the first item in the list, then this mode becomes *Current*. If there was already another input node who was *Current*, that one has `~ControllerInputMode.OnModeEnded` called and the new one has `~ControllerInputMode.OnModeStarted`. This pair of `~ControllerInputMode.OnModeStarted`/`~ControllerInputMode.OnModeEnded` methods are where an input mode should setup or teardown its state. It is guaranteed that only one input mode can be in use in this way at any one time.

When an input mode is no longer valid (when its game object becomes disabled), `~ControllerManager.RemoveInputMode` is called to remove the input mode. If it was the current input mode, then the next mode in the list will become the new current input mode.

Consider the following scenario:

* Initially, the application has no input modes
* The main menu is displayed. This activates the UI Input Mode
* The UI Input Mode becomes the `~ControllerManager.CurrentInputMode`. The controller displays a cursor and can be used to interact with UI
* The user connects to a simulation. The simulation input mode is also enabled. However, it has a lower priority than the UI Input Mode and nothing happens
* The menu disappears when the connection is established. The UI input mode deactivates and the Simulation Input Mode is now the `~ControllerManager.CurrentInputMode`. The cursor becomes an interactable point and the user can now manipulate the box and interact with molecules. The user can also bring up the radial menus
* If the user opens a radial menu, the UI input mode becomes active again. Even through the simulation input mode is still active, becomes the UI has a higher priority it becomes the `~ControllerManager.CurrentInputMode`. The cursor goes back to being for UI, and interacting with the system is disabled

World Space UI
--------------

Narupa uses the standard Unity UI system. However, to allow interacting with UI with the controller directly, instead of a laser pointer style approach, the following occurs:

* There is a global `~UI.WorldSpaceCursorInput` component, which derives from `~UnityEngine.EventSystems.BaseInput`. This is the standard way for feeding in fake UI input, as it allows the cursors position to be overriden without having to reimplements the rest of the UI system. To work, this requires

    * A reference to the current UI `~UnityEngine.UI.Canvas`
    * A reference to the cursor position. This is normally based upon the *cursor* pivot described in the section on VR Controller Prefabs
    * A reference to an action which represents a 'click'. This is mapped to a SteamVR action

* The standard `~UnityEngine.EventSystems.EventSystem` and `~UnityEngine.EventSystems.StandaloneInputModule` are also overriden with Narupa specific implementations, but this is purely to expose a few protected variables and to ensure that losing focus of the application does not prevent the UI from functioning.

This system currently means only one canvas can be active at once. To be able to use the world space input system, `~WorldSpaceCursorInput.SetCanvasAndCursor` must be called, providing the three parameters describes above. When the canvas closes, `~WorldSpaceCursorInput.ReleaseCanvas` should be called. Attaching the `~UI.PhysicalCanvasInput` component to a canvas handles these methods automatically in `~UnityEngine.MonoBehaviour.OnEnable`/`~UnityEngine.MonoBehaviour.OnDisable`.

User Interface Management
-------------------------

User interface is nominally divided into *scenes* (not to be confused with the Unity scene), such as a pause menu or a connect screen. Switching between these scenes is the domain of the `~UI.UserInterfaceManager`. The method `~UserInterfaceManager.GotoScene` instantiates the provided game object in the given scene and displays it. A subsequent to call to `~UserInterfaceManager.CloseScene` will destroy the current scene. If `~UserInterfaceManager.GotoSceneAndAddToStack` is used, the previous scene is stored in a stack. This allows the following scene to call `~UserInterfaceManager.GoBack`. This will open up the previous scene, ensuring that scene's with go back buttons don't actually have to know what they go back to.

The radial menu's on the left and right hand both have separate `~UI.PopupUserInterfaceManager`, which has additional methods which ensure that it spawns facing the user at the location of the hand, and that they disappear when the button which brought them up is let go. This makes them ideal for quick action menus, and prevents visual clutter from showing UI when not in use.
