BondPair
========
.. cs:setscope:: Narupa.Frame

.. cs:struct:: [Serializable] public struct BondPair
    
    Represents a bond between two particles with indices A and B
    
    

    
    .. cs:field:: public int A
        
        The index of the first particle
        
        

        :returns System.Int32: 
        
    .. cs:field:: public int B
        
        The index of the second particle
        
        

        :returns System.Int32: 
        
    .. cs:constructor:: public BondPair(int a, int b)
        
        Construct a BondPair for a pair of two indices
        
        

        :param System.Int32 a: 
        :param System.Int32 b: 
        
    .. cs:indexer:: public int this[int i] { get; }
        
        Get the first and second particles involved in this bond.
        
        

        :param System.Int32 i: 
        :returns System.Int32: 
        
