OutgoingStreamCollection<TOutgoing, TReply>.CreateStreamCall
============================================================
.. cs:setscope:: Narupa.Grpc.Stream

.. cs:delegate:: public delegate OutgoingStream<TOutgoing, TReply> CreateStreamCall(CancellationToken token);
    
    :param System.Threading.CancellationToken token: 
    :returns Narupa.Grpc.Stream.OutgoingStream{{TOutgoing},{TReply}}: 
    
