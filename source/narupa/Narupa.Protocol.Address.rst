Address
=======
.. cs:setscope:: Narupa.Protocol

.. cs:class:: public sealed class Address : Object, IMessage<Address>, IMessage, IEquatable<Address>, IDeepCloneable<Address>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Address}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Address}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Address}`
    
    .. cs:field:: public const int Address_FieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int PortFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public Address()
        
        
    .. cs:constructor:: public Address(Address other)
        
        :param Narupa.Protocol.Address other: 
        
    .. cs:method:: public Address Clone()
        
        :returns Narupa.Protocol.Address: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(Address other)
        
        :param Narupa.Protocol.Address other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(Address other)
        
        :param Narupa.Protocol.Address other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<Address> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Address}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string Address_ { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public uint Port { get; set; }
        
        :returns System.UInt32: 
        
