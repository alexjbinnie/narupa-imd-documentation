GrpcConnection
==============
.. cs:setscope:: Narupa.Grpc

.. cs:class:: public sealed class GrpcConnection : ICancellationTokenSource, IAsyncClosable
    
    Represents a connection to a server using the GRPC protocol. A GRPC
    server provides several services, which can be connected to using
    :cs:any:`~Narupa.Grpc.GrpcConnection.Channel`. An implementation of a client for
    a GRPC service should hold this connection and reference the
    CancellationToken to shut itself down when the connection is terminated.
    
    

    :implements: :cs:any:`~Narupa.Core.Async.ICancellationTokenSource`
    :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable`
    
    .. cs:property:: [CanBeNull] public Channel Channel { get; }
        
        GRPC channel which represents a connection to a GRPC server.
        
        

        :returns Grpc.Core.Channel: 
        
    .. cs:property:: public bool IsCancelled { get; }
        
        Is this connection cancelled?
        
        

        :returns System.Boolean: 
        
    .. cs:constructor:: public GrpcConnection(string address, int port)
        
        Create a new connection to a GRPC server and begin connecting.
        
        

        :param System.String address: The ip address of the server.
        :param System.Int32 port: The port of the server.
        
    .. cs:method:: public CancellationToken GetCancellationToken()
        
        

        :implements: :cs:any:`~Narupa.Core.Async.ICancellationTokenSource.GetCancellationToken`
        :returns System.Threading.CancellationToken: 
        
    .. cs:method:: public Task CloseAsync()
        
        Closes the GRPC channel asynchronously. This can be awaited or
        executed in the background.
        
        

        :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable.CloseAsync`
        :returns System.Threading.Tasks.Task: 
        
