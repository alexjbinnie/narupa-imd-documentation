IPosedObject
============
.. cs:setscope:: Narupa.Frontend.Input

.. cs:interface:: public interface IPosedObject
    
    Represents an object posed in 3D space.
    
    

    
    .. cs:property:: Transformation? Pose { get; }
        
        The pose of the object, if available.
        
        

        :returns System.Nullable{Narupa.Core.Math.Transformation}: 
        
    .. cs:event:: event Action PoseChanged
        
        Occurs when the object's pose changes.
        
        

        :returns System.Action: 
        
