ElementVdwRadii
===============
.. cs:setscope:: Narupa.Core.Science

.. cs:class:: public static class ElementVdwRadii
    
    Van der Waals radii of the atomic elements
    
    

    The value of the Van der Waals radius is from three sources
    [1] Bondi, A. (1964). "Van der Waals Volumes and Radii".  J. Phys. Chem. 68
    (3): 441-451. doi:10.1021/j100785a001.
    [2] Rowland and Taylor (1996). "Intermolecular Nonbonded Contact Distances in
    Organic Crystal Structures: Comparison with Distances Expected from van der
    Waals Radii". J. Phys. Chem., 1996, 100 (18), 7384.7391. doi:10.1021/jp953141+.
    [3] Mantina, et al. (2009). "Consistent van der Waals Radii for the Whole Main
    Group". J. Phys. Chem. A, 2009, 113 (19), 5806-5812. doi:10.1021/jp8111556.
    Some elements do not have a Van der Waals radii provided, as there is
    insufficient literature to provide a value.
    
    

    
    .. cs:method:: public static float? GetVdwRadius(this Element element)
        
        Get the Van der Waals radius of the element, returning null if that data does
        not exist.
        
        

        :param Narupa.Core.Science.Element element: 
        :returns System.Nullable{System.Single}: 
        
