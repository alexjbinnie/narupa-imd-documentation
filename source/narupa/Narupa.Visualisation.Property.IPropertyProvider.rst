IPropertyProvider
=================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:interface:: public interface IPropertyProvider
    
    Describes something which provides access to visualisation properties.
    
    

    See :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty` for more information.
    
    

    
    .. cs:method:: IReadOnlyProperty GetProperty(string name)
        
        Get a property which exists with the given name. Returns null if the property
        with a given name is null.
        
        

        :param System.String name: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:method:: IEnumerable<(string name, IReadOnlyProperty property)> GetProperties()
        
        Get all properties and their associated keys that exist on this object.
        
        

        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,Narupa.Visualisation.Property.IReadOnlyProperty}}: 
        
