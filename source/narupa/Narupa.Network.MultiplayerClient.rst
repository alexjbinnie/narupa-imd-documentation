MultiplayerClient
=================
.. cs:setscope:: Narupa.Network

.. cs:class:: public class MultiplayerClient : GrpcClient<Multiplayer.MultiplayerClient>, ICancellable, ICancellationTokenSource, IDisposable, IAsyncClosable
    
    Wraps a :cs:any:`~Narupa.Protocol.Multiplayer.Multiplayer.MultiplayerClient` and
    provides access to avatars and the shared key/value store on the server
    over a :cs:any:`~Narupa.Grpc.GrpcConnection`.
    
    

    :inherits: :cs:any:`~Narupa.Grpc.GrpcClient{Narupa.Protocol.Multiplayer.Multiplayer.MultiplayerClient}`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellationTokenSource`
    :implements: :cs:any:`~System.IDisposable`
    :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable`
    
    .. cs:constructor:: public MultiplayerClient([NotNull] GrpcConnection connection)
        
        :param Narupa.Grpc.GrpcConnection connection: 
        
    .. cs:method:: public Task<CreatePlayerResponse> CreatePlayer(string name)
        
        Requests a new player ID from the server.
        
        

        Corresponds to the CreatePlayer gRPC call.
        
        

        :param System.String name: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Multiplayer.CreatePlayerResponse}: 
        
    .. cs:method:: public IncomingStream<Avatar> SubscribeAvatars(float updateInterval = 0.0333333351F, string ignorePlayerId = "", CancellationToken externalToken = default(CancellationToken))
        
        Starts an :cs:any:`~Narupa.Grpc.Stream.IncomingStream`1` on which the server
        provides the latest avatar positions for each player, at the 
        requested time interval (in seconds).
        
        

        Corresponds to the SubscribePlayerAvatars gRPC call.
        
        

        :param System.Single updateInterval: How many seconds the service should wait and aggregate updates  between  sending them to us.
        :param System.String ignorePlayerId: 
        :param System.Threading.CancellationToken externalToken: 
        :returns Narupa.Grpc.Stream.IncomingStream{Narupa.Protocol.Multiplayer.Avatar}: 
        
    .. cs:method:: public OutgoingStream<Avatar, StreamEndedResponse> PublishAvatar(string playerId, CancellationToken externalToken = default(CancellationToken))
        
        Starts an :cs:any:`~Narupa.Grpc.Stream.OutgoingStream`2` 
        where updates to the local player's avatar can be published to the
        service.
        
        

        Corresponds to the UpdatePlayerAvatar gRPC call.
        
        

        :param System.String playerId: 
        :param System.Threading.CancellationToken externalToken: 
        :returns Narupa.Grpc.Stream.OutgoingStream{Narupa.Protocol.Multiplayer.Avatar,Narupa.Protocol.Multiplayer.StreamEndedResponse}: 
        
    .. cs:method:: public IncomingStream<ResourceValuesUpdate> SubscribeAllResourceValues(float updateInterval = 0F, CancellationToken externalToken = default(CancellationToken))
        
        Starts an :cs:any:`~Narupa.Grpc.Stream.IncomingStream`1` on 
        which the server provides updates to the shared key/value store at 
        the requested time interval (in seconds).
        
        

        Corresponds to the SubscribeAllResourceValues gRPC call.
        
        

        :param System.Single updateInterval: 
        :param System.Threading.CancellationToken externalToken: 
        :returns Narupa.Grpc.Stream.IncomingStream{Narupa.Protocol.Multiplayer.ResourceValuesUpdate}: 
        
    .. cs:method:: public Task<bool> SetResourceValue(string playerId, string key, Value value)
        
        Attempts a change, on behalf of the given player, to the shared
        key value store. This will fail if the key is locked by someone 
        other than the player making the attempt.
        
        

        Corresponds to the SetResourceValueAsync gRPC call.
        
        

        :param System.String playerId: 
        :param System.String key: 
        :param Google.Protobuf.WellKnownTypes.Value value: 
        :returns System.Threading.Tasks.Task{System.Boolean}: 
        
    .. cs:method:: public Task<bool> LockResource(string playerId, string key)
        
        Attempts to lock, on behalf of the given player, a key in the 
        shared key value store. This will fail if the key is locked by 
        someone other than the player making the attempt.
        
        

        Corresponds to the AcquireResourceLockAsync gRPC call.
        
        

        :param System.String playerId: 
        :param System.String key: 
        :returns System.Threading.Tasks.Task{System.Boolean}: 
        
    .. cs:method:: public Task<bool> ReleaseResource(string playerId, string key)
        
        Attempts, on behalf of the given player, to unlock a key in the
        shared key value store. This will fail if the key is not locked by
        that player.
        
        

        Corresponds to the ReleaseResourceLockAsync gRPC call.
        
        

        :param System.String playerId: 
        :param System.String key: 
        :returns System.Threading.Tasks.Task{System.Boolean}: 
        
