SteamVrControllerDefinition
===========================
.. cs:setscope:: Narupa.Frontend.Controllers

.. cs:class:: [CreateAssetMenu(menuName = "SteamVR Controller Definition", fileName = "controller")] public class SteamVrControllerDefinition : ScriptableObject
    
    Defines for a given SteamVR controller type the prefabs to use for each hand.
    These provide the pivots for where to draw tools, as well as where to position
    UI over the physical buttons of the controller.
    
    

    :inherits: :cs:any:`~UnityEngine.ScriptableObject`
    
    .. cs:method:: public static SteamVrControllerDefinition GetControllerDefinition(string id)
        
        Get the controller definition for a given SteamVR id.
        
        

        :param System.String id: 
        :returns Narupa.Frontend.Controllers.SteamVrControllerDefinition: 
        
    .. cs:method:: public VrControllerPrefab GetPrefab(SteamVR_Input_Sources input)
        
        Get the prefab for the given input source.
        
        

        :param Valve.VR.SteamVR_Input_Sources input: 
        :returns Narupa.Frontend.Controllers.VrControllerPrefab: 
        
