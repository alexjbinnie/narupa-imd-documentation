Spline
======
.. cs:setscope:: Narupa.Visualisation.Components.Calculator

.. cs:class:: public class Spline : VisualisationComponent<SplineNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<SplineNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Spline.SplineNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Spline.SplineNode}`
    
