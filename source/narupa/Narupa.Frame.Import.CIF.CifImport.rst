CifImport
=========
.. cs:setscope:: Narupa.Frame.Import.CIF

.. cs:class:: public class CifImport : CifBaseImport
    
    Importer for the mmCIF file format, used by the RCSB database.
    
    

    Information on this file format can be found at
    http://mmcif.wwpdb.org/pdbx-mmcif-home-page.html
    
    

    :inherits: :cs:any:`~Narupa.Frame.Import.CIF.CifBaseImport`
    
    .. cs:method:: public static Frame Import(TextReader reader, ChemicalComponentDictionary dictionary, IProgress<string> progress = null)
        
        Import a CIF file from the given source, with an optional chemical component
        dictionary
        
        

        :param System.IO.TextReader reader: 
        :param Narupa.Frame.Import.CIF.Components.ChemicalComponentDictionary dictionary: 
        :param System.IProgress{System.String} progress: 
        :returns Narupa.Frame.Frame: 
        
    .. cs:method:: protected override CifBaseImport.ParseTableRow GetTableHandler(string category, List<string> keywords)
        
        :param System.String category: 
        :param System.Collections.Generic.List{System.String} keywords: 
        :returns Narupa.Frame.Import.CIF.CifBaseImport.ParseTableRow: 
        
    .. cs:method:: protected override bool ShouldParseCategory(string category)
        
        :param System.String category: 
        :returns System.Boolean: 
        
