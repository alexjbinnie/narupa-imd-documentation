VisualisationComponent<TNode>
=============================
.. cs:setscope:: Narupa.Visualisation.Components

.. cs:class:: [ExecuteAlways] public abstract class VisualisationComponent<TNode> : VisualisationComponent, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<TNode> where TNode : new()
    
    Wrapper :cs:any:`~UnityEngine.MonoBehaviour` around a node of the visualisation system,
    storing the node has a serialized parameter.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{{TNode}}`
    
    .. cs:field:: [SerializeField] protected TNode node
        
        :returns {TNode}: 
        
    .. cs:method:: protected override Type GetWrappedVisualisationNodeType()
        
        :returns System.Type: 
        
    .. cs:method:: public override object GetWrappedVisualisationNode()
        
        :returns System.Object: 
        
    .. cs:property:: public TNode Node { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{{TNode}}.Node`
        :returns {TNode}: 
        
