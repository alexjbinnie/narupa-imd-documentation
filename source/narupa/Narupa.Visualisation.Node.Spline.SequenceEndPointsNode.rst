SequenceEndPointsNode
=====================
.. cs:setscope:: Narupa.Visualisation.Node.Spline

.. cs:class:: [Serializable] public class SequenceEndPointsNode : GenericOutputNode
    
    Get the indices of the end points of a set of sequences.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:method:: protected override void UpdateOutput()
        
        

        
    .. cs:method:: protected override void ClearOutput()
        
        

        
