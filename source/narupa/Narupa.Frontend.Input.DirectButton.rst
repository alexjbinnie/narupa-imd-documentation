DirectButton
============
.. cs:setscope:: Narupa.Frontend.Input

.. cs:class:: public sealed class DirectButton : IButton
    
    An :cs:any:`~Narupa.Frontend.Input.IButton` that can be triggered using the
    :cs:any:`~Narupa.Frontend.Input.DirectButton.Press`, :cs:any:`~Narupa.Frontend.Input.DirectButton.Release` and :cs:any:`~Narupa.Frontend.Input.DirectButton.Toggle`
    methods.
    
    

    :implements: :cs:any:`~Narupa.Frontend.Input.IButton`
    
    .. cs:property:: public bool IsPressed { get; }
        
        

        :implements: :cs:any:`~Narupa.Frontend.Input.IButton.IsPressed`
        :returns System.Boolean: 
        
    .. cs:event:: public event Action Pressed
        
        

        :implements: :cs:any:`~Narupa.Frontend.Input.IButton.Pressed`
        :returns System.Action: 
        
    .. cs:event:: public event Action Released
        
        

        :implements: :cs:any:`~Narupa.Frontend.Input.IButton.Released`
        :returns System.Action: 
        
    .. cs:method:: public void Press()
        
        Put this button into the pressed state if it is not already
        pressed.
        
        

        
    .. cs:method:: public void Release()
        
        Put this button into the released state if it is not already
        released.
        
        

        
    .. cs:method:: public void Toggle()
        
        Toggle this button between the pressed and released states.
        
        

        
