RepresentationService.RepresentationServiceBase
===============================================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public abstract class RepresentationServiceBase : Object
    
    
    .. cs:method:: public virtual Task<SetRepresentationResponse> SetRepresentation(SetRepresentationRequest request, ServerCallContext context)
        
        :param Narupa.Protocol.Instance.SetRepresentationRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Instance.SetRepresentationResponse}: 
        
    .. cs:constructor:: protected RepresentationServiceBase()
        
        
