IGpuProgram
===========
.. cs:setscope:: Narupa.Visualisation.Utility

.. cs:interface:: public interface IGpuProgram
    
    An abstraction of both traditional rendering shaders and compute shaders,
    representing a GPU program which can have data uploaded to it using
    :cs:any:`~UnityEngine.ComputeBuffer`'s.
    
    

    
    .. cs:method:: void SetBuffer(string id, ComputeBuffer buffer)
        
        Set a :cs:any:`~UnityEngine.ComputeBuffer` with the provided :code:`id` for
        the GPU program. This will then be available from within the shader.
        
        

        :param System.String id: 
        :param UnityEngine.ComputeBuffer buffer: 
        
