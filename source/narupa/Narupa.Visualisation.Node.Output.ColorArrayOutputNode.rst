ColorArrayOutputNode
====================
.. cs:setscope:: Narupa.Visualisation.Node.Output

.. cs:class:: [Serializable] public class ColorArrayOutputNode : OutputNode<ColorArrayProperty>, IOutputNode
    
    :inherits: :cs:any:`~Narupa.Visualisation.Node.Output.OutputNode{Narupa.Visualisation.Properties.Collections.ColorArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Output.IOutputNode`
    
