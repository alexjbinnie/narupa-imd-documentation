RepresentationService.RepresentationServiceClient
=================================================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public class RepresentationServiceClient : ClientBase<RepresentationService.RepresentationServiceClient>
    
    
    .. cs:constructor:: public RepresentationServiceClient(Channel channel)
        
        :param Grpc.Core.Channel channel: 
        
    .. cs:constructor:: public RepresentationServiceClient(CallInvoker callInvoker)
        
        :param Grpc.Core.CallInvoker callInvoker: 
        
    .. cs:constructor:: protected RepresentationServiceClient()
        
        
    .. cs:constructor:: protected RepresentationServiceClient(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        
    .. cs:method:: public virtual SetRepresentationResponse SetRepresentation(SetRepresentationRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Instance.SetRepresentationRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Instance.SetRepresentationResponse: 
        
    .. cs:method:: public virtual SetRepresentationResponse SetRepresentation(SetRepresentationRequest request, CallOptions options)
        
        :param Narupa.Protocol.Instance.SetRepresentationRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Instance.SetRepresentationResponse: 
        
    .. cs:method:: public virtual AsyncUnaryCall<SetRepresentationResponse> SetRepresentationAsync(SetRepresentationRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Instance.SetRepresentationRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Instance.SetRepresentationResponse}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<SetRepresentationResponse> SetRepresentationAsync(SetRepresentationRequest request, CallOptions options)
        
        :param Narupa.Protocol.Instance.SetRepresentationRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Instance.SetRepresentationResponse}: 
        
    .. cs:method:: protected override RepresentationService.RepresentationServiceClient NewInstance(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        :returns Narupa.Protocol.Instance.RepresentationService.RepresentationServiceClient: 
        
