MultiplayerAvatar
=================
.. cs:setscope:: Narupa.Session

.. cs:class:: public sealed class MultiplayerAvatar
    
    
    .. cs:property:: public string PlayerId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public Dictionary<string, Transformation?> Components { get; }
        
        :returns System.Collections.Generic.Dictionary{System.String,System.Nullable{Narupa.Core.Math.Transformation}}: 
        
