HermiteCurve
============
.. cs:setscope:: Narupa.Visualisation.Components.Spline

.. cs:class:: public class HermiteCurve : VisualisationComponent<HermiteCurveNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<HermiteCurveNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Spline.HermiteCurveNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Spline.HermiteCurveNode}`
    
