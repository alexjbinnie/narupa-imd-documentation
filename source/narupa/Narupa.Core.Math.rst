Narupa.Core.Math
================
.. cs:namespace:: Narupa.Core.Math

.. toctree::
    :maxdepth: 4
    
    Narupa.Core.Math.AffineTransformation
    Narupa.Core.Math.ITransformation
    Narupa.Core.Math.LinearTransformation
    Narupa.Core.Math.MatrixExtensions
    Narupa.Core.Math.SearchAlgorithms
    Narupa.Core.Math.Transformation
    Narupa.Core.Math.UniformScaleTransformation
    Narupa.Core.Math.UnitScaleTransformation
    
    
