Vector3ArrayInputNode
=====================
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class Vector3ArrayInputNode : InputNode<Vector3ArrayProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~Vector3[]` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.Collections.Vector3ArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
