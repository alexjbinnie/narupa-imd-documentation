ResourceValuesUpdate
====================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class ResourceValuesUpdate : Object, IMessage<ResourceValuesUpdate>, IMessage, IEquatable<ResourceValuesUpdate>, IDeepCloneable<ResourceValuesUpdate>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.ResourceValuesUpdate}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.ResourceValuesUpdate}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.ResourceValuesUpdate}`
    
    .. cs:field:: public const int ResourceValueChangesFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int ResourceValueRemovalsFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public ResourceValuesUpdate()
        
        
    .. cs:constructor:: public ResourceValuesUpdate(ResourceValuesUpdate other)
        
        :param Narupa.Protocol.Multiplayer.ResourceValuesUpdate other: 
        
    .. cs:method:: public ResourceValuesUpdate Clone()
        
        :returns Narupa.Protocol.Multiplayer.ResourceValuesUpdate: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(ResourceValuesUpdate other)
        
        :param Narupa.Protocol.Multiplayer.ResourceValuesUpdate other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(ResourceValuesUpdate other)
        
        :param Narupa.Protocol.Multiplayer.ResourceValuesUpdate other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<ResourceValuesUpdate> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.ResourceValuesUpdate}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public Struct ResourceValueChanges { get; set; }
        
        :returns Google.Protobuf.WellKnownTypes.Struct: 
        
    .. cs:property:: public RepeatedField<string> ResourceValueRemovals { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{System.String}: 
        
