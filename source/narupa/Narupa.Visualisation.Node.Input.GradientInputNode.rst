GradientInputNode
=================
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class GradientInputNode : InputNode<GradientProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~UnityEngine.Gradient` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.GradientProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
