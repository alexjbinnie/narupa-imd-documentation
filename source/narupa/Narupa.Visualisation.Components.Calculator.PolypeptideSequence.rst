PolypeptideSequence
===================
.. cs:setscope:: Narupa.Visualisation.Components.Calculator

.. cs:class:: public class PolypeptideSequence : VisualisationComponent<PolypeptideSequenceNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<PolypeptideSequenceNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Protein.PolypeptideSequenceNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Protein.PolypeptideSequenceNode}`
    
