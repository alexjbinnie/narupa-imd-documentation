Multiplayer
===========
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public static class Multiplayer : Object
    
    
    .. cs:method:: public static ServerServiceDefinition BindService(Multiplayer.MultiplayerBase serviceImpl)
        
        :param Narupa.Protocol.Multiplayer.Multiplayer.MultiplayerBase serviceImpl: 
        :returns Grpc.Core.ServerServiceDefinition: 
        
    .. cs:method:: public static void BindService(ServiceBinderBase serviceBinder, Multiplayer.MultiplayerBase serviceImpl)
        
        :param Grpc.Core.ServiceBinderBase serviceBinder: 
        :param Narupa.Protocol.Multiplayer.Multiplayer.MultiplayerBase serviceImpl: 
        
    .. cs:property:: public static ServiceDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.ServiceDescriptor: 
        
