ImdReflection
=============
.. cs:setscope:: Narupa.Protocol.Imd

.. cs:class:: public static class ImdReflection : Object
    
    
    .. cs:property:: public static FileDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.FileDescriptor: 
        
