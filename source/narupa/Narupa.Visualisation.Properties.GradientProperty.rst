GradientProperty
================
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class GradientProperty : SerializableProperty<Gradient>, IProperty<Gradient>, IReadOnlyProperty<Gradient>, IProperty, IReadOnlyProperty
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for a :cs:any:`~UnityEngine.Gradient` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.SerializableProperty{UnityEngine.Gradient}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{UnityEngine.Gradient}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{UnityEngine.Gradient}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
