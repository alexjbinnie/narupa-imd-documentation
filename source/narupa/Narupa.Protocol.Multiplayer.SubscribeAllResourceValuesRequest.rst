SubscribeAllResourceValuesRequest
=================================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class SubscribeAllResourceValuesRequest : Object, IMessage<SubscribeAllResourceValuesRequest>, IMessage, IEquatable<SubscribeAllResourceValuesRequest>, IDeepCloneable<SubscribeAllResourceValuesRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.SubscribeAllResourceValuesRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.SubscribeAllResourceValuesRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.SubscribeAllResourceValuesRequest}`
    
    .. cs:field:: public const int UpdateIntervalFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public SubscribeAllResourceValuesRequest()
        
        
    .. cs:constructor:: public SubscribeAllResourceValuesRequest(SubscribeAllResourceValuesRequest other)
        
        :param Narupa.Protocol.Multiplayer.SubscribeAllResourceValuesRequest other: 
        
    .. cs:method:: public SubscribeAllResourceValuesRequest Clone()
        
        :returns Narupa.Protocol.Multiplayer.SubscribeAllResourceValuesRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(SubscribeAllResourceValuesRequest other)
        
        :param Narupa.Protocol.Multiplayer.SubscribeAllResourceValuesRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(SubscribeAllResourceValuesRequest other)
        
        :param Narupa.Protocol.Multiplayer.SubscribeAllResourceValuesRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<SubscribeAllResourceValuesRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.SubscribeAllResourceValuesRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public float UpdateInterval { get; set; }
        
        :returns System.Single: 
        
