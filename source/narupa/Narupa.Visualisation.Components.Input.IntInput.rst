IntInput
========
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class IntInput : VisualisationComponent<IntInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<IntInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.IntInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.IntInputNode}`
    
