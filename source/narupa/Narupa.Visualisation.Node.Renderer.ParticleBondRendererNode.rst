ParticleBondRendererNode
========================
.. cs:setscope:: Narupa.Visualisation.Node.Renderer

.. cs:class:: [Serializable] public class ParticleBondRendererNode : IndirectMeshRenderer, IDisposable
    
    Visualisation node for rendering bonds between particles.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Renderer.IndirectMeshRenderer`
    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:property:: public IProperty<BondPair[]> BondPairs { get; }
        
        Set of bond pairs to render.
        
        

        :returns Narupa.Visualisation.Property.IProperty{Narupa.Frame.BondPair[]}: 
        
    .. cs:property:: public IProperty<Vector3[]> ParticlePositions { get; }
        
        Positions of particles which will be connected by bonds.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Vector3[]}: 
        
    .. cs:property:: public IProperty<Color[]> ParticleColors { get; }
        
        Color of particles which will be connected by bonds.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Color[]}: 
        
    .. cs:property:: public IProperty<float[]> ParticleScales { get; }
        
        Scale of particles which will be connected by bonds.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Single[]}: 
        
    .. cs:property:: public IProperty<Color> RendererColor { get; }
        
        Overall color of the renderer. Each particle color will be multiplied by this
        value.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Color}: 
        
    .. cs:property:: public IProperty<float> ParticleScale { get; }
        
        Scaling of the particles. Each particle scale will be multiplied by this
        value.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Single}: 
        
    .. cs:property:: public IProperty<Mesh> Mesh { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Mesh}: 
        
    .. cs:property:: public IProperty<Material> Material { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Material}: 
        
    .. cs:property:: public FloatProperty BondScale { get; }
        
        Scale of the bonds.
        
        

        :returns Narupa.Visualisation.Properties.FloatProperty: 
        
    .. cs:property:: public IProperty<int[]> BondOrders { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{System.Int32[]}: 
        
    .. cs:property:: public override bool ShouldRender { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: public override bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: public override void UpdateInput()
        
        
    .. cs:method:: protected void UpdateBuffers()
        
        
    .. cs:property:: protected override IndirectMeshDrawCommand DrawCommand { get; }
        
        :returns Narupa.Visualisation.IndirectMeshDrawCommand: 
        
    .. cs:method:: public override void ResetBuffers()
        
        Indicate that a deserialisation or other event which resets buffers has occured.
        
        

        
