VisualisationComponentExtensions
================================
.. cs:setscope:: Narupa.Visualisation.Components

.. cs:class:: public static class VisualisationComponentExtensions
    
    Extension methods related to :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent`.
    
    

    
    .. cs:method:: public static IEnumerable<TNode> GetVisualisationNodesInChildren<TNode>(this GameObject go)
        
        Get all visualisation nodes of a given type that are in children of the game object.
        
        

        :param UnityEngine.GameObject go: 
        :returns System.Collections.Generic.IEnumerable{{TNode}}: 
        
    .. cs:method:: public static IEnumerable<TNode> GetVisualisationNodes<TNode>(this GameObject go)
        
        Get all visualisation nodes of a given type that are in this game object.
        
        

        :param UnityEngine.GameObject go: 
        :returns System.Collections.Generic.IEnumerable{{TNode}}: 
        
    .. cs:method:: public static TNode GetVisualisationNode<TNode>(this GameObject go)      where TNode : class
        
        Get the first visualisation nodes of a given type that are in this game object.
        
        

        :param UnityEngine.GameObject go: 
        :returns {TNode}: 
        
