SetRepresentationResponse
=========================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public sealed class SetRepresentationResponse : Object, IMessage<SetRepresentationResponse>, IMessage, IEquatable<SetRepresentationResponse>, IDeepCloneable<SetRepresentationResponse>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Instance.SetRepresentationResponse}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Instance.SetRepresentationResponse}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Instance.SetRepresentationResponse}`
    
    .. cs:field:: public const int InstanceIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public SetRepresentationResponse()
        
        
    .. cs:constructor:: public SetRepresentationResponse(SetRepresentationResponse other)
        
        :param Narupa.Protocol.Instance.SetRepresentationResponse other: 
        
    .. cs:method:: public SetRepresentationResponse Clone()
        
        :returns Narupa.Protocol.Instance.SetRepresentationResponse: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(SetRepresentationResponse other)
        
        :param Narupa.Protocol.Instance.SetRepresentationResponse other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(SetRepresentationResponse other)
        
        :param Narupa.Protocol.Instance.SetRepresentationResponse other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<SetRepresentationResponse> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Instance.SetRepresentationResponse}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string InstanceId { get; set; }
        
        :returns System.String: 
        
