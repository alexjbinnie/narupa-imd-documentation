LinearTransformation
====================
.. cs:setscope:: Narupa.Core.Math

.. cs:struct:: [Serializable] public struct LinearTransformation : ITransformation
    
    A linear transformation between two 3D spaces, defined by where it maps the
    three axes of cartesian space. This can represent any combination of rotations,
    reflections, scaling and shears.
    
    

    All linear transformations preserve the origin.
    
    

    :implements: :cs:any:`~Narupa.Core.Math.ITransformation`
    
    .. cs:property:: ITransformation ITransformation.inverse { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.inverse`
        :returns Narupa.Core.Math.ITransformation: 
        
    .. cs:field:: public Vector3 xAxis
        
        The vector to which this transformation maps the direction (1, 0, 0).
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Vector3 yAxis
        
        The vector to which this transformation maps the direction (0, 1, 0).
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Vector3 zAxis
        
        The vector to which this transformation maps the direction (0, 0, 1).
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:constructor:: public LinearTransformation(Vector3 xAxis, Vector3 yAxis, Vector3 zAxis)
        
        Create a linear transformation which maps the x, y and z directions to new
        vectors.
        
        

        :param UnityEngine.Vector3 xAxis: 
        :param UnityEngine.Vector3 yAxis: 
        :param UnityEngine.Vector3 zAxis: 
        
    .. cs:constructor:: public LinearTransformation(Matrix4x4 matrix)
        
        Create a linear transformation from the upper 3x3 matrix.
        
        

        :param UnityEngine.Matrix4x4 matrix: 
        
    .. cs:property:: public static LinearTransformation identity { get; }
        
        The identity transformation.
        
        

        :returns Narupa.Core.Math.LinearTransformation: 
        
    .. cs:property:: public Vector3 axesMagnitudes { get; }
        
        The magnitudes of the three axes that define this linear transformation.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:property:: public LinearTransformation inverse { get; }
        
        

        :returns Narupa.Core.Math.LinearTransformation: 
        
    .. cs:property:: public Matrix4x4 matrix { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.matrix`
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:property:: public Matrix4x4 inverseMatrix { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.inverseMatrix`
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:conversion:: public static implicit operator Matrix4x4(LinearTransformation transformation)
        
        :param Narupa.Core.Math.LinearTransformation transformation: 
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:conversion:: public static implicit operator AffineTransformation(LinearTransformation transformation)
        
        :param Narupa.Core.Math.LinearTransformation transformation: 
        :returns Narupa.Core.Math.AffineTransformation: 
        
    .. cs:operator:: public static LinearTransformation operator *(LinearTransformation a, LinearTransformation b)
        
        :param Narupa.Core.Math.LinearTransformation a: 
        :param Narupa.Core.Math.LinearTransformation b: 
        :returns Narupa.Core.Math.LinearTransformation: 
        
    .. cs:method:: public Vector3 TransformPoint(Vector3 point)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.TransformPoint(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 InverseTransformPoint(Vector3 point)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.InverseTransformPoint(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 TransformDirection(Vector3 direction)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.TransformDirection(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 direction: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 InverseTransformDirection(Vector3 direction)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.InverseTransformDirection(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 direction: 
        :returns UnityEngine.Vector3: 
        
