CurvedBond
==========
.. cs:setscope:: Narupa.Visualisation.Components.Calculator

.. cs:class:: public class CurvedBond : VisualisationComponent<CurvedBondNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<CurvedBondNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Spline.CurvedBondNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Spline.CurvedBondNode}`
    
