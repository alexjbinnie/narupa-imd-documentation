ImdSession
==========
.. cs:setscope:: Narupa.Session

.. cs:class:: public class ImdSession : IDisposable
    
    Provides functionality to manage the sending of particle interactions
    over an :cs:any:`~Narupa.Grpc.Interactive.ImdClient`.
    
    

    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:property:: public OutgoingStreamCollection<ParticleInteraction, InteractionEndReply> InteractionStreams { get; }
        
        :returns Narupa.Grpc.Stream.OutgoingStreamCollection{Narupa.Protocol.Imd.ParticleInteraction,Narupa.Protocol.Imd.InteractionEndReply}: 
        
    .. cs:property:: public Dictionary<string, ImdSession.InteractionData> Interactions { get; }
        
        Dictionary of all currently known interactions.
        
        

        :returns System.Collections.Generic.Dictionary{System.String,Narupa.Session.ImdSession.InteractionData}: 
        
    .. cs:method:: public void OpenClient(GrpcConnection connection)
        
        Connect to an IMD service over the given connection. Closes any 
        existing client.
        
        

        :param Narupa.Grpc.GrpcConnection connection: 
        
    .. cs:method:: public void CloseClient()
        
        Close the current IMD client and dispose all streams.
        
        

        
    .. cs:method:: public void SetInteraction(string id, Vector3 position, float forceScale = 100F, string forceModel = "spring", IEnumerable<int> particles = null, Dictionary<string, object> properties = null)
        
        Set the active interaction for a particular stream id. If the 
        stream doesn't exist, it will be started.
        
        

        :param System.String id: 
        :param UnityEngine.Vector3 position: 
        :param System.Single forceScale: 
        :param System.String forceModel: 
        :param System.Collections.Generic.IEnumerable{System.Int32} particles: 
        :param System.Collections.Generic.Dictionary{System.String,System.Object} properties: 
        
    .. cs:method:: public void UnsetInteraction(string id)
        
        Stop the active interaction for a particular stream id, if it 
        exists.
        
        

        :param System.String id: 
        
    .. cs:method:: public void SetInteraction(string id, ParticleInteraction interaction)
        
        Set the active interaction for a particular stream id. If the
        interaction to be set is null, the stream will be ended. If the
        stream doesn't exist, it was be started.
        
        

        :param System.String id: 
        :param Narupa.Protocol.Imd.ParticleInteraction interaction: 
        
    .. cs:method:: public void FlushInteractions()
        
        Send the latest state for all interactions changed since the last
        flush.
        
        

        
    .. cs:method:: public void Dispose()
        
        

        :implements: :cs:any:`~System.IDisposable.Dispose`
        
