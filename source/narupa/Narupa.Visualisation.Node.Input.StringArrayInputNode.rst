StringArrayInputNode
====================
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class StringArrayInputNode : InputNode<StringArrayProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~string[]` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.Collections.StringArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
