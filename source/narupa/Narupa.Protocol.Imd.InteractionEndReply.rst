InteractionEndReply
===================
.. cs:setscope:: Narupa.Protocol.Imd

.. cs:class:: public sealed class InteractionEndReply : Object, IMessage<InteractionEndReply>, IMessage, IEquatable<InteractionEndReply>, IDeepCloneable<InteractionEndReply>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Imd.InteractionEndReply}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Imd.InteractionEndReply}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Imd.InteractionEndReply}`
    
    .. cs:constructor:: public InteractionEndReply()
        
        
    .. cs:constructor:: public InteractionEndReply(InteractionEndReply other)
        
        :param Narupa.Protocol.Imd.InteractionEndReply other: 
        
    .. cs:method:: public InteractionEndReply Clone()
        
        :returns Narupa.Protocol.Imd.InteractionEndReply: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(InteractionEndReply other)
        
        :param Narupa.Protocol.Imd.InteractionEndReply other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(InteractionEndReply other)
        
        :param Narupa.Protocol.Imd.InteractionEndReply other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<InteractionEndReply> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Imd.InteractionEndReply}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
