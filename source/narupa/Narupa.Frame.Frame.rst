Frame
=====
.. cs:setscope:: Narupa.Frame

.. cs:class:: public class Frame : IFrame
    
    A single Frame in a trajectory. It is a snapshot of a system, represented by a
    set of properties. These properties include particle positions, types and
    bonds.
    
    

    :implements: :cs:any:`~Narupa.Frame.IFrame`
    
    .. cs:constructor:: public Frame()
        
        
    .. cs:property:: public Vector3[] ParticlePositions { get; set; }
        
        Array of particle positions
        
        

        :returns UnityEngine.Vector3[]: 
        
    .. cs:property:: public string[] ParticleTypes { get; set; }
        
        Array of particle types
        
        

        :returns System.String[]: 
        
    .. cs:property:: public Element[] ParticleElements { get; set; }
        
        Array of particle elements
        
        

        :returns Narupa.Core.Science.Element[]: 
        
    .. cs:property:: public BondPair[] BondPairs { get; set; }
        
        Array of bond pairs
        
        

        :returns Narupa.Frame.BondPair[]: 
        
    .. cs:property:: public int[] BondOrders { get; set; }
        
        Array of bond pairs
        
        

        :returns System.Int32[]: 
        
    .. cs:property:: public int[] ParticleResidues { get; set; }
        
        Array of residue indices for each particle.
        
        

        :returns System.Int32[]: 
        
    .. cs:property:: public string[] ResidueNames { get; set; }
        
        Array of residue names.
        
        

        :returns System.String[]: 
        
    .. cs:property:: public int[] ResidueEntities { get; set; }
        
        Array of entity indices for each residue.
        
        

        Entities are groupings of residues, such as polypeptide chains or strands of DNA.
        
        

        :returns System.Int32[]: 
        
    .. cs:property:: public string[] ParticleNames { get; set; }
        
        Array of particle names.
        
        

        :returns System.String[]: 
        
    .. cs:property:: public int ParticleCount { get; set; }
        
        The number of particles.
        
        

        :returns System.Int32: 
        
    .. cs:property:: public LinearTransformation? BoxVectors { get; set; }
        
        The transformation that represents the unit cell.
        
        

        :returns System.Nullable{Narupa.Core.Math.LinearTransformation}: 
        
    .. cs:property:: public int ResidueCount { get; set; }
        
        The number of residues.
        
        

        :returns System.Int32: 
        
    .. cs:property:: public int EntityCount { get; set; }
        
        The number of entities.
        
        

        :returns System.Int32: 
        
    .. cs:property:: [NotNull] public IReadOnlyList<IParticle> Particles { get; }
        
        

        :implements: :cs:any:`~Narupa.Frame.IFrame.Particles`
        :returns System.Collections.Generic.IReadOnlyList{Narupa.Frame.IParticle}: 
        
    .. cs:property:: [NotNull] public IReadOnlyList<BondPair> Bonds { get; }
        
        

        :implements: :cs:any:`~Narupa.Frame.IFrame.Bonds`
        :returns System.Collections.Generic.IReadOnlyList{Narupa.Frame.BondPair}: 
        
    .. cs:property:: public IDictionary<string, object> Data { get; }
        
        

        :implements: :cs:any:`~Narupa.Frame.IFrame.Data`
        :returns System.Collections.Generic.IDictionary{System.String,System.Object}: 
        
    .. cs:method:: public static Frame ShallowCopy([NotNull] Frame originalFrame)
        
        Obtain a copy of a frame, referencing the old arrays such that if a new array
        is not provided, a Frame will use the last obtained value.
        
        

        :param Narupa.Frame.Frame originalFrame: 
        :returns Narupa.Frame.Frame: 
        
