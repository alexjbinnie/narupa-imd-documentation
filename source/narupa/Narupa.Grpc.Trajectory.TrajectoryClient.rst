TrajectoryClient
================
.. cs:setscope:: Narupa.Grpc.Trajectory

.. cs:class:: public sealed class TrajectoryClient : GrpcClient<TrajectoryService.TrajectoryServiceClient>, ICancellable, ICancellationTokenSource, IDisposable, IAsyncClosable
    
    Wraps a :cs:any:`~Narupa.Protocol.Trajectory.TrajectoryService.TrajectoryServiceClient` and
    provides access to a stream of frames from a trajectory provided by a
    server over a :cs:any:`~Narupa.Grpc.GrpcConnection`.
    
    

    :inherits: :cs:any:`~Narupa.Grpc.GrpcClient{Narupa.Protocol.Trajectory.TrajectoryService.TrajectoryServiceClient}`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellationTokenSource`
    :implements: :cs:any:`~System.IDisposable`
    :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable`
    
    .. cs:field:: public const string CommandPlay = "playback/play"
        
        Command the server to play the simulation if it is paused.
        
        

        :returns System.String: 
        
    .. cs:field:: public const string CommandPause = "playback/pause"
        
        Command the server to pause the simulation if it is playing.
        
        

        :returns System.String: 
        
    .. cs:field:: public const string CommandStep = "playback/step"
        
        Command the server to advance by one simulation step.
        
        

        :returns System.String: 
        
    .. cs:field:: public const string CommandReset = "playback/reset"
        
        Command the server to reset the simulation to its initial state.
        
        

        :returns System.String: 
        
    .. cs:constructor:: public TrajectoryClient([NotNull] GrpcConnection connection)
        
        :param Narupa.Grpc.GrpcConnection connection: 
        
    .. cs:method:: public IncomingStream<GetFrameResponse> SubscribeLatestFrames(float updateInterval = 0.0333333351F, CancellationToken externalToken = default(CancellationToken))
        
        Begin a new subscription to trajectory frames and asynchronously
        enumerate over them, calling the provided callback for each frame
        received.
        
        

        Corresponds to the SubscribeLatestFrames gRPC call.
        
        

        :param System.Single updateInterval: How many seconds the service should wait and aggregate updates  between  sending them to us.
        :param System.Threading.CancellationToken externalToken: 
        :returns Narupa.Grpc.Stream.IncomingStream{Narupa.Protocol.Trajectory.GetFrameResponse}: 
        
