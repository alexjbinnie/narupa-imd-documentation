StringArray
===========
.. cs:setscope:: Narupa.Protocol

.. cs:class:: public sealed class StringArray : Object, IMessage<StringArray>, IMessage, IEquatable<StringArray>, IDeepCloneable<StringArray>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.StringArray}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.StringArray}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.StringArray}`
    
    .. cs:field:: public const int ValuesFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public StringArray()
        
        
    .. cs:constructor:: public StringArray(StringArray other)
        
        :param Narupa.Protocol.StringArray other: 
        
    .. cs:method:: public StringArray Clone()
        
        :returns Narupa.Protocol.StringArray: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(StringArray other)
        
        :param Narupa.Protocol.StringArray other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(StringArray other)
        
        :param Narupa.Protocol.StringArray other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<StringArray> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.StringArray}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public RepeatedField<string> Values { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{System.String}: 
        
