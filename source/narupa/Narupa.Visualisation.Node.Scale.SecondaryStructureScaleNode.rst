SecondaryStructureScaleNode
===========================
.. cs:setscope:: Narupa.Visualisation.Node.Scale

.. cs:class:: [Serializable] public class SecondaryStructureScaleNode : VisualiserScaleNode
    
    Provides scales that depend on the secondary structure.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Scale.VisualiserScaleNode`
    
    .. cs:property:: protected override bool IsInputValid { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        
    .. cs:method:: protected override void ClearOutput()
        
        
    .. cs:method:: protected override void UpdateOutput()
        
        
