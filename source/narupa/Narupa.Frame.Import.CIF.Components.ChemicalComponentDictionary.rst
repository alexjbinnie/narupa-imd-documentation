ChemicalComponentDictionary
===========================
.. cs:setscope:: Narupa.Frame.Import.CIF.Components

.. cs:class:: public class ChemicalComponentDictionary : ScriptableObject, ISerializationCallbackReceiver
    
    A global :cs:any:`~UnityEngine.ScriptableObject` which holds the mmCIF component
    dictionary.
    
    

    :inherits: :cs:any:`~UnityEngine.ScriptableObject`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    
    .. cs:property:: public static ChemicalComponentDictionary Instance { get; }
        
        The chemical component dictionary.
        
        

        :returns Narupa.Frame.Import.CIF.Components.ChemicalComponentDictionary: 
        
    .. cs:method:: public ChemicalComponent GetResidue(string residueName)
        
        :param System.String residueName: 
        :returns Narupa.Frame.Import.CIF.Components.ChemicalComponent: 
        
    .. cs:method:: public void OnBeforeSerialize()
        
        :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize`
        
    .. cs:method:: public void OnAfterDeserialize()
        
        :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize`
        
