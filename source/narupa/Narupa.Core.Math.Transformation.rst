Transformation
==============
.. cs:setscope:: Narupa.Core.Math

.. cs:struct:: public struct Transformation : ITransformation
    
    Bundles position, rotation, and scale of a transformation.
    
    

    :implements: :cs:any:`~Narupa.Core.Math.ITransformation`
    
    .. cs:property:: ITransformation ITransformation.inverse { get; }
        
        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.inverse`
        :returns Narupa.Core.Math.ITransformation: 
        
    .. cs:property:: Matrix4x4 ITransformation.matrix { get; }
        
        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.matrix`
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:property:: Matrix4x4 ITransformation.inverseMatrix { get; }
        
        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.inverseMatrix`
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:method:: Vector3 ITransformation.TransformPoint(Vector3 point)
        
        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.TransformPoint(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: Vector3 ITransformation.InverseTransformPoint(Vector3 point)
        
        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.InverseTransformPoint(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: Vector3 ITransformation.TransformDirection(Vector3 point)
        
        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.TransformDirection(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: Vector3 ITransformation.InverseTransformDirection(Vector3 point)
        
        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.InverseTransformDirection(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public static Transformation FromTransformRelativeToWorld(Transform transform)
        
        Construct a transformation from the translation, rotation, and
        scale of a Unity :cs:any:`~UnityEngine.Transform` relative to world space.
        
        

        The scale is inherently lossy, as the composition of multiple
        transforms is not necessarily a transform.
        
        

        :param UnityEngine.Transform transform: 
        :returns Narupa.Core.Math.Transformation: 
        
    .. cs:method:: public static Transformation FromTransformRelativeToParent(Transform transform)
        
        Construct a transformation from the translation, rotation, and
        scale of a Unity :cs:any:`~UnityEngine.Transform` relative to world space.
        
        

        :param UnityEngine.Transform transform: 
        :returns Narupa.Core.Math.Transformation: 
        
    .. cs:property:: public static Transformation Identity { get; }
        
        The identity transformation.
        
        

        :returns Narupa.Core.Math.Transformation: 
        
    .. cs:field:: public Vector3 Position
        
        Position of this transformation.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Quaternion Rotation
        
        Rotation of this transformation.
        
        

        :returns UnityEngine.Quaternion: 
        
    .. cs:field:: public Vector3 Scale
        
        Scale of this transformation.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:property:: public Matrix4x4 Matrix { get; }
        
        :cs:any:`~UnityEngine.Matrix4x4` representation of this transformation.
        
        

        :returns UnityEngine.Matrix4x4: 
        
    .. cs:property:: public Matrix4x4 InverseMatrix { get; }
        
        The :cs:any:`~UnityEngine.Matrix4x4` representation of the inverse of this
        transformation. Note that the inverse itself cannot be necessarily represented
        as a single :cs:any:`~Narupa.Core.Math.Transformation`.
        
        

        :returns UnityEngine.Matrix4x4: 
        
    .. cs:property:: public Matrix4x4 InverseTransposeMatrix { get; }
        
        The inverse transpose of the matrix representation of this transformation. This
        is equivalent to the transformation with the same position and rotation, but
        with inverted scales.
        
        

        Some shaders require the inverse transpose, as normal vectors transform using
        the inverse transpose of the transformation the positions undergo.
        
        

        :returns UnityEngine.Matrix4x4: 
        
    .. cs:constructor:: public Transformation(Vector3 position, Quaternion rotation, Vector3 scale)
        
        :param UnityEngine.Vector3 position: 
        :param UnityEngine.Quaternion rotation: 
        :param UnityEngine.Vector3 scale: 
        
    .. cs:method:: public void CopyToTransformRelativeToParent(Transform transform)
        
        Set the transform's position, rotation and scale relative to its parent from
        this transformation.
        
        

        :param UnityEngine.Transform transform: 
        
    .. cs:method:: public void CopyToTransformRelativeToWorld(Transform transform)
        
        Set the transform's position, rotation and scale relative to the world space
        from this transformation.
        
        

        :param UnityEngine.Transform transform: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public UnitScaleTransformation AsUnitTransformWithoutScale()
        
        Convert to a transformation with unit scale, discarding any scaling associated
        with this transformation.
        
        

        :returns Narupa.Core.Math.UnitScaleTransformation: 
        
