ColorLerp
=========
.. cs:setscope:: Narupa.Visualisation.Components.Spline

.. cs:class:: public class ColorLerp : VisualisationComponent<ColorLerpNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ColorLerpNode>
    
    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Calculator.ColorLerpNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Calculator.ColorLerpNode}`
    
    .. cs:method:: public void Update()
        
        
