FloatArrayProperty
==================
.. cs:setscope:: Narupa.Visualisation.Properties.Collections

.. cs:class:: [Serializable] public class FloatArrayProperty : ArrayProperty<float>, IProperty<float[]>, IReadOnlyProperty<float[]>, IProperty, IReadOnlyProperty, IEnumerable<float>, IEnumerable
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an array of :cs:any:`~System.Single`
    values.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Properties.Collections.ArrayProperty{System.Single}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{System.Single[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.Single[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Single}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
