LineByLineParser
================
.. cs:setscope:: Narupa.Frame.Import

.. cs:class:: public abstract class LineByLineParser
    
    
    .. cs:property:: protected string CurrentLine { get; }
        
        The current line the parser is looking at.
        
        

        :returns System.String: 
        
    .. cs:method:: protected void Parse(TextReader reader)
        
        Call the :cs:any:`~Narupa.Frame.Import.LineByLineParser.Parse` function on a given source, catching exceptions
        and wrapping them with the line number.
        
        

        :param System.IO.TextReader reader: 
        
    .. cs:method:: protected abstract void Parse()
        
        
    .. cs:property:: protected bool HasLine { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: protected void NextLine()
        
        
    .. cs:method:: protected void SkipLines(int n)
        
        :param System.Int32 n: 
        
    .. cs:method:: protected void FinishParsing()
        
        
    .. cs:method:: protected bool IsBlankLine()
        
        :returns System.Boolean: 
        
    .. cs:method:: protected bool IsCommentLine(char commentChar)
        
        :param System.Char commentChar: 
        :returns System.Boolean: 
        
    .. cs:method:: protected float ParseFloat(string str, string errorMsg)
        
        :param System.String str: 
        :param System.String errorMsg: 
        :returns System.Single: 
        
    .. cs:method:: protected int ParseInt(string str, string errorMsg)
        
        :param System.String str: 
        :param System.String errorMsg: 
        :returns System.Int32: 
        
    .. cs:method:: protected string ParseString(string str)
        
        :param System.String str: 
        :returns System.String: 
        
    .. cs:method:: protected void LogProgress(string message)
        
        Log a message to a progress reporter if present.
        
        

        :param System.String message: 
        
    .. cs:constructor:: protected LineByLineParser(IProgress<string> progress = null)
        
        :param System.IProgress{System.String} progress: 
        
    .. cs:method:: protected Element? ParseElementSymbol(string symbol)
        
        :param System.String symbol: 
        :returns System.Nullable{Narupa.Core.Science.Element}: 
        
    .. cs:method:: protected GroupCollection MatchLine(string pattern, string errorMsg)
        
        :param System.String pattern: 
        :param System.String errorMsg: 
        :returns System.Text.RegularExpressions.GroupCollection: 
        
