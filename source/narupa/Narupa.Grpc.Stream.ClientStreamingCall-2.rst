ClientStreamingCall<TOutgoing, TIncoming>
=========================================
.. cs:setscope:: Narupa.Grpc.Stream

.. cs:delegate:: public delegate AsyncClientStreamingCall<TOutgoing, TIncoming> ClientStreamingCall<TOutgoing, TIncoming>(Metadata headers = null, DateTime? deadline = default(DateTime? ), CancellationToken cancellationToken = default(CancellationToken));
    
    Delegate for a gRPC server streaming call which takes a number of
    TOutgoing messages then replies with a single TIncoming message.
    
    

    :param Grpc.Core.Metadata headers: 
    :param System.Nullable{System.DateTime} deadline: 
    :param System.Threading.CancellationToken cancellationToken: 
    :returns Grpc.Core.AsyncClientStreamingCall{{TOutgoing},{TIncoming}}: 
    
