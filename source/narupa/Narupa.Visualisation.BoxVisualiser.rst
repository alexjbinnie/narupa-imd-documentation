BoxVisualiser
=============
.. cs:setscope:: Narupa.Visualisation

.. cs:class:: [ExecuteAlways] public class BoxVisualiser : MonoBehaviour
    
    Visualiser a box (as represented by an :cs:any:`~Narupa.Core.Math.AffineTransformation`).
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:method:: public void SetBox(AffineTransformation transformation)
        
        Set the box to be visualised.
        
        

        :param Narupa.Core.Math.AffineTransformation transformation: 
        
