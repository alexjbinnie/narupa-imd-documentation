Narupa.Frontend.Manipulation
============================
.. cs:namespace:: Narupa.Frontend.Manipulation

.. toctree::
    :maxdepth: 4
    
    Narupa.Frontend.Manipulation.ActiveParticleGrab
    Narupa.Frontend.Manipulation.AttemptableManipulator
    Narupa.Frontend.Manipulation.IActiveManipulation
    Narupa.Frontend.Manipulation.IInteractableParticles
    Narupa.Frontend.Manipulation.ManipulableParticles
    Narupa.Frontend.Manipulation.ManipulableTransform
    Narupa.Frontend.Manipulation.ManipulationAttemptHandler
    Narupa.Frontend.Manipulation.Manipulator
    Narupa.Frontend.Manipulation.OnePointTranslateRotateGesture
    Narupa.Frontend.Manipulation.TwoPointTranslateRotateScaleGesture
    
    
