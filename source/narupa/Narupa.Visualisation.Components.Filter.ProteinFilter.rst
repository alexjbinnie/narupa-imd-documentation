ProteinFilter
=============
.. cs:setscope:: Narupa.Visualisation.Components.Filter

.. cs:class:: public sealed class ProteinFilter : VisualisationComponent<ProteinFilterNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ProteinFilterNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Filter.ProteinFilterNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Filter.ProteinFilterNode}`
    
