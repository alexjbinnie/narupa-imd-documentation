TrajectoryServiceReflection
===========================
.. cs:setscope:: Narupa.Protocol.Trajectory

.. cs:class:: public static class TrajectoryServiceReflection : Object
    
    
    .. cs:property:: public static FileDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.FileDescriptor: 
        
