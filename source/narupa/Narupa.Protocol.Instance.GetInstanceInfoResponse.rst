GetInstanceInfoResponse
=======================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public sealed class GetInstanceInfoResponse : Object, IMessage<GetInstanceInfoResponse>, IMessage, IEquatable<GetInstanceInfoResponse>, IDeepCloneable<GetInstanceInfoResponse>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Instance.GetInstanceInfoResponse}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Instance.GetInstanceInfoResponse}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Instance.GetInstanceInfoResponse}`
    
    .. cs:field:: public const int InstanceIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int InfoFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public GetInstanceInfoResponse()
        
        
    .. cs:constructor:: public GetInstanceInfoResponse(GetInstanceInfoResponse other)
        
        :param Narupa.Protocol.Instance.GetInstanceInfoResponse other: 
        
    .. cs:method:: public GetInstanceInfoResponse Clone()
        
        :returns Narupa.Protocol.Instance.GetInstanceInfoResponse: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(GetInstanceInfoResponse other)
        
        :param Narupa.Protocol.Instance.GetInstanceInfoResponse other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(GetInstanceInfoResponse other)
        
        :param Narupa.Protocol.Instance.GetInstanceInfoResponse other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<GetInstanceInfoResponse> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Instance.GetInstanceInfoResponse}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string InstanceId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public Struct Info { get; set; }
        
        :returns Google.Protobuf.WellKnownTypes.Struct: 
        
