CifBaseImport
=============
.. cs:setscope:: Narupa.Frame.Import.CIF

.. cs:class:: public abstract class CifBaseImport : LineByLineParser
    
    Base importer for files using the CIF format.
    
    

    :inherits: :cs:any:`~Narupa.Frame.Import.LineByLineParser`
    
    .. cs:constructor:: protected CifBaseImport(IProgress<string> progress = null)
        
        :param System.IProgress{System.String} progress: 
        
    .. cs:method:: protected override void Parse()
        
        
    .. cs:method:: protected bool IsLoop()
        
        Does the current line mark the state of a multi line data table.
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected virtual bool ShouldParseCategory(string category)
        
        Should this table category be parsed, or skipped over?
        
        

        :param System.String category: 
        :returns System.Boolean: 
        
    .. cs:method:: public static string[] SplitWithDelimiter(string currentLine)
        
        Parse space separate items, but account for quotation marks delimiting strings.
        
        

        :param System.String currentLine: 
        :returns System.String[]: 
        
    .. cs:method:: protected virtual CifBaseImport.ParseTableRow GetTableHandler(string category, List<string> keywords)
        
        Get the function which handles reading in table rows.
        
        

        :param System.String category: 
        :param System.Collections.Generic.List{System.String} keywords: 
        :returns Narupa.Frame.Import.CIF.CifBaseImport.ParseTableRow: 
        
    .. cs:method:: protected virtual void ParseDataBlockLine()
        
        Parse the data block.
        
        

        
    .. cs:method:: protected int? ParseInt(string str, string errorMsg)
        
        Parse an int, treating '?' and '.' as null.
        
        

        :param System.String str: 
        :param System.String errorMsg: 
        :returns System.Nullable{System.Int32}: 
        
    .. cs:method:: protected float? ParseFloat(string str, string errorMsg)
        
        Parse a float, treating '?' and '.' as null.
        
        

        :param System.String str: 
        :param System.String errorMsg: 
        :returns System.Nullable{System.Single}: 
        
    .. cs:method:: protected string ParseString(string str)
        
        Parse a string, treating '?' and '.' as null.
        
        

        :param System.String str: 
        :returns System.String: 
        
    .. cs:method:: protected int ParseNonNullInt(string str, string errorMsg)
        
        :param System.String str: 
        :param System.String errorMsg: 
        :returns System.Int32: 
        
    .. cs:method:: protected float ParseNonNullFloat(string str, string errorMsg)
        
        :param System.String str: 
        :param System.String errorMsg: 
        :returns System.Single: 
        
