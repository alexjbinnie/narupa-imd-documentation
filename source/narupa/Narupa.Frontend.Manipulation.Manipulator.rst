Manipulator
===========
.. cs:setscope:: Narupa.Frontend.Manipulation

.. cs:class:: public class Manipulator : IPosedObject
    
    Represents an input device posed in 3D space (e.g a VR controller) that can
    engage in a single manipulation.
    
    

    :implements: :cs:any:`~Narupa.Frontend.Input.IPosedObject`
    
    .. cs:property:: public Transformation? Pose { get; }
        
        :implements: :cs:any:`~Narupa.Frontend.Input.IPosedObject.Pose`
        :returns System.Nullable{Narupa.Core.Math.Transformation}: 
        
    .. cs:event:: public event Action PoseChanged
        
        :implements: :cs:any:`~Narupa.Frontend.Input.IPosedObject.PoseChanged`
        :returns System.Action: 
        
    .. cs:constructor:: public Manipulator(IPosedObject posedObject)
        
        :param Narupa.Frontend.Input.IPosedObject posedObject: 
        
    .. cs:method:: public void SetActiveManipulation(IActiveManipulation manipulation)
        
        Set the active manipulation, ending any existing manipulation first.
        
        

        :param Narupa.Frontend.Manipulation.IActiveManipulation manipulation: 
        
    .. cs:method:: public void EndActiveManipulation()
        
        End the active manipulation if there is any.
        
        

        
