SequenceEndPoints
=================
.. cs:setscope:: Narupa.Visualisation.Components.Spline

.. cs:class:: public class SequenceEndPoints : VisualisationComponent<SequenceEndPointsNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<SequenceEndPointsNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Spline.SequenceEndPointsNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Spline.SequenceEndPointsNode}`
    
