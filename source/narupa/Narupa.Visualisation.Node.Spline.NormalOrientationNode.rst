NormalOrientationNode
=====================
.. cs:setscope:: Narupa.Visualisation.Node.Spline

.. cs:class:: [Serializable] public class NormalOrientationNode : GenericOutputNode
    
    Rotates normals so within each sequence, the normals only rotate by up to 90 degrees.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:method:: public void RotateNormals(Vector3[] normals, Vector3[] tangents)
        
        Calculate rotated normals from an existing set of normals and tangents.
        
        

        :param UnityEngine.Vector3[] normals: 
        :param UnityEngine.Vector3[] tangents: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:method:: protected override void UpdateOutput()
        
        

        
    .. cs:method:: protected override void ClearOutput()
        
        

        
