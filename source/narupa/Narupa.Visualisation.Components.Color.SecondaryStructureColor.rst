SecondaryStructureColor
=======================
.. cs:setscope:: Narupa.Visualisation.Components.Color

.. cs:class:: public class SecondaryStructureColor : VisualisationComponent<SecondaryStructureColor>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<SecondaryStructureColor>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Color.SecondaryStructureColor}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Color.SecondaryStructureColor}`
    
