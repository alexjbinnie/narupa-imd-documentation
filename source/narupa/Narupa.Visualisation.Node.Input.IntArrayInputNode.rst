IntArrayInputNode
=================
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class IntArrayInputNode : InputNode<IntArrayProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~int[]` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.IntArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
