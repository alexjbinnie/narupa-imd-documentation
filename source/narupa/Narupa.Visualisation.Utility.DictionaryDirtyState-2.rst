DictionaryDirtyState<TKey, TValue>
==================================
.. cs:setscope:: Narupa.Visualisation.Utility

.. cs:class:: public class DictionaryDirtyState<TKey, TValue> : CollectionDirtyState<TKey>, IReadOnlyDictionary<TKey, bool>, IReadOnlyCollection<KeyValuePair<TKey, bool>>, IEnumerable<KeyValuePair<TKey, bool>>, IEnumerable
    
    Represents the dirty state of an :cs:any:`~System.Collections.Generic.IDictionary`2`,
    keeping track of the keys of items
    which have been modified.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Utility.CollectionDirtyState{{TKey}}`
    :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{{TKey},System.Boolean}`
    :implements: :cs:any:`~System.Collections.Generic.IReadOnlyCollection{System.Collections.Generic.KeyValuePair{{TKey},System.Boolean}}`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Collections.Generic.KeyValuePair{{TKey},System.Boolean}}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
    .. cs:constructor:: public DictionaryDirtyState(IDictionary<TKey, TValue> dictionary)
        
        :param System.Collections.Generic.IDictionary{{TKey},{TValue}} dictionary: 
        
    .. cs:property:: public IEnumerable<TKey> DirtyKeys { get; }
        
        Collection of dirty keys of the dictionary
        
        

        :returns System.Collections.Generic.IEnumerable{{TKey}}: 
        
    .. cs:property:: public IEnumerable<TValue> DirtyValues { get; }
        
        Collection of dirty values of the dictionary
        
        

        :returns System.Collections.Generic.IEnumerable{{TValue}}: 
        
    .. cs:property:: public IEnumerable<KeyValuePair<TKey, TValue>> DirtyKeyValuePairs { get; }
        
        Collection of dirty key-value pairs in the dictionary
        
        

        :returns System.Collections.Generic.IEnumerable{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}: 
        
