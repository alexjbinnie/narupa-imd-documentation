IReadOnlyProperty
=================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:interface:: public interface IReadOnlyProperty
    
    Properties allow the linking together of inputs and outputs of various objects
    in an observer pattern. Read-only properties are value providers, whilst
    :cs:any:`~Narupa.Visualisation.Property.IProperty` can also be linked to other properties to
    obtain their values.
    
    

    
    .. cs:property:: object Value { get; }
        
        The current value of the property. This should only be called if :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.HasValue` has been ensured to be true.
        
        

        :returns System.Object: 
        
    .. cs:property:: bool HasValue { get; }
        
        Does the property currently have a value? If true, then accessing :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.Value` should be valid.
        
        

        :returns System.Boolean: 
        
    .. cs:event:: event Action ValueChanged
        
        Callback for when the value is changed.
        
        

        :returns System.Action: 
        
    .. cs:property:: Type PropertyType { get; }
        
        The :cs:any:`~System.Type` that this property wraps.
        
        

        :returns System.Type: 
        
