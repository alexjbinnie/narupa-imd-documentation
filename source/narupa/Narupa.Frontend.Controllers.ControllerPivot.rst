ControllerPivot
===============
.. cs:setscope:: Narupa.Frontend.Controllers

.. cs:class:: public class ControllerPivot : MonoBehaviour, IPosedObject
    
    Component to indicate a part of the controller with a position and radius.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    :implements: :cs:any:`~Narupa.Frontend.Input.IPosedObject`
    
    .. cs:property:: public float Radius { get; }
        
        :returns System.Single: 
        
    .. cs:property:: public Transformation? Pose { get; }
        
        

        :implements: :cs:any:`~Narupa.Frontend.Input.IPosedObject.Pose`
        :returns System.Nullable{Narupa.Core.Math.Transformation}: 
        
    .. cs:event:: public event Action PoseChanged
        
        

        :implements: :cs:any:`~Narupa.Frontend.Input.IPosedObject.PoseChanged`
        :returns System.Action: 
        
