ImdSession.InteractionData
==========================
.. cs:setscope:: Narupa.Session

.. cs:struct:: public struct InteractionData
    
    
    .. cs:property:: public string InteractionId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public Vector3 Position { get; set; }
        
        :returns UnityEngine.Vector3: 
        
    .. cs:property:: public int[] ParticleIds { get; set; }
        
        :returns System.Int32[]: 
        
