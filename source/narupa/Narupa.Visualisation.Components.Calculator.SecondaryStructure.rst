SecondaryStructure
==================
.. cs:setscope:: Narupa.Visualisation.Components.Calculator

.. cs:class:: public class SecondaryStructure : VisualisationComponent<SecondaryStructureNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<SecondaryStructureNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Protein.SecondaryStructureNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Protein.SecondaryStructureNode}`
    
