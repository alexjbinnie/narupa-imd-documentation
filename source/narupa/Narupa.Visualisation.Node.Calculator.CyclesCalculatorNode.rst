CyclesCalculatorNode
====================
.. cs:setscope:: Narupa.Visualisation.Node.Calculator

.. cs:class:: [Serializable] public class CyclesCalculatorNode : GenericOutputNode
    
    Locate cycles by looking at the bonding in a molecule, optionally limiting the search
    to only include cycles wholely within a single residue.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:property:: public IProperty<BondPair[]> Bonds { get; }
        
        Bonds involved in the molecule.
        
        

        :returns Narupa.Visualisation.Property.IProperty{Narupa.Frame.BondPair[]}: 
        
    .. cs:property:: public IProperty<int> ParticleCount { get; }
        
        Total number of particles involved in the system.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Int32}: 
        
    .. cs:property:: public IReadOnlyProperty<Cycle[]> Cycles { get; }
        
        List of set of indices that make up each cycle.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Visualisation.Node.Calculator.Cycle[]}: 
        
    .. cs:property:: public IReadOnlyProperty<int[]> ParticleResidues { get; }
        
        Set of particle residue indices.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}: 
        
    .. cs:property:: public IReadOnlyProperty<int> CyclesCount { get; }
        
        Number of cycles.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32}: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:method:: protected override void UpdateOutput()
        
        

        
    .. cs:method:: protected override void ClearOutput()
        
        

        
