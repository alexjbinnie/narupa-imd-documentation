SplineSegment
=============
.. cs:setscope:: Narupa.Visualisation.Node.Spline

.. cs:struct:: [Serializable] public struct SplineSegment
    
    
    .. cs:field:: public Vector3 StartPoint
        
        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Vector3 EndPoint
        
        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Vector3 StartTangent
        
        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Vector3 EndTangent
        
        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Vector3 StartNormal
        
        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Vector3 EndNormal
        
        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Color StartColor
        
        :returns UnityEngine.Color: 
        
    .. cs:field:: public Color EndColor
        
        :returns UnityEngine.Color: 
        
    .. cs:field:: public Vector3 StartScale
        
        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Vector3 EndScale
        
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 GetPoint(float t)
        
        :param System.Single t: 
        :returns UnityEngine.Vector3: 
        
