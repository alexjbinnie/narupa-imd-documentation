IntInputNode
============
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class IntInputNode : InputNode<IntProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~System.Int32` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.IntProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
