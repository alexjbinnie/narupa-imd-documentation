SecondaryStructureNode
======================
.. cs:setscope:: Narupa.Visualisation.Node.Protein

.. cs:class:: [Serializable] public class SecondaryStructureNode
    
    Calculates secondary structure using the DSSP Algorithm.
    
    

    
    .. cs:property:: public IProperty<Vector3[]> AtomPositions { get; }
        
        Array of atomic positions. This should contains the atoms which are relevant to
        the protein backbone.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Vector3[]}: 
        
    .. cs:property:: public IProperty<int[]> AtomResidues { get; }
        
        Array of residue indices which may appear in
        :cs:any:`~Narupa.Visualisation.Node.Protein.SecondaryStructureNode.PeptideResidueSequences` for each atom.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Int32[]}: 
        
    .. cs:property:: public IProperty<string[]> AtomNames { get; }
        
        Array of atom names. Each amino acid should have atoms named 'CA', 'C', 'N' and
        'O'.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.String[]}: 
        
    .. cs:property:: public IProperty<int> ResidueCount { get; }
        
        Number of residues involved. The maximum index referenced in both
        :cs:any:`~Narupa.Visualisation.Node.Protein.SecondaryStructureNode.AtomResidues` and :cs:any:`~Narupa.Visualisation.Node.Protein.SecondaryStructureNode.PeptideResidueSequences` should
        be less than this value.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Int32}: 
        
    .. cs:property:: public IProperty<IReadOnlyList<int>[]> PeptideResidueSequences { get; }
        
        Array of residue indices that indicate which residues are involved in a protein
        chain.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Collections.Generic.IReadOnlyList{System.Int32}[]}: 
        
    .. cs:property:: public DsspOptions DsspOptions { get; set; }
        
        Options to configure the DSSP algorithm.
        
        

        :returns Narupa.Visualisation.Node.Protein.DsspOptions: 
        
    .. cs:property:: public IReadOnlyProperty<SecondaryStructureAssignment[]> ResidueSecondaryStructure { get; }
        
        Secondary structure assignments for each residue. The size of this array will
        be equal to :cs:any:`~Narupa.Visualisation.Node.Protein.SecondaryStructureNode.ResidueCount`, with residues that are not in one of
        the peptide chains provided in :cs:any:`~Narupa.Visualisation.Node.Protein.SecondaryStructureNode.PeptideResidueSequences` being
        given the assignment :cs:any:`~Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment.None`
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment[]}: 
        
    .. cs:property:: public IReadOnlyProperty<BondPair[]> HydrogenBonds { get; }
        
        Array of calculated hydrogen bonds, based on indices of atoms in the
        :cs:any:`~Narupa.Visualisation.Node.Protein.SecondaryStructureNode.AtomPositions`.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Frame.BondPair[]}: 
        
    .. cs:property:: public bool IsInputValid { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: public bool AreResiduesDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: public bool AreResiduesValid { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: public void Refresh()
        
        
