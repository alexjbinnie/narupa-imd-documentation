FloatInput
==========
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class FloatInput : VisualisationComponent<FloatInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<FloatInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.FloatInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.FloatInputNode}`
    
