GoodsellRendererNode
====================
.. cs:setscope:: Narupa.Visualisation.Node.Renderer

.. cs:class:: [ExecuteAlways] [Serializable] public class GoodsellRendererNode : CommandBufferRendererNode, IDisposable
    
    Renderer which draws spheres using a command buffer, allowing them to be
    outlined depending on their relative residue indices, reminiscent of the Goodsell style.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Renderer.CommandBufferRendererNode`
    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:property:: public Transform Transform { get; set; }
        
        The transform to center this renderer on.
        
        

        :returns UnityEngine.Transform: 
        
    .. cs:method:: public override void Dispose()
        
        

        
    .. cs:method:: public void Setup()
        
        Setup this renderer.
        
        

        
    .. cs:method:: public override void Render(Camera cam)
        
        

        :param UnityEngine.Camera cam: 
        
    .. cs:method:: protected override IEnumerable<(CameraEvent Event, CommandBuffer Buffer)> GetBuffers(Camera camera)
        
        

        :param UnityEngine.Camera camera: 
        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer}}: 
        
