FloatLerpNode
=============
.. cs:setscope:: Narupa.Visualisation.Node.Calculator

.. cs:class:: [Serializable] public class FloatLerpNode : LerpNode<float, FloatArrayProperty>
    
    :inherits: :cs:any:`~Narupa.Visualisation.Node.Calculator.LerpNode{System.Single,Narupa.Visualisation.Properties.Collections.FloatArrayProperty}`
    
    .. cs:method:: protected override float MoveTowards(float current, float target)
        
        :param System.Single current: 
        :param System.Single target: 
        :returns System.Single: 
        
