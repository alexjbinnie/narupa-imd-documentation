ControllerInputMode
===================
.. cs:setscope:: Narupa.Frontend.Controllers

.. cs:class:: public abstract class ControllerInputMode : MonoBehaviour
    
    Define the mode of the controller as 'interaction', setting up the correct
    gizmo.
    
    

    This is a temporary object until a more sophisticated multi-mode tool setup is
    completed.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: protected ControllerManager Controllers { get; }
        
        :returns Narupa.Frontend.Controllers.ControllerManager: 
        
    .. cs:property:: protected bool IsCurrentInputMode { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: public abstract int Priority { get; }
        
        :returns System.Int32: 
        
    .. cs:method:: public abstract void OnModeStarted()
        
        
    .. cs:method:: public abstract void OnModeEnded()
        
        
    .. cs:method:: public abstract void SetupController(VrController controller, SteamVR_Input_Sources inputSource)
        
        :param Narupa.Frontend.Controllers.VrController controller: 
        :param Valve.VR.SteamVR_Input_Sources inputSource: 
        
