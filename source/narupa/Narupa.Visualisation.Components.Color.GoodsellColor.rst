GoodsellColor
=============
.. cs:setscope:: Narupa.Visualisation.Components.Color

.. cs:class:: public sealed class GoodsellColor : VisualisationComponent<GoodsellColorNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<GoodsellColorNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Color.GoodsellColorNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Color.GoodsellColorNode}`
    
