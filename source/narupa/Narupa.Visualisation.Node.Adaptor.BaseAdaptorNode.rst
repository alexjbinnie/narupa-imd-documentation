BaseAdaptorNode
===============
.. cs:setscope:: Narupa.Visualisation.Node.Adaptor

.. cs:class:: public abstract class BaseAdaptorNode : IDynamicPropertyProvider, IPropertyProvider
    
    An Adaptor node is a node which can provide any property. This allows it to act
    as a gateway, for both connecting the visualisation system to external sources
    (such as a frame source) and for filtering out values.
    
    

    :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    
    .. cs:property:: protected IReadOnlyDictionary<string, Property> Properties { get; }
        
        Dynamic properties created by the system, with the keys corresponding to the
        keys in the frame's data.
        
        

        :returns System.Collections.Generic.IReadOnlyDictionary{System.String,Narupa.Visualisation.Property.Property}: 
        
    .. cs:method:: public virtual IReadOnlyProperty<T> GetOrCreateProperty<T>(string name)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider.GetOrCreateProperty``1(System.String)`
        :param System.String name: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{{T}}: 
        
    .. cs:method:: public IEnumerable<(string name, Type type)> GetPotentialProperties()
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider.GetPotentialProperties`
        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,System.Type}}: 
        
    .. cs:method:: public bool CanDynamicallyProvideProperty<T>(string name)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider.CanDynamicallyProvideProperty``1(System.String)`
        :param System.String name: 
        :returns System.Boolean: 
        
    .. cs:method:: public virtual IReadOnlyProperty GetProperty(string key)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider.GetProperty(System.String)`
        :param System.String key: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:method:: public virtual IEnumerable<(string name, IReadOnlyProperty property)> GetProperties()
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider.GetProperties`
        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,Narupa.Visualisation.Property.IReadOnlyProperty}}: 
        
    .. cs:method:: protected virtual void OnCreateProperty<T>(string key, IProperty<T> property)
        
        Callback when a property has been requested, created and added to the
        :cs:any:`~Narupa.Visualisation.Node.Adaptor.BaseAdaptorNode.Properties` dictionary, but has not yet been returned to the
        requestor.
        
        

        :param System.String key: 
        :param Narupa.Visualisation.Property.IProperty{{T}} property: 
        
    .. cs:property:: public IReadOnlyProperty<Element[]> ParticleElements { get; }
        
        Array of elements of the provided frame.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Core.Science.Element[]}: 
        
    .. cs:property:: public IReadOnlyProperty<Vector3[]> ParticlePositions { get; }
        
        Array of particle positions of the provided frame.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{UnityEngine.Vector3[]}: 
        
    .. cs:property:: public IReadOnlyProperty<BondPair[]> BondPairs { get; }
        
        Array of bonds of the provided frame.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Frame.BondPair[]}: 
        
    .. cs:property:: public IReadOnlyProperty<int[]> BondOrders { get; }
        
        Array of bond orders of the provided frame.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}: 
        
    .. cs:property:: public IReadOnlyProperty<int[]> ParticleResidues { get; }
        
        Array of particle residues of the provided frame.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}: 
        
    .. cs:property:: public IReadOnlyProperty<string[]> ParticleNames { get; }
        
        Array of particle names of the provided frame.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.String[]}: 
        
    .. cs:property:: public IReadOnlyProperty<string[]> ResidueNames { get; }
        
        Array of residue names of the provided frame.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.String[]}: 
        
    .. cs:property:: public IReadOnlyProperty<int[]> ResidueEntities { get; }
        
        Array of residue entities of the provided frame.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}: 
        
    .. cs:property:: public IReadOnlyProperty<int> ResidueCount { get; }
        
        Array of residue entities of the provided frame.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32}: 
        
    .. cs:method:: public virtual void Refresh()
        
        Refresh the adaptor. Should be called every frame.
        
        

        
