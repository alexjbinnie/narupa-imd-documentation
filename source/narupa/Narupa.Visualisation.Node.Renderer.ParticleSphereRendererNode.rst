ParticleSphereRendererNode
==========================
.. cs:setscope:: Narupa.Visualisation.Node.Renderer

.. cs:class:: [Serializable] public class ParticleSphereRendererNode : IndirectMeshRenderer, IDisposable
    
    Visualisation node for rendering particles using spheres.
    
    

    A call to :cs:any:`~Narupa.Visualisation.Node.Renderer.IndirectMeshRenderer.UpdateRenderer` should be made every frame. Depending
    on use case, either call :cs:any:`~Narupa.Visualisation.Node.Renderer.IndirectMeshRenderer.Render(UnityEngine.Camera)` to draw for a single frame, or
    add to a :cs:any:`~UnityEngine.Rendering.CommandBuffer` using
    :cs:any:`~Narupa.Visualisation.Node.Renderer.IndirectMeshRenderer.AppendToCommandBuffer(UnityEngine.Rendering.CommandBuffer)`.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Renderer.IndirectMeshRenderer`
    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:property:: public IProperty<Vector3[]> ParticlePositions { get; }
        
        Positions of particles.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Vector3[]}: 
        
    .. cs:property:: public IProperty<Color[]> ParticleColors { get; }
        
        Color of particles.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Color[]}: 
        
    .. cs:property:: public IProperty<float[]> ParticleScales { get; }
        
        Scale of particles.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Single[]}: 
        
    .. cs:property:: public IProperty<Color> RendererColor { get; }
        
        Overall color of the renderer. Each particle color will be multiplied by this
        value.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Color}: 
        
    .. cs:property:: public IProperty<float> RendererScale { get; }
        
        Overall scaling of the renderer. Each particle scale will be multiplied by this
        value.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Single}: 
        
    .. cs:property:: public IProperty<Mesh> Mesh { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Mesh}: 
        
    .. cs:property:: public IProperty<Material> Material { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Material}: 
        
    .. cs:property:: public override bool ShouldRender { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: public override bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: public override void UpdateInput()
        
        
    .. cs:method:: protected virtual void UpdateBuffers()
        
        
    .. cs:property:: protected override IndirectMeshDrawCommand DrawCommand { get; }
        
        :returns Narupa.Visualisation.IndirectMeshDrawCommand: 
        
    .. cs:method:: public override void ResetBuffers()
        
        
