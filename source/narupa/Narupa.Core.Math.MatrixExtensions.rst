MatrixExtensions
================
.. cs:setscope:: Narupa.Core.Math

.. cs:class:: public static class MatrixExtensions
    
    Extension methods for Unity's :cs:any:`~UnityEngine.Matrix4x4`
    
    

    
    .. cs:method:: public static Vector3 GetTranslation(this Matrix4x4 matrix)
        
        Extract the translation component of the given TRS matrix. This is
        the worldspace origin of matrix's coordinate space.
        
        

        :param UnityEngine.Matrix4x4 matrix: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public static Quaternion GetRotation(this Matrix4x4 matrix)
        
        Extract the rotation component of the given TRS matrix. This is
        the quaternion that rotates worldspace forward, up, right vectors
        into the matrix's coordinate space.
        
        

        :param UnityEngine.Matrix4x4 matrix: 
        :returns UnityEngine.Quaternion: 
        
    .. cs:method:: public static Vector3 GetScale(this Matrix4x4 matrix)
        
        Extract the scale component of the given TRS matrix, assuming it is orthogonal.
        
        

        :param UnityEngine.Matrix4x4 matrix: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public static Matrix4x4 GetTransformationTo(this Matrix4x4 from, Matrix4x4 to)
        
        Get the matrix that transforms from this matrix to another.
        
        

        In Unity transformations are from local-space to world-space, so
        the transformation is multiplied on the right-hand side.
        
        

        :param UnityEngine.Matrix4x4 from: 
        :param UnityEngine.Matrix4x4 to: 
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:method:: public static Matrix4x4 TransformedBy(this Matrix4x4 matrix, Matrix4x4 transformation)
        
        Return this matrix transformed by the given transformation matrix.
        
        

        In Unity transformations are from local-space to world-space, so
        the transformation is multiplied on the right-hand side.
        
        

        :param UnityEngine.Matrix4x4 matrix: 
        :param UnityEngine.Matrix4x4 transformation: 
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:method:: public static Vector3 TransformPoint(this Matrix4x4 matrix, Vector3 point)
        
        

        :param UnityEngine.Matrix4x4 matrix: 
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public static Vector3 InverseTransformPoint(this Matrix4x4 matrix, Vector3 point)
        
        

        :param UnityEngine.Matrix4x4 matrix: 
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public static Vector3 TransformDirection(this Matrix4x4 matrix, Vector3 point)
        
        

        :param UnityEngine.Matrix4x4 matrix: 
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public static Vector3 InverseTransformDirection(this Matrix4x4 matrix, Vector3 point)
        
        

        :param UnityEngine.Matrix4x4 matrix: 
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
