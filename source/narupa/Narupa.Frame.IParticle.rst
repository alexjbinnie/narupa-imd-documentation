IParticle
=========
.. cs:setscope:: Narupa.Frame

.. cs:interface:: public interface IParticle
    
    A particle in a trajectory. Minimally, it has a 0-based index in the frame and
    a position.
    
    

    
    .. cs:property:: int Index { get; }
        
        The 0-based index of the particle
        
        

        :returns System.Int32: 
        
    .. cs:property:: string Type { get; }
        
        A string describing the 'type' of the particle
        
        

        :returns System.String: 
        
    .. cs:property:: Vector3 Position { get; }
        
        The position of the particle
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:property:: Element? Element { get; }
        
        The element of the particle, if it is an atom.
        
        

        :returns System.Nullable{Narupa.Core.Science.Element}: 
        
