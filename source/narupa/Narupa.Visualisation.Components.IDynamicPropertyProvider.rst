IDynamicPropertyProvider
========================
.. cs:setscope:: Narupa.Visualisation.Components

.. cs:interface:: public interface IDynamicPropertyProvider : IPropertyProvider
    
    
    .. cs:method:: IReadOnlyProperty<T> GetOrCreateProperty<T>(string name)
        
        Get an existing property, or attempt to dynamically create one with the given
        type.
        
        

        :param System.String name: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{{T}}: 
        
    .. cs:method:: IEnumerable<(string name, Type type)> GetPotentialProperties()
        
        Get a list of potential properties, some of which may already exist.
        
        

        A returned item of this method indicates that
        :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider.GetOrCreateProperty``1(System.String)` will be successful with the given name
        and type. However, that method may also support arbitary names/types
        depending on the implementation.
        
        

        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,System.Type}}: 
        
    .. cs:method:: bool CanDynamicallyProvideProperty<T>(string name)
        
        Could this provider give a property on a call to
        :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider.GetOrCreateProperty``1(System.String)`?.
        
        

        :param System.String name: 
        :returns System.Boolean: 
        
