IndexArray
==========
.. cs:setscope:: Narupa.Protocol

.. cs:class:: public sealed class IndexArray : Object, IMessage<IndexArray>, IMessage, IEquatable<IndexArray>, IDeepCloneable<IndexArray>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.IndexArray}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.IndexArray}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.IndexArray}`
    
    .. cs:field:: public const int ValuesFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public IndexArray()
        
        
    .. cs:constructor:: public IndexArray(IndexArray other)
        
        :param Narupa.Protocol.IndexArray other: 
        
    .. cs:method:: public IndexArray Clone()
        
        :returns Narupa.Protocol.IndexArray: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(IndexArray other)
        
        :param Narupa.Protocol.IndexArray other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(IndexArray other)
        
        :param Narupa.Protocol.IndexArray other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<IndexArray> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.IndexArray}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public RepeatedField<uint> Values { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{System.UInt32}: 
        
