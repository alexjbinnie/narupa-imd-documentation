ElementArrayInputNode
=====================
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class ElementArrayInputNode : InputNode<ElementArrayProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~Element[]` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.Collections.ElementArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
