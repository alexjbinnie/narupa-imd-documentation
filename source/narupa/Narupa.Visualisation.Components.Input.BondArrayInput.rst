BondArrayInput
==============
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class BondArrayInput : VisualisationComponent<BondArrayInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<BondArrayInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.BondArrayInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.BondArrayInputNode}`
    
