SubscribePlayerAvatarsRequest
=============================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class SubscribePlayerAvatarsRequest : Object, IMessage<SubscribePlayerAvatarsRequest>, IMessage, IEquatable<SubscribePlayerAvatarsRequest>, IDeepCloneable<SubscribePlayerAvatarsRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.SubscribePlayerAvatarsRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.SubscribePlayerAvatarsRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.SubscribePlayerAvatarsRequest}`
    
    .. cs:field:: public const int UpdateIntervalFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int IgnorePlayerIdFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public SubscribePlayerAvatarsRequest()
        
        
    .. cs:constructor:: public SubscribePlayerAvatarsRequest(SubscribePlayerAvatarsRequest other)
        
        :param Narupa.Protocol.Multiplayer.SubscribePlayerAvatarsRequest other: 
        
    .. cs:method:: public SubscribePlayerAvatarsRequest Clone()
        
        :returns Narupa.Protocol.Multiplayer.SubscribePlayerAvatarsRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(SubscribePlayerAvatarsRequest other)
        
        :param Narupa.Protocol.Multiplayer.SubscribePlayerAvatarsRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(SubscribePlayerAvatarsRequest other)
        
        :param Narupa.Protocol.Multiplayer.SubscribePlayerAvatarsRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<SubscribePlayerAvatarsRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.SubscribePlayerAvatarsRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public float UpdateInterval { get; set; }
        
        :returns System.Single: 
        
    .. cs:property:: public string IgnorePlayerId { get; set; }
        
        :returns System.String: 
        
