SelectionArrayProperty
======================
.. cs:setscope:: Narupa.Visualisation.Properties.Collections

.. cs:class:: [Serializable] public class SelectionArrayProperty : ArrayProperty<IReadOnlyList<int>>, IProperty<IReadOnlyList<int>[]>, IReadOnlyProperty<IReadOnlyList<int>[]>, IProperty, IReadOnlyProperty, IEnumerable<IReadOnlyList<int>>, IEnumerable
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an array of :cs:any:`~System.Int32` values.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Properties.Collections.ArrayProperty{System.Collections.Generic.IReadOnlyList{System.Int32}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{System.Collections.Generic.IReadOnlyList{System.Int32}[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.Collections.Generic.IReadOnlyList{System.Int32}[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Collections.Generic.IReadOnlyList{System.Int32}}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
