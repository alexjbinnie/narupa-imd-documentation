TransformExtensions
===================
.. cs:setscope:: Narupa.Core

.. cs:class:: public static class TransformExtensions
    
    
    .. cs:method:: public static void SetToLocalIdentity(this Transform transform)
        
        Set the transform to be the identity in local space.
        
        

        :param UnityEngine.Transform transform: 
        
