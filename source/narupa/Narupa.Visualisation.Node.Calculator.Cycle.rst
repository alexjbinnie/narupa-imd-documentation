Cycle
=====
.. cs:setscope:: Narupa.Visualisation.Node.Calculator

.. cs:class:: [Serializable] public class Cycle : IReadOnlyList<int>, IReadOnlyCollection<int>, IEnumerable<int>, IEnumerable
    
    List of indices representing a closed cycle, stored in canonical order (first
    index is the
    lowest, with the rest sorted such that the second index is lower than the last
    index.
    
    

    For example, the indices (0, 1, 2, 3),  (0, 3, 2, 1) and (2, 3, 0, 1) are all
    converted
    to the canonical (0, 1, 2, 3).
    
    

    :implements: :cs:any:`~System.Collections.Generic.IReadOnlyList{System.Int32}`
    :implements: :cs:any:`~System.Collections.Generic.IReadOnlyCollection{System.Int32}`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Int32}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
    .. cs:method:: protected bool Equals(Cycle other)
        
        :param Narupa.Visualisation.Node.Calculator.Cycle other: 
        :returns System.Boolean: 
        
    .. cs:method:: public IEnumerator<int> GetEnumerator()
        
        :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Int32}.GetEnumerator`
        :returns System.Collections.Generic.IEnumerator{System.Int32}: 
        
    .. cs:method:: public override bool Equals(object obj)
        
        :param System.Object obj: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:property:: public int[] Indices { get; }
        
        Indices stored in the cycle.
        
        

        :returns System.Int32[]: 
        
    .. cs:constructor:: public Cycle(params int[] indices)
        
        :param System.Int32[] indices: 
        
    .. cs:property:: public int Count { get; }
        
        Length of the cycle.
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyCollection{System.Int32}.Count`
        :returns System.Int32: 
        
    .. cs:property:: public int Length { get; }
        
        Length of the cycle.
        
        

        :returns System.Int32: 
        
    .. cs:indexer:: public int this[int i] { get; }
        
        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyList{System.Int32}.Item(System.Int32)`
        :param System.Int32 i: 
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: IEnumerator IEnumerable.GetEnumerator()
        
        :implements: :cs:any:`~System.Collections.IEnumerable.GetEnumerator`
        :returns System.Collections.IEnumerator: 
        
