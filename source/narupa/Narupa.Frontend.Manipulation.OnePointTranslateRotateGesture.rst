OnePointTranslateRotateGesture
==============================
.. cs:setscope:: Narupa.Frontend.Manipulation

.. cs:class:: public sealed class OnePointTranslateRotateGesture
    
    Gesture that ties an object's position and orientation to a control
    point's  position and orientation. Updating the control point
    transformation yields an updated object transformation such that the
    object has moved to preserve the relative position and orientation of
    the object and control point.
    Example usage: A single hand grabbing a box, the box then moves and
    rotates with the motion of the hand.
    
    

    
    .. cs:method:: public void BeginGesture(Transformation objectTransformation, UniformScaleTransformation controlTransformation)
        
        Begin the gesture with an initial object transform and initial control
        point transform. Throughout the gesture the relationship between these
        transforms will remain invariant.
        
        

        :param Narupa.Core.Math.Transformation objectTransformation: 
        :param Narupa.Core.Math.UniformScaleTransformation controlTransformation: 
        
    .. cs:method:: public Transformation UpdateControlPoint(UniformScaleTransformation controlTransformation)
        
        Return an object transform that is the initial object transform
        altered in the same manner as the gesture transformation since the
        gesture began.
        The relative coordinates of the gesture within the object space
        remains invariant.
        
        

        :param Narupa.Core.Math.UniformScaleTransformation controlTransformation: 
        :returns Narupa.Core.Math.Transformation: 
        
