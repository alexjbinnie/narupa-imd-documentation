IFrame
======
.. cs:setscope:: Narupa.Frame

.. cs:interface:: public interface IFrame
    
    A single frame with a trajectory. It contains a snapshot of the system, with a
    list of particles with types, positions etc.
    
    

    
    .. cs:property:: IReadOnlyList<IParticle> Particles { get; }
        
        List of particles in the current frame.
        
        

        :returns System.Collections.Generic.IReadOnlyList{Narupa.Frame.IParticle}: 
        
    .. cs:property:: IReadOnlyList<BondPair> Bonds { get; }
        
        List of bonds between particles.
        
        

        :returns System.Collections.Generic.IReadOnlyList{Narupa.Frame.BondPair}: 
        
    .. cs:property:: IDictionary<string, object> Data { get; }
        
        General data associated with the current frame.
        
        

        :returns System.Collections.Generic.IDictionary{System.String,System.Object}: 
        
