VisualisationComponent
======================
.. cs:setscope:: Narupa.Visualisation.Components

.. cs:class:: public abstract class VisualisationComponent : MonoBehaviour, ISerializationCallbackReceiver, IPropertyProvider
    
    Wrapper :cs:any:`~UnityEngine.MonoBehaviour` around a node of the visualisation system,
    serializing links between various nodes. This allows the node system to be used
    as multiple nodes in a single :cs:any:`~UnityEngine.MonoBehaviour` or by using the
    subclasses of :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent` to setup a visualisation
    graph in the Editor.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    
    .. cs:method:: protected virtual void OnEnable()
        
        
    .. cs:method:: protected virtual void OnDisable()
        
        
    .. cs:method:: protected virtual void Awake()
        
        
    .. cs:method:: public static void LinkProperties<T>(IPropertyProvider source, string sourceName, IPropertyProvider destination, string destName)
        
        Setup a link between two property providers
        
        

        :param Narupa.Visualisation.Property.IPropertyProvider source: 
        :param System.String sourceName: 
        :param Narupa.Visualisation.Property.IPropertyProvider destination: 
        :param System.String destName: 
        
    .. cs:method:: public abstract object GetWrappedVisualisationNode()
        
        Get the visualisation node wrapped by this component.
        
        

        :returns System.Object: 
        
    .. cs:method:: protected abstract Type GetWrappedVisualisationNodeType()
        
        Get the type of the node wrapped by this component.
        
        

        :returns System.Type: 
        
    .. cs:method:: public void OnBeforeSerialize()
        
        :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize`
        
    .. cs:method:: public void OnAfterDeserialize()
        
        :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize`
        
    .. cs:method:: public virtual IReadOnlyProperty GetProperty(string name)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider.GetProperty(System.String)`
        :param System.String name: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:method:: public virtual IEnumerable<(string name, IReadOnlyProperty property)> GetProperties()
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider.GetProperties`
        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,Narupa.Visualisation.Property.IReadOnlyProperty}}: 
        
    .. cs:method:: protected virtual void OnDestroy()
        
        
