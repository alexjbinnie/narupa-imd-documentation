SecondaryStructureColor
=======================
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [Serializable] public class SecondaryStructureColor : VisualiserColorNode
    
    :inherits: :cs:any:`~Narupa.Visualisation.Node.Color.VisualiserColorNode`
    
    .. cs:property:: protected override bool IsInputValid { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        
    .. cs:method:: protected override void ClearOutput()
        
        
    .. cs:method:: protected override void UpdateOutput()
        
        
