FrameChanged
============
.. cs:setscope:: Narupa.Frame

.. cs:delegate:: public delegate void FrameChanged(IFrame frame, FrameChanges changes);
    
    :param Narupa.Frame.IFrame frame: 
    :param Narupa.Frame.Event.FrameChanges changes: 
    
