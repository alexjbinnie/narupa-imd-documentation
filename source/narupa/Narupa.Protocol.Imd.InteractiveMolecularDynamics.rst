InteractiveMolecularDynamics
============================
.. cs:setscope:: Narupa.Protocol.Imd

.. cs:class:: public static class InteractiveMolecularDynamics : Object
    
    
    .. cs:method:: public static ServerServiceDefinition BindService(InteractiveMolecularDynamics.InteractiveMolecularDynamicsBase serviceImpl)
        
        :param Narupa.Protocol.Imd.InteractiveMolecularDynamics.InteractiveMolecularDynamicsBase serviceImpl: 
        :returns Grpc.Core.ServerServiceDefinition: 
        
    .. cs:method:: public static void BindService(ServiceBinderBase serviceBinder, InteractiveMolecularDynamics.InteractiveMolecularDynamicsBase serviceImpl)
        
        :param Grpc.Core.ServiceBinderBase serviceBinder: 
        :param Narupa.Protocol.Imd.InteractiveMolecularDynamics.InteractiveMolecularDynamicsBase serviceImpl: 
        
    .. cs:property:: public static ServiceDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.ServiceDescriptor: 
        
