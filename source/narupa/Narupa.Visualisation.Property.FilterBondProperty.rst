FilterBondProperty
==================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:class:: public class FilterBondProperty : IReadOnlyProperty<BondPair[]>, IReadOnlyProperty
    
    Filters a list of bonds by some index property.
    
    

    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Frame.BondPair[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
    .. cs:constructor:: public FilterBondProperty(IReadOnlyProperty<BondPair[]> property, IReadOnlyProperty<int[]> filter)
        
        :param Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Frame.BondPair[]} property: 
        :param Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]} filter: 
        
    .. cs:method:: public void Update()
        
        
    .. cs:property:: public bool HasValue { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.HasValue`
        :returns System.Boolean: 
        
    .. cs:event:: public event Action ValueChanged
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.ValueChanged`
        :returns System.Action: 
        
    .. cs:property:: public Type PropertyType { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.PropertyType`
        :returns System.Type: 
        
    .. cs:property:: object IReadOnlyProperty.Value { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.Value`
        :returns System.Object: 
        
    .. cs:property:: public BondPair[] Value { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Frame.BondPair[]}.Value`
        :returns Narupa.Frame.BondPair[]: 
        
