IncomingStream<TIncoming>
=========================
.. cs:setscope:: Narupa.Grpc.Stream

.. cs:class:: public sealed class IncomingStream<TIncoming> : Cancellable, ICancellable, ICancellationTokenSource, IDisposable, IAsyncClosable
    
    Wraps the incoming response stream of a gRPC call and raises an event
    when new content is received.
    
    

    :inherits: :cs:any:`~Narupa.Core.Async.Cancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellationTokenSource`
    :implements: :cs:any:`~System.IDisposable`
    :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable`
    
    .. cs:event:: public event Action<TIncoming> MessageReceived
        
        Callback for when a new item is received from the stream.
        
        

        :returns System.Action{{TIncoming}}: 
        
    .. cs:method:: public static IncomingStream<TIncoming> CreateStreamFromServerCall<TRequest>(ServerStreamingCall<TRequest, TIncoming> grpcCall, TRequest request, params CancellationToken[] externalTokens)
        
        Call a gRPC method with the provided :code:`request`,
        and return a stream which has not been started yet.
        
        

        :param Narupa.Grpc.Stream.ServerStreamingCall{{TRequest},{TIncoming}} grpcCall: 
        :param {TRequest} request: 
        :param System.Threading.CancellationToken[] externalTokens: 
        :returns Narupa.Grpc.Stream.IncomingStream`1: 
        
    .. cs:method:: public Task StartReceiving()
        
        Start consuming the stream and raising events. Returns the
        iteration task.
        
        

        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public Task CloseAsync()
        
        

        :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable.CloseAsync`
        :returns System.Threading.Tasks.Task: 
        
