FrameChangedEventArgs
=====================
.. cs:setscope:: Narupa.Frame.Event

.. cs:class:: public class FrameChangedEventArgs : EventArgs
    
    Event arguments for when a :cs:any:`~Narupa.Frame.Event.FrameChangedEventArgs.Frame` has been updated.
    
    

    :inherits: :cs:any:`~System.EventArgs`
    
    .. cs:constructor:: public FrameChangedEventArgs(IFrame frame, [NotNull] FrameChanges changes)
        
        Create event arguments that represent a frame that has updated.
        
        

        :param Narupa.Frame.IFrame frame: 
        :param Narupa.Frame.Event.FrameChanges changes: 
        
    .. cs:property:: public IFrame Frame { get; }
        
        The new :cs:any:`~Narupa.Frame.Event.FrameChangedEventArgs.Frame`.
        
        

        :returns Narupa.Frame.IFrame: 
        
    .. cs:property:: [NotNull] public FrameChanges Changes { get; }
        
        Information about what has changed since the previous frame.
        
        

        :returns Narupa.Frame.Event.FrameChanges: 
        
