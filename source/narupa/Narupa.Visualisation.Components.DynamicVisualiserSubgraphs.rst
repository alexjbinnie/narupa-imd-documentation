DynamicVisualiserSubgraphs
==========================
.. cs:setscope:: Narupa.Visualisation.Components

.. cs:class:: [ExecuteAlways] public class DynamicVisualiserSubgraphs : MonoBehaviour
    
    Instantiates a set of visualisation subgraphs (collection of visualisation
    nodes with inputs and outputs), and links them together by name.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: public IDynamicPropertyProvider FrameAdaptor { get; set; }
        
        The :cs:any:`~Narupa.Visualisation.Components.DynamicVisualiserSubgraphs.FrameAdaptor` used to provide keys that are not provided
        anywhere else and do not have a default value.
        
        

        :returns Narupa.Visualisation.Components.IDynamicPropertyProvider: 
        
    .. cs:method:: public void SetSubgraphs(params GameObject[] subgraphs)
        
        Set the subgraph objects that make up this renderer.
        
        

        :param UnityEngine.GameObject[] subgraphs: 
        
