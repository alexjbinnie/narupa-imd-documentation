GetCommandsReply
================
.. cs:setscope:: Narupa.Protocol.Command

.. cs:class:: public sealed class GetCommandsReply : Object, IMessage<GetCommandsReply>, IMessage, IEquatable<GetCommandsReply>, IDeepCloneable<GetCommandsReply>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Command.GetCommandsReply}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Command.GetCommandsReply}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Command.GetCommandsReply}`
    
    .. cs:field:: public const int CommandsFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public GetCommandsReply()
        
        
    .. cs:constructor:: public GetCommandsReply(GetCommandsReply other)
        
        :param Narupa.Protocol.Command.GetCommandsReply other: 
        
    .. cs:method:: public GetCommandsReply Clone()
        
        :returns Narupa.Protocol.Command.GetCommandsReply: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(GetCommandsReply other)
        
        :param Narupa.Protocol.Command.GetCommandsReply other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(GetCommandsReply other)
        
        :param Narupa.Protocol.Command.GetCommandsReply other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<GetCommandsReply> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Command.GetCommandsReply}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public RepeatedField<CommandMessage> Commands { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{Narupa.Protocol.Command.CommandMessage}: 
        
