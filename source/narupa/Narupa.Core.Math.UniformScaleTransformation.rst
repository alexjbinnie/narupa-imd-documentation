UniformScaleTransformation
==========================
.. cs:setscope:: Narupa.Core.Math

.. cs:struct:: public struct UniformScaleTransformation : ITransformation
    
    A transformation consisting of a scaling by a uniform factor, a rotation and
    then a translation.
    
    

    :cs:any:`~Narupa.Core.Math.UniformScaleTransformation`s are closed under composition
    (combining two of them yields a third). These transformations preserve angles.
    
    

    :implements: :cs:any:`~Narupa.Core.Math.ITransformation`
    
    .. cs:property:: ITransformation ITransformation.inverse { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.inverse`
        :returns Narupa.Core.Math.ITransformation: 
        
    .. cs:field:: public Vector3 position
        
        The translation this transformation applies. When considered as a
        transformation from an object's local space to world space, describes the
        position of the object.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Quaternion rotation
        
        The rotation this transformation applies. When considered as a transformation
        from an object's local space to world space, describes the rotation of the
        object.
        
        

        :returns UnityEngine.Quaternion: 
        
    .. cs:field:: public float scale
        
        The uniform scaling this transformation applies. When considered as a
        transformation from an object's local space to world space, describes the scale
        of the object.
        
        

        :returns System.Single: 
        
    .. cs:constructor:: public UniformScaleTransformation(Vector3 position, Quaternion rotation, float scale)
        
        Create a transformation from its three actions.
        
        

        :param UnityEngine.Vector3 position: The translation this transformation applies.
        :param UnityEngine.Quaternion rotation: The rotation this transformation applies.
        :param System.Single scale: The scale this transformation applies.
        
    .. cs:property:: public static UniformScaleTransformation identity { get; }
        
        The identity transformation.
        
        

        :returns Narupa.Core.Math.UniformScaleTransformation: 
        
    .. cs:property:: public UniformScaleTransformation inverse { get; }
        
        

        :returns Narupa.Core.Math.UniformScaleTransformation: 
        
    .. cs:property:: public Matrix4x4 matrix { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.matrix`
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:property:: public Matrix4x4 inverseMatrix { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.inverseMatrix`
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:conversion:: public static implicit operator Matrix4x4(UniformScaleTransformation transformation)
        
        :param Narupa.Core.Math.UniformScaleTransformation transformation: 
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:conversion:: public static implicit operator Transformation(UniformScaleTransformation transformation)
        
        :param Narupa.Core.Math.UniformScaleTransformation transformation: 
        :returns Narupa.Core.Math.Transformation: 
        
    .. cs:operator:: public static UniformScaleTransformation operator *(UniformScaleTransformation a, UniformScaleTransformation b)
        
        :param Narupa.Core.Math.UniformScaleTransformation a: 
        :param Narupa.Core.Math.UniformScaleTransformation b: 
        :returns Narupa.Core.Math.UniformScaleTransformation: 
        
    .. cs:operator:: public static Transformation operator *(UniformScaleTransformation a, Transformation b)
        
        :param Narupa.Core.Math.UniformScaleTransformation a: 
        :param Narupa.Core.Math.Transformation b: 
        :returns Narupa.Core.Math.Transformation: 
        
    .. cs:method:: public Vector3 TransformPoint(Vector3 pt)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.TransformPoint(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 pt: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 InverseTransformPoint(Vector3 pt)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.InverseTransformPoint(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 pt: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 TransformDirection(Vector3 direction)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.TransformDirection(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 direction: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 InverseTransformDirection(Vector3 pt)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.InverseTransformDirection(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 pt: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Transformation TransformationTo(Transformation to)
        
        Get the transformation matrix that takes this transformation to the one provided.
        
        

        :param Narupa.Core.Math.Transformation to: 
        :returns Narupa.Core.Math.Transformation: 
        
    .. cs:method:: public Transformation TransformBy(Transformation trans)
        
        Right multiply by the provided transformation matrix to give another.
        
        

        :param Narupa.Core.Math.Transformation trans: 
        :returns Narupa.Core.Math.Transformation: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
