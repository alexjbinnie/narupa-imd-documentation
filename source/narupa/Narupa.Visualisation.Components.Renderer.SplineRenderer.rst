SplineRenderer
==============
.. cs:setscope:: Narupa.Visualisation.Components.Renderer

.. cs:class:: public class SplineRenderer : VisualisationComponentRenderer<SplineRendererNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<SplineRendererNode>
    
    :inherits: :cs:any:`~Narupa.Visualisation.Components.Renderer.VisualisationComponentRenderer{Narupa.Visualisation.Node.Renderer.SplineRendererNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Renderer.SplineRendererNode}`
    
    .. cs:method:: protected override void OnEnable()
        
        
    .. cs:method:: protected override void Render(Camera camera)
        
        :param UnityEngine.Camera camera: 
        
