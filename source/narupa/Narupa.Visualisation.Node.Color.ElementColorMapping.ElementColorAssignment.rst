ElementColorMapping.ElementColorAssignment
==========================================
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [Serializable] public class ElementColorAssignment
    
    Key-value pair for atomic element to color mappings for serialisation in Unity
    
    

    
    .. cs:field:: public int atomicNumber
        
        :returns System.Int32: 
        
    .. cs:field:: public Color color
        
        :returns UnityEngine.Color: 
        
