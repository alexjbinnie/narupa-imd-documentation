FrameAdaptorProperty
====================
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class FrameAdaptorProperty : InterfaceProperty<IDynamicPropertyProvider>, IProperty<IDynamicPropertyProvider>, IReadOnlyProperty<IDynamicPropertyProvider>, IProperty, IReadOnlyProperty, ISerializationCallbackReceiver
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for a :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.InterfaceProperty{Narupa.Visualisation.Components.IDynamicPropertyProvider}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{Narupa.Visualisation.Components.IDynamicPropertyProvider}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Visualisation.Components.IDynamicPropertyProvider}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    
