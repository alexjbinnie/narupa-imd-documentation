TwoPointTranslateRotateScaleGesture
===================================
.. cs:setscope:: Narupa.Frontend.Manipulation

.. cs:class:: public sealed class TwoPointTranslateRotateScaleGesture
    
    Gesture that ties an object's position, orientation, and scale to the
    positions of two control points. Updating the control point matrices yields
    an updated object matrix such that the object has moved to preserve the
    local positions of the control points within the object.
    Example usage: Two hands grab corners of a box, and as the hands move
    the box stretches and pivots with the motion of the hands.
    
    

    
    .. cs:method:: public void BeginGesture(Transformation objectTransformation, UnitScaleTransformation controlTransformation1, UnitScaleTransformation controlTransformation2)
        
        Begin the gesture with an initial object transformation and initial
        transformations for the two control points. Throughout the gesture
        the positions of the control points within object space will remain
        invariant.
        
        

        :param Narupa.Core.Math.Transformation objectTransformation: 
        :param Narupa.Core.Math.UnitScaleTransformation controlTransformation1: 
        :param Narupa.Core.Math.UnitScaleTransformation controlTransformation2: 
        
    .. cs:method:: public Transformation UpdateControlPoints(UnitScaleTransformation controlTransformation1, UnitScaleTransformation controlTransformation2)
        
        Return an updated object transformation to account for the changes
        in the control point transformations.
        
        

        :param Narupa.Core.Math.UnitScaleTransformation controlTransformation1: 
        :param Narupa.Core.Math.UnitScaleTransformation controlTransformation2: 
        :returns Narupa.Core.Math.Transformation: 
        
