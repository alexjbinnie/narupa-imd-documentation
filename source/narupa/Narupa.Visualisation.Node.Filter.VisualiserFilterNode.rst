VisualiserFilterNode
====================
.. cs:setscope:: Narupa.Visualisation.Node.Filter

.. cs:class:: public abstract class VisualiserFilterNode : GenericOutputNode
    
    Filters particles by some criterea.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:property:: protected abstract int MaximumFilterCount { get; }
        
        The maximum possible number of filtered indices. Used to allocate an initial array.
        
        

        :returns System.Int32: 
        
    .. cs:method:: protected override void UpdateOutput()
        
        

        
    .. cs:method:: protected override void ClearOutput()
        
        

        
    .. cs:method:: protected abstract IEnumerable<int> GetFilteredIndices()
        
        Return the indices of particles which meet the criteria.
        
        

        :returns System.Collections.Generic.IEnumerable{System.Int32}: 
        
    .. cs:property:: public IReadOnlyProperty<int[]> ParticleFilter { get; }
        
        List of particle indices parsed by this filter.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}: 
        
