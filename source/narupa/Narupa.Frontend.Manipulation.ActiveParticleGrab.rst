ActiveParticleGrab
==================
.. cs:setscope:: Narupa.Frontend.Manipulation

.. cs:class:: public class ActiveParticleGrab : IActiveManipulation
    
    Represents a grab between a particle and world-space position
    
    

    :implements: :cs:any:`~Narupa.Frontend.Manipulation.IActiveManipulation`
    
    .. cs:event:: public event Action ManipulationEnded
        
        

        :implements: :cs:any:`~Narupa.Frontend.Manipulation.IActiveManipulation.ManipulationEnded`
        :returns System.Action: 
        
    .. cs:property:: public string Id { get; }
        
        A unique identifier for this interaction.
        
        

        :returns System.String: 
        
    .. cs:property:: public IReadOnlyList<int> ParticleIndices { get; }
        
        The set of particle indices involved in this interaction.
        
        

        :returns System.Collections.Generic.IReadOnlyList{System.Int32}: 
        
    .. cs:property:: public Vector3 GrabPosition { get; }
        
        The position of the manipulator.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:property:: public Dictionary<string, object> Properties { get; }
        
        A set of properties associated with this manipulation.
        
        

        :returns System.Collections.Generic.Dictionary{System.String,System.Object}: 
        
    .. cs:event:: public event Action ParticleGrabUpdated
        
        Callback for when the grab position is updated.
        
        

        :returns System.Action: 
        
    .. cs:constructor:: public ActiveParticleGrab(IEnumerable<int> particleIndices)
        
        :param System.Collections.Generic.IEnumerable{System.Int32} particleIndices: 
        
    .. cs:method:: public void UpdateManipulatorPose(UnitScaleTransformation manipulatorPose)
        
        

        :implements: :cs:any:`~Narupa.Frontend.Manipulation.IActiveManipulation.UpdateManipulatorPose(Narupa.Core.Math.UnitScaleTransformation)`
        :param Narupa.Core.Math.UnitScaleTransformation manipulatorPose: 
        
    .. cs:method:: public void EndManipulation()
        
        

        :implements: :cs:any:`~Narupa.Frontend.Manipulation.IActiveManipulation.EndManipulation`
        
    .. cs:property:: public bool ResetVelocities { set; }
        
        :returns System.Boolean: 
        
