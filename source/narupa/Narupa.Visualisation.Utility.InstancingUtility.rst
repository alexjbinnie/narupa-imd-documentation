InstancingUtility
=================
.. cs:setscope:: Narupa.Visualisation.Utility

.. cs:class:: public static class InstancingUtility
    
    Utility methods for setting up Narupa-specific instancing parameters
    for shaders.
    
    

    
    .. cs:method:: public static void SetPositions(IndirectMeshDrawCommand command, Vector3[] positions)
        
        Enable the position array in the shader and set the position values.
        
        

        :param Narupa.Visualisation.IndirectMeshDrawCommand command: 
        :param UnityEngine.Vector3[] positions: 
        
    .. cs:method:: public static void SetColors(IndirectMeshDrawCommand command, Color[] colors)
        
        Enable the color array in the shader and set the color values.
        
        

        :param Narupa.Visualisation.IndirectMeshDrawCommand command: 
        :param UnityEngine.Color[] colors: 
        
    .. cs:method:: public static void SetScales(IndirectMeshDrawCommand command, float[] scales)
        
        Enable the scales array in the shader and set the scale values.
        
        

        :param Narupa.Visualisation.IndirectMeshDrawCommand command: 
        :param System.Single[] scales: 
        
    .. cs:method:: public static void SetEdges(IndirectMeshDrawCommand command, BondPair[] edges)
        
        Enable the edge array in the shader and set the edge values.
        
        

        :param Narupa.Visualisation.IndirectMeshDrawCommand command: 
        :param Narupa.Frame.BondPair[] edges: 
        
    .. cs:method:: public static void SetEdgeCounts(IndirectMeshDrawCommand command, int[] edgeCounts)
        
        Enable the edge count array in the shader and set the edge count values.
        
        

        :param Narupa.Visualisation.IndirectMeshDrawCommand command: 
        :param System.Int32[] edgeCounts: 
        
    .. cs:method:: public static void SetTransform(IndirectMeshDrawCommand command, Transform transform)
        
        Copy the transform's world to object and object to world transform
        matrixes into the shader.
        
        

        :param Narupa.Visualisation.IndirectMeshDrawCommand command: 
        :param UnityEngine.Transform transform: 
        
    .. cs:method:: public static ComputeBuffer CreateBuffer<T>(T[] array)
        
        :param {T}[] array: 
        :returns UnityEngine.ComputeBuffer: 
        
