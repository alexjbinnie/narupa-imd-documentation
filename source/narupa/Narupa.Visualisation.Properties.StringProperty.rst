StringProperty
==============
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class StringProperty : SerializableProperty<string>, IProperty<string>, IReadOnlyProperty<string>, IProperty, IReadOnlyProperty
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for a :cs:any:`~System.String` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.SerializableProperty{System.String}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{System.String}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.String}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
