Multiplayer.MultiplayerClient
=============================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public class MultiplayerClient : ClientBase<Multiplayer.MultiplayerClient>
    
    
    .. cs:constructor:: public MultiplayerClient(Channel channel)
        
        :param Grpc.Core.Channel channel: 
        
    .. cs:constructor:: public MultiplayerClient(CallInvoker callInvoker)
        
        :param Grpc.Core.CallInvoker callInvoker: 
        
    .. cs:constructor:: protected MultiplayerClient()
        
        
    .. cs:constructor:: protected MultiplayerClient(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<Avatar> SubscribePlayerAvatars(SubscribePlayerAvatarsRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.SubscribePlayerAvatarsRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Multiplayer.Avatar}: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<Avatar> SubscribePlayerAvatars(SubscribePlayerAvatarsRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.SubscribePlayerAvatarsRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Multiplayer.Avatar}: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<ResourceValuesUpdate> SubscribeAllResourceValues(SubscribeAllResourceValuesRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.SubscribeAllResourceValuesRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Multiplayer.ResourceValuesUpdate}: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<ResourceValuesUpdate> SubscribeAllResourceValues(SubscribeAllResourceValuesRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.SubscribeAllResourceValuesRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Multiplayer.ResourceValuesUpdate}: 
        
    .. cs:method:: public virtual CreatePlayerResponse CreatePlayer(CreatePlayerRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.CreatePlayerRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Multiplayer.CreatePlayerResponse: 
        
    .. cs:method:: public virtual CreatePlayerResponse CreatePlayer(CreatePlayerRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.CreatePlayerRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Multiplayer.CreatePlayerResponse: 
        
    .. cs:method:: public virtual AsyncUnaryCall<CreatePlayerResponse> CreatePlayerAsync(CreatePlayerRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.CreatePlayerRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Multiplayer.CreatePlayerResponse}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<CreatePlayerResponse> CreatePlayerAsync(CreatePlayerRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.CreatePlayerRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Multiplayer.CreatePlayerResponse}: 
        
    .. cs:method:: public virtual AsyncClientStreamingCall<Avatar, StreamEndedResponse> UpdatePlayerAvatar(Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncClientStreamingCall{Narupa.Protocol.Multiplayer.Avatar,Narupa.Protocol.Multiplayer.StreamEndedResponse}: 
        
    .. cs:method:: public virtual AsyncClientStreamingCall<Avatar, StreamEndedResponse> UpdatePlayerAvatar(CallOptions options)
        
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncClientStreamingCall{Narupa.Protocol.Multiplayer.Avatar,Narupa.Protocol.Multiplayer.StreamEndedResponse}: 
        
    .. cs:method:: public virtual ResourceRequestResponse AcquireResourceLock(AcquireLockRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.AcquireLockRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Multiplayer.ResourceRequestResponse: 
        
    .. cs:method:: public virtual ResourceRequestResponse AcquireResourceLock(AcquireLockRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.AcquireLockRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Multiplayer.ResourceRequestResponse: 
        
    .. cs:method:: public virtual AsyncUnaryCall<ResourceRequestResponse> AcquireResourceLockAsync(AcquireLockRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.AcquireLockRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<ResourceRequestResponse> AcquireResourceLockAsync(AcquireLockRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.AcquireLockRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:method:: public virtual ResourceRequestResponse ReleaseResourceLock(ReleaseLockRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.ReleaseLockRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Multiplayer.ResourceRequestResponse: 
        
    .. cs:method:: public virtual ResourceRequestResponse ReleaseResourceLock(ReleaseLockRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.ReleaseLockRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Multiplayer.ResourceRequestResponse: 
        
    .. cs:method:: public virtual AsyncUnaryCall<ResourceRequestResponse> ReleaseResourceLockAsync(ReleaseLockRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.ReleaseLockRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<ResourceRequestResponse> ReleaseResourceLockAsync(ReleaseLockRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.ReleaseLockRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:method:: public virtual ResourceRequestResponse SetResourceValue(SetResourceValueRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.SetResourceValueRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Multiplayer.ResourceRequestResponse: 
        
    .. cs:method:: public virtual ResourceRequestResponse SetResourceValue(SetResourceValueRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.SetResourceValueRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Multiplayer.ResourceRequestResponse: 
        
    .. cs:method:: public virtual AsyncUnaryCall<ResourceRequestResponse> SetResourceValueAsync(SetResourceValueRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.SetResourceValueRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<ResourceRequestResponse> SetResourceValueAsync(SetResourceValueRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.SetResourceValueRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:method:: public virtual ResourceRequestResponse RemoveResourceKey(RemoveResourceKeyRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Multiplayer.ResourceRequestResponse: 
        
    .. cs:method:: public virtual ResourceRequestResponse RemoveResourceKey(RemoveResourceKeyRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Multiplayer.ResourceRequestResponse: 
        
    .. cs:method:: public virtual AsyncUnaryCall<ResourceRequestResponse> RemoveResourceKeyAsync(RemoveResourceKeyRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<ResourceRequestResponse> RemoveResourceKeyAsync(RemoveResourceKeyRequest request, CallOptions options)
        
        :param Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:method:: protected override Multiplayer.MultiplayerClient NewInstance(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        :returns Narupa.Protocol.Multiplayer.Multiplayer.MultiplayerClient: 
        
