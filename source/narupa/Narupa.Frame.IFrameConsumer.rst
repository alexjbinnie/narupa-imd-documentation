IFrameConsumer
==============
.. cs:setscope:: Narupa.Frame

.. cs:interface:: public interface IFrameConsumer
    
    Represents something that can consume a source of :cs:any:`~Narupa.Frame.IFrame`.
    
    

    
    .. cs:property:: ITrajectorySnapshot FrameSource { set; }
        
        The source of :cs:any:`~Narupa.Frame.IFrame` to use.
        
        

        :returns Narupa.Frame.ITrajectorySnapshot: 
        
