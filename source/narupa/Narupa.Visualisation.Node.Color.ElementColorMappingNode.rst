ElementColorMappingNode
=======================
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [Serializable] public class ElementColorMappingNode : PerElementColorNode
    
    Visualiser node that generates atomic colors from atomic positions, based
    upon an :cs:any:`~Narupa.Visualisation.Node.Color.ElementColorMapping`
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Color.PerElementColorNode`
    
    .. cs:property:: public IProperty<IMapping<Element, Color>> Mapping { get; }
        
        The color mapping between elements and colors.
        
        

        :returns Narupa.Visualisation.Property.IProperty{Narupa.Core.IMapping{Narupa.Core.Science.Element,UnityEngine.Color}}: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:method:: protected override Color GetColor(Element element)
        
        

        :param Narupa.Core.Science.Element element: 
        :returns UnityEngine.Color: 
        
