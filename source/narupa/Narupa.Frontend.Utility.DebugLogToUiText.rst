DebugLogToUiText
================
.. cs:setscope:: Narupa.Frontend.Utility

.. cs:class:: public class DebugLogToUiText : MonoBehaviour
    
    Component that listens to Unity's log output and outputs it to a
    linked TextMeshPro :cs:any:`~TMPro.TextMeshProUGUI` component.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
