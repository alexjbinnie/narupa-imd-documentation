FrameReflection
===============
.. cs:setscope:: Narupa.Protocol.Trajectory

.. cs:class:: public static class FrameReflection : Object
    
    
    .. cs:property:: public static FileDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.FileDescriptor: 
        
