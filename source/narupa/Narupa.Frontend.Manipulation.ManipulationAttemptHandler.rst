ManipulationAttemptHandler
==========================
.. cs:setscope:: Narupa.Frontend.Manipulation

.. cs:delegate:: public delegate IActiveManipulation ManipulationAttemptHandler(UnitScaleTransformation manipulatorPose);
    
    A function that attempts to start a manipulation given an manipulator pose.
    
    

    :param Narupa.Core.Math.UnitScaleTransformation manipulatorPose: 
    :returns Narupa.Frontend.Manipulation.IActiveManipulation: 
    
