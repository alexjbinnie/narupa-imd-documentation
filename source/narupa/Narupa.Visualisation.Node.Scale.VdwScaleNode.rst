VdwScaleNode
============
.. cs:setscope:: Narupa.Visualisation.Node.Scale

.. cs:class:: [Serializable] public class VdwScaleNode : PerElementScaleNode
    
    Visualiser node which scales each particle by its Van der Waals radius
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Scale.PerElementScaleNode`
    
    .. cs:property:: public IProperty<float> Scale { get; }
        
        Multiplier for each radius.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Single}: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override float GetScale(Element element)
        
        Get the scale of the provided atomic element.
        
        

        :param Narupa.Core.Science.Element element: 
        :returns System.Single: 
        
