InteriorCyclesBonds
===================
.. cs:setscope:: Narupa.Visualisation.Components.Calculator

.. cs:class:: public class InteriorCyclesBonds : VisualisationComponent<InteriorCyclesBondsNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<InteriorCyclesBondsNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Calculator.InteriorCyclesBondsNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Calculator.InteriorCyclesBondsNode}`
    
