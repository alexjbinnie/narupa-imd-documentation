IndexFilteredProperty
=====================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:class:: public class IndexFilteredProperty : IReadOnlyProperty<int[]>, IReadOnlyProperty, IFilteredProperty
    
    Take an array that represents indices in some data set, and set of indices in
    the same data set as a filter, and return the set of indices in the filtered
    data.
    
    

    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IFilteredProperty`
    
    .. cs:property:: IReadOnlyProperty IFilteredProperty.SourceProperty { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IFilteredProperty.SourceProperty`
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:property:: IReadOnlyProperty<int[]> IFilteredProperty.FilterProperty { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IFilteredProperty.FilterProperty`
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}: 
        
    .. cs:constructor:: public IndexFilteredProperty(IReadOnlyProperty<int[]> property, IReadOnlyProperty<int[]> filter)
        
        :param Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]} property: 
        :param Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]} filter: 
        
    .. cs:method:: public void Update()
        
        
    .. cs:property:: public bool HasValue { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.HasValue`
        :returns System.Boolean: 
        
    .. cs:event:: public event Action ValueChanged
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.ValueChanged`
        :returns System.Action: 
        
    .. cs:property:: public Type PropertyType { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.PropertyType`
        :returns System.Type: 
        
    .. cs:property:: object IReadOnlyProperty.Value { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.Value`
        :returns System.Object: 
        
    .. cs:property:: public int[] Value { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}.Value`
        :returns System.Int32[]: 
        
