ComputeBufferCollection
=======================
.. cs:setscope:: Narupa.Visualisation.Utility

.. cs:class:: public class ComputeBufferCollection : IDisposable, IReadOnlyDictionary<string, ComputeBuffer>, IReadOnlyCollection<KeyValuePair<string, ComputeBuffer>>, IEnumerable<KeyValuePair<string, ComputeBuffer>>, IEnumerable
    
    A collection of :cs:any:`~UnityEngine.ComputeBuffer`'s, which can be assigned to by
    directly passing arrays.
    
    

    :implements: :cs:any:`~System.IDisposable`
    :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{System.String,UnityEngine.ComputeBuffer}`
    :implements: :cs:any:`~System.Collections.Generic.IReadOnlyCollection{System.Collections.Generic.KeyValuePair{System.String,UnityEngine.ComputeBuffer}}`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Collections.Generic.KeyValuePair{System.String,UnityEngine.ComputeBuffer}}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
    .. cs:constructor:: public ComputeBufferCollection()
        
        
    .. cs:method:: public void Dispose()
        
        Dispose of all :cs:any:`~UnityEngine.ComputeBuffer`'s.
        
        

        :implements: :cs:any:`~System.IDisposable.Dispose`
        
    .. cs:method:: public IEnumerator<KeyValuePair<string, ComputeBuffer>> GetEnumerator()
        
        

        :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Collections.Generic.KeyValuePair{System.String,UnityEngine.ComputeBuffer}}.GetEnumerator`
        :returns System.Collections.Generic.IEnumerator{System.Collections.Generic.KeyValuePair{System.String,UnityEngine.ComputeBuffer}}: 
        
    .. cs:method:: IEnumerator IEnumerable.GetEnumerator()
        
        

        :implements: :cs:any:`~System.Collections.IEnumerable.GetEnumerator`
        :returns System.Collections.IEnumerator: 
        
    .. cs:property:: public int Count { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyCollection{System.Collections.Generic.KeyValuePair{System.String,UnityEngine.ComputeBuffer}}.Count`
        :returns System.Int32: 
        
    .. cs:method:: public bool ContainsKey(string key)
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{System.String,UnityEngine.ComputeBuffer}.ContainsKey(System.String)`
        :param System.String key: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool TryGetValue(string bufferName, out ComputeBuffer value)
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{System.String,UnityEngine.ComputeBuffer}.TryGetValue(System.String,UnityEngine.ComputeBuffer@)`
        :param System.String bufferName: 
        :param UnityEngine.ComputeBuffer value: 
        :returns System.Boolean: 
        
    .. cs:indexer:: public ComputeBuffer this[string bufferName] { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{System.String,UnityEngine.ComputeBuffer}.Item(System.String)`
        :param System.String bufferName: 
        :returns UnityEngine.ComputeBuffer: 
        
    .. cs:property:: public IEnumerable<string> Keys { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{System.String,UnityEngine.ComputeBuffer}.Keys`
        :returns System.Collections.Generic.IEnumerable{System.String}: 
        
    .. cs:property:: public IEnumerable<ComputeBuffer> Values { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{System.String,UnityEngine.ComputeBuffer}.Values`
        :returns System.Collections.Generic.IEnumerable{UnityEngine.ComputeBuffer}: 
        
    .. cs:method:: public void SetBuffer<T>(string bufferName, T[] content)      where T : struct
        
        Passes an array of data as :code:`content` to a renderer, where it
        is uploaded as a
        :cs:any:`~UnityEngine.ComputeBuffer` with the provided :code:`bufferName`
        as an identifier.
        
        

        :param System.String bufferName: 
        :param {T}[] content: 
        
    .. cs:method:: public IEnumerable<(string Id, ComputeBuffer Buffer)> GetDirtyBuffers()
        
        Enumerate over all buffers which have been updated since the last call to clear
        dirty buffers.
        
        

        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,UnityEngine.ComputeBuffer}}: 
        
    .. cs:method:: public void ClearDirtyBuffers()
        
        Clear all dirty flags.
        
        

        
    .. cs:method:: public void ApplyAllBuffersToShader(IGpuProgram shader)
        
        Apply all buffers (dirty or otherwise) to :code:`shader`.
        
        

        :param Narupa.Visualisation.Utility.IGpuProgram shader: 
        
    .. cs:method:: public void ApplyDirtyBuffersToShader(IGpuProgram shader)
        
        Apply all dirty buffers to :code:`shader`.
        
        

        :param Narupa.Visualisation.Utility.IGpuProgram shader: 
        
    .. cs:method:: public void Clear()
        
        
    .. cs:method:: public void RemoveBuffer(string key)
        
        :param System.String key: 
        
