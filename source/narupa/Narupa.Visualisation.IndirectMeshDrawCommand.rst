IndirectMeshDrawCommand
=======================
.. cs:setscope:: Narupa.Visualisation

.. cs:class:: public class IndirectMeshDrawCommand : IDisposable
    
    Wraps
    :cs:any:`~UnityEngine.Graphics.DrawMeshInstancedIndirect(UnityEngine.Mesh,System.Int32,UnityEngine.Material,UnityEngine.Bounds,UnityEngine.ComputeBuffer)`
    to add state management
    for mesh, material, and computer buffers.
    
    

    To actually execute the draw command, two approaches are possible. The first is
    to call :cs:any:`~Narupa.Visualisation.IndirectMeshDrawCommand.MarkForRenderingThisFrame(UnityEngine.Camera)`, which will draw just for this
    frame. The other is to append it to a :cs:any:`~UnityEngine.Rendering.CommandBuffer` using
    :cs:any:`~Narupa.Visualisation.IndirectMeshDrawCommand.AppendToCommandBuffer(UnityEngine.Rendering.CommandBuffer)`.
    
    

    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:method:: public void Dispose()
        
        Dispose the compute buffers used for draw arguments and shader data.
        
        

        :implements: :cs:any:`~System.IDisposable.Dispose`
        
    .. cs:method:: public void SetInstanceCount(int instanceCount)
        
        Set the number of mesh copies to be drawn.
        
        

        :param System.Int32 instanceCount: 
        
    .. cs:method:: public void SetMesh(Mesh mesh, int submeshIndex = 0)
        
        Set the mesh/submesh to be drawn
        
        

        :param UnityEngine.Mesh mesh: 
        :param System.Int32 submeshIndex: 
        
    .. cs:method:: public void SetMaterial(Material material)
        
        Set the source material to use for drawing. A copy will be made.
        
        

        :param UnityEngine.Material material: 
        
    .. cs:method:: public void SetMaterialDirect(Material material)
        
        Set the actual material to use for drawing. The material will be
        directly manipulated.
        
        

        :param UnityEngine.Material material: 
        
    .. cs:method:: public void SetDataBuffer<T>(string id, T[] content)      where T : struct
        
        Set a named buffer of data for use by the material when drawing.
        
        

        :param System.String id: 
        :param {T}[] content: 
        
    .. cs:method:: public void SetKeyword(string keyword, bool active = true)
        
        Enable/disable a named keyword in the material.
        
        

        :param System.String keyword: 
        :param System.Boolean active: 
        
    .. cs:method:: public void SetFloat(string id, float value)
        
        

        :param System.String id: 
        :param System.Single value: 
        
    .. cs:method:: public void SetMatrix(string id, Matrix4x4 value)
        
        

        :param System.String id: 
        :param UnityEngine.Matrix4x4 value: 
        
    .. cs:method:: public void SetColor(string id, Color value)
        
        

        :param System.String id: 
        :param UnityEngine.Color value: 
        
    .. cs:method:: public void MarkForRenderingThisFrame(Camera camera = null)
        
        Indicate that the draw command should render this frame.
        
        

        :param UnityEngine.Camera camera: 
        
    .. cs:method:: public void AppendToCommandBuffer(CommandBuffer buffer)
        
        Add a command to the provided buffer that would execute this draw command.
        
        

        :param UnityEngine.Rendering.CommandBuffer buffer: 
        
    .. cs:method:: public void ResetCommand()
        
        Clear all buffers
        
        

        
    .. cs:method:: public void ClearDataBuffer(string key)
        
        :param System.String key: 
        
