MultiplayerSession
==================
.. cs:setscope:: Narupa.Session

.. cs:class:: public sealed class MultiplayerSession : IDisposable
    
    Manages the state of a single user engaging in multiplayer. Tracks
    local and remote avatars and maintains a copy of the latest values in
    the shared key/value store.
    
    

    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:field:: public const string HeadsetName = "headset"
        
        :returns System.String: 
        
    .. cs:field:: public const string LeftHandName = "hand.left"
        
        :returns System.String: 
        
    .. cs:field:: public const string RightHandName = "hand.right"
        
        :returns System.String: 
        
    .. cs:field:: public const string SimulationPoseKey = "scene"
        
        :returns System.String: 
        
    .. cs:constructor:: public MultiplayerSession()
        
        
    .. cs:field:: public readonly MultiplayerResource<Transformation> SimulationPose
        
        The transformation of the simulation box.
        
        

        :returns Narupa.Grpc.Multiplayer.MultiplayerResource{Narupa.Core.Math.Transformation}: 
        
    .. cs:property:: public Dictionary<string, MultiplayerAvatar> Avatars { get; }
        
        Dictionary of player ids to their last known avatar.
        
        

        :returns System.Collections.Generic.Dictionary{System.String,Narupa.Session.MultiplayerAvatar}: 
        
    .. cs:property:: public Dictionary<string, object> SharedStateDictionary { get; }
        
        Dictionary of the currently known shared state.
        
        

        :returns System.Collections.Generic.Dictionary{System.String,System.Object}: 
        
    .. cs:property:: public bool IsOpen { get; }
        
        Is there an open client on this session?
        
        

        :returns System.Boolean: 
        
    .. cs:property:: public bool HasPlayer { get; }
        
        Has this session created a user?
        
        

        :returns System.Boolean: 
        
    .. cs:property:: public string PlayerName { get; }
        
        Username of the current player, if any.
        
        

        :returns System.String: 
        
    .. cs:property:: public string PlayerId { get; }
        
        ID of the current player, if any.
        
        

        :returns System.String: 
        
    .. cs:property:: public int AvatarPublishInterval { get; set; }
        
        How many milliseconds to put between sending updates to our avatar.
        
        

        :returns System.Int32: 
        
    .. cs:property:: public int ValuePublishInterval { get; set; }
        
        How many milliseconds to put between sending our requested value
        changes.
        
        

        :returns System.Int32: 
        
    .. cs:field:: public const float AvatarUpdateInterval = 0.0333333351F
        
        The interval at which avatar updates should be sent to this client.
        
        

        :returns System.Single: 
        
    .. cs:event:: public event Action<string, object> SharedStateDictionaryKeyUpdated
        
        :returns System.Action{System.String,System.Object}: 
        
    .. cs:event:: public event Action<string> SharedStateDictionaryKeyRemoved
        
        :returns System.Action{System.String}: 
        
    .. cs:event:: public event Action MultiplayerJoined
        
        :returns System.Action: 
        
    .. cs:method:: public void OpenClient(GrpcConnection connection)
        
        Connect to a Multiplayer service over the given connection. 
        Closes any existing client.
        
        

        :param Narupa.Grpc.GrpcConnection connection: 
        
    .. cs:method:: public void CloseClient()
        
        Close the current Multiplayer client and dispose all streams.
        
        

        
    .. cs:method:: public Task JoinMultiplayer(string playerName)
        
        Create a new multiplayer with the given username, subscribe avatar 
        and value updates, and begin publishing our avatar.
        
        

        :param System.String playerName: 
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public void StartAvatars()
        
        
    .. cs:method:: public void SetVRAvatar(Transformation? headset = default(Transformation? ), Transformation? leftHand = default(Transformation? ), Transformation? rightHand = default(Transformation? ))
        
        Set the headset and hand poses of our avatar.
        
        

        :param System.Nullable{Narupa.Core.Math.Transformation} headset: 
        :param System.Nullable{Narupa.Core.Math.Transformation} leftHand: 
        :param System.Nullable{Narupa.Core.Math.Transformation} rightHand: 
        
    .. cs:method:: public void SetVRAvatar(MultiplayerAvatar avatar)
        
        Set the headset and hand poses of our avatar.
        
        

        :param Narupa.Session.MultiplayerAvatar avatar: 
        
    .. cs:method:: public void SetVRAvatarEmpty()
        
        Set our avatar to empty.
        
        

        
    .. cs:method:: public void SetSharedState(string key, object value)
        
        Set the given key in the shared state dictionary, which will be
        sent to the server according in the future according to the publish 
        interval.
        
        

        :param System.String key: 
        :param System.Object value: 
        
    .. cs:method:: public object GetSharedState(string key)
        
        Get a key in the shared state dictionary.
        
        

        :param System.String key: 
        :returns System.Object: 
        
    .. cs:method:: public Task<bool> LockResource(string id)
        
        Attempt to gain exclusive write access to the shared value of the given key.
        
        

        :param System.String id: 
        :returns System.Threading.Tasks.Task{System.Boolean}: 
        
    .. cs:method:: public Task<bool> ReleaseResource(string id)
        
        Release the lock on the given object of a given key.
        
        

        :param System.String id: 
        :returns System.Threading.Tasks.Task{System.Boolean}: 
        
    .. cs:method:: public void Dispose()
        
        

        :implements: :cs:any:`~System.IDisposable.Dispose`
        
    .. cs:method:: public MultiplayerResource<object> GetSharedResource(string key)
        
        :param System.String key: 
        :returns Narupa.Grpc.Multiplayer.MultiplayerResource{System.Object}: 
        
