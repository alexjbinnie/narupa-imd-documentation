FrameExtensions
===============
.. cs:setscope:: Narupa.Frame

.. cs:class:: public static class FrameExtensions
    
    
    .. cs:method:: public static void RecenterAroundOrigin(this Frame frame, bool massWeighted = false)
        
        Recenter particles about the origin.
        
        

        :param Narupa.Frame.Frame frame: 
        :param System.Boolean massWeighted: 
        
