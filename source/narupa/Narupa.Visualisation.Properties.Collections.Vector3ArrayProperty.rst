Vector3ArrayProperty
====================
.. cs:setscope:: Narupa.Visualisation.Properties.Collections

.. cs:class:: [Serializable] public class Vector3ArrayProperty : ArrayProperty<Vector3>, IProperty<Vector3[]>, IReadOnlyProperty<Vector3[]>, IProperty, IReadOnlyProperty, IEnumerable<Vector3>, IEnumerable
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an array of :cs:any:`~UnityEngine.Vector3`
    values.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Properties.Collections.ArrayProperty{UnityEngine.Vector3}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{UnityEngine.Vector3[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{UnityEngine.Vector3[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{UnityEngine.Vector3}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
