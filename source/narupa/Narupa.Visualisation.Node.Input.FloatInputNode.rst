FloatInputNode
==============
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class FloatInputNode : InputNode<FloatProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~System.Single` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.FloatProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
