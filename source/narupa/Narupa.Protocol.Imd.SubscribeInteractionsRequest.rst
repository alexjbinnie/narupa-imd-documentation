SubscribeInteractionsRequest
============================
.. cs:setscope:: Narupa.Protocol.Imd

.. cs:class:: public sealed class SubscribeInteractionsRequest : Object, IMessage<SubscribeInteractionsRequest>, IMessage, IEquatable<SubscribeInteractionsRequest>, IDeepCloneable<SubscribeInteractionsRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Imd.SubscribeInteractionsRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Imd.SubscribeInteractionsRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Imd.SubscribeInteractionsRequest}`
    
    .. cs:field:: public const int UpdateIntervalFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int IgnorePlayerIdFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public SubscribeInteractionsRequest()
        
        
    .. cs:constructor:: public SubscribeInteractionsRequest(SubscribeInteractionsRequest other)
        
        :param Narupa.Protocol.Imd.SubscribeInteractionsRequest other: 
        
    .. cs:method:: public SubscribeInteractionsRequest Clone()
        
        :returns Narupa.Protocol.Imd.SubscribeInteractionsRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(SubscribeInteractionsRequest other)
        
        :param Narupa.Protocol.Imd.SubscribeInteractionsRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(SubscribeInteractionsRequest other)
        
        :param Narupa.Protocol.Imd.SubscribeInteractionsRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<SubscribeInteractionsRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Imd.SubscribeInteractionsRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public float UpdateInterval { get; set; }
        
        :returns System.Single: 
        
    .. cs:property:: public string IgnorePlayerId { get; set; }
        
        :returns System.String: 
        
