StandardFrameProperties
=======================
.. cs:setscope:: Narupa.Frame

.. cs:class:: public static class StandardFrameProperties
    
    Standard names and types for :cs:any:`~Narupa.Frame.Frame`s.
    
    

    
    .. cs:field:: public static readonly (string Key, Type Type) Bonds
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) BondOrders
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) EntityCount
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) EntityName
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) KineticEnergy
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) ParticleCount
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) ParticleElements
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) ParticleNames
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) ParticlePositions
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) ParticleResidues
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) ParticleTypes
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) PotentialEnergy
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) ResidueEntities
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) ResidueCount
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) ResidueNames
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type) BoxTransformation
        
        :returns System.ValueTuple{System.String,System.Type}: 
        
    .. cs:field:: public static readonly (string Key, Type Type)[] All
        
        :returns System.ValueTuple{System.String,System.Type}[]: 
        
