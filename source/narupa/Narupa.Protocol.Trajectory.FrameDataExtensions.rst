FrameDataExtensions
===================
.. cs:setscope:: Narupa.Protocol.Trajectory

.. cs:class:: public static class FrameDataExtensions : Object
    
    
    .. cs:method:: public static bool HasBonds(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetBondPairs(this FrameData data, IEnumerable<uint> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IEnumerable{System.UInt32} values: 
        
    .. cs:method:: public static bool TryGetBondPairs(this FrameData data, out IReadOnlyList<uint> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IReadOnlyList{System.UInt32} values: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyList<uint> GetBondPairs(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Collections.Generic.IReadOnlyList{System.UInt32}: 
        
    .. cs:method:: public static bool HasBondOrders(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetBondOrders(this FrameData data, IEnumerable<float> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IEnumerable{System.Single} values: 
        
    .. cs:method:: public static bool TryGetBondOrders(this FrameData data, out IReadOnlyList<float> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IReadOnlyList{System.Single} values: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyList<float> GetBondOrders(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Collections.Generic.IReadOnlyList{System.Single}: 
        
    .. cs:method:: public static bool HasParticlePositions(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetParticlePositions(this FrameData data, IEnumerable<float> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IEnumerable{System.Single} values: 
        
    .. cs:method:: public static bool TryGetParticlePositions(this FrameData data, out IReadOnlyList<float> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IReadOnlyList{System.Single} values: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyList<float> GetParticlePositions(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Collections.Generic.IReadOnlyList{System.Single}: 
        
    .. cs:method:: public static bool HasParticleElements(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetParticleElements(this FrameData data, IEnumerable<uint> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IEnumerable{System.UInt32} values: 
        
    .. cs:method:: public static bool TryGetParticleElements(this FrameData data, out IReadOnlyList<uint> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IReadOnlyList{System.UInt32} values: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyList<uint> GetParticleElements(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Collections.Generic.IReadOnlyList{System.UInt32}: 
        
    .. cs:method:: public static bool HasParticleTypes(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetParticleTypes(this FrameData data, IEnumerable<string> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IEnumerable{System.String} values: 
        
    .. cs:method:: public static bool TryGetParticleTypes(this FrameData data, out IReadOnlyList<string> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IReadOnlyList{System.String} values: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyList<string> GetParticleTypes(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Collections.Generic.IReadOnlyList{System.String}: 
        
    .. cs:method:: public static bool HasParticleNames(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetParticleNames(this FrameData data, IEnumerable<string> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IEnumerable{System.String} values: 
        
    .. cs:method:: public static bool TryGetParticleNames(this FrameData data, out IReadOnlyList<string> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IReadOnlyList{System.String} values: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyList<string> GetParticleNames(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Collections.Generic.IReadOnlyList{System.String}: 
        
    .. cs:method:: public static bool HasParticleResidues(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetParticleResidues(this FrameData data, IEnumerable<uint> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IEnumerable{System.UInt32} values: 
        
    .. cs:method:: public static bool TryGetParticleResidues(this FrameData data, out IReadOnlyList<uint> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IReadOnlyList{System.UInt32} values: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyList<uint> GetParticleResidues(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Collections.Generic.IReadOnlyList{System.UInt32}: 
        
    .. cs:method:: public static bool HasResidueNames(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetResidueNames(this FrameData data, IEnumerable<string> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IEnumerable{System.String} values: 
        
    .. cs:method:: public static bool TryGetResidueNames(this FrameData data, out IReadOnlyList<string> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IReadOnlyList{System.String} values: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyList<string> GetResidueNames(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Collections.Generic.IReadOnlyList{System.String}: 
        
    .. cs:method:: public static bool HasResidueIds(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetResidueIds(this FrameData data, IEnumerable<string> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IEnumerable{System.String} values: 
        
    .. cs:method:: public static bool TryGetResidueIds(this FrameData data, out IReadOnlyList<string> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IReadOnlyList{System.String} values: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyList<string> GetResidueIds(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Collections.Generic.IReadOnlyList{System.String}: 
        
    .. cs:method:: public static bool HasResidueChains(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetResidueChains(this FrameData data, IEnumerable<uint> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IEnumerable{System.UInt32} values: 
        
    .. cs:method:: public static bool TryGetResidueChains(this FrameData data, out IReadOnlyList<uint> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IReadOnlyList{System.UInt32} values: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyList<uint> GetResidueChains(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Collections.Generic.IReadOnlyList{System.UInt32}: 
        
    .. cs:method:: public static bool HasResidueCount(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetResidueCount(this FrameData data, int value)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Int32 value: 
        
    .. cs:method:: public static bool TryGetResidueCount(this FrameData data, out int value)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Int32 value: 
        :returns System.Boolean: 
        
    .. cs:method:: public static Nullable<int> GetResidueCount(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Nullable{System.Int32}: 
        
    .. cs:method:: public static bool HasChainNames(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetChainNames(this FrameData data, IEnumerable<string> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IEnumerable{System.String} values: 
        
    .. cs:method:: public static bool TryGetChainNames(this FrameData data, out IReadOnlyList<string> values)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Collections.Generic.IReadOnlyList{System.String} values: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyList<string> GetChainNames(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Collections.Generic.IReadOnlyList{System.String}: 
        
    .. cs:method:: public static bool HasChainCount(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetChainCount(this FrameData data, int value)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Int32 value: 
        
    .. cs:method:: public static bool TryGetChainCount(this FrameData data, out int value)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Int32 value: 
        :returns System.Boolean: 
        
    .. cs:method:: public static Nullable<int> GetChainCount(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Nullable{System.Int32}: 
        
    .. cs:method:: public static bool HasParticleCount(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetParticleCount(this FrameData data, int value)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Int32 value: 
        
    .. cs:method:: public static bool TryGetParticleCount(this FrameData data, out int value)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Int32 value: 
        :returns System.Boolean: 
        
    .. cs:method:: public static Nullable<int> GetParticleCount(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Nullable{System.Int32}: 
        
    .. cs:method:: public static bool HasKineticEnergy(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetKineticEnergy(this FrameData data, double value)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Double value: 
        
    .. cs:method:: public static bool TryGetKineticEnergy(this FrameData data, out int value)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Int32 value: 
        :returns System.Boolean: 
        
    .. cs:method:: public static Nullable<float> GetKineticEnergy(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Nullable{System.Single}: 
        
    .. cs:method:: public static bool HasPotentialEnergy(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetPotentialEnergy(this FrameData data, double value)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Double value: 
        
    .. cs:method:: public static bool TryGetPotentialEnergy(this FrameData data, out int value)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.Int32 value: 
        :returns System.Boolean: 
        
    .. cs:method:: public static Nullable<float> GetPotentialEnergy(this FrameData data)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :returns System.Nullable{System.Single}: 
        
    .. cs:method:: public static IReadOnlyList<string> GetStringArray(this FrameData data, string id)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.String id: 
        :returns System.Collections.Generic.IReadOnlyList{System.String}: 
        
    .. cs:method:: public static IReadOnlyList<uint> GetIndexArray(this FrameData data, string id)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.String id: 
        :returns System.Collections.Generic.IReadOnlyList{System.UInt32}: 
        
    .. cs:method:: public static IReadOnlyList<float> GetFloatArray(this FrameData data, string id)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.String id: 
        :returns System.Collections.Generic.IReadOnlyList{System.Single}: 
        
    .. cs:method:: public static Nullable<double> GetNumericValue(this FrameData data, string id)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.String id: 
        :returns System.Nullable{System.Double}: 
        
    .. cs:method:: public static bool TryGetIntegerValue(this FrameData data, string id, out int output)
        
        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param System.String id: 
        :param System.Int32 output: 
        :returns System.Boolean: 
        
