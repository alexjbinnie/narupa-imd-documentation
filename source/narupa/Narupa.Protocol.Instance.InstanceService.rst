InstanceService
===============
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public static class InstanceService : Object
    
    
    .. cs:method:: public static ServerServiceDefinition BindService(InstanceService.InstanceServiceBase serviceImpl)
        
        :param Narupa.Protocol.Instance.InstanceService.InstanceServiceBase serviceImpl: 
        :returns Grpc.Core.ServerServiceDefinition: 
        
    .. cs:method:: public static void BindService(ServiceBinderBase serviceBinder, InstanceService.InstanceServiceBase serviceImpl)
        
        :param Grpc.Core.ServiceBinderBase serviceBinder: 
        :param Narupa.Protocol.Instance.InstanceService.InstanceServiceBase serviceImpl: 
        
    .. cs:property:: public static ServiceDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.ServiceDescriptor: 
        
