ParticleSphereRenderer
======================
.. cs:setscope:: Narupa.Visualisation.Components.Visualiser

.. cs:class:: public class ParticleSphereRenderer : VisualisationComponentRenderer<ParticleSphereRendererNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ParticleSphereRendererNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.Renderer.VisualisationComponentRenderer{Narupa.Visualisation.Node.Renderer.ParticleSphereRendererNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Renderer.ParticleSphereRendererNode}`
    
    .. cs:method:: protected override void Render(Camera camera)
        
        :param UnityEngine.Camera camera: 
        
    .. cs:method:: protected override void UpdateInEditor()
        
        
