SecondaryStructureArrayInputNode
================================
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class SecondaryStructureArrayInputNode : InputNode<SecondaryStructureArrayProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.Collections.SecondaryStructureArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
