DirectPosedObject
=================
.. cs:setscope:: Narupa.Frontend.Input

.. cs:class:: public sealed class DirectPosedObject : IPosedObject
    
    An IPosedObject whose pose can be set using
    :cs:any:`~Narupa.Frontend.Input.DirectPosedObject.SetPose(System.Nullable{Narupa.Core.Math.Transformation})`
    
    

    :implements: :cs:any:`~Narupa.Frontend.Input.IPosedObject`
    
    .. cs:property:: public Transformation? Pose { get; }
        
        

        :implements: :cs:any:`~Narupa.Frontend.Input.IPosedObject.Pose`
        :returns System.Nullable{Narupa.Core.Math.Transformation}: 
        
    .. cs:event:: public event Action PoseChanged
        
        

        :implements: :cs:any:`~Narupa.Frontend.Input.IPosedObject.PoseChanged`
        :returns System.Action: 
        
    .. cs:method:: public void SetPose(Transformation? pose)
        
        Set the pose of this object. This invokes PoseChanged.
        
        

        :param System.Nullable{Narupa.Core.Math.Transformation} pose: 
        
