CyclesCalculator
================
.. cs:setscope:: Narupa.Visualisation.Components.Calculator

.. cs:class:: public class CyclesCalculator : VisualisationComponent<CyclesCalculatorNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<CyclesCalculatorNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Calculator.CyclesCalculatorNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Calculator.CyclesCalculatorNode}`
    
