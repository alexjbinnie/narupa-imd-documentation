GoodsellColorNode
=================
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [Serializable] public class GoodsellColorNode : VisualiserColorNode
    
    Base code for a Visualiser node which generates colors based upon atomic
    elements.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Color.VisualiserColorNode`
    
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: protected override void UpdateOutput()
        
        
    .. cs:method:: protected override void ClearOutput()
        
        
    .. cs:method:: protected override void ClearDirty()
        
        
