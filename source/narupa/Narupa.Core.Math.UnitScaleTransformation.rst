UnitScaleTransformation
=======================
.. cs:setscope:: Narupa.Core.Math

.. cs:struct:: public struct UnitScaleTransformation : ITransformation
    
    A transformation consisting of a rotation followed by a translation.
    
    

    Also known as a Rigid Motion or a Proper Rigid Transformation. These
    transformations preserve orientation, distances and angles.
    
    

    :implements: :cs:any:`~Narupa.Core.Math.ITransformation`
    
    .. cs:property:: ITransformation ITransformation.inverse { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.inverse`
        :returns Narupa.Core.Math.ITransformation: 
        
    .. cs:field:: public Vector3 position
        
        The translation this transformation applies. When considered as a
        transformation from an object's local space to world space, describes the
        position of the object.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Quaternion rotation
        
        The rotation this transformation applies. When considered as a transformation
        from an object's local space to world space, describes the rotation of the
        object.
        
        

        :returns UnityEngine.Quaternion: 
        
    .. cs:constructor:: public UnitScaleTransformation(Vector3 position, Quaternion rotation)
        
        Create a transformation from its two actions.
        
        

        :param UnityEngine.Vector3 position: The translation this transformation applies.
        :param UnityEngine.Quaternion rotation: The rotation this transformation applies.
        
    .. cs:property:: public static UnitScaleTransformation identity { get; }
        
        The identity transformation.
        
        

        :returns Narupa.Core.Math.UnitScaleTransformation: 
        
    .. cs:property:: public UnitScaleTransformation inverse { get; }
        
        

        :returns Narupa.Core.Math.UnitScaleTransformation: 
        
    .. cs:property:: public Matrix4x4 matrix { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.matrix`
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:property:: public Matrix4x4 inverseMatrix { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.inverseMatrix`
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:conversion:: public static implicit operator Matrix4x4(UnitScaleTransformation transformation)
        
        :param Narupa.Core.Math.UnitScaleTransformation transformation: 
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:conversion:: public static implicit operator Transformation(UnitScaleTransformation transformation)
        
        :param Narupa.Core.Math.UnitScaleTransformation transformation: 
        :returns Narupa.Core.Math.Transformation: 
        
    .. cs:conversion:: public static implicit operator UniformScaleTransformation(UnitScaleTransformation transformation)
        
        :param Narupa.Core.Math.UnitScaleTransformation transformation: 
        :returns Narupa.Core.Math.UniformScaleTransformation: 
        
    .. cs:operator:: public static UnitScaleTransformation operator *(UnitScaleTransformation a, UnitScaleTransformation b)
        
        :param Narupa.Core.Math.UnitScaleTransformation a: 
        :param Narupa.Core.Math.UnitScaleTransformation b: 
        :returns Narupa.Core.Math.UnitScaleTransformation: 
        
    .. cs:operator:: public static Transformation operator *(UnitScaleTransformation a, Transformation b)
        
        :param Narupa.Core.Math.UnitScaleTransformation a: 
        :param Narupa.Core.Math.Transformation b: 
        :returns Narupa.Core.Math.Transformation: 
        
    .. cs:method:: public Vector3 TransformPoint(Vector3 pt)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.TransformPoint(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 pt: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 InverseTransformPoint(Vector3 pt)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.InverseTransformPoint(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 pt: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 TransformDirection(Vector3 direction)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.TransformDirection(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 direction: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 InverseTransformDirection(Vector3 pt)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.InverseTransformDirection(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 pt: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
