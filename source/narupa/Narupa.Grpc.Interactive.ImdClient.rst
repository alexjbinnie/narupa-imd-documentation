ImdClient
=========
.. cs:setscope:: Narupa.Grpc.Interactive

.. cs:class:: public sealed class ImdClient : GrpcClient<InteractiveMolecularDynamics.InteractiveMolecularDynamicsClient>, ICancellable, ICancellationTokenSource, IDisposable, IAsyncClosable
    
    Wraps a
    :cs:any:`~Narupa.Protocol.Imd.InteractiveMolecularDynamics.InteractiveMolecularDynamicsClient`
    and provides the ability to publish a list of interactions to a server over a
    :cs:any:`~Narupa.Grpc.GrpcConnection`.
    
    

    :inherits: :cs:any:`~Narupa.Grpc.GrpcClient{Narupa.Protocol.Imd.InteractiveMolecularDynamics.InteractiveMolecularDynamicsClient}`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellationTokenSource`
    :implements: :cs:any:`~System.IDisposable`
    :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable`
    
    .. cs:constructor:: public ImdClient([NotNull] GrpcConnection connection)
        
        :param Narupa.Grpc.GrpcConnection connection: 
        
    .. cs:method:: public OutgoingStream<ParticleInteraction, InteractionEndReply> PublishInteractions(CancellationToken externalToken = default(CancellationToken))
        
        Starts an :cs:any:`~Narupa.Grpc.Stream.OutgoingStream`2` which
        allows interactions to be sent to the server.
        
        

        Corresponds to the PublicInteractions gRPC call.
        
        

        :param System.Threading.CancellationToken externalToken: 
        :returns Narupa.Grpc.Stream.OutgoingStream{Narupa.Protocol.Imd.ParticleInteraction,Narupa.Protocol.Imd.InteractionEndReply}: 
        
    .. cs:method:: public IncomingStream<InteractionsUpdate> SubscribeAllInteractions(float updateInterval = 0F, CancellationToken externalToken = default(CancellationToken))
        
        Starts an :cs:any:`~Narupa.Grpc.Stream.IncomingStream`1` on 
        which the server provides updates to all published interactions
        at requested time interval (in seconds).
        
        

        Corresponds to the SubscribeAllResourceValues gRPC call.
        
        

        :param System.Single updateInterval: 
        :param System.Threading.CancellationToken externalToken: 
        :returns Narupa.Grpc.Stream.IncomingStream{Narupa.Protocol.Imd.InteractionsUpdate}: 
        
