ImportException
===============
.. cs:setscope:: Narupa.Frame.Import

.. cs:class:: public class ImportException : Exception, ISerializable
    
    Exception thrown when parsing fails for a given line.
    
    

    :inherits: :cs:any:`~System.Exception`
    :implements: :cs:any:`~System.Runtime.Serialization.ISerializable`
    
    .. cs:constructor:: public ImportException(string line, int lineNumber, Exception e = null)
        
        :param System.String line: 
        :param System.Int32 lineNumber: 
        :param System.Exception e: 
        
