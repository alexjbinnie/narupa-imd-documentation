AvatarComponent
===============
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class AvatarComponent : Object, IMessage<AvatarComponent>, IMessage, IEquatable<AvatarComponent>, IDeepCloneable<AvatarComponent>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.AvatarComponent}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.AvatarComponent}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.AvatarComponent}`
    
    .. cs:field:: public const int NameFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int PositionFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:field:: public const int RotationFieldNumber = 3
        
        :returns System.Int32: 
        
    .. cs:constructor:: public AvatarComponent()
        
        
    .. cs:constructor:: public AvatarComponent(AvatarComponent other)
        
        :param Narupa.Protocol.Multiplayer.AvatarComponent other: 
        
    .. cs:method:: public AvatarComponent Clone()
        
        :returns Narupa.Protocol.Multiplayer.AvatarComponent: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(AvatarComponent other)
        
        :param Narupa.Protocol.Multiplayer.AvatarComponent other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(AvatarComponent other)
        
        :param Narupa.Protocol.Multiplayer.AvatarComponent other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<AvatarComponent> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.AvatarComponent}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string Name { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public RepeatedField<float> Position { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{System.Single}: 
        
    .. cs:property:: public RepeatedField<float> Rotation { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{System.Single}: 
        
