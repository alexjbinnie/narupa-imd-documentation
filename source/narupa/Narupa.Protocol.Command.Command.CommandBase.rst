Command.CommandBase
===================
.. cs:setscope:: Narupa.Protocol.Command

.. cs:class:: public abstract class CommandBase : Object
    
    
    .. cs:method:: public virtual Task<GetCommandsReply> GetCommands(GetCommandsRequest request, ServerCallContext context)
        
        :param Narupa.Protocol.Command.GetCommandsRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Command.GetCommandsReply}: 
        
    .. cs:method:: public virtual Task<CommandReply> RunCommand(CommandMessage request, ServerCallContext context)
        
        :param Narupa.Protocol.Command.CommandMessage request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Command.CommandReply}: 
        
    .. cs:constructor:: protected CommandBase()
        
        
