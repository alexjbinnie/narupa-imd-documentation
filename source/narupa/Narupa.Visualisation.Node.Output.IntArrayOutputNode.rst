IntArrayOutputNode
==================
.. cs:setscope:: Narupa.Visualisation.Node.Output

.. cs:class:: [Serializable] public class IntArrayOutputNode : OutputNode<IntArrayProperty>, IOutputNode
    
    :inherits: :cs:any:`~Narupa.Visualisation.Node.Output.OutputNode{Narupa.Visualisation.Properties.IntArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Output.IOutputNode`
    
