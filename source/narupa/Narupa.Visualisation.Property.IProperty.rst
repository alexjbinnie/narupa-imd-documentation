IProperty
=========
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:interface:: public interface IProperty : IReadOnlyProperty
    
    An property (see :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`) which can have its
    value altered.
    
    

    
    .. cs:method:: void UndefineValue()
        
        Remove the value from this property.
        
        

        
    .. cs:method:: void TrySetValue(object value)
        
        Attempt to set the value without knowing the types involved.
        
        

        :param System.Object value: 
        
    .. cs:property:: bool HasLinkedProperty { get; }
        
        Is this property linked to another?
        
        

        :returns System.Boolean: 
        
    .. cs:method:: void TrySetLinkedProperty(object property)
        
        Attempt to set the linked property without knowing the types involved.
        
        

        :param System.Object property: 
        
    .. cs:property:: IReadOnlyProperty LinkedProperty { get; }
        
        Linked property that will override this value.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
