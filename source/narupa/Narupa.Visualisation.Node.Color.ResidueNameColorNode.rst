ResidueNameColorNode
====================
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [Serializable] public class ResidueNameColorNode : VisualiserColorNode
    
    Visualiser node which generates colors based upon residue names.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Color.VisualiserColorNode`
    
    .. cs:property:: protected override bool IsInputValid { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        
    .. cs:method:: protected override void UpdateOutput()
        
        
    .. cs:method:: protected override void ClearOutput()
        
        
