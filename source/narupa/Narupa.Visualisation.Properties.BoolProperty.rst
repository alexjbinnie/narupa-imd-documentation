BoolProperty
============
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class BoolProperty : SerializableProperty<bool>, IProperty<bool>, IReadOnlyProperty<bool>, IProperty, IReadOnlyProperty
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for a :cs:any:`~System.Single` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.SerializableProperty{System.Boolean}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{System.Boolean}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.Boolean}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
