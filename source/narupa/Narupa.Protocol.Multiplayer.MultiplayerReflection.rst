MultiplayerReflection
=====================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public static class MultiplayerReflection : Object
    
    
    .. cs:property:: public static FileDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.FileDescriptor: 
        
