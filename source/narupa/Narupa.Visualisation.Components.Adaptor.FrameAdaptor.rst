FrameAdaptor
============
.. cs:setscope:: Narupa.Visualisation.Components.Adaptor

.. cs:class:: public sealed class FrameAdaptor : FrameAdaptorComponent<FrameAdaptorNode>, ISerializationCallbackReceiver, IVisualisationComponent<FrameAdaptorNode>, IDynamicPropertyProvider, IPropertyProvider, IFrameConsumer
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.Adaptor.FrameAdaptorComponent{Narupa.Visualisation.Node.Adaptor.FrameAdaptorNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Adaptor.FrameAdaptorNode}`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Frame.IFrameConsumer`
    
    .. cs:property:: public ITrajectorySnapshot FrameSource { set; }
        
        

        :implements: :cs:any:`~Narupa.Frame.IFrameConsumer.FrameSource`
        :returns Narupa.Frame.ITrajectorySnapshot: 
        
    .. cs:method:: protected override void OnDisable()
        
        
