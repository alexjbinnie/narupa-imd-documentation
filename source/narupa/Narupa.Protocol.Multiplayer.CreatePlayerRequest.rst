CreatePlayerRequest
===================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class CreatePlayerRequest : Object, IMessage<CreatePlayerRequest>, IMessage, IEquatable<CreatePlayerRequest>, IDeepCloneable<CreatePlayerRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.CreatePlayerRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.CreatePlayerRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.CreatePlayerRequest}`
    
    .. cs:field:: public const int PlayerNameFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public CreatePlayerRequest()
        
        
    .. cs:constructor:: public CreatePlayerRequest(CreatePlayerRequest other)
        
        :param Narupa.Protocol.Multiplayer.CreatePlayerRequest other: 
        
    .. cs:method:: public CreatePlayerRequest Clone()
        
        :returns Narupa.Protocol.Multiplayer.CreatePlayerRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(CreatePlayerRequest other)
        
        :param Narupa.Protocol.Multiplayer.CreatePlayerRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(CreatePlayerRequest other)
        
        :param Narupa.Protocol.Multiplayer.CreatePlayerRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<CreatePlayerRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.CreatePlayerRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string PlayerName { get; set; }
        
        :returns System.String: 
        
