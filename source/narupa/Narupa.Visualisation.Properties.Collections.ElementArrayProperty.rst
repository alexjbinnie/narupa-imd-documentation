ElementArrayProperty
====================
.. cs:setscope:: Narupa.Visualisation.Properties.Collections

.. cs:class:: [Serializable] public class ElementArrayProperty : ArrayProperty<Element>, IProperty<Element[]>, IReadOnlyProperty<Element[]>, IProperty, IReadOnlyProperty, IEnumerable<Element>, IEnumerable
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an array of :cs:any:`~Narupa.Core.Science.Element`
    values.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Properties.Collections.ArrayProperty{Narupa.Core.Science.Element}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{Narupa.Core.Science.Element[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Core.Science.Element[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{Narupa.Core.Science.Element}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
