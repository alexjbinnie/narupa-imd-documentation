FloatingCanvas
==============
.. cs:setscope:: Narupa.Frontend.UI

.. cs:class:: public class FloatingCanvas : MonoBehaviour
    
    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
