InteractionInputMode
====================
.. cs:setscope:: Narupa.Frontend.Controllers

.. cs:class:: public class InteractionInputMode : ControllerInputMode
    
    :inherits: :cs:any:`~Narupa.Frontend.Controllers.ControllerInputMode`
    
    .. cs:property:: public override int Priority { get; }
        
        :returns System.Int32: 
        
    .. cs:method:: public override void OnModeStarted()
        
        
    .. cs:method:: public override void OnModeEnded()
        
        
    .. cs:method:: public override void SetupController(VrController controller, SteamVR_Input_Sources inputSource)
        
        :param Narupa.Frontend.Controllers.VrController controller: 
        :param Valve.VR.SteamVR_Input_Sources inputSource: 
        
