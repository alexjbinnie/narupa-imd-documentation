InstanceService.InstanceServiceBase
===================================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public abstract class InstanceServiceBase : Object
    
    
    .. cs:method:: public virtual Task<LoadTrajectoryResponse> LoadTrajectory(LoadTrajectoryRequest request, ServerCallContext context)
        
        :param Narupa.Protocol.Instance.LoadTrajectoryRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Instance.LoadTrajectoryResponse}: 
        
    .. cs:method:: public virtual Task<ConnectToTrajectoryResponse> ConnectToTrajectory(ConnectToTrajectoryRequest request, ServerCallContext context)
        
        :param ConnectToTrajectoryRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{ConnectToTrajectoryResponse}: 
        
    .. cs:method:: public virtual Task<GetInstanceInfoResponse> GetInstanceInfo(GetInstanceInfoRequest request, ServerCallContext context)
        
        :param Narupa.Protocol.Instance.GetInstanceInfoRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Instance.GetInstanceInfoResponse}: 
        
    .. cs:method:: public virtual Task<CloseInstanceResponse> CloseInstance(CloseInstanceRequest request, ServerCallContext context)
        
        :param Narupa.Protocol.Instance.CloseInstanceRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Instance.CloseInstanceResponse}: 
        
    .. cs:constructor:: protected InstanceServiceBase()
        
        
