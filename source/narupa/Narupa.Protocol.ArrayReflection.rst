ArrayReflection
===============
.. cs:setscope:: Narupa.Protocol

.. cs:class:: public static class ArrayReflection : Object
    
    
    .. cs:property:: public static FileDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.FileDescriptor: 
        
