ParticleFilteredAdaptorNode
===========================
.. cs:setscope:: Narupa.Visualisation.Node.Adaptor

.. cs:class:: [Serializable] public class ParticleFilteredAdaptorNode : ParentedAdaptorNode, IDynamicPropertyProvider, IPropertyProvider
    
    A variation of a :cs:any:`~Narupa.Visualisation.Node.Adaptor.FrameAdaptorNode` which applies a filter to any key of the form 'particle.*'.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Adaptor.ParentedAdaptorNode`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    
    .. cs:property:: public IProperty<int[]> ParticleFilter { get; }
        
        A filter which will affect all fields of the form 'particle.*'.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Int32[]}: 
        
    .. cs:method:: public override IEnumerable<(string name, IReadOnlyProperty property)> GetProperties()
        
        

        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,Narupa.Visualisation.Property.IReadOnlyProperty}}: 
        
    .. cs:method:: public override IReadOnlyProperty GetProperty(string key)
        
        

        :param System.String key: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:method:: public override IReadOnlyProperty<T> GetOrCreateProperty<T>(string name)
        
        

        :param System.String name: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{{T}}: 
        
