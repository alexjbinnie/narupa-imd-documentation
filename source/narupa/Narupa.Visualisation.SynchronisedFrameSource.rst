SynchronisedFrameSource
=======================
.. cs:setscope:: Narupa.Visualisation

.. cs:class:: [DisallowMultipleComponent] public class SynchronisedFrameSource : MonoBehaviour, ITrajectorySnapshot, IFrameConsumer
    
    Interface between a :cs:any:`~Narupa.Frame.ITrajectorySnapshot` and Unity. Frontend
    specific tasks such as rendering should utilise this to track the frame, as it
    delays frame updating to the main thread.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    :implements: :cs:any:`~Narupa.Frame.ITrajectorySnapshot`
    :implements: :cs:any:`~Narupa.Frame.IFrameConsumer`
    
    .. cs:property:: public Frame CurrentFrame { get; }
        
        

        :implements: :cs:any:`~Narupa.Frame.ITrajectorySnapshot.CurrentFrame`
        :returns Narupa.Frame.Frame: 
        
    .. cs:event:: public event FrameChanged FrameChanged
        
        

        :implements: :cs:any:`~Narupa.Frame.ITrajectorySnapshot.FrameChanged`
        :returns Narupa.Frame.FrameChanged: 
        
    .. cs:property:: public ITrajectorySnapshot FrameSource { set; }
        
        Source for the frames to be displayed.
        
        

        :implements: :cs:any:`~Narupa.Frame.IFrameConsumer.FrameSource`
        :returns Narupa.Frame.ITrajectorySnapshot: 
        
