IFilteredProperty
=================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:interface:: public interface IFilteredProperty
    
    
    .. cs:property:: IReadOnlyProperty SourceProperty { get; }
        
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:property:: IReadOnlyProperty<int[]> FilterProperty { get; }
        
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}: 
        
