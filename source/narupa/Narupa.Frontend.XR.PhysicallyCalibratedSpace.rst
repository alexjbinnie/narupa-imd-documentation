PhysicallyCalibratedSpace
=========================
.. cs:setscope:: Narupa.Frontend.XR

.. cs:class:: public class PhysicallyCalibratedSpace
    
    Represents a shared coordinate space (with uniform scale) that has been 
    calibrated with respect to a physical space.
    
    

    
    .. cs:property:: public Matrix4x4 LocalToWorldMatrix { get; }
        
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:property:: public Matrix4x4 WorldToLocalMatrix { get; }
        
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:method:: public Transformation TransformPoseCalibratedToWorld(Transformation calibratedPose)
        
        Transform from the shared calibrated space to our personal world 
        space.
        
        

        :param Narupa.Core.Math.Transformation calibratedPose: 
        :returns Narupa.Core.Math.Transformation: 
        
    .. cs:method:: public Transformation TransformPoseWorldToCalibrated(Transformation worldPose)
        
        Transform from our personal world space to the shared calibrated 
        space.
        
        

        :param Narupa.Core.Math.Transformation worldPose: 
        :returns Narupa.Core.Math.Transformation: 
        
    .. cs:method:: public void CalibrateFromLighthouses()
        
        Calibrate the space by assuming that everyone agrees on the 
        physical location of two XR tracking points.
        
        

        It is assumed that the two tracking points with the lowest uniqueID
        are the desired lighthouses.
        
        

        
    .. cs:method:: public void CalibrateFromTwoControlPoints(Vector3 worldPoint0, Vector3 worldPoint1)
        
        Calibrate the space from our personal world position of two 
        physical points.
        
        

        :param UnityEngine.Vector3 worldPoint0: 
        :param UnityEngine.Vector3 worldPoint1: 
        
