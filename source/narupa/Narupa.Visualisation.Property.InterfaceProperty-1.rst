InterfaceProperty<TValue>
=========================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:class:: public class InterfaceProperty<TValue> : LinkableProperty<TValue>, IProperty<TValue>, IReadOnlyProperty<TValue>, IProperty, IReadOnlyProperty, ISerializationCallbackReceiver
    
    A interface-based property which is serialised as a Unity Object.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.LinkableProperty{{TValue}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{{TValue}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{{TValue}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    
    .. cs:property:: protected override TValue ProvidedValue { get; set; }
        
        :returns {TValue}: 
        
    .. cs:property:: protected override bool HasProvidedValue { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: public override void UndefineValue()
        
        
    .. cs:method:: public void OnBeforeSerialize()
        
        :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize`
        
    .. cs:method:: public void OnAfterDeserialize()
        
        :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize`
        
