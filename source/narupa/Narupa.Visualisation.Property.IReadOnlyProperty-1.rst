IReadOnlyProperty<TValue>
=========================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:interface:: public interface IReadOnlyProperty<out TValue> : IReadOnlyProperty
    
    

    
    .. cs:property:: TValue Value { get; }
        
        

        :returns {TValue}: 
        
