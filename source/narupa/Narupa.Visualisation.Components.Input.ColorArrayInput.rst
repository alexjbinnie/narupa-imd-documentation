ColorArrayInput
===============
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class ColorArrayInput : VisualisationComponent<ColorArrayInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ColorArrayInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.ColorArrayInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.ColorArrayInputNode}`
    
