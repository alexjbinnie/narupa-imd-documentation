VisualisationComponentRenderer<T>
=================================
.. cs:setscope:: Narupa.Visualisation.Components.Renderer

.. cs:class:: public abstract class VisualisationComponentRenderer<T> : VisualisationComponent<T>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<T> where T : new()
    
    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{{T}}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{{T}}`
    
    .. cs:method:: protected override void OnEnable()
        
        
    .. cs:method:: protected override void OnDisable()
        
        
    .. cs:method:: protected abstract void Render(Camera camera)
        
        :param UnityEngine.Camera camera: 
        
    .. cs:method:: protected virtual void UpdateInEditor()
        
        
