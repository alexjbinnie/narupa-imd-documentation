ByEntitySequence
================
.. cs:setscope:: Narupa.Visualisation.Components.Calculator

.. cs:class:: public class ByEntitySequence : VisualisationComponent<ByEntitySequenceNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ByEntitySequenceNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Protein.ByEntitySequenceNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Protein.ByEntitySequenceNode}`
    
