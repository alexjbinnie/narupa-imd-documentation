VrControllerPrefab.UnityEventString
===================================
.. cs:setscope:: Narupa.Frontend.Controllers

.. cs:class:: [Serializable] public class UnityEventString : UnityEvent<string>, ISerializationCallbackReceiver
    
    :inherits: :cs:any:`~UnityEngine.Events.UnityEvent{System.String}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    
