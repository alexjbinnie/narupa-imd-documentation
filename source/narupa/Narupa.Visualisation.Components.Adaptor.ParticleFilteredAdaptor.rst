ParticleFilteredAdaptor
=======================
.. cs:setscope:: Narupa.Visualisation.Components.Adaptor

.. cs:class:: public class ParticleFilteredAdaptor : FrameAdaptorComponent<ParticleFilteredAdaptorNode>, ISerializationCallbackReceiver, IVisualisationComponent<ParticleFilteredAdaptorNode>, IDynamicPropertyProvider, IPropertyProvider
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.Adaptor.FrameAdaptorComponent{Narupa.Visualisation.Node.Adaptor.ParticleFilteredAdaptorNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Adaptor.ParticleFilteredAdaptorNode}`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    
    .. cs:method:: protected override void OnDisable()
        
        
