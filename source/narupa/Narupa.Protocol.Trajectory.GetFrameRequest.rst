GetFrameRequest
===============
.. cs:setscope:: Narupa.Protocol.Trajectory

.. cs:class:: public sealed class GetFrameRequest : Object, IMessage<GetFrameRequest>, IMessage, IEquatable<GetFrameRequest>, IDeepCloneable<GetFrameRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Trajectory.GetFrameRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Trajectory.GetFrameRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Trajectory.GetFrameRequest}`
    
    .. cs:field:: public const int DataFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int FrameIntervalFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public GetFrameRequest()
        
        
    .. cs:constructor:: public GetFrameRequest(GetFrameRequest other)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest other: 
        
    .. cs:method:: public GetFrameRequest Clone()
        
        :returns Narupa.Protocol.Trajectory.GetFrameRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(GetFrameRequest other)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(GetFrameRequest other)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<GetFrameRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Trajectory.GetFrameRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public Struct Data { get; set; }
        
        :returns Google.Protobuf.WellKnownTypes.Struct: 
        
    .. cs:property:: public float FrameInterval { get; set; }
        
        :returns System.Single: 
        
