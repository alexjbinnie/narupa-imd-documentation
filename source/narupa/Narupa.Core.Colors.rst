Colors
======
.. cs:setscope:: Narupa.Core

.. cs:class:: public static class Colors
    
    Utility classes related to colors.
    
    

    
    .. cs:method:: public static Color? GetColorFromName(string name)
        
        Get a color from its web color name.
        
        

        These colors are sources from the CSS3 specification
        (https://www.w3.org/TR/css-color-3/),
        which in turn is based upon the X11 color palette.
        
        

        :param System.String name: 
        :returns System.Nullable{UnityEngine.Color}: 
        
    .. cs:property:: public static Color Pink { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightPink { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color HotPink { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DeepPink { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color PaleVioletRed { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MediumVioletRed { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightSalmon { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Salmon { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkSalmon { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightCoral { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color IndianRed { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Crimson { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Firebrick { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkRed { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Red { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color OrangeRed { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Tomato { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Coral { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkOrange { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Orange { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Yellow { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightYellow { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LemonChiffon { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightGoldenrodYellow { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color PapayaWhip { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Moccasin { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color PeachPuff { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color PaleGoldenrod { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Khaki { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkKhaki { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Gold { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Cornsilk { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color BlanchedAlmond { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Bisque { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color NavajoWhite { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Wheat { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Burlywood { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Tan { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color RosyBrown { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color SandyBrown { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Goldenrod { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkGoldenrod { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Peru { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Chocolate { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color SaddleBrown { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Sienna { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Brown { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Maroon { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkOliveGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Olive { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color OliveDrab { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color YellowGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LimeGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Lime { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LawnGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Chartreuse { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color GreenYellow { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color SpringGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MediumSpringGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color PaleGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkSeaGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MediumAquamarine { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MediumSeaGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color SeaGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color ForestGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Green { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Aqua { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Cyan { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightCyan { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color PaleTurquoise { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Aquamarine { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Turquoise { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MediumTurquoise { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkTurquoise { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightSeaGreen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color CadetBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkCyan { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Teal { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightSteelBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color PowderBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color SkyBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightSkyBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DeepSkyBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DodgerBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color CornflowerBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color SteelBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color RoyalBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Blue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MediumBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Navy { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MidnightBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Lavender { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Thistle { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Plum { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Violet { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Orchid { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Fuchsia { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Magenta { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MediumOrchid { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MediumPurple { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color BlueViolet { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkViolet { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkOrchid { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkMagenta { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Purple { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Indigo { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkSlateBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color SlateBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MediumSlateBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color White { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Snow { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Honeydew { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MintCream { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Azure { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color AliceBlue { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color GhostWhite { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color WhiteSmoke { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Seashell { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Beige { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color OldLace { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color FloralWhite { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Ivory { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color AntiqueWhite { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Linen { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LavenderBlush { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color MistyRose { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Gainsboro { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightGray { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightGrey { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Silver { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkGray { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkGrey { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Gray { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Grey { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DimGray { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DimGrey { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightSlateGray { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color LightSlateGrey { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color SlateGray { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color SlateGrey { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkSlateGray { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color DarkSlateGrey { get; }
        
        :returns UnityEngine.Color: 
        
    .. cs:property:: public static Color Black { get; }
        
        :returns UnityEngine.Color: 
        
