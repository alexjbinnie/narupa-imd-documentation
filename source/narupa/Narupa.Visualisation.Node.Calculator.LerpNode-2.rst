LerpNode<TValue, TProperty>
===========================
.. cs:setscope:: Narupa.Visualisation.Node.Calculator

.. cs:class:: public abstract class LerpNode<TValue, TProperty> : GenericOutputNode where TProperty : ArrayProperty<TValue>, new()
    
    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:property:: protected override bool IsInputValid { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        
    .. cs:method:: protected override void UpdateOutput()
        
        
    .. cs:method:: protected abstract TValue MoveTowards(TValue current, TValue target)
        
        :param {TValue} current: 
        :param {TValue} target: 
        :returns {TValue}: 
        
    .. cs:method:: protected override void ClearOutput()
        
        
