OutgoingStream<TOutgoing, TIncoming>
====================================
.. cs:setscope:: Narupa.Grpc.Stream

.. cs:class:: public sealed class OutgoingStream<TOutgoing, TIncoming> : Cancellable, ICancellable, ICancellationTokenSource, IDisposable, IAsyncClosable
    
    Wraps the outgoing request stream of a gRPC call.
    
    

    Currently provides no mechanism to deal with the final response message
    from the server.
    
    

    :inherits: :cs:any:`~Narupa.Core.Async.Cancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellationTokenSource`
    :implements: :cs:any:`~System.IDisposable`
    :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable`
    
    .. cs:method:: public static OutgoingStream<TOutgoing, TIncoming> CreateStreamFromClientCall(ClientStreamingCall<TOutgoing, TIncoming> grpcCall, params CancellationToken[] externalTokens)
        
        Create a stream from a gRPC call.
        
        

        :param Narupa.Grpc.Stream.ClientStreamingCall{{TOutgoing},{TIncoming}} grpcCall: 
        :param System.Threading.CancellationToken[] externalTokens: 
        :returns Narupa.Grpc.Stream.OutgoingStream`2: 
        
    .. cs:property:: public bool IsSendingStarted { get; }
        
        Is the stream currently active (A call to :cs:any:`~Narupa.Grpc.Stream.OutgoingStream`2.StartSending` has
        occured).
        
        

        :returns System.Boolean: 
        
    .. cs:method:: public Task QueueMessageAsync(TOutgoing message)
        
        Send a message asynchronously over the stream.
        
        

        :param {TOutgoing} message: 
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public Task CloseAsync()
        
        

        :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable.CloseAsync`
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public Task StartSending()
        
        Start the stream, allowing it to accept input.
        
        

        :returns System.Threading.Tasks.Task: 
        
