IntProperty
===========
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class IntProperty : SerializableProperty<int>, IProperty<int>, IReadOnlyProperty<int>, IProperty, IReadOnlyProperty
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an :cs:any:`~System.Int32` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.SerializableProperty{System.Int32}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{System.Int32}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
