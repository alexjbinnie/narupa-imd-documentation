TrajectoryService.TrajectoryServiceBase
=======================================
.. cs:setscope:: Narupa.Protocol.Trajectory

.. cs:class:: public abstract class TrajectoryServiceBase : Object
    
    
    .. cs:method:: public virtual Task SubscribeFrames(GetFrameRequest request, IServerStreamWriter<GetFrameResponse> responseStream, ServerCallContext context)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.IServerStreamWriter{Narupa.Protocol.Trajectory.GetFrameResponse} responseStream: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public virtual Task SubscribeLatestFrames(GetFrameRequest request, IServerStreamWriter<GetFrameResponse> responseStream, ServerCallContext context)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.IServerStreamWriter{Narupa.Protocol.Trajectory.GetFrameResponse} responseStream: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public virtual Task GetFrames(GetFrameRequest request, IServerStreamWriter<GetFrameResponse> responseStream, ServerCallContext context)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.IServerStreamWriter{Narupa.Protocol.Trajectory.GetFrameResponse} responseStream: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public virtual Task<GetFrameResponse> GetFrame(GetFrameRequest request, ServerCallContext context)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Trajectory.GetFrameResponse}: 
        
    .. cs:constructor:: protected TrajectoryServiceBase()
        
        
