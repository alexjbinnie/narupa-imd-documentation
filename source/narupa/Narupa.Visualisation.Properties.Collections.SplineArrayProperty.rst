SplineArrayProperty
===================
.. cs:setscope:: Narupa.Visualisation.Properties.Collections

.. cs:class:: [Serializable] public class SplineArrayProperty : ArrayProperty<SplineSegment>, IProperty<SplineSegment[]>, IReadOnlyProperty<SplineSegment[]>, IProperty, IReadOnlyProperty, IEnumerable<SplineSegment>, IEnumerable
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an array of :cs:any:`~Narupa.Visualisation.Node.Spline.SplineSegment` values.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Properties.Collections.ArrayProperty{Narupa.Visualisation.Node.Spline.SplineSegment}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{Narupa.Visualisation.Node.Spline.SplineSegment[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Visualisation.Node.Spline.SplineSegment[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{Narupa.Visualisation.Node.Spline.SplineSegment}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
