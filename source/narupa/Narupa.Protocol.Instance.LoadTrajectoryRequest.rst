LoadTrajectoryRequest
=====================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public sealed class LoadTrajectoryRequest : Object, IMessage<LoadTrajectoryRequest>, IMessage, IEquatable<LoadTrajectoryRequest>, IDeepCloneable<LoadTrajectoryRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Instance.LoadTrajectoryRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Instance.LoadTrajectoryRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Instance.LoadTrajectoryRequest}`
    
    .. cs:field:: public const int InstanceIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int PathFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public LoadTrajectoryRequest()
        
        
    .. cs:constructor:: public LoadTrajectoryRequest(LoadTrajectoryRequest other)
        
        :param Narupa.Protocol.Instance.LoadTrajectoryRequest other: 
        
    .. cs:method:: public LoadTrajectoryRequest Clone()
        
        :returns Narupa.Protocol.Instance.LoadTrajectoryRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(LoadTrajectoryRequest other)
        
        :param Narupa.Protocol.Instance.LoadTrajectoryRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(LoadTrajectoryRequest other)
        
        :param Narupa.Protocol.Instance.LoadTrajectoryRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<LoadTrajectoryRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Instance.LoadTrajectoryRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string InstanceId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public string Path { get; set; }
        
        :returns System.String: 
        
