ICancellationTokenSource
========================
.. cs:setscope:: Narupa.Core.Async

.. cs:interface:: public interface ICancellationTokenSource
    
    An object which provides a :cs:any:`~System.Threading.CancellationToken` using
    :cs:any:`~Narupa.Core.Async.ICancellationTokenSource.GetCancellationToken`, which allows related things to be
    cancelled when this object is.
    
    

    
    .. cs:method:: CancellationToken GetCancellationToken()
        
        Get a unique token which can be passed to most asynchronous operations.
        
        

        :returns System.Threading.CancellationToken: 
        
