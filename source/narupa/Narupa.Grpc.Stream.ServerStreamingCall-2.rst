ServerStreamingCall<TRequest, TReply>
=====================================
.. cs:setscope:: Narupa.Grpc.Stream

.. cs:delegate:: public delegate AsyncServerStreamingCall<TReply> ServerStreamingCall<in TRequest, TReply>(TRequest request, Metadata headers = null, DateTime? deadline = default(DateTime? ), CancellationToken cancellationToken = default(CancellationToken));
    
    Delegate for a gRPC server streaming call
    
    

    :param {TRequest} request: 
    :param Grpc.Core.Metadata headers: 
    :param System.Nullable{System.DateTime} deadline: 
    :param System.Threading.CancellationToken cancellationToken: 
    :returns Grpc.Core.AsyncServerStreamingCall{{TReply}}: 
    
