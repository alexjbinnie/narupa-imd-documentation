ColorOutputNode
===============
.. cs:setscope:: Narupa.Visualisation.Node.Output

.. cs:class:: [Serializable] public class ColorOutputNode : OutputNode<ColorProperty>, IOutputNode
    
    :inherits: :cs:any:`~Narupa.Visualisation.Node.Output.OutputNode{Narupa.Visualisation.Properties.ColorProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Output.IOutputNode`
    
