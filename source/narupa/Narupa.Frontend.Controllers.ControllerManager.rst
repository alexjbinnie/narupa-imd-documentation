ControllerManager
=================
.. cs:setscope:: Narupa.Frontend.Controllers

.. cs:class:: public class ControllerManager : MonoBehaviour
    
    Manager class for accessing the left and right controller.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: public SteamVR_Input_Sources DominantHand { get; }
        
        :returns Valve.VR.SteamVR_Input_Sources: 
        
    .. cs:event:: public event Action DominantHandChanged
        
        :returns System.Action: 
        
    .. cs:property:: public VrController LeftController { get; }
        
        The left :cs:any:`~Narupa.Frontend.Controllers.VrController`.
        
        

        :returns Narupa.Frontend.Controllers.VrController: 
        
    .. cs:property:: public VrController RightController { get; }
        
        The right :cs:any:`~Narupa.Frontend.Controllers.VrController`.
        
        

        :returns Narupa.Frontend.Controllers.VrController: 
        
    .. cs:method:: public VrController GetController(SteamVR_Input_Sources inputSource)
        
        Get the :cs:any:`~Narupa.Frontend.Controllers.VrController` corresponding to the given input source.
        
        

        :param Valve.VR.SteamVR_Input_Sources inputSource: One of :cs:any:`~Valve.VR.SteamVR_Input_Sources.LeftHand` or :cs:any:`~Valve.VR.SteamVR_Input_Sources.LeftHand`
        :returns Narupa.Frontend.Controllers.VrController: 
        :throws System.ArgumentOutOfRangeException: If :code:`inputSource` is not one of :cs:any:`~Valve.VR.SteamVR_Input_Sources.LeftHand` or :cs:any:`~Valve.VR.SteamVR_Input_Sources.RightHand`
        
    .. cs:method:: public void SetDominantHand(SteamVR_Input_Sources inputSource)
        
        Set the dominant hand, which is the one which is currently in control of UI.
        
        

        :param Valve.VR.SteamVR_Input_Sources inputSource: 
        
    .. cs:property:: public ControllerInputMode CurrentInputMode { get; }
        
        :returns Narupa.Frontend.Controllers.ControllerInputMode: 
        
    .. cs:method:: public void AddInputMode(ControllerInputMode mode)
        
        :param Narupa.Frontend.Controllers.ControllerInputMode mode: 
        
    .. cs:method:: public void RemoveInputMode(ControllerInputMode mode)
        
        :param Narupa.Frontend.Controllers.ControllerInputMode mode: 
        
    .. cs:method:: public bool WouldBecomeCurrentMode(ControllerInputMode mode)
        
        If the given :cs:any:`~Narupa.Frontend.Controllers.ControllerInputMode` were to be added with
        :cs:any:`~Narupa.Frontend.Controllers.ControllerManager.AddInputMode(Narupa.Frontend.Controllers.ControllerInputMode)`, would its priority mean that it would become the
        current input?
        
        

        :param Narupa.Frontend.Controllers.ControllerInputMode mode: 
        :returns System.Boolean: 
        
