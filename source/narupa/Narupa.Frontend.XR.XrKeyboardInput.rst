XrKeyboardInput
===============
.. cs:setscope:: Narupa.Frontend.XR

.. cs:class:: [RequireComponent(typeof(Button))] public class XrKeyboardInput : MonoBehaviour
    
    Allows an :cs:any:`~TMPro.TMP_InputField` to be modified in VR.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
