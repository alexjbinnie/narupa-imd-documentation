TrajectorySnapshot
==================
.. cs:setscope:: Narupa.Frame

.. cs:class:: public class TrajectorySnapshot : ITrajectorySnapshot
    
    Maintains a single :cs:any:`~Narupa.Frame.Frame`, which is updated by receiving new data.
    When updated, old fields are maintained except when replaced by data in the
    previous frame.
    
    

    :implements: :cs:any:`~Narupa.Frame.ITrajectorySnapshot`
    
    .. cs:property:: public Frame CurrentFrame { get; }
        
        

        :implements: :cs:any:`~Narupa.Frame.ITrajectorySnapshot.CurrentFrame`
        :returns Narupa.Frame.Frame: 
        
    .. cs:property:: public FrameChanges CurrentFrameChanges { get; }
        
        :returns Narupa.Frame.Event.FrameChanges: 
        
    .. cs:method:: public void Clear()
        
        Set the current frame to completely empty, with every field changed.
        
        

        
    .. cs:method:: public void SetCurrentFrame(Frame frame, FrameChanges changes)
        
        Set the current frame, replacing the existing one.
        
        

        :param Narupa.Frame.Frame frame: 
        :param Narupa.Frame.Event.FrameChanges changes: 
        
    .. cs:event:: public event FrameChanged FrameChanged
        
        

        :implements: :cs:any:`~Narupa.Frame.ITrajectorySnapshot.FrameChanged`
        :returns Narupa.Frame.FrameChanged: 
        
