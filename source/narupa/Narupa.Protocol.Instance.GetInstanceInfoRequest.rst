GetInstanceInfoRequest
======================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public sealed class GetInstanceInfoRequest : Object, IMessage<GetInstanceInfoRequest>, IMessage, IEquatable<GetInstanceInfoRequest>, IDeepCloneable<GetInstanceInfoRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Instance.GetInstanceInfoRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Instance.GetInstanceInfoRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Instance.GetInstanceInfoRequest}`
    
    .. cs:field:: public const int InstanceIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int FieldsFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public GetInstanceInfoRequest()
        
        
    .. cs:constructor:: public GetInstanceInfoRequest(GetInstanceInfoRequest other)
        
        :param Narupa.Protocol.Instance.GetInstanceInfoRequest other: 
        
    .. cs:method:: public GetInstanceInfoRequest Clone()
        
        :returns Narupa.Protocol.Instance.GetInstanceInfoRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(GetInstanceInfoRequest other)
        
        :param Narupa.Protocol.Instance.GetInstanceInfoRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(GetInstanceInfoRequest other)
        
        :param Narupa.Protocol.Instance.GetInstanceInfoRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<GetInstanceInfoRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Instance.GetInstanceInfoRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string InstanceId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public FieldMask Fields { get; set; }
        
        :returns Google.Protobuf.WellKnownTypes.FieldMask: 
        
