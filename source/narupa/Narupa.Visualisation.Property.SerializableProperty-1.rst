SerializableProperty<TValue>
============================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:class:: public class SerializableProperty<TValue> : LinkableProperty<TValue>, IProperty<TValue>, IReadOnlyProperty<TValue>, IProperty, IReadOnlyProperty
    
    A property who's value and its presence are both serialized.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.LinkableProperty{{TValue}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{{TValue}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{{TValue}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
    .. cs:property:: protected override TValue ProvidedValue { get; set; }
        
        :returns {TValue}: 
        
    .. cs:property:: protected override bool HasProvidedValue { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: public override void UndefineValue()
        
        
