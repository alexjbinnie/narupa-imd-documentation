AsyncEnumeratorExtensions
=========================
.. cs:setscope:: Narupa.Grpc.Stream

.. cs:class:: public static class AsyncEnumeratorExtensions
    
    Extension methods for iterating over asynchronous enumerators
    
    

    
    .. cs:method:: public static Task ForEachAsync<T>(this IAsyncStreamReader<T> enumerator, Action<T> callback, CancellationToken cancellationToken)
        
        Asynchronously iterate over an enumerator and execute the callback with each
        element.
        
        

        If an exception occurs at any point in the iteration, the whole task will end.
        
        

        :param Grpc.Core.IAsyncStreamReader{{T}} enumerator: 
        :param System.Action{{T}} callback: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns System.Threading.Tasks.Task: 
        
