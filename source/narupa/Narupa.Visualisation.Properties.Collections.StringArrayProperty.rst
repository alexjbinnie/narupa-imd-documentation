StringArrayProperty
===================
.. cs:setscope:: Narupa.Visualisation.Properties.Collections

.. cs:class:: [Serializable] public class StringArrayProperty : ArrayProperty<string>, IProperty<string[]>, IReadOnlyProperty<string[]>, IProperty, IReadOnlyProperty, IEnumerable<string>, IEnumerable
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an array of :cs:any:`~System.String`
    values.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Properties.Collections.ArrayProperty{System.String}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{System.String[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.String[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.String}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
