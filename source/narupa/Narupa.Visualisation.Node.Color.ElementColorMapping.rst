ElementColorMapping
===================
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [CreateAssetMenu(menuName = "Definition/Element Color Mapping")] public class ElementColorMapping : ScriptableObject, IMapping<Element, Color>
    
    Stores key-value pair of atomic number and color, for defining CPK style
    coloring.
    
    

    :inherits: :cs:any:`~UnityEngine.ScriptableObject`
    :implements: :cs:any:`~Narupa.Core.IMapping{Narupa.Core.Science.Element,UnityEngine.Color}`
    
    .. cs:method:: public Color Map(Element element)
        
        Get the color for the given atomic element, returning a default color if the
        element is not defined.
        
        

        :implements: :cs:any:`~Narupa.Core.IMapping{Narupa.Core.Science.Element,UnityEngine.Color}.Map(Narupa.Core.Science.Element)`
        :param Narupa.Core.Science.Element element: 
        :returns UnityEngine.Color: 
        
