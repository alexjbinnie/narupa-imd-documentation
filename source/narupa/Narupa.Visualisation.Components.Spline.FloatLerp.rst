FloatLerp
=========
.. cs:setscope:: Narupa.Visualisation.Components.Spline

.. cs:class:: public class FloatLerp : VisualisationComponent<FloatLerpNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<FloatLerpNode>
    
    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Calculator.FloatLerpNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Calculator.FloatLerpNode}`
    
    .. cs:method:: public void Update()
        
        
