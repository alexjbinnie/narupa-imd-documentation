MappingExtensions
=================
.. cs:setscope:: Narupa.Core

.. cs:class:: public static class MappingExtensions
    
    Extensions methods for :cs:any:`~Narupa.Core.IMapping`2`.
    
    

    
    .. cs:method:: public static IMapping<TFrom, TTo> AsMapping<TFrom, TTo>(this IReadOnlyDictionary<TFrom, TTo> dict, TTo defaultValue = null)
        
        Convert a dictionary into an :cs:any:`~Narupa.Core.IMapping`2`.
        
        

        :param System.Collections.Generic.IReadOnlyDictionary{{TFrom},{TTo}} dict: 
        :param {TTo} defaultValue: 
        :returns Narupa.Core.IMapping{{TFrom},{TTo}}: 
        
