DsspOptions
===========
.. cs:setscope:: Narupa.Visualisation.Node.Protein

.. cs:class:: [Serializable] public class DsspOptions
    
    
    .. cs:field:: public float Cutoff
        
        :returns System.Single: 
        
