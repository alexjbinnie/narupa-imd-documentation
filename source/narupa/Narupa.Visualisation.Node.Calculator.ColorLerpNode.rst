ColorLerpNode
=============
.. cs:setscope:: Narupa.Visualisation.Node.Calculator

.. cs:class:: [Serializable] public class ColorLerpNode : LerpNode<Color, ColorArrayProperty>
    
    :inherits: :cs:any:`~Narupa.Visualisation.Node.Calculator.LerpNode{UnityEngine.Color,Narupa.Visualisation.Properties.Collections.ColorArrayProperty}`
    
    .. cs:method:: protected override Color MoveTowards(Color current, Color target)
        
        :param UnityEngine.Color current: 
        :param UnityEngine.Color target: 
        :returns UnityEngine.Color: 
        
