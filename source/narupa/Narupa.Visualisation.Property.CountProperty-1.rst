CountProperty<TValue>
=====================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:class:: public class CountProperty<TValue> : IReadOnlyProperty<int>, IReadOnlyProperty
    
    Property that watches a collection :cs:any:`~Narupa.Visualisation.Property.Property` and returns its
    count.
    
    

    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
    .. cs:constructor:: public CountProperty(IReadOnlyProperty<ICollection<TValue>> property)
        
        :param Narupa.Visualisation.Property.IReadOnlyProperty{System.Collections.Generic.ICollection{{TValue}}} property: 
        
    .. cs:property:: public int Value { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32}.Value`
        :returns System.Int32: 
        
    .. cs:property:: public bool HasValue { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.HasValue`
        :returns System.Boolean: 
        
    .. cs:property:: public bool HasNonNullValue { get; }
        
        :returns System.Boolean: 
        
    .. cs:event:: public event Action ValueChanged
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.ValueChanged`
        :returns System.Action: 
        
    .. cs:property:: public Type PropertyType { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.PropertyType`
        :returns System.Type: 
        
    .. cs:property:: object IReadOnlyProperty.Value { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.Value`
        :returns System.Object: 
        
