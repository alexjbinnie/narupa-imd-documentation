ObservableDictionary<TKey, TValue>
==================================
.. cs:setscope:: Narupa.Core.Collections

.. cs:class:: public class ObservableDictionary<TKey, TValue> : INotifyCollectionChanged, IDictionary<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerable
    
    A dictionary which implements :cs:any:`~System.Collections.Specialized.INotifyCollectionChanged` on the
    keys, such that :cs:any:`~Narupa.Core.Collections.ObservableDictionary`2.CollectionChanged` is invoked when something is
    added, removed or updated in the dictionary.
    
    

    :implements: :cs:any:`~System.Collections.Specialized.INotifyCollectionChanged`
    :implements: :cs:any:`~System.Collections.Generic.IDictionary{{TKey},{TValue}}`
    :implements: :cs:any:`~System.Collections.Generic.ICollection{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
    .. cs:constructor:: public ObservableDictionary([NotNull] IDictionary<TKey, TValue> dictionary)
        
        :param System.Collections.Generic.IDictionary{{TKey},{TValue}} dictionary: 
        
    .. cs:constructor:: public ObservableDictionary()
        
        
    .. cs:method:: public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        
        

        :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}.GetEnumerator`
        :returns System.Collections.Generic.IEnumerator{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}: 
        
    .. cs:method:: IEnumerator IEnumerable.GetEnumerator()
        
        

        :implements: :cs:any:`~System.Collections.IEnumerable.GetEnumerator`
        :returns System.Collections.IEnumerator: 
        
    .. cs:method:: public void Add(KeyValuePair<TKey, TValue> item)
        
        

        :implements: :cs:any:`~System.Collections.Generic.ICollection{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}.Add(System.Collections.Generic.KeyValuePair{{TKey},{TValue}})`
        :param System.Collections.Generic.KeyValuePair{{TKey},{TValue}} item: 
        
    .. cs:method:: public void Clear()
        
        

        :implements: :cs:any:`~System.Collections.Generic.ICollection{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}.Clear`
        
    .. cs:method:: public bool Contains(KeyValuePair<TKey, TValue> item)
        
        

        :implements: :cs:any:`~System.Collections.Generic.ICollection{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}.Contains(System.Collections.Generic.KeyValuePair{{TKey},{TValue}})`
        :param System.Collections.Generic.KeyValuePair{{TKey},{TValue}} item: 
        :returns System.Boolean: 
        
    .. cs:method:: public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        
        

        :implements: :cs:any:`~System.Collections.Generic.ICollection{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}.CopyTo(System.Collections.Generic.KeyValuePair{{TKey},{TValue}}[],System.Int32)`
        :param System.Collections.Generic.KeyValuePair{{TKey},{TValue}}[] array: 
        :param System.Int32 arrayIndex: 
        
    .. cs:method:: public bool Remove(KeyValuePair<TKey, TValue> item)
        
        

        :implements: :cs:any:`~System.Collections.Generic.ICollection{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}.Remove(System.Collections.Generic.KeyValuePair{{TKey},{TValue}})`
        :param System.Collections.Generic.KeyValuePair{{TKey},{TValue}} item: 
        :returns System.Boolean: 
        
    .. cs:property:: public int Count { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.ICollection{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}.Count`
        :returns System.Int32: 
        
    .. cs:property:: public bool IsReadOnly { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.ICollection{System.Collections.Generic.KeyValuePair{{TKey},{TValue}}}.IsReadOnly`
        :returns System.Boolean: 
        
    .. cs:method:: public void Add(TKey key, TValue value)
        
        

        :implements: :cs:any:`~System.Collections.Generic.IDictionary{{TKey},{TValue}}.Add({TKey},{TValue})`
        :param {TKey} key: 
        :param {TValue} value: 
        
    .. cs:method:: public bool ContainsKey(TKey key)
        
        

        :implements: :cs:any:`~System.Collections.Generic.IDictionary{{TKey},{TValue}}.ContainsKey({TKey})`
        :param {TKey} key: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Remove(TKey key)
        
        

        :implements: :cs:any:`~System.Collections.Generic.IDictionary{{TKey},{TValue}}.Remove({TKey})`
        :param {TKey} key: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool TryGetValue(TKey key, out TValue value)
        
        

        :implements: :cs:any:`~System.Collections.Generic.IDictionary{{TKey},{TValue}}.TryGetValue({TKey},{TValue}@)`
        :param {TKey} key: 
        :param {TValue} value: 
        :returns System.Boolean: 
        
    .. cs:indexer:: public TValue this[TKey key] { get; set; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.IDictionary{{TKey},{TValue}}.Item({TKey})`
        :param {TKey} key: 
        :returns {TValue}: 
        
    .. cs:property:: public ICollection<TKey> Keys { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.IDictionary{{TKey},{TValue}}.Keys`
        :returns System.Collections.Generic.ICollection{{TKey}}: 
        
    .. cs:property:: public ICollection<TValue> Values { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.IDictionary{{TKey},{TValue}}.Values`
        :returns System.Collections.Generic.ICollection{{TValue}}: 
        
    .. cs:event:: public event NotifyCollectionChangedEventHandler CollectionChanged
        
        

        :implements: :cs:any:`~System.Collections.Specialized.INotifyCollectionChanged.CollectionChanged`
        :returns System.Collections.Specialized.NotifyCollectionChangedEventHandler: 
        
