Conversions
===========
.. cs:setscope:: Narupa.Grpc

.. cs:class:: public static class Conversions
    
    Utility methods for converting from protobuf data structures to C# objects.
    
    

    
    .. cs:method:: public static object ToArray(this ValueArray valueArray)
        
        Convert a protobuf :cs:any:`~Narupa.Protocol.ValueArray` to the corresponding C# array.
        
        

        :param Narupa.Protocol.ValueArray valueArray: 
        :returns System.Object: 
        
    .. cs:method:: public static List<object> ToList(this ListValue value)
        
        Convert a protobuf :cs:any:`~Google.Protobuf.WellKnownTypes.ListValue` to a list of C# objects.
        
        

        :param Google.Protobuf.WellKnownTypes.ListValue value: 
        :returns System.Collections.Generic.List{System.Object}: 
        
    .. cs:method:: public static Dictionary<string, object> ToDictionary(this Struct value)
        
        Convert a protobuf :cs:any:`~Google.Protobuf.WellKnownTypes.Struct` to a C# dictionary.
        
        

        :param Google.Protobuf.WellKnownTypes.Struct value: 
        :returns System.Collections.Generic.Dictionary{System.String,System.Object}: 
        
    .. cs:method:: public static Vector3[] ToVector3Array(this ValueArray valueArray)
        
        Convert a protobuf :cs:any:`~Narupa.Protocol.ValueArray` to an array of
        :cs:any:`~UnityEngine.Vector3`.
        
        

        :param Narupa.Protocol.ValueArray valueArray: 
        :returns UnityEngine.Vector3[]: 
        
    .. cs:method:: public static object ToObject(this Value value)
        
        Convert a protobuf :cs:any:`~Google.Protobuf.WellKnownTypes.Value` to the corresponding C# type.
        
        

        :param Google.Protobuf.WellKnownTypes.Value value: 
        :returns System.Object: 
        
    .. cs:method:: public static Value ToProtobufValue(this IDictionary<string, object> dictionary)
        
        Convert a C# dictionary to a protobuf Value (as a Struct).
        
        

        :param System.Collections.Generic.IDictionary{System.String,System.Object} dictionary: 
        :returns Google.Protobuf.WellKnownTypes.Value: 
        
    .. cs:method:: public static Struct ToProtobufStruct(this IDictionary<string, object> dictionary)
        
        Convert a C# dictionary to a protobuf Struct.
        
        

        :param System.Collections.Generic.IDictionary{System.String,System.Object} dictionary: 
        :returns Google.Protobuf.WellKnownTypes.Struct: 
        
    .. cs:method:: public static Value ToProtobufValue(this IEnumerable<object> enumerable)
        
        Convert a C# IEnumerable to a protobuf Value (as a Value list).
        
        

        :param System.Collections.Generic.IEnumerable{System.Object} enumerable: 
        :returns Google.Protobuf.WellKnownTypes.Value: 
        
    .. cs:method:: public static Value ToProtobufValue(this object object)
        
        Convert a C# object to a protobuf Value.
        
        

        :param System.Object object: 
        :returns Google.Protobuf.WellKnownTypes.Value: 
        
    .. cs:method:: public static Vector3 GetVector3(this RepeatedField<float> values, int offset = 0)
        
        Retrieve a Vector3 from a Protobuf array of floats, optionally
        starting from a given offset in the array.
        
        

        :param Google.Protobuf.Collections.RepeatedField{System.Single} values: 
        :param System.Int32 offset: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public static Quaternion GetQuaternion(this RepeatedField<float> values, int offset = 0)
        
        Retrieve a Quaternion from a Protobuf array of floats, optionally
        starting from a given offset in the array.
        
        

        :param Google.Protobuf.Collections.RepeatedField{System.Single} values: 
        :param System.Int32 offset: 
        :returns UnityEngine.Quaternion: 
        
    .. cs:method:: public static void PutValues(this RepeatedField<float> values, Vector3 vector)
        
        Append the components of a Vector3 to a Protobuf array of floats.
        
        

        :param Google.Protobuf.Collections.RepeatedField{System.Single} values: 
        :param UnityEngine.Vector3 vector: 
        
    .. cs:method:: public static void PutValues(this RepeatedField<float> values, Quaternion quaternion)
        
        Append the components of a Quaternion to a Protobuf array of floats.
        
        

        :param Google.Protobuf.Collections.RepeatedField{System.Single} values: 
        :param UnityEngine.Quaternion quaternion: 
        
    .. cs:method:: public static int ToInt(this Value value)
        
        Convert a protobuf :cs:any:`~Google.Protobuf.WellKnownTypes.Value` to an integer.
        
        

        :param Google.Protobuf.WellKnownTypes.Value value: 
        :returns System.Int32: 
        
