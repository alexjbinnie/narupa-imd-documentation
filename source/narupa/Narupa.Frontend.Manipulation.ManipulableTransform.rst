ManipulableTransform
====================
.. cs:setscope:: Narupa.Frontend.Manipulation

.. cs:class:: public class ManipulableTransform
    
    Wrapper around a Unity Transform that allows up to two grab manipulations
    to translate, rotate, and scale it.
    
    

    
    .. cs:constructor:: public ManipulableTransform(Transform transform)
        
        :param UnityEngine.Transform transform: 
        
    .. cs:method:: public IActiveManipulation StartGrabManipulation(UnitScaleTransformation manipulatorPose)
        
        Start an additional grab manipulation on this object using the
        given pose of the manipulator (e.g controller), returning an object
        which accepts updates to the manipulator pose.
        A second grab will end the one-point gesture and begin a new
        two-point gesture.
        
        

        :param Narupa.Core.Math.UnitScaleTransformation manipulatorPose: 
        :returns Narupa.Frontend.Manipulation.IActiveManipulation: 
        
    .. cs:method:: public void UpdateGesturesFromActiveManipulations()
        
        Integrate updates to the grab points into the active gesture and
        update the transform to match.
        
        

        
