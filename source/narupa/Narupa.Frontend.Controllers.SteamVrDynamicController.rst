SteamVrDynamicController
========================
.. cs:setscope:: Narupa.Frontend.Controllers

.. cs:class:: public class SteamVrDynamicController : MonoBehaviour
    
    Reacts to a change in the SteamVR device name and loads the appropriate prefab.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
