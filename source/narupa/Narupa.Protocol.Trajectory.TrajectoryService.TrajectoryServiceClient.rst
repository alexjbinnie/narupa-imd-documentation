TrajectoryService.TrajectoryServiceClient
=========================================
.. cs:setscope:: Narupa.Protocol.Trajectory

.. cs:class:: public class TrajectoryServiceClient : ClientBase<TrajectoryService.TrajectoryServiceClient>
    
    
    .. cs:constructor:: public TrajectoryServiceClient(Channel channel)
        
        :param Grpc.Core.Channel channel: 
        
    .. cs:constructor:: public TrajectoryServiceClient(CallInvoker callInvoker)
        
        :param Grpc.Core.CallInvoker callInvoker: 
        
    .. cs:constructor:: protected TrajectoryServiceClient()
        
        
    .. cs:constructor:: protected TrajectoryServiceClient(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<GetFrameResponse> SubscribeFrames(GetFrameRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Trajectory.GetFrameResponse}: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<GetFrameResponse> SubscribeFrames(GetFrameRequest request, CallOptions options)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Trajectory.GetFrameResponse}: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<GetFrameResponse> SubscribeLatestFrames(GetFrameRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Trajectory.GetFrameResponse}: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<GetFrameResponse> SubscribeLatestFrames(GetFrameRequest request, CallOptions options)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Trajectory.GetFrameResponse}: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<GetFrameResponse> GetFrames(GetFrameRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Trajectory.GetFrameResponse}: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<GetFrameResponse> GetFrames(GetFrameRequest request, CallOptions options)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Trajectory.GetFrameResponse}: 
        
    .. cs:method:: public virtual GetFrameResponse GetFrame(GetFrameRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Trajectory.GetFrameResponse: 
        
    .. cs:method:: public virtual GetFrameResponse GetFrame(GetFrameRequest request, CallOptions options)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Trajectory.GetFrameResponse: 
        
    .. cs:method:: public virtual AsyncUnaryCall<GetFrameResponse> GetFrameAsync(GetFrameRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Trajectory.GetFrameResponse}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<GetFrameResponse> GetFrameAsync(GetFrameRequest request, CallOptions options)
        
        :param Narupa.Protocol.Trajectory.GetFrameRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Trajectory.GetFrameResponse}: 
        
    .. cs:method:: protected override TrajectoryService.TrajectoryServiceClient NewInstance(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        :returns Narupa.Protocol.Trajectory.TrajectoryService.TrajectoryServiceClient: 
        
