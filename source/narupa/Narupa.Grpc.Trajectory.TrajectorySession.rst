TrajectorySession
=================
.. cs:setscope:: Narupa.Grpc.Trajectory

.. cs:class:: public class TrajectorySession : ITrajectorySnapshot, IDisposable
    
    Adapts :cs:any:`~Narupa.Grpc.Trajectory.TrajectoryClient` into an
    :cs:any:`~Narupa.Frame.ITrajectorySnapshot` where
    :cs:any:`~Narupa.Frame.ITrajectorySnapshot.CurrentFrame` is the latest received frame.
    
    

    :implements: :cs:any:`~Narupa.Frame.ITrajectorySnapshot`
    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:property:: public Frame CurrentFrame { get; }
        
        

        :implements: :cs:any:`~Narupa.Frame.ITrajectorySnapshot.CurrentFrame`
        :returns Narupa.Frame.Frame: 
        
    .. cs:property:: public int CurrentFrameIndex { get; }
        
        :returns System.Int32: 
        
    .. cs:event:: public event FrameChanged FrameChanged
        
        

        :implements: :cs:any:`~Narupa.Frame.ITrajectorySnapshot.FrameChanged`
        :returns Narupa.Frame.FrameChanged: 
        
    .. cs:constructor:: public TrajectorySession()
        
        
    .. cs:method:: public void OpenClient(GrpcConnection connection)
        
        Connect to a trajectory service over the given connection and
        listen in the background for frame changes. Closes any existing
        client.
        
        

        :param Narupa.Grpc.GrpcConnection connection: 
        
    .. cs:method:: public void CloseClient()
        
        Close the current trajectory client.
        
        

        
    .. cs:method:: public void Dispose()
        
        

        :implements: :cs:any:`~System.IDisposable.Dispose`
        
    .. cs:method:: public void Play()
        
        

        
    .. cs:method:: public void Pause()
        
        

        
    .. cs:method:: public void Reset()
        
        

        
    .. cs:method:: public void Step()
        
        

        
    .. cs:property:: public TrajectoryClient Client { get; }
        
        :returns Narupa.Grpc.Trajectory.TrajectoryClient: 
        
