VrController
============
.. cs:setscope:: Narupa.Frontend.Controllers

.. cs:class:: public class VrController : MonoBehaviour
    
    The persistent script to represent a left or right controller in VR.
    
    

    This class exposes various poses (such as cursor and grip points) in a way that
    the posed object always exists, and is automatically linked to the active
    controller if there is one.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: public NarupaRenderModel RenderModel { get; }
        
        :returns Narupa.Frontend.Controllers.NarupaRenderModel: 
        
    .. cs:method:: public void ResetController(VrControllerPrefab controller)
        
        Indicate the controller has been reset (connected or disconnected).
        
        

        :param Narupa.Frontend.Controllers.VrControllerPrefab controller: 
        
    .. cs:method:: public void PushNotification(string text)
        
        :param System.String text: 
        
    .. cs:property:: public IPosedObject GripPose { get; }
        
        The pose marking the location of a gripped hand.
        
        

        :returns Narupa.Frontend.Input.IPosedObject: 
        
    .. cs:property:: public IPosedObject HeadPose { get; }
        
        The pose marking the location where the bulk of the controller is
        
        

        :returns Narupa.Frontend.Input.IPosedObject: 
        
    .. cs:property:: public IPosedObject CursorPose { get; }
        
        The cursor point where tools should be centered.
        
        

        :returns Narupa.Frontend.Input.IPosedObject: 
        
    .. cs:property:: public bool IsControllerActive { get; }
        
        Is the controller currently active?
        
        

        :returns System.Boolean: 
        
    .. cs:event:: public event Action ControllerReset
        
        :returns System.Action: 
        
    .. cs:method:: public void InstantiateCursorGizmo(GameObject interactionGizmo)
        
        Set the current gizmo at the end of the controller.
        
        

        :param UnityEngine.GameObject interactionGizmo: A :cs:any:`~UnityEngine.GameObject` representing the gizmo at the end of the controller, or null if there should be no gizmo.
        
