CommandMessage
==============
.. cs:setscope:: Narupa.Protocol.Command

.. cs:class:: public sealed class CommandMessage : Object, IMessage<CommandMessage>, IMessage, IEquatable<CommandMessage>, IDeepCloneable<CommandMessage>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Command.CommandMessage}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Command.CommandMessage}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Command.CommandMessage}`
    
    .. cs:field:: public const int NameFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int ArgumentsFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public CommandMessage()
        
        
    .. cs:constructor:: public CommandMessage(CommandMessage other)
        
        :param Narupa.Protocol.Command.CommandMessage other: 
        
    .. cs:method:: public CommandMessage Clone()
        
        :returns Narupa.Protocol.Command.CommandMessage: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(CommandMessage other)
        
        :param Narupa.Protocol.Command.CommandMessage other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(CommandMessage other)
        
        :param Narupa.Protocol.Command.CommandMessage other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<CommandMessage> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Command.CommandMessage}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string Name { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public Struct Arguments { get; set; }
        
        :returns Google.Protobuf.WellKnownTypes.Struct: 
        
