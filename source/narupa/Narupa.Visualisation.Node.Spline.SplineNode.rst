SplineNode
==========
.. cs:setscope:: Narupa.Visualisation.Node.Spline

.. cs:class:: [Serializable] public class SplineNode : GenericOutputNode
    
    Generate a set of :cs:any:`~Narupa.Visualisation.Node.Spline.SplineSegment`s from a set of positions.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:method:: protected (Vector3 position, Vector3 tangent, Vector3 normal, Color color, float size) GetVertex(int offset)
        
        :param System.Int32 offset: 
        :returns System.ValueTuple{UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single}: 
        
    .. cs:method:: protected override void UpdateOutput()
        
        

        
    .. cs:method:: protected override void ClearOutput()
        
        

        
