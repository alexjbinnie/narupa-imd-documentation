CloseInstanceResponse
=====================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public sealed class CloseInstanceResponse : Object, IMessage<CloseInstanceResponse>, IMessage, IEquatable<CloseInstanceResponse>, IDeepCloneable<CloseInstanceResponse>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Instance.CloseInstanceResponse}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Instance.CloseInstanceResponse}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Instance.CloseInstanceResponse}`
    
    .. cs:field:: public const int InstanceIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public CloseInstanceResponse()
        
        
    .. cs:constructor:: public CloseInstanceResponse(CloseInstanceResponse other)
        
        :param Narupa.Protocol.Instance.CloseInstanceResponse other: 
        
    .. cs:method:: public CloseInstanceResponse Clone()
        
        :returns Narupa.Protocol.Instance.CloseInstanceResponse: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(CloseInstanceResponse other)
        
        :param Narupa.Protocol.Instance.CloseInstanceResponse other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(CloseInstanceResponse other)
        
        :param Narupa.Protocol.Instance.CloseInstanceResponse other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<CloseInstanceResponse> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Instance.CloseInstanceResponse}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string InstanceId { get; set; }
        
        :returns System.String: 
        
