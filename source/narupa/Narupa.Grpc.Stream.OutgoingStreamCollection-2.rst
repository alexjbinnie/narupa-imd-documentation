OutgoingStreamCollection<TOutgoing, TReply>
===========================================
.. cs:setscope:: Narupa.Grpc.Stream

.. cs:class:: public sealed class OutgoingStreamCollection<TOutgoing, TReply> : Cancellable, ICancellable, ICancellationTokenSource, IDisposable, IAsyncClosable
    
    Grouping of several outgoing streams to the same location, each with a string
    identifier. Instead of having to manage an
    :cs:any:`~Narupa.Grpc.Stream.OutgoingStream`2`, a user can use
    :cs:any:`~Narupa.Grpc.Stream.OutgoingStreamCollection`2.StartStream(System.String)`, :cs:any:`~Narupa.Grpc.Stream.OutgoingStreamCollection`2.QueueMessageAsync(System.String,`0)` and
    :cs:any:`~Narupa.Grpc.Stream.OutgoingStreamCollection`2.EndStreamAsync(System.String)`
    to update a uniquely identified stream without maintaining a reference to it.
    
    

    :inherits: :cs:any:`~Narupa.Core.Async.Cancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellationTokenSource`
    :implements: :cs:any:`~System.IDisposable`
    :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable`
    
    .. cs:constructor:: public OutgoingStreamCollection(OutgoingStreamCollection<TOutgoing, TReply>.CreateStreamCall createStream, CancellationToken externalToken = default(CancellationToken))
        
        Create an collection which starts new streams using
        :code:`createStream`
        
        

        :param Narupa.Grpc.Stream.OutgoingStreamCollection`2.CreateStreamCall createStream: 
        :param System.Threading.CancellationToken externalToken: 
        
    .. cs:method:: public bool HasStream(string id)
        
        Returns whether there is an open stream with the given id.
        
        

        :param System.String id: 
        :returns System.Boolean: 
        
    .. cs:method:: public Task StartStream(string id)
        
        Start a new outgoing stream with the provided ID and return the
        task representing the sending of messages over that stream.
        
        

        :param System.String id: 
        :returns System.Threading.Tasks.Task: 
        :throws System.InvalidOperationException: The stream already exists
        
    .. cs:method:: public Task QueueMessageAsync(string id, TOutgoing message)
        
        Send a message using the outgoing stream with the provided ID
        
        

        :param System.String id: 
        :param {TOutgoing} message: 
        :returns System.Threading.Tasks.Task: 
        :throws System.InvalidOperationException: The stream does not exist
        
    .. cs:method:: public Task EndStreamAsync(string id)
        
        Close an outgoing stream with the provided ID
        
        

        :param System.String id: 
        :returns System.Threading.Tasks.Task: 
        :throws System.InvalidOperationException: The stream does not exist
        
    .. cs:method:: public Task CloseAsync()
        
        Close all managed streams and then dispose them.
        
        

        :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable.CloseAsync`
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public override void Dispose()
        
        

        
