IVisualisationComponent<TNode>
==============================
.. cs:setscope:: Narupa.Visualisation.Components

.. cs:interface:: public interface IVisualisationComponent<out TNode>
    
    An object which wraps a node for use in visualisation.
    
    

    
    .. cs:property:: TNode Node { get; }
        
        The node wrapped by this component.
        
        

        :returns {TNode}: 
        
