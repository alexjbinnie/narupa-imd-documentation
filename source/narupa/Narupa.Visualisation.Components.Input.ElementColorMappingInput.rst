ElementColorMappingInput
========================
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class ElementColorMappingInput : VisualisationComponent<ElementColorMappingInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ElementColorMappingInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.ElementColorMappingInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.ElementColorMappingInputNode}`
    
