ValueArray
==========
.. cs:setscope:: Narupa.Protocol

.. cs:class:: public sealed class ValueArray : Object, IMessage<ValueArray>, IMessage, IEquatable<ValueArray>, IDeepCloneable<ValueArray>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.ValueArray}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.ValueArray}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.ValueArray}`
    
    .. cs:field:: public const int FloatValuesFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int IndexValuesFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:field:: public const int StringValuesFieldNumber = 3
        
        :returns System.Int32: 
        
    .. cs:constructor:: public ValueArray()
        
        
    .. cs:constructor:: public ValueArray(ValueArray other)
        
        :param Narupa.Protocol.ValueArray other: 
        
    .. cs:method:: public ValueArray Clone()
        
        :returns Narupa.Protocol.ValueArray: 
        
    .. cs:method:: public void ClearValues()
        
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(ValueArray other)
        
        :param Narupa.Protocol.ValueArray other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(ValueArray other)
        
        :param Narupa.Protocol.ValueArray other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<ValueArray> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.ValueArray}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public FloatArray FloatValues { get; set; }
        
        :returns Narupa.Protocol.FloatArray: 
        
    .. cs:property:: public IndexArray IndexValues { get; set; }
        
        :returns Narupa.Protocol.IndexArray: 
        
    .. cs:property:: public StringArray StringValues { get; set; }
        
        :returns Narupa.Protocol.StringArray: 
        
    .. cs:property:: public ValueArray.ValuesOneofCase ValuesCase { get; }
        
        :returns Narupa.Protocol.ValueArray.ValuesOneofCase: 
        
