IMapping<TFrom, TTo>
====================
.. cs:setscope:: Narupa.Core

.. cs:interface:: public interface IMapping<in TFrom, out TTo>
    
    Defines an interface which provides a mapping between two types. It is
    essentially a :cs:any:`~System.Converter`2` as an interface.
    
    

    
    .. cs:method:: TTo Map(TFrom from)
        
        Map an input value to an output value.
        
        

        :param {TFrom} from: 
        :returns {TTo}: 
        
