PolypeptideCurve
================
.. cs:setscope:: Narupa.Visualisation.Components.Spline

.. cs:class:: public class PolypeptideCurve : VisualisationComponent<PolypeptideCurveNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<PolypeptideCurveNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Spline.PolypeptideCurveNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Spline.PolypeptideCurveNode}`
    
