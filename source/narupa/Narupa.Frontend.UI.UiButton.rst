UiButton
========
.. cs:setscope:: Narupa.Frontend.UI

.. cs:class:: public class UiButton : MonoBehaviour
    
    Component to abstract different button prefabs to give a common interface for
    controlling the icon, label and callback.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:event:: public event Action OnClick
        
        Callback when the button is clicked.
        
        

        :returns System.Action: 
        
    .. cs:property:: public Sprite Image { set; }
        
        The sprite that will display as an icon for the button.
        
        

        :returns UnityEngine.Sprite: 
        
    .. cs:property:: public string Text { set; }
        
        The label of the button.
        
        

        :returns System.String: 
        
    .. cs:property:: public string Subtext { set; }
        
        The sublabel of the button.
        
        

        :returns System.String: 
        
