FrameAdaptorComponent<TAdaptor>
===============================
.. cs:setscope:: Narupa.Visualisation.Components.Adaptor

.. cs:class:: public class FrameAdaptorComponent<TAdaptor> : VisualisationComponent<TAdaptor>, ISerializationCallbackReceiver, IVisualisationComponent<TAdaptor>, IDynamicPropertyProvider, IPropertyProvider where TAdaptor : BaseAdaptorNode, new()
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{{TAdaptor}}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{{TAdaptor}}`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    
    .. cs:property:: public BaseAdaptorNode Adaptor { get; }
        
        The wrapped :cs:any:`~Narupa.Visualisation.Components.Adaptor.FrameAdaptor`.
        
        

        :returns Narupa.Visualisation.Node.Adaptor.BaseAdaptorNode: 
        
    .. cs:method:: protected override void OnEnable()
        
        
    .. cs:method:: public IEnumerable<(string name, Type type)> GetPotentialProperties()
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider.GetPotentialProperties`
        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,System.Type}}: 
        
    .. cs:method:: public override IEnumerable<(string name, IReadOnlyProperty property)> GetProperties()
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider.GetProperties`
        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,Narupa.Visualisation.Property.IReadOnlyProperty}}: 
        
    .. cs:method:: public override IReadOnlyProperty GetProperty(string name)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider.GetProperty(System.String)`
        :param System.String name: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:method:: public IReadOnlyProperty<T> GetOrCreateProperty<T>(string name)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider.GetOrCreateProperty``1(System.String)`
        :param System.String name: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{{T}}: 
        
    .. cs:method:: public bool CanDynamicallyProvideProperty<T>(string name)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider.CanDynamicallyProvideProperty``1(System.String)`
        :param System.String name: 
        :returns System.Boolean: 
        
