PerElementColorNode
===================
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [Serializable] public abstract class PerElementColorNode : VisualiserColorNode
    
    Base code for a Visualiser node which generates colors based upon atomic
    elements.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Color.VisualiserColorNode`
    
    .. cs:property:: public IProperty<Element[]> Elements { get; }
        
        Atomic element array input.
        
        

        :returns Narupa.Visualisation.Property.IProperty{Narupa.Core.Science.Element[]}: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:method:: protected override void UpdateOutput()
        
        

        
    .. cs:method:: protected override void ClearOutput()
        
        

        
    .. cs:method:: protected abstract Color GetColor(Element element)
        
        Get the color for the given atomic element.
        
        

        :param Narupa.Core.Science.Element element: 
        :returns UnityEngine.Color: 
        
