LinkableProperty<TValue>
========================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:class:: public abstract class LinkableProperty<TValue> : Property, IProperty<TValue>, IReadOnlyProperty<TValue>, IProperty, IReadOnlyProperty
    
    Implementation of a property which does not define if it itself provides a value, but allows linking to other properties.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.Property`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{{TValue}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{{TValue}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
    .. cs:property:: protected override IReadOnlyProperty NonGenericLinkedProperty { get; }
        
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:property:: protected override object NonGenericValue { get; }
        
        :returns System.Object: 
        
    .. cs:property:: public override bool HasValue { get; }
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.HasValue`
        :returns System.Boolean: 
        
    .. cs:method:: public override void MarkValueAsChanged()
        
        

        
    .. cs:property:: protected abstract TValue ProvidedValue { get; set; }
        
        :returns {TValue}: 
        
    .. cs:property:: protected abstract bool HasProvidedValue { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: public virtual TValue Value { get; set; }
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{{TValue}}.Value`
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{{TValue}}.Value`
        :returns {TValue}: 
        
    .. cs:property:: public override bool HasLinkedProperty { get; }
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty.HasLinkedProperty`
        :returns System.Boolean: 
        
    .. cs:property:: [CanBeNull] public IReadOnlyProperty<TValue> LinkedProperty { get; set; }
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{{TValue}}.LinkedProperty`
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{{TValue}}: 
        
    .. cs:method:: public override void UndefineValue()
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty.UndefineValue`
        
    .. cs:conversion:: public static implicit operator TValue(LinkableProperty<TValue> property)
        
        Implicit conversion of the property to its value
        
        

        :param Narupa.Visualisation.Property.LinkableProperty`1 property: 
        :returns {TValue}: 
        
    .. cs:property:: public override Type PropertyType { get; }
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.PropertyType`
        :returns System.Type: 
        
    .. cs:method:: public override void TrySetValue(object value)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty.TrySetValue(System.Object)`
        :param System.Object value: 
        
    .. cs:method:: public override void TrySetLinkedProperty(object value)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty.TrySetLinkedProperty(System.Object)`
        :param System.Object value: 
        
