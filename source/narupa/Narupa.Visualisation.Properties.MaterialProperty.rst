MaterialProperty
================
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class MaterialProperty : SerializableProperty<Material>, IProperty<Material>, IReadOnlyProperty<Material>, IProperty, IReadOnlyProperty
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for a :cs:any:`~UnityEngine.Material` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.SerializableProperty{UnityEngine.Material}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{UnityEngine.Material}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{UnityEngine.Material}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
