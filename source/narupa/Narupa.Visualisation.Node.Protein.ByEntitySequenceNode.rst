ByEntitySequenceNode
====================
.. cs:setscope:: Narupa.Visualisation.Node.Protein

.. cs:class:: [Serializable] public class ByEntitySequenceNode : GenericOutputNode
    
    Works out groupings of continuous entity IDs.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:property:: protected override bool IsInputValid { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        
    .. cs:method:: protected override void UpdateOutput()
        
        
    .. cs:method:: protected override void ClearOutput()
        
        
