Vector3ArrayOutputNode
======================
.. cs:setscope:: Narupa.Visualisation.Node.Output

.. cs:class:: [Serializable] public class Vector3ArrayOutputNode : OutputNode<Vector3ArrayProperty>, IOutputNode
    
    :inherits: :cs:any:`~Narupa.Visualisation.Node.Output.OutputNode{Narupa.Visualisation.Properties.Collections.Vector3ArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Output.IOutputNode`
    
