InteractiveMolecularDynamics.InteractiveMolecularDynamicsBase
=============================================================
.. cs:setscope:: Narupa.Protocol.Imd

.. cs:class:: public abstract class InteractiveMolecularDynamicsBase : Object
    
    
    .. cs:method:: public virtual Task<InteractionEndReply> PublishInteraction(IAsyncStreamReader<ParticleInteraction> requestStream, ServerCallContext context)
        
        :param Grpc.Core.IAsyncStreamReader{Narupa.Protocol.Imd.ParticleInteraction} requestStream: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Imd.InteractionEndReply}: 
        
    .. cs:method:: public virtual Task SubscribeInteractions(SubscribeInteractionsRequest request, IServerStreamWriter<InteractionsUpdate> responseStream, ServerCallContext context)
        
        :param Narupa.Protocol.Imd.SubscribeInteractionsRequest request: 
        :param Grpc.Core.IServerStreamWriter{Narupa.Protocol.Imd.InteractionsUpdate} responseStream: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task: 
        
    .. cs:constructor:: protected InteractiveMolecularDynamicsBase()
        
        
