ColorInputNode
==============
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class ColorInputNode : InputNode<ColorProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~Narupa.Visualisation.Node.Color` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.ColorProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
