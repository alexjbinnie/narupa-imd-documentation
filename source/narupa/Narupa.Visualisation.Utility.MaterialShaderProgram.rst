MaterialShaderProgram
=====================
.. cs:setscope:: Narupa.Visualisation.Utility

.. cs:class:: public class MaterialShaderProgram : IGpuRenderProgram, IGpuProgram
    
    A Unity :cs:any:`~UnityEngine.Material` represented as an abstract GPU
    program.
    
    

    :implements: :cs:any:`~Narupa.Visualisation.Utility.IGpuRenderProgram`
    :implements: :cs:any:`~Narupa.Visualisation.Utility.IGpuProgram`
    
    .. cs:property:: public Material Material { get; }
        
        :returns UnityEngine.Material: 
        
    .. cs:constructor:: public MaterialShaderProgram(Material material)
        
        :param UnityEngine.Material material: 
        
    .. cs:method:: public void SetBuffer(string id, ComputeBuffer buffer)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Utility.IGpuProgram.SetBuffer(System.String,UnityEngine.ComputeBuffer)`
        :param System.String id: 
        :param UnityEngine.ComputeBuffer buffer: 
        
    .. cs:method:: public void SetKeyword(string keyword, bool active = true)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Utility.IGpuRenderProgram.SetKeyword(System.String,System.Boolean)`
        :param System.String keyword: 
        :param System.Boolean active: 
        
    .. cs:method:: public void SetFloat(string id, float value)
        
        Set the float value for a shader parameter in the Unity material.
        
        

        :param System.String id: 
        :param System.Single value: 
        
    .. cs:method:: public void SetMatrix(string id, Matrix4x4 value)
        
        Set the matrix value for a shader parameter in the Unity material.
        
        

        :param System.String id: 
        :param UnityEngine.Matrix4x4 value: 
        
    .. cs:method:: public void SetColor(string id, Color value)
        
        Set the color value for a shader parameter in the Unity material.
        
        

        :param System.String id: 
        :param UnityEngine.Color value: 
        
