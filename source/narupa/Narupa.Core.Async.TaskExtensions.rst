TaskExtensions
==============
.. cs:setscope:: Narupa.Core.Async

.. cs:class:: public static class TaskExtensions
    
    Extension methods for :cs:any:`~System.Threading.Tasks.Task`
    
    

    
    .. cs:method:: public static void AwaitInBackground(this Task task)
        
        Await an async task in the background and defer exception handling
        to Unity.
        
        

        Exceptions thrown in this task will appear in the Unity console
        whereas exceptions thrown in unawaited async Tasks are not reliably
        caught.
        
        

        :param System.Threading.Tasks.Task task: 
        
    .. cs:method:: public static void AwaitInBackgroundIgnoreCancellation(this Task task)
        
        Await an async task in the background and defer exception handling
        to Unity, except for exceptions relating to task cancellation which
        are ignore.
        
        

        Exceptions thrown in this task will appear in the Unity console
        whereas exceptions thrown in unawaited async Tasks are not reliably
        caught.
        
        

        :param System.Threading.Tasks.Task task: 
        
