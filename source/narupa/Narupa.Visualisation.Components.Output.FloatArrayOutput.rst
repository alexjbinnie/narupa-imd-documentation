FloatArrayOutput
================
.. cs:setscope:: Narupa.Visualisation.Components.Output

.. cs:class:: public class FloatArrayOutput : VisualisationComponent<FloatArrayOutputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<FloatArrayOutputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Output.FloatArrayOutputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Output.FloatArrayOutputNode}`
    
