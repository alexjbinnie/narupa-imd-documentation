GetFrameResponse
================
.. cs:setscope:: Narupa.Protocol.Trajectory

.. cs:class:: public sealed class GetFrameResponse : Object, IMessage<GetFrameResponse>, IMessage, IEquatable<GetFrameResponse>, IDeepCloneable<GetFrameResponse>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Trajectory.GetFrameResponse}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Trajectory.GetFrameResponse}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Trajectory.GetFrameResponse}`
    
    .. cs:field:: public const int FrameIndexFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int FrameFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public GetFrameResponse()
        
        
    .. cs:constructor:: public GetFrameResponse(GetFrameResponse other)
        
        :param Narupa.Protocol.Trajectory.GetFrameResponse other: 
        
    .. cs:method:: public GetFrameResponse Clone()
        
        :returns Narupa.Protocol.Trajectory.GetFrameResponse: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(GetFrameResponse other)
        
        :param Narupa.Protocol.Trajectory.GetFrameResponse other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(GetFrameResponse other)
        
        :param Narupa.Protocol.Trajectory.GetFrameResponse other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<GetFrameResponse> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Trajectory.GetFrameResponse}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public uint FrameIndex { get; set; }
        
        :returns System.UInt32: 
        
    .. cs:property:: public FrameData Frame { get; set; }
        
        :returns Narupa.Protocol.Trajectory.FrameData: 
        
