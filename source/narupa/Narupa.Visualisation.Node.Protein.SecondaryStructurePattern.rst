SecondaryStructurePattern
=========================
.. cs:setscope:: Narupa.Visualisation.Node.Protein

.. cs:enum:: [Flags] public enum SecondaryStructurePattern
    
    Patterns that occur in protein secondary structure.
    
    

    
    .. cs:enummember:: None = 0
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructurePattern: 
        
    .. cs:enummember:: ThreeTurn = 1
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructurePattern: 
        
    .. cs:enummember:: FourTurn = 2
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructurePattern: 
        
    .. cs:enummember:: FiveTurn = 4
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructurePattern: 
        
    .. cs:enummember:: ParallelBridge = 8
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructurePattern: 
        
    .. cs:enummember:: AntiparallelBridge = 16
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructurePattern: 
        
    .. cs:enummember:: Bridge = 24
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructurePattern: 
        
    .. cs:enummember:: Turn = 7
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructurePattern: 
        
