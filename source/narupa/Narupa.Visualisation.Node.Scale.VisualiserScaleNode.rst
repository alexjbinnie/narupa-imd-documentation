VisualiserScaleNode
===================
.. cs:setscope:: Narupa.Visualisation.Node.Scale

.. cs:class:: [Serializable] public abstract class VisualiserScaleNode : GenericOutputNode
    
    Base code for visualiser node which generates a set of scales.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:field:: protected readonly FloatArrayProperty scales
        
        :returns Narupa.Visualisation.Properties.Collections.FloatArrayProperty: 
        
    .. cs:property:: public IReadOnlyProperty<float[]> Scales { get; }
        
        Scale array output.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Single[]}: 
        
