IGpuRenderProgram
=================
.. cs:setscope:: Narupa.Visualisation.Utility

.. cs:interface:: public interface IGpuRenderProgram : IGpuProgram
    
    A GPU program that also supports keywords that modify how the shader works
    
    

    
    .. cs:method:: void SetKeyword(string keyword, bool enabled = true)
        
        Set a keyword on the material
        
        

        :param System.String keyword: 
        :param System.Boolean enabled: 
        
