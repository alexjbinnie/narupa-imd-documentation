GetFrameReflection
==================
.. cs:setscope:: Narupa.Protocol.Trajectory

.. cs:class:: public static class GetFrameReflection : Object
    
    
    .. cs:property:: public static FileDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.FileDescriptor: 
        
