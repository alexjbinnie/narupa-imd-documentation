CifUtility
==========
.. cs:setscope:: Narupa.Frame.Import.CIF

.. cs:class:: public static class CifUtility
    
    
    .. cs:method:: public static string GetCategory(string line)
        
        Get the category from a data item line.
        
        

        :param System.String line: 
        :returns System.String: 
        
    .. cs:method:: public static (string Category, string Keyword, string value) GetCategoryAndKeyword(string line)
        
        Get the category, keyword and value from a data item line.
        
        

        :param System.String line: 
        :returns System.ValueTuple{System.String,System.String,System.String}: 
        
