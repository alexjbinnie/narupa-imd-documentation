ElementArrayInput
=================
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class ElementArrayInput : VisualisationComponent<ElementArrayInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ElementArrayInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.ElementArrayInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.ElementArrayInputNode}`
    
