ITransformation
===============
.. cs:setscope:: Narupa.Core.Math

.. cs:interface:: public interface ITransformation
    
    
    .. cs:property:: ITransformation inverse { get; }
        
        The inverse of this transformation, which undoes this transformation.
        
        

        :returns Narupa.Core.Math.ITransformation: 
        
    .. cs:property:: Matrix4x4 matrix { get; }
        
        The 4x4 augmented matrix representing this transformation as it acts upon
        vectors and directions in homogeneous coordinates.
        
        

        :returns UnityEngine.Matrix4x4: 
        
    .. cs:property:: Matrix4x4 inverseMatrix { get; }
        
        The 4x4 augmented matrix representing the inverse of this transformation as it
        acts upon vectors and directions in homogeneous coordinates.
        
        

        :returns UnityEngine.Matrix4x4: 
        
    .. cs:method:: Vector3 TransformPoint(Vector3 point)
        
        Transform a point in space using this transformation. When considered as a
        transformation from an object's local space to world space, this takes points
        in the object's local space to world space.
        
        

        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: Vector3 InverseTransformPoint(Vector3 point)
        
        Transform a point in space using the inverse of this transformation. When
        considered as a transformation from an object's local space to world space,
        this takes points in world space to the object's local space.
        
        

        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: Vector3 TransformDirection(Vector3 direction)
        
        Transform a direction in space using this transformation. When considered as a
        transformation from an object's local space to world space, this takes
        directions in the object's local space to world space.
        
        

        :param UnityEngine.Vector3 direction: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: Vector3 InverseTransformDirection(Vector3 direction)
        
        Transform a direction in space using the inverse of this transformation. When
        considered as a transformation from an object's local space to world space,
        this takes directions in world space to the object's local space.
        
        

        :param UnityEngine.Vector3 direction: 
        :returns UnityEngine.Vector3: 
        
