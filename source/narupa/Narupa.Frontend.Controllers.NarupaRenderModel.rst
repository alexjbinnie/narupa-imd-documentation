NarupaRenderModel
=================
.. cs:setscope:: Narupa.Frontend.Controllers

.. cs:class:: [RequireComponent(typeof(SteamVR_RenderModel))] public class NarupaRenderModel : MonoBehaviour
    
    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:method:: public void SetColor(Color color)
        
        Set the color of the current controller. The alpha channel of this color is used
        to fade out a controller.
        
        

        :param UnityEngine.Color color: 
        
