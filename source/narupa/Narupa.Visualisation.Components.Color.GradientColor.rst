GradientColor
=============
.. cs:setscope:: Narupa.Visualisation.Components.Color

.. cs:class:: public sealed class GradientColor : VisualisationComponent<GradientColorNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<GradientColorNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Color.GradientColorNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Color.GradientColorNode}`
    
