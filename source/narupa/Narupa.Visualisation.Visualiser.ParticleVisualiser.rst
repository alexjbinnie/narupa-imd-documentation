ParticleVisualiser
==================
.. cs:setscope:: Narupa.Visualisation.Visualiser

.. cs:class:: [ExecuteAlways] public class ParticleVisualiser : MonoBehaviour, IFrameConsumer, ISerializationCallbackReceiver
    
    Simple integrated visualiser, with several visualisation nodes embedded into
    the same :cs:any:`~UnityEngine.MonoBehaviour` to render a sphere-and-bond
    representation of a frame.
    
    

    The five visualisation nodes that make up this visualisation are serialized by
    the Unity editor.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    :implements: :cs:any:`~Narupa.Frame.IFrameConsumer`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    
    .. cs:property:: public IProperty<Material> ParticleMaterial { get; }
        
        The material used by the visualiser for particles.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Material}: 
        
    .. cs:property:: public IProperty<Material> BondMaterial { get; }
        
        The material used by the visualiser for bonds.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Material}: 
        
    .. cs:property:: public IProperty<Mesh> ParticleMesh { get; }
        
        The mesh used by the visualiser for particles.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Mesh}: 
        
    .. cs:property:: public IProperty<Mesh> BondMesh { get; }
        
        The mesh used by the visualiser for bonds.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Mesh}: 
        
    .. cs:property:: public IProperty<Color> Tint { get; }
        
        Tint applied to the color of particles and bonds.
        
        

        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Color}: 
        
    .. cs:property:: public IProperty<IMapping<Element, Color>> ColorScheme { get; }
        
        Per-element color scheme for coloring atoms.
        
        

        :returns Narupa.Visualisation.Property.IProperty{Narupa.Core.IMapping{Narupa.Core.Science.Element,UnityEngine.Color}}: 
        
    .. cs:property:: public IProperty<float> ParticleScale { get; }
        
        Multiplier for the scale of the particles.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Single}: 
        
    .. cs:property:: public IProperty<bool> UseVdwRadii { get; }
        
        Should this use VdW radii?
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Boolean}: 
        
    .. cs:property:: public IProperty<float> BondScale { get; }
        
        Multiplier for the scale of the bonds.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Single}: 
        
    .. cs:property:: public ITrajectorySnapshot FrameSource { set; }
        
        

        :implements: :cs:any:`~Narupa.Frame.IFrameConsumer.FrameSource`
        :returns Narupa.Frame.ITrajectorySnapshot: 
        
    .. cs:method:: public void OnBeforeSerialize()
        
        :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize`
        
    .. cs:method:: public void OnAfterDeserialize()
        
        :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize`
        
