SecondaryStructureAssignment
============================
.. cs:setscope:: Narupa.Visualisation.Node.Protein

.. cs:enum:: [Flags] public enum SecondaryStructureAssignment
    
    
    .. cs:enummember:: None = 0
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
    .. cs:enummember:: AlphaHelix = 1
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
    .. cs:enummember:: ThreeTenHelix = 2
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
    .. cs:enummember:: PiHelix = 4
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
    .. cs:enummember:: Sheet = 8
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
    .. cs:enummember:: Bridge = 16
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
    .. cs:enummember:: Turn = 32
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
    .. cs:enummember:: Bend = 64
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
    .. cs:enummember:: Helix = 7
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
    .. cs:enummember:: Strand = 24
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
    .. cs:enummember:: Loop = 96
        
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
