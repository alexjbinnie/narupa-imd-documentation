Avatar
======
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class Avatar : Object, IMessage<Avatar>, IMessage, IEquatable<Avatar>, IDeepCloneable<Avatar>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.Avatar}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.Avatar}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.Avatar}`
    
    .. cs:field:: public const int PlayerIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int ComponentsFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public Avatar()
        
        
    .. cs:constructor:: public Avatar(Avatar other)
        
        :param Narupa.Protocol.Multiplayer.Avatar other: 
        
    .. cs:method:: public Avatar Clone()
        
        :returns Narupa.Protocol.Multiplayer.Avatar: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(Avatar other)
        
        :param Narupa.Protocol.Multiplayer.Avatar other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(Avatar other)
        
        :param Narupa.Protocol.Multiplayer.Avatar other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<Avatar> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.Avatar}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string PlayerId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public RepeatedField<AvatarComponent> Components { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{Narupa.Protocol.Multiplayer.AvatarComponent}: 
        
