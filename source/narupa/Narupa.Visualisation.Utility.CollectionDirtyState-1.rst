CollectionDirtyState<T>
=======================
.. cs:setscope:: Narupa.Visualisation.Utility

.. cs:class:: public class CollectionDirtyState<T> : IReadOnlyDictionary<T, bool>, IReadOnlyCollection<KeyValuePair<T, bool>>, IEnumerable<KeyValuePair<T, bool>>, IEnumerable
    
    Tracks the dirty state of an :cs:any:`~System.Collections.IEnumerable`, by tracking items
    which have changed since the last
    time the dirty state of this object was reset. If the provided collection
    implements
    :cs:any:`~System.Collections.Specialized.INotifyCollectionChanged`, it will automatically use
    :cs:any:`~System.Collections.Specialized.INotifyCollectionChanged.CollectionChanged` to update the dirty
    state.
    
    

    The dirty items are stored in no particular order.
    
    

    :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{{T},System.Boolean}`
    :implements: :cs:any:`~System.Collections.Generic.IReadOnlyCollection{System.Collections.Generic.KeyValuePair{{T},System.Boolean}}`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Collections.Generic.KeyValuePair{{T},System.Boolean}}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
    .. cs:constructor:: public CollectionDirtyState([NotNull] ICollection<T> collection)
        
        :param System.Collections.Generic.ICollection{{T}} collection: 
        
    .. cs:property:: public IReadOnlyCollection<T> DirtyItems { get; }
        
        Collection of dirty items.
        
        

        :returns System.Collections.Generic.IReadOnlyCollection{{T}}: 
        
    .. cs:method:: public IEnumerator<KeyValuePair<T, bool>> GetEnumerator()
        
        

        :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Collections.Generic.KeyValuePair{{T},System.Boolean}}.GetEnumerator`
        :returns System.Collections.Generic.IEnumerator{System.Collections.Generic.KeyValuePair{{T},System.Boolean}}: 
        
    .. cs:method:: IEnumerator IEnumerable.GetEnumerator()
        
        

        :implements: :cs:any:`~System.Collections.IEnumerable.GetEnumerator`
        :returns System.Collections.IEnumerator: 
        
    .. cs:property:: public int Count { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyCollection{System.Collections.Generic.KeyValuePair{{T},System.Boolean}}.Count`
        :returns System.Int32: 
        
    .. cs:method:: public bool ContainsKey(T key)
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{{T},System.Boolean}.ContainsKey({T})`
        :param {T} key: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool TryGetValue(T key, out bool value)
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{{T},System.Boolean}.TryGetValue({T},System.Boolean@)`
        :param {T} key: 
        :param System.Boolean value: 
        :returns System.Boolean: 
        
    .. cs:indexer:: public bool this[T key] { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{{T},System.Boolean}.Item({T})`
        :param {T} key: 
        :returns System.Boolean: 
        
    .. cs:property:: public IEnumerable<T> Keys { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{{T},System.Boolean}.Keys`
        :returns System.Collections.Generic.IEnumerable{{T}}: 
        
    .. cs:property:: public IEnumerable<bool> Values { get; }
        
        

        :implements: :cs:any:`~System.Collections.Generic.IReadOnlyDictionary{{T},System.Boolean}.Values`
        :returns System.Collections.Generic.IEnumerable{System.Boolean}: 
        
    .. cs:method:: protected void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        
        Handler for an :cs:any:`~System.Collections.Specialized.INotifyCollectionChanged`, where additions,
        removals, replacements and
        resets all result in the dirty state being updated.
        
        

        :param System.Object sender: 
        :param System.Collections.Specialized.NotifyCollectionChangedEventArgs e: 
        
    .. cs:method:: public bool IsDirty(T item)
        
        Has :code:`item` been altered since the last time its dirty state
        was reset.
        
        

        :param {T} item: 
        :returns System.Boolean: 
        
    .. cs:method:: public void MarkDirty(T item)
        
        Marks :code:`item` as being dirty.
        
        

        :param {T} item: 
        
    .. cs:method:: public void SetDirty(T item, bool dirty)
        
        Set the dirty state of :code:`item` to be :code:`dirty`
        .
        
        

        :param {T} item: 
        :param System.Boolean dirty: 
        
    .. cs:method:: public void ClearDirty(T item)
        
        Reset the dirty state of :code:`item`.
        
        

        :param {T} item: 
        
    .. cs:method:: public void ClearAllDirty()
        
        Reset the dirty state of all items.
        
        

        
