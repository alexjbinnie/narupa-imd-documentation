CommandServiceReflection
========================
.. cs:setscope:: Narupa.Protocol.Command

.. cs:class:: public static class CommandServiceReflection : Object
    
    
    .. cs:property:: public static FileDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.FileDescriptor: 
        
