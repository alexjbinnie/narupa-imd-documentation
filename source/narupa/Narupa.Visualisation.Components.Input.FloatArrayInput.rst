FloatArrayInput
===============
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class FloatArrayInput : VisualisationComponent<FloatArrayInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<FloatArrayInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.FloatArrayInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.FloatArrayInputNode}`
    
