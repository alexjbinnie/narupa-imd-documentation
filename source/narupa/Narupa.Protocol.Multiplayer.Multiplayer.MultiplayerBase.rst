Multiplayer.MultiplayerBase
===========================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public abstract class MultiplayerBase : Object
    
    
    .. cs:method:: public virtual Task SubscribePlayerAvatars(SubscribePlayerAvatarsRequest request, IServerStreamWriter<Avatar> responseStream, ServerCallContext context)
        
        :param Narupa.Protocol.Multiplayer.SubscribePlayerAvatarsRequest request: 
        :param Grpc.Core.IServerStreamWriter{Narupa.Protocol.Multiplayer.Avatar} responseStream: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public virtual Task SubscribeAllResourceValues(SubscribeAllResourceValuesRequest request, IServerStreamWriter<ResourceValuesUpdate> responseStream, ServerCallContext context)
        
        :param Narupa.Protocol.Multiplayer.SubscribeAllResourceValuesRequest request: 
        :param Grpc.Core.IServerStreamWriter{Narupa.Protocol.Multiplayer.ResourceValuesUpdate} responseStream: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public virtual Task<CreatePlayerResponse> CreatePlayer(CreatePlayerRequest request, ServerCallContext context)
        
        :param Narupa.Protocol.Multiplayer.CreatePlayerRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Multiplayer.CreatePlayerResponse}: 
        
    .. cs:method:: public virtual Task<StreamEndedResponse> UpdatePlayerAvatar(IAsyncStreamReader<Avatar> requestStream, ServerCallContext context)
        
        :param Grpc.Core.IAsyncStreamReader{Narupa.Protocol.Multiplayer.Avatar} requestStream: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Multiplayer.StreamEndedResponse}: 
        
    .. cs:method:: public virtual Task<ResourceRequestResponse> AcquireResourceLock(AcquireLockRequest request, ServerCallContext context)
        
        :param Narupa.Protocol.Multiplayer.AcquireLockRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:method:: public virtual Task<ResourceRequestResponse> ReleaseResourceLock(ReleaseLockRequest request, ServerCallContext context)
        
        :param Narupa.Protocol.Multiplayer.ReleaseLockRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:method:: public virtual Task<ResourceRequestResponse> SetResourceValue(SetResourceValueRequest request, ServerCallContext context)
        
        :param Narupa.Protocol.Multiplayer.SetResourceValueRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:method:: public virtual Task<ResourceRequestResponse> RemoveResourceKey(RemoveResourceKeyRequest request, ServerCallContext context)
        
        :param Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest request: 
        :param Grpc.Core.ServerCallContext context: 
        :returns System.Threading.Tasks.Task{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:constructor:: protected MultiplayerBase()
        
        
