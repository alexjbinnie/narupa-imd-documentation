InstanceService.InstanceServiceClient
=====================================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public class InstanceServiceClient : ClientBase<InstanceService.InstanceServiceClient>
    
    
    .. cs:constructor:: public InstanceServiceClient(Channel channel)
        
        :param Grpc.Core.Channel channel: 
        
    .. cs:constructor:: public InstanceServiceClient(CallInvoker callInvoker)
        
        :param Grpc.Core.CallInvoker callInvoker: 
        
    .. cs:constructor:: protected InstanceServiceClient()
        
        
    .. cs:constructor:: protected InstanceServiceClient(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        
    .. cs:method:: public virtual LoadTrajectoryResponse LoadTrajectory(LoadTrajectoryRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Instance.LoadTrajectoryRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Instance.LoadTrajectoryResponse: 
        
    .. cs:method:: public virtual LoadTrajectoryResponse LoadTrajectory(LoadTrajectoryRequest request, CallOptions options)
        
        :param Narupa.Protocol.Instance.LoadTrajectoryRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Instance.LoadTrajectoryResponse: 
        
    .. cs:method:: public virtual AsyncUnaryCall<LoadTrajectoryResponse> LoadTrajectoryAsync(LoadTrajectoryRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Instance.LoadTrajectoryRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Instance.LoadTrajectoryResponse}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<LoadTrajectoryResponse> LoadTrajectoryAsync(LoadTrajectoryRequest request, CallOptions options)
        
        :param Narupa.Protocol.Instance.LoadTrajectoryRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Instance.LoadTrajectoryResponse}: 
        
    .. cs:method:: public virtual ConnectToTrajectoryResponse ConnectToTrajectory(ConnectToTrajectoryRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param ConnectToTrajectoryRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns ConnectToTrajectoryResponse: 
        
    .. cs:method:: public virtual ConnectToTrajectoryResponse ConnectToTrajectory(ConnectToTrajectoryRequest request, CallOptions options)
        
        :param ConnectToTrajectoryRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns ConnectToTrajectoryResponse: 
        
    .. cs:method:: public virtual AsyncUnaryCall<ConnectToTrajectoryResponse> ConnectToTrajectoryAsync(ConnectToTrajectoryRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param ConnectToTrajectoryRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{ConnectToTrajectoryResponse}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<ConnectToTrajectoryResponse> ConnectToTrajectoryAsync(ConnectToTrajectoryRequest request, CallOptions options)
        
        :param ConnectToTrajectoryRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{ConnectToTrajectoryResponse}: 
        
    .. cs:method:: public virtual GetInstanceInfoResponse GetInstanceInfo(GetInstanceInfoRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Instance.GetInstanceInfoRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Instance.GetInstanceInfoResponse: 
        
    .. cs:method:: public virtual GetInstanceInfoResponse GetInstanceInfo(GetInstanceInfoRequest request, CallOptions options)
        
        :param Narupa.Protocol.Instance.GetInstanceInfoRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Instance.GetInstanceInfoResponse: 
        
    .. cs:method:: public virtual AsyncUnaryCall<GetInstanceInfoResponse> GetInstanceInfoAsync(GetInstanceInfoRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Instance.GetInstanceInfoRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Instance.GetInstanceInfoResponse}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<GetInstanceInfoResponse> GetInstanceInfoAsync(GetInstanceInfoRequest request, CallOptions options)
        
        :param Narupa.Protocol.Instance.GetInstanceInfoRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Instance.GetInstanceInfoResponse}: 
        
    .. cs:method:: public virtual CloseInstanceResponse CloseInstance(CloseInstanceRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Instance.CloseInstanceRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Instance.CloseInstanceResponse: 
        
    .. cs:method:: public virtual CloseInstanceResponse CloseInstance(CloseInstanceRequest request, CallOptions options)
        
        :param Narupa.Protocol.Instance.CloseInstanceRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Instance.CloseInstanceResponse: 
        
    .. cs:method:: public virtual AsyncUnaryCall<CloseInstanceResponse> CloseInstanceAsync(CloseInstanceRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Instance.CloseInstanceRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Instance.CloseInstanceResponse}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<CloseInstanceResponse> CloseInstanceAsync(CloseInstanceRequest request, CallOptions options)
        
        :param Narupa.Protocol.Instance.CloseInstanceRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Instance.CloseInstanceResponse}: 
        
    .. cs:method:: protected override InstanceService.InstanceServiceClient NewInstance(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        :returns Narupa.Protocol.Instance.InstanceService.InstanceServiceClient: 
        
