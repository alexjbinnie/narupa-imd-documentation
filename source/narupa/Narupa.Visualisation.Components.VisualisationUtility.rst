VisualisationUtility
====================
.. cs:setscope:: Narupa.Visualisation.Components

.. cs:class:: public class VisualisationUtility
    
    
    .. cs:method:: public static IEnumerable<(string, IReadOnlyProperty)> GetAllPropertyFields(object obj)
        
        Get all fields on an object which are visualisation properties.
        
        

        :param System.Object obj: 
        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,Narupa.Visualisation.Property.IReadOnlyProperty}}: 
        
