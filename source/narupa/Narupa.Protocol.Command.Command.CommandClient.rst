Command.CommandClient
=====================
.. cs:setscope:: Narupa.Protocol.Command

.. cs:class:: public class CommandClient : ClientBase<Command.CommandClient>
    
    
    .. cs:constructor:: public CommandClient(Channel channel)
        
        :param Grpc.Core.Channel channel: 
        
    .. cs:constructor:: public CommandClient(CallInvoker callInvoker)
        
        :param Grpc.Core.CallInvoker callInvoker: 
        
    .. cs:constructor:: protected CommandClient()
        
        
    .. cs:constructor:: protected CommandClient(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        
    .. cs:method:: public virtual GetCommandsReply GetCommands(GetCommandsRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Command.GetCommandsRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Command.GetCommandsReply: 
        
    .. cs:method:: public virtual GetCommandsReply GetCommands(GetCommandsRequest request, CallOptions options)
        
        :param Narupa.Protocol.Command.GetCommandsRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Command.GetCommandsReply: 
        
    .. cs:method:: public virtual AsyncUnaryCall<GetCommandsReply> GetCommandsAsync(GetCommandsRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Command.GetCommandsRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Command.GetCommandsReply}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<GetCommandsReply> GetCommandsAsync(GetCommandsRequest request, CallOptions options)
        
        :param Narupa.Protocol.Command.GetCommandsRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Command.GetCommandsReply}: 
        
    .. cs:method:: public virtual CommandReply RunCommand(CommandMessage request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Command.CommandMessage request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Narupa.Protocol.Command.CommandReply: 
        
    .. cs:method:: public virtual CommandReply RunCommand(CommandMessage request, CallOptions options)
        
        :param Narupa.Protocol.Command.CommandMessage request: 
        :param Grpc.Core.CallOptions options: 
        :returns Narupa.Protocol.Command.CommandReply: 
        
    .. cs:method:: public virtual AsyncUnaryCall<CommandReply> RunCommandAsync(CommandMessage request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Command.CommandMessage request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Command.CommandReply}: 
        
    .. cs:method:: public virtual AsyncUnaryCall<CommandReply> RunCommandAsync(CommandMessage request, CallOptions options)
        
        :param Narupa.Protocol.Command.CommandMessage request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncUnaryCall{Narupa.Protocol.Command.CommandReply}: 
        
    .. cs:method:: protected override Command.CommandClient NewInstance(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        :returns Narupa.Protocol.Command.Command.CommandClient: 
        
