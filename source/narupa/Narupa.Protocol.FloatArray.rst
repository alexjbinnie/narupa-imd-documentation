FloatArray
==========
.. cs:setscope:: Narupa.Protocol

.. cs:class:: public sealed class FloatArray : Object, IMessage<FloatArray>, IMessage, IEquatable<FloatArray>, IDeepCloneable<FloatArray>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.FloatArray}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.FloatArray}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.FloatArray}`
    
    .. cs:field:: public const int ValuesFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public FloatArray()
        
        
    .. cs:constructor:: public FloatArray(FloatArray other)
        
        :param Narupa.Protocol.FloatArray other: 
        
    .. cs:method:: public FloatArray Clone()
        
        :returns Narupa.Protocol.FloatArray: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(FloatArray other)
        
        :param Narupa.Protocol.FloatArray other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(FloatArray other)
        
        :param Narupa.Protocol.FloatArray other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<FloatArray> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.FloatArray}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public RepeatedField<float> Values { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{System.Single}: 
        
