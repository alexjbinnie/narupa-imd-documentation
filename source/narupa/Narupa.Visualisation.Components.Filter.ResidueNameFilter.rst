ResidueNameFilter
=================
.. cs:setscope:: Narupa.Visualisation.Components.Filter

.. cs:class:: public sealed class ResidueNameFilter : VisualisationComponent<ResidueNameFilterNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ResidueNameFilterNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Filter.ResidueNameFilterNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Filter.ResidueNameFilterNode}`
    
