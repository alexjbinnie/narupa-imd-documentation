INotifyCollectionChangedExtensions
==================================
.. cs:setscope:: Narupa.Core.Collections

.. cs:class:: public static class INotifyCollectionChangedExtensions
    
    
    .. cs:method:: public static (IEnumerable<TValue> changes, IEnumerable<TValue> removals) AsChangesAndRemovals<TValue>(this NotifyCollectionChangedEventArgs args)
        
        Expresses a :cs:any:`~System.Collections.Specialized.NotifyCollectionChangedEventArgs` as a set of changed
        values and a set of removed values.
        
        

        :param System.Collections.Specialized.NotifyCollectionChangedEventArgs args: 
        :returns System.ValueTuple{System.Collections.Generic.IEnumerable{{TValue}},System.Collections.Generic.IEnumerable{{TValue}}}: 
        
