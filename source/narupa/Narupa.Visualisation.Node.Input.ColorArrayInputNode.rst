ColorArrayInputNode
===================
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class ColorArrayInputNode : InputNode<ColorArrayProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~Color[]` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.Collections.ColorArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
