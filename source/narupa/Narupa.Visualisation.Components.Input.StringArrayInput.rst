StringArrayInput
================
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class StringArrayInput : VisualisationComponent<StringArrayInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<StringArrayInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.StringArrayInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.StringArrayInputNode}`
    
