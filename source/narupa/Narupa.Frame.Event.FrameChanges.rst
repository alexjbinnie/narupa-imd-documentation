FrameChanges
============
.. cs:setscope:: Narupa.Frame.Event

.. cs:class:: public class FrameChanges
    
    A record of known changes to a Narupa frame. Everything is assumed
    unchanged unless explicitly set.
    
    

    
    .. cs:property:: public static FrameChanges None { get; }
        
        A :cs:any:`~Narupa.Frame.Event.FrameChanges` where no key is marked as having changed.
        
        

        :returns Narupa.Frame.Event.FrameChanges: 
        
    .. cs:property:: public static FrameChanges All { get; }
        
        A :cs:any:`~Narupa.Frame.Event.FrameChanges` where all keys are marked as having changed.
        
        

        :returns Narupa.Frame.Event.FrameChanges: 
        
    .. cs:method:: public static FrameChanges WithChanges(params string[] keys)
        
        A :cs:any:`~Narupa.Frame.Event.FrameChanges` where only the provided keys are marked as having changed.
        
        

        :param System.String[] keys: 
        :returns Narupa.Frame.Event.FrameChanges: 
        
    .. cs:property:: public bool HasAnythingChanged { get; }
        
        Indicates whether any keys have been changed in comparison to a
        previous frame.
        
        

        :returns System.Boolean: 
        
    .. cs:method:: public void MergeChanges(FrameChanges otherChanges)
        
        Merge another :cs:any:`~Narupa.Frame.Event.FrameChanges`, such that the update
        state reflects the combination of the two.
        
        

        :param Narupa.Frame.Event.FrameChanges otherChanges: 
        
    .. cs:method:: public bool HasChanged(string id)
        
        Check if the field with the given id as having been changed from the previous
        frame.
        
        

        :param System.String id: 
        :returns System.Boolean: 
        
    .. cs:method:: public void MarkAsChanged(string id)
        
        Mark the field with the given id as having been changed from the previous
        frame.
        
        

        :param System.String id: 
        
