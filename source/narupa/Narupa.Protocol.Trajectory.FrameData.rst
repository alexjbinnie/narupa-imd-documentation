FrameData
=========
.. cs:setscope:: Narupa.Protocol.Trajectory

.. cs:class:: public sealed class FrameData : Object, IEnumerable, IMessage<FrameData>, IMessage, IEquatable<FrameData>, IDeepCloneable<FrameData>
    
    :implements: :cs:any:`~System.Collections.IEnumerable`
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Trajectory.FrameData}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Trajectory.FrameData}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Trajectory.FrameData}`
    
    .. cs:field:: public const string BondArrayKey = "bond.pairs"
        
        :returns System.String: 
        
    .. cs:field:: public const string BondOrderArrayKey = "bond.orders"
        
        :returns System.String: 
        
    .. cs:field:: public const string ParticlePositionArrayKey = "particle.positions"
        
        :returns System.String: 
        
    .. cs:field:: public const string ParticleElementArrayKey = "particle.elements"
        
        :returns System.String: 
        
    .. cs:field:: public const string ParticleTypeArrayKey = "particle.types"
        
        :returns System.String: 
        
    .. cs:field:: public const string ParticleNameArrayKey = "particle.names"
        
        :returns System.String: 
        
    .. cs:field:: public const string ParticleResidueArrayKey = "particle.residues"
        
        :returns System.String: 
        
    .. cs:field:: public const string ParticleCountValueKey = "particle.count"
        
        :returns System.String: 
        
    .. cs:field:: public const string ResidueNameArrayKey = "residue.names"
        
        :returns System.String: 
        
    .. cs:field:: public const string ResidueIdArrayKey = "residue.ids"
        
        :returns System.String: 
        
    .. cs:field:: public const string ResidueChainArrayKey = "residue.chains"
        
        :returns System.String: 
        
    .. cs:field:: public const string ResidueCountValueKey = "residue.count"
        
        :returns System.String: 
        
    .. cs:field:: public const string ChainNameArrayKey = "chain.names"
        
        :returns System.String: 
        
    .. cs:field:: public const string ChainCountValueKey = "chain.count"
        
        :returns System.String: 
        
    .. cs:field:: public const string KineticEnergyValueKey = "energy.kinetic"
        
        :returns System.String: 
        
    .. cs:field:: public const string PotentialEnergyValueKey = "energy.potential"
        
        :returns System.String: 
        
    .. cs:field:: public const int ValuesFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int ArraysFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:method:: public IEnumerator GetEnumerator()
        
        :returns System.Collections.IEnumerator: 
        
    .. cs:method:: public bool TryGetIndexArray(string id, out IReadOnlyList<uint> value)
        
        :param System.String id: 
        :param System.Collections.Generic.IReadOnlyList{System.UInt32} value: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool TryGetFloatArray(string id, out IReadOnlyList<float> value)
        
        :param System.String id: 
        :param System.Collections.Generic.IReadOnlyList{System.Single} value: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool TryGetStringArray(string id, out IReadOnlyList<string> value)
        
        :param System.String id: 
        :param System.Collections.Generic.IReadOnlyList{System.String} value: 
        :returns System.Boolean: 
        
    .. cs:method:: public void Add(string id, object item)
        
        :param System.String id: 
        :param System.Object item: 
        
    .. cs:method:: public void AddFloatArray(string id, IEnumerable<float> array)
        
        :param System.String id: 
        :param System.Collections.Generic.IEnumerable{System.Single} array: 
        
    .. cs:method:: public void AddIndexArray(string id, IEnumerable<uint> array)
        
        :param System.String id: 
        :param System.Collections.Generic.IEnumerable{System.UInt32} array: 
        
    .. cs:method:: public void AddStringArray(string id, IEnumerable<string> array)
        
        :param System.String id: 
        :param System.Collections.Generic.IEnumerable{System.String} array: 
        
    .. cs:method:: public void AddNumericValue(string id, double value)
        
        :param System.String id: 
        :param System.Double value: 
        
    .. cs:method:: public bool TryGetNumericValue(string id, out double value)
        
        :param System.String id: 
        :param System.Double value: 
        :returns System.Boolean: 
        
    .. cs:constructor:: public FrameData()
        
        
    .. cs:constructor:: public FrameData(FrameData other)
        
        :param Narupa.Protocol.Trajectory.FrameData other: 
        
    .. cs:method:: public FrameData Clone()
        
        :returns Narupa.Protocol.Trajectory.FrameData: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(FrameData other)
        
        :param Narupa.Protocol.Trajectory.FrameData other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(FrameData other)
        
        :param Narupa.Protocol.Trajectory.FrameData other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<FrameData> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Trajectory.FrameData}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public MapField<string, Value> Values { get; }
        
        :returns Google.Protobuf.Collections.MapField{System.String,Google.Protobuf.WellKnownTypes.Value}: 
        
    .. cs:property:: public MapField<string, ValueArray> Arrays { get; }
        
        :returns Google.Protobuf.Collections.MapField{System.String,Narupa.Protocol.ValueArray}: 
        
