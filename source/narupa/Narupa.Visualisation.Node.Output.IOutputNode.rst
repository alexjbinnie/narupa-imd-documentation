IOutputNode
===========
.. cs:setscope:: Narupa.Visualisation.Node.Output

.. cs:interface:: public interface IOutputNode
    
    
    .. cs:property:: string Name { get; }
        
        :returns System.String: 
        
    .. cs:property:: IReadOnlyProperty Output { get; }
        
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
