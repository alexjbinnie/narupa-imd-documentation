SecondaryStructureArrayInput
============================
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class SecondaryStructureArrayInput : VisualisationComponent<SecondaryStructureArrayInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<SecondaryStructureArrayInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.SecondaryStructureArrayInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.SecondaryStructureArrayInputNode}`
    
