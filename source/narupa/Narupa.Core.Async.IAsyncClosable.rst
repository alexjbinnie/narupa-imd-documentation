IAsyncClosable
==============
.. cs:setscope:: Narupa.Core.Async

.. cs:interface:: public interface IAsyncClosable
    
    Provides a mechanism to close a resource asynchronously and allow
    awaiting its completion.
    
    

    
    .. cs:method:: Task CloseAsync()
        
        Close the object asynchronously. The object should be allowed time
        to clear up cleanly, finishing any tasks it is currently doing as
        soon as possible.
        
        

        :returns System.Threading.Tasks.Task: 
        
