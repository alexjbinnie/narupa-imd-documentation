ColorOutput
===========
.. cs:setscope:: Narupa.Visualisation.Components.Output

.. cs:class:: public class ColorOutput : VisualisationComponent<ColorOutputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ColorOutputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Output.ColorOutputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Output.ColorOutputNode}`
    
