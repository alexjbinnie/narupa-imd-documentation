ComputeShaderProgram
====================
.. cs:setscope:: Narupa.Visualisation.Utility

.. cs:class:: public class ComputeShaderProgram : IGpuProgram
    
    A specific kernel (executable program) of a :cs:any:`~UnityEngine.ComputeShader`, which
    is an example of an abstract
    GPU program which can have data passed to it from the CPU using
    :cs:any:`~UnityEngine.ComputeBuffer`'s.
    
    

    :implements: :cs:any:`~Narupa.Visualisation.Utility.IGpuProgram`
    
    .. cs:constructor:: public ComputeShaderProgram(ComputeShader shader, int kernelIndex)
        
        Create a :cs:any:`~Narupa.Visualisation.Utility.ComputeShaderProgram` that represents the kernel
        (executable program) of the
        :cs:any:`~UnityEngine.ComputeShader` :code:`shader` with the provided
        :code:`kernelIndex`
        
        

        :param UnityEngine.ComputeShader shader: 
        :param System.Int32 kernelIndex: 
        
    .. cs:method:: public void SetBuffer(string id, ComputeBuffer buffer)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Utility.IGpuProgram.SetBuffer(System.String,UnityEngine.ComputeBuffer)`
        :param System.String id: 
        :param UnityEngine.ComputeBuffer buffer: 
        
    .. cs:method:: public void Dispatch(int threadGroupsX, int threadGroupsY, int threadGroupsZ)
        
        Execute this GPU program by dispatching the corresponding kernel of the
        :cs:any:`~UnityEngine.ComputeShader`
        
        

        :param System.Int32 threadGroupsX: 
        :param System.Int32 threadGroupsY: 
        :param System.Int32 threadGroupsZ: 
        
