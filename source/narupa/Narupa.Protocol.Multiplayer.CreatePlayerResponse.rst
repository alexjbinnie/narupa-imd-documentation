CreatePlayerResponse
====================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class CreatePlayerResponse : Object, IMessage<CreatePlayerResponse>, IMessage, IEquatable<CreatePlayerResponse>, IDeepCloneable<CreatePlayerResponse>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.CreatePlayerResponse}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.CreatePlayerResponse}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.CreatePlayerResponse}`
    
    .. cs:field:: public const int PlayerIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public CreatePlayerResponse()
        
        
    .. cs:constructor:: public CreatePlayerResponse(CreatePlayerResponse other)
        
        :param Narupa.Protocol.Multiplayer.CreatePlayerResponse other: 
        
    .. cs:method:: public CreatePlayerResponse Clone()
        
        :returns Narupa.Protocol.Multiplayer.CreatePlayerResponse: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(CreatePlayerResponse other)
        
        :param Narupa.Protocol.Multiplayer.CreatePlayerResponse other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(CreatePlayerResponse other)
        
        :param Narupa.Protocol.Multiplayer.CreatePlayerResponse other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<CreatePlayerResponse> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.CreatePlayerResponse}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string PlayerId { get; set; }
        
        :returns System.String: 
        
