Cancellable
===========
.. cs:setscope:: Narupa.Core.Async

.. cs:class:: public abstract class Cancellable : ICancellable, ICancellationTokenSource, IDisposable
    
    A base implementation of an object that wraps a
    :cs:any:`~System.Threading.CancellationTokenSource` using a linked source from other
    external cancellation tokens.
    
    

    :implements: :cs:any:`~Narupa.Core.Async.ICancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellationTokenSource`
    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:property:: public bool IsCancelled { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Async.ICancellable.IsCancelled`
        :returns System.Boolean: 
        
    .. cs:constructor:: protected Cancellable(params CancellationToken[] externalTokens)
        
        :param System.Threading.CancellationToken[] externalTokens: 
        
    .. cs:method:: public CancellationToken GetCancellationToken()
        
        

        :implements: :cs:any:`~Narupa.Core.Async.ICancellationTokenSource.GetCancellationToken`
        :returns System.Threading.CancellationToken: 
        
    .. cs:method:: public void Cancel()
        
        

        :implements: :cs:any:`~Narupa.Core.Async.ICancellable.Cancel`
        
    .. cs:method:: public virtual void Dispose()
        
        

        :implements: :cs:any:`~System.IDisposable.Dispose`
        
