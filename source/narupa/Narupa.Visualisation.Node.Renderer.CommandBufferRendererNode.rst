CommandBufferRendererNode
=========================
.. cs:setscope:: Narupa.Visualisation.Node.Renderer

.. cs:class:: public abstract class CommandBufferRendererNode : IDisposable
    
    Base node for a renderer based upon using :cs:any:`~UnityEngine.Rendering.CommandBuffer`,
    which is a list of commands (rendering to textures, blitting, etc.)
    that can be executed at some point in the rendering pipeline.
    
    

    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:method:: public virtual void Dispose()
        
        Cleanup all buffers, removing them from the cameras.
        
        

        :implements: :cs:any:`~System.IDisposable.Dispose`
        
    .. cs:method:: protected Material CreateMaterial(Shader shader)
        
        Create a new material for use with this renderer.
        
        

        :param UnityEngine.Shader shader: 
        :returns UnityEngine.Material: 
        
    .. cs:method:: public virtual void Render(Camera cam)
        
        Render to the given camera using a command buffer, using a cached buffer
        if it has already been created.
        
        

        :param UnityEngine.Camera cam: 
        
    .. cs:method:: protected abstract IEnumerable<(CameraEvent Event, CommandBuffer Buffer)> GetBuffers(Camera camera)
        
        Get the command buffers and their triggering events for a given camera.
        
        

        :param UnityEngine.Camera camera: 
        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer}}: 
        
