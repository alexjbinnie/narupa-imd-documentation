GetCommandsRequest
==================
.. cs:setscope:: Narupa.Protocol.Command

.. cs:class:: public sealed class GetCommandsRequest : Object, IMessage<GetCommandsRequest>, IMessage, IEquatable<GetCommandsRequest>, IDeepCloneable<GetCommandsRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Command.GetCommandsRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Command.GetCommandsRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Command.GetCommandsRequest}`
    
    .. cs:constructor:: public GetCommandsRequest()
        
        
    .. cs:constructor:: public GetCommandsRequest(GetCommandsRequest other)
        
        :param Narupa.Protocol.Command.GetCommandsRequest other: 
        
    .. cs:method:: public GetCommandsRequest Clone()
        
        :returns Narupa.Protocol.Command.GetCommandsRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(GetCommandsRequest other)
        
        :param Narupa.Protocol.Command.GetCommandsRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(GetCommandsRequest other)
        
        :param Narupa.Protocol.Command.GetCommandsRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<GetCommandsRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Command.GetCommandsRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
