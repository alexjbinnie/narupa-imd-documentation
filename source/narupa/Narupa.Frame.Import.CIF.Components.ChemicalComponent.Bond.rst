ChemicalComponent.Bond
======================
.. cs:setscope:: Narupa.Frame.Import.CIF.Components

.. cs:struct:: [Serializable] public struct Bond
    
    
    .. cs:field:: [SerializeField] public string a
        
        :returns System.String: 
        
    .. cs:field:: [SerializeField] public string b
        
        :returns System.String: 
        
    .. cs:field:: [SerializeField] public int order
        
        :returns System.Int32: 
        
