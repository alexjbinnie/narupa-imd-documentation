ResidueNameColor
================
.. cs:setscope:: Narupa.Visualisation.Components.Color

.. cs:class:: public class ResidueNameColor : VisualisationComponent<ResidueNameColorNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ResidueNameColorNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Color.ResidueNameColorNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Color.ResidueNameColorNode}`
    
