GradientInput
=============
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class GradientInput : VisualisationComponent<GradientInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<GradientInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.GradientInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.GradientInputNode}`
    
