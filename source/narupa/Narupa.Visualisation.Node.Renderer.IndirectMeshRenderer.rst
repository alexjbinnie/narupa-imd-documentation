IndirectMeshRenderer
====================
.. cs:setscope:: Narupa.Visualisation.Node.Renderer

.. cs:class:: public abstract class IndirectMeshRenderer : IDisposable
    
    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:method:: public void AppendToCommandBuffer(CommandBuffer buffer)
        
        :param UnityEngine.Rendering.CommandBuffer buffer: 
        
    .. cs:method:: public void Render(Camera camera = null)
        
        Render the provided bonds
        
        

        :param UnityEngine.Camera camera: 
        
    .. cs:property:: protected abstract IndirectMeshDrawCommand DrawCommand { get; }
        
        :returns Narupa.Visualisation.IndirectMeshDrawCommand: 
        
    .. cs:method:: public void Dispose()
        
        

        :implements: :cs:any:`~System.IDisposable.Dispose`
        
    .. cs:method:: public virtual void ResetBuffers()
        
        Indicate that a deserialisation or other event which resets buffers has
        occured.
        
        

        
    .. cs:property:: public abstract bool ShouldRender { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: public abstract bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: public abstract void UpdateInput()
        
        
    .. cs:property:: public Transform Transform { get; set; }
        
        :returns UnityEngine.Transform: 
        
    .. cs:method:: public bool UpdateRenderer()
        
        Update the draw command based upon the input values, by updating the mesh,
        material and buffers.
        
        

        This does not actually render the spheres. Either call :cs:any:`~Narupa.Visualisation.Node.Renderer.IndirectMeshRenderer.Render(UnityEngine.Camera)`
        each frame or call :cs:any:`~Narupa.Visualisation.Node.Renderer.IndirectMeshRenderer.AppendToCommandBuffer(UnityEngine.Rendering.CommandBuffer)`.
        
        

        :returns System.Boolean: 
        
