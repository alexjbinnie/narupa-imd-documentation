SearchAlgorithms
================
.. cs:setscope:: Narupa.Core.Math

.. cs:class:: public class SearchAlgorithms
    
    
    .. cs:method:: public static bool BinarySearch(int value, IReadOnlyList<int> set)
        
        Binary search to find an integer in a set of ordered integers.
        
        

        :param System.Int32 value: The value that is being searched for.
        :param System.Collections.Generic.IReadOnlyList{System.Int32} set: A set of integers ordered low to high.
        :returns System.Boolean: 
        
    .. cs:method:: public static int BinarySearchIndex(int value, IReadOnlyList<int> set)
        
        Binary search to find the index of an integer in a set of ordered integers.
        
        

        :param System.Int32 value: The value that is being searched for.
        :param System.Collections.Generic.IReadOnlyList{System.Int32} set: A set of integers ordered low to high.
        :returns System.Int32: The index of :code:`value` in :code:`set`, or -1 if value is not present.
        
