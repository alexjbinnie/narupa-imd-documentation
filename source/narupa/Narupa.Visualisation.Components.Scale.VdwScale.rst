VdwScale
========
.. cs:setscope:: Narupa.Visualisation.Components.Scale

.. cs:class:: public sealed class VdwScale : VisualisationComponent<VdwScaleNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<VdwScaleNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Scale.VdwScaleNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Scale.VdwScaleNode}`
    
    .. cs:method:: protected override void OnEnable()
        
        
