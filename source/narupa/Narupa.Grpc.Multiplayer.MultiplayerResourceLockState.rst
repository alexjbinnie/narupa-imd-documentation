MultiplayerResourceLockState
============================
.. cs:setscope:: Narupa.Grpc.Multiplayer

.. cs:enum:: public enum MultiplayerResourceLockState
    
    The state of a lock on a multiplayer resource.
    
    

    
    .. cs:enummember:: Unlocked = 0
        
        The state of the resource is not known, and shouldn't be altered.
        
        

        :returns Narupa.Grpc.Multiplayer.MultiplayerResourceLockState: 
        
    .. cs:enummember:: Pending = 1
        
        A request to obtain a lock has been sent, and we are awaiting the result.
        
        

        :returns Narupa.Grpc.Multiplayer.MultiplayerResourceLockState: 
        
    .. cs:enummember:: Locked = 2
        
        The lock has been accepted and we have a lock on the object.
        
        

        :returns Narupa.Grpc.Multiplayer.MultiplayerResourceLockState: 
        
