IInputNode
==========
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:interface:: public interface IInputNode
    
    
    .. cs:property:: string Name { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: IProperty Input { get; }
        
        :returns Narupa.Visualisation.Property.IProperty: 
        
    .. cs:property:: Type InputType { get; }
        
        :returns System.Type: 
        
