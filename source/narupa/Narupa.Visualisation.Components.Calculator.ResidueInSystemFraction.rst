ResidueInSystemFraction
=======================
.. cs:setscope:: Narupa.Visualisation.Components.Calculator

.. cs:class:: public class ResidueInSystemFraction : VisualisationComponent<ResidueInSystemFractionNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ResidueInSystemFractionNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Calculator.ResidueInSystemFractionNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Calculator.ResidueInSystemFractionNode}`
    
