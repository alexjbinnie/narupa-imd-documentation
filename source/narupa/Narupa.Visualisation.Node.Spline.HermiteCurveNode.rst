HermiteCurveNode
================
.. cs:setscope:: Narupa.Visualisation.Node.Spline

.. cs:class:: [Serializable] public class HermiteCurveNode : GenericOutputNode
    
    Calculates normals and tangents for splines going through a set of points, assuming each segment is a hermite spline.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:property:: public IProperty<Vector3[]> Positions { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Vector3[]}: 
        
    .. cs:property:: public IReadOnlyProperty<Vector3[]> Tangents { get; }
        
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{UnityEngine.Vector3[]}: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:method:: protected override void UpdateOutput()
        
        

        
    .. cs:method:: protected override void ClearOutput()
        
        

        
