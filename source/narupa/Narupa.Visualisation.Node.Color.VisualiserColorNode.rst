VisualiserColorNode
===================
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [Serializable] public abstract class VisualiserColorNode : GenericOutputNode
    
    Base code for a visualiser node which generates a set of colors.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:field:: protected readonly ColorArrayProperty colors
        
        :returns Narupa.Visualisation.Properties.Collections.ColorArrayProperty: 
        
    .. cs:property:: public IReadOnlyProperty<Color[]> Colors { get; }
        
        Color array output.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{UnityEngine.Color[]}: 
        
