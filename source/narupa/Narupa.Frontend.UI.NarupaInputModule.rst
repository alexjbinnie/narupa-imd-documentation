NarupaInputModule
=================
.. cs:setscope:: Narupa.Frontend.UI

.. cs:class:: public class NarupaInputModule : StandaloneInputModule
    
    Override for :cs:any:`~UnityEngine.EventSystems.StandaloneInputModule` that exposes
    the current hovered game object.
    
    

    :inherits: :cs:any:`~UnityEngine.EventSystems.StandaloneInputModule`
    
    .. cs:property:: public GameObject CurrentHoverTarget { get; }
        
        Get the current hovered over game object.
        
        

        :returns UnityEngine.GameObject: 
        
    .. cs:method:: public void ClearSelection()
        
        
