TrajectoryService
=================
.. cs:setscope:: Narupa.Protocol.Trajectory

.. cs:class:: public static class TrajectoryService : Object
    
    
    .. cs:method:: public static ServerServiceDefinition BindService(TrajectoryService.TrajectoryServiceBase serviceImpl)
        
        :param Narupa.Protocol.Trajectory.TrajectoryService.TrajectoryServiceBase serviceImpl: 
        :returns Grpc.Core.ServerServiceDefinition: 
        
    .. cs:method:: public static void BindService(ServiceBinderBase serviceBinder, TrajectoryService.TrajectoryServiceBase serviceImpl)
        
        :param Grpc.Core.ServiceBinderBase serviceBinder: 
        :param Narupa.Protocol.Trajectory.TrajectoryService.TrajectoryServiceBase serviceImpl: 
        
    .. cs:property:: public static ServiceDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.ServiceDescriptor: 
        
