Property
========
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:class:: [Serializable] public abstract class Property : IProperty, IReadOnlyProperty
    
    Abstract base class for an implementation of :cs:any:`~Narupa.Visualisation.Property.IProperty`.
    
    

    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
    .. cs:property:: IReadOnlyProperty IProperty.LinkedProperty { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty.LinkedProperty`
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:property:: object IReadOnlyProperty.Value { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.Value`
        :returns System.Object: 
        
    .. cs:property:: protected abstract IReadOnlyProperty NonGenericLinkedProperty { get; }
        
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:property:: protected abstract object NonGenericValue { get; }
        
        :returns System.Object: 
        
    .. cs:event:: public event Action ValueChanged
        
        Callback for when the value is changed or undefined.
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.ValueChanged`
        :returns System.Action: 
        
    .. cs:method:: protected void OnValueChanged()
        
        Internal method, called when the value is changed or undefined.
        
        

        
    .. cs:property:: public abstract bool HasValue { get; }
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.HasValue`
        :returns System.Boolean: 
        
    .. cs:method:: public abstract void UndefineValue()
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty.UndefineValue`
        
    .. cs:property:: public abstract Type PropertyType { get; }
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty.PropertyType`
        :returns System.Type: 
        
    .. cs:property:: public bool IsDirty { get; set; }
        
        Has the value of the property changed since the last time the :cs:any:`~Narupa.Visualisation.Property.Property.IsDirty` flag was cleared.
        
        

        :returns System.Boolean: 
        
    .. cs:method:: public virtual void MarkValueAsChanged()
        
        Mark the value as having changed.
        
        

        
    .. cs:property:: public abstract bool HasLinkedProperty { get; }
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty.HasLinkedProperty`
        :returns System.Boolean: 
        
    .. cs:method:: public abstract void TrySetValue(object value)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty.TrySetValue(System.Object)`
        :param System.Object value: 
        
    .. cs:method:: public abstract void TrySetLinkedProperty(object property)
        
        

        :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty.TrySetLinkedProperty(System.Object)`
        :param System.Object property: 
        
