GradientColorNode
=================
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [Serializable] public class GradientColorNode : VisualiserColorNode
    
    :inherits: :cs:any:`~Narupa.Visualisation.Node.Color.VisualiserColorNode`
    
    .. cs:property:: public IProperty<Gradient> Gradient { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{UnityEngine.Gradient}: 
        
    .. cs:property:: public IProperty<float[]> Values { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{System.Single[]}: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        
    .. cs:method:: protected override void UpdateOutput()
        
        
    .. cs:method:: protected override void ClearOutput()
        
        
