StreamEndedResponse
===================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class StreamEndedResponse : Object, IMessage<StreamEndedResponse>, IMessage, IEquatable<StreamEndedResponse>, IDeepCloneable<StreamEndedResponse>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.StreamEndedResponse}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.StreamEndedResponse}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.StreamEndedResponse}`
    
    .. cs:constructor:: public StreamEndedResponse()
        
        
    .. cs:constructor:: public StreamEndedResponse(StreamEndedResponse other)
        
        :param Narupa.Protocol.Multiplayer.StreamEndedResponse other: 
        
    .. cs:method:: public StreamEndedResponse Clone()
        
        :returns Narupa.Protocol.Multiplayer.StreamEndedResponse: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(StreamEndedResponse other)
        
        :param Narupa.Protocol.Multiplayer.StreamEndedResponse other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(StreamEndedResponse other)
        
        :param Narupa.Protocol.Multiplayer.StreamEndedResponse other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<StreamEndedResponse> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.StreamEndedResponse}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
