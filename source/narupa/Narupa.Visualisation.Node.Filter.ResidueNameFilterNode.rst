ResidueNameFilterNode
=====================
.. cs:setscope:: Narupa.Visualisation.Node.Filter

.. cs:class:: [Serializable] public class ResidueNameFilterNode : VisualiserFilterNode
    
    Filters particles by the name of their residues.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Filter.VisualiserFilterNode`
    
    .. cs:property:: public IProperty<string> Pattern { get; }
        
        Regex pattern to identify which residue names to filter.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.String}: 
        
    .. cs:property:: public IProperty<int[]> ParticleResidues { get; }
        
        The indices of the residue for each particle.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Int32[]}: 
        
    .. cs:property:: public IProperty<string[]> ResidueNames { get; }
        
        The residue names.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.String[]}: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:property:: protected override int MaximumFilterCount { get; }
        
        

        :returns System.Int32: 
        
    .. cs:method:: protected override IEnumerable<int> GetFilteredIndices()
        
        

        :returns System.Collections.Generic.IEnumerable{System.Int32}: 
        
