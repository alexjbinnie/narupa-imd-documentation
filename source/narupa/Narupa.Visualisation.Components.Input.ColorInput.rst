ColorInput
==========
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class ColorInput : VisualisationComponent<ColorInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ColorInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.ColorInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.ColorInputNode}`
    
