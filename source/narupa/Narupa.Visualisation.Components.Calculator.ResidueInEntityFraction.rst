ResidueInEntityFraction
=======================
.. cs:setscope:: Narupa.Visualisation.Components.Calculator

.. cs:class:: public class ResidueInEntityFraction : VisualisationComponent<ResidueInEntityFractionNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ResidueInEntityFractionNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Calculator.ResidueInEntityFractionNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Calculator.ResidueInEntityFractionNode}`
    
