ColorProperty
=============
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class ColorProperty : SerializableProperty<Color>, IProperty<Color>, IReadOnlyProperty<Color>, IProperty, IReadOnlyProperty
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for a :cs:any:`~UnityEngine.Color` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.SerializableProperty{UnityEngine.Color}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{UnityEngine.Color}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{UnityEngine.Color}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
