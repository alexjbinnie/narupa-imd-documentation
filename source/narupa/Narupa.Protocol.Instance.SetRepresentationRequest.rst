SetRepresentationRequest
========================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public sealed class SetRepresentationRequest : Object, IMessage<SetRepresentationRequest>, IMessage, IEquatable<SetRepresentationRequest>, IDeepCloneable<SetRepresentationRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Instance.SetRepresentationRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Instance.SetRepresentationRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Instance.SetRepresentationRequest}`
    
    .. cs:field:: public const int InstanceIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int SelectionIdFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:field:: public const int RepresentationFieldNumber = 3
        
        :returns System.Int32: 
        
    .. cs:constructor:: public SetRepresentationRequest()
        
        
    .. cs:constructor:: public SetRepresentationRequest(SetRepresentationRequest other)
        
        :param Narupa.Protocol.Instance.SetRepresentationRequest other: 
        
    .. cs:method:: public SetRepresentationRequest Clone()
        
        :returns Narupa.Protocol.Instance.SetRepresentationRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(SetRepresentationRequest other)
        
        :param Narupa.Protocol.Instance.SetRepresentationRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(SetRepresentationRequest other)
        
        :param Narupa.Protocol.Instance.SetRepresentationRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<SetRepresentationRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Instance.SetRepresentationRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string InstanceId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public string SelectionId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public Struct Representation { get; set; }
        
        :returns Google.Protobuf.WellKnownTypes.Struct: 
        
