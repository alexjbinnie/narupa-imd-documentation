IntArrayProperty
================
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class IntArrayProperty : ArrayProperty<int>, IProperty<int[]>, IReadOnlyProperty<int[]>, IProperty, IReadOnlyProperty, IEnumerable<int>, IEnumerable
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an array of :cs:any:`~System.Int32` values.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Properties.Collections.ArrayProperty{System.Int32}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{System.Int32[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{System.Int32}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
