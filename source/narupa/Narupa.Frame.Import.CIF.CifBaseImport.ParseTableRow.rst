CifBaseImport.ParseTableRow
===========================
.. cs:setscope:: Narupa.Frame.Import.CIF

.. cs:delegate:: protected delegate void ParseTableRow(IReadOnlyList<string> row);
    
    :param System.Collections.Generic.IReadOnlyList{System.String} row: 
    
