OutputNode<TProperty>
=====================
.. cs:setscope:: Narupa.Visualisation.Node.Output

.. cs:class:: [Serializable] public abstract class OutputNode<TProperty> : IOutputNode where TProperty : Property, new()
    
    Generic output for the visualisation system that provides some value with a
    given key.
    
    

    :implements: :cs:any:`~Narupa.Visualisation.Node.Output.IOutputNode`
    
    .. cs:property:: IReadOnlyProperty IOutputNode.Output { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Node.Output.IOutputNode.Output`
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:property:: public string Name { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Node.Output.IOutputNode.Name`
        :returns System.String: 
        
