CloseInstanceRequest
====================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public sealed class CloseInstanceRequest : Object, IMessage<CloseInstanceRequest>, IMessage, IEquatable<CloseInstanceRequest>, IDeepCloneable<CloseInstanceRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Instance.CloseInstanceRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Instance.CloseInstanceRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Instance.CloseInstanceRequest}`
    
    .. cs:field:: public const int InstanceIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public CloseInstanceRequest()
        
        
    .. cs:constructor:: public CloseInstanceRequest(CloseInstanceRequest other)
        
        :param Narupa.Protocol.Instance.CloseInstanceRequest other: 
        
    .. cs:method:: public CloseInstanceRequest Clone()
        
        :returns Narupa.Protocol.Instance.CloseInstanceRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(CloseInstanceRequest other)
        
        :param Narupa.Protocol.Instance.CloseInstanceRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(CloseInstanceRequest other)
        
        :param Narupa.Protocol.Instance.CloseInstanceRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<CloseInstanceRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Instance.CloseInstanceRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string InstanceId { get; set; }
        
        :returns System.String: 
        
