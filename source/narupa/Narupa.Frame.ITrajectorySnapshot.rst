ITrajectorySnapshot
===================
.. cs:setscope:: Narupa.Frame

.. cs:interface:: public interface ITrajectorySnapshot
    
    A single :cs:any:`~Narupa.Frame.Frame` in a trajectory, representing a single point in
    time. Use :cs:any:`~Narupa.Frame.ITrajectorySnapshot.FrameChanged` for a
    
    

    
    .. cs:property:: Frame CurrentFrame { get; }
        
        Current frame.
        
        

        :returns Narupa.Frame.Frame: 
        
    .. cs:event:: event FrameChanged FrameChanged
        
        Event invoked when the frame is changed.
        
        

        :returns Narupa.Frame.FrameChanged: 
        
