PropertyExtensions
==================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:class:: public static class PropertyExtensions
    
    
    .. cs:method:: public static bool HasNonNullValue<TValue>(this IReadOnlyProperty<TValue> property)
        
        Does the property have a value which is not null?
        
        

        :param Narupa.Visualisation.Property.IReadOnlyProperty{{TValue}} property: 
        :returns System.Boolean: 
        
    .. cs:method:: public static bool HasNonEmptyValue<TValue>(this IReadOnlyProperty<IEnumerable<TValue>> property)
        
        Does this property have a value which is non empty?
        
        

        :param Narupa.Visualisation.Property.IReadOnlyProperty{System.Collections.Generic.IEnumerable{{TValue}}} property: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyProperty<int> Count<TValue>(this IReadOnlyProperty<ICollection<TValue>> property)
        
        Get a property representing the count of a enumerable property.
        
        

        :param Narupa.Visualisation.Property.IReadOnlyProperty{System.Collections.Generic.ICollection{{TValue}}} property: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32}: 
        
