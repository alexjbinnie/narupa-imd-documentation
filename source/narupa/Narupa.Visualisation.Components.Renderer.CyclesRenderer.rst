CyclesRenderer
==============
.. cs:setscope:: Narupa.Visualisation.Components.Renderer

.. cs:class:: public class CyclesRenderer : VisualisationComponentRenderer<CyclesRendererNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<CyclesRendererNode>
    
    :inherits: :cs:any:`~Narupa.Visualisation.Components.Renderer.VisualisationComponentRenderer{Narupa.Visualisation.Node.Renderer.CyclesRendererNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Renderer.CyclesRendererNode}`
    
    .. cs:method:: protected override void Render(Camera camera)
        
        :param UnityEngine.Camera camera: 
        
