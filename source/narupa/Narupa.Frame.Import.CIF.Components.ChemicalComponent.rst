ChemicalComponent
=================
.. cs:setscope:: Narupa.Frame.Import.CIF.Components

.. cs:class:: [Serializable] public class ChemicalComponent
    
    Serializable representation of a chemical component
    
    

    
    .. cs:property:: public IEnumerable<ChemicalComponent.Bond> Bonds { get; }
        
        :returns System.Collections.Generic.IEnumerable{Narupa.Frame.Import.CIF.Components.ChemicalComponent.Bond}: 
        
    .. cs:property:: public string ResId { get; set; }
        
        :returns System.String: 
        
    .. cs:method:: public void AddBond(string a, string b, int order)
        
        :param System.String a: 
        :param System.String b: 
        :param System.Int32 order: 
        
