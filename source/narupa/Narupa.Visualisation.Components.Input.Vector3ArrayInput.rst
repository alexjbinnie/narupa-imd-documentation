Vector3ArrayInput
=================
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class Vector3ArrayInput : VisualisationComponent<Vector3ArrayInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<Vector3ArrayInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.Vector3ArrayInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.Vector3ArrayInputNode}`
    
