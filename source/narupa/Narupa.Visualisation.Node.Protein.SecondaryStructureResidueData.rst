SecondaryStructureResidueData
=============================
.. cs:setscope:: Narupa.Visualisation.Node.Protein

.. cs:class:: public class SecondaryStructureResidueData
    
    Utility class for storing information about protein residues for use with DSSP.
    
    

    
    .. cs:property:: public int AlphaCarbonIndex { get; set; }
        
        Index of the alpha carbon.
        
        

        :returns System.Int32: 
        
    .. cs:property:: public Vector3 AlphaCarbonPosition { get; set; }
        
        Position of the alpha carbon.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:property:: public int CarbonIndex { get; set; }
        
        Index of the carbonyl carbon.
        
        

        :returns System.Int32: 
        
    .. cs:property:: public Vector3 CarbonPosition { get; set; }
        
        Position of the carbonyl carbon.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:property:: public int HydrogenIndex { get; set; }
        
        Index of the amine hydrogen.
        
        

        :returns System.Int32: 
        
    .. cs:property:: public Vector3 HydrogenPosition { get; set; }
        
        Position of the amine hydrogen.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:property:: public int NitrogenIndex { get; set; }
        
        Index of the amine nitrogen.
        
        

        :returns System.Int32: 
        
    .. cs:property:: public Vector3 NitrogenPosition { get; set; }
        
        Position of the amine nitrogen.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:property:: public int OxygenIndex { get; set; }
        
        Index of the carbonyl oxygen.
        
        

        :returns System.Int32: 
        
    .. cs:property:: public Vector3 OxygenPosition { get; set; }
        
        Position of the carbonyl oxygen.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public SecondaryStructurePattern Pattern
        
        Secondary structure patterns that this residue exhibits.
        
        

        :returns Narupa.Visualisation.Node.Protein.SecondaryStructurePattern: 
        
    .. cs:property:: public SecondaryStructureAssignment SecondaryStructure { get; set; }
        
        Assignment for this residue's secondary structure.
        
        

        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment: 
        
    .. cs:property:: public double AcceptorHydrogenBondEnergy { get; set; }
        
        The lowest energy hydrogen bond formed by this residue's carbonyl.
        
        

        :returns System.Double: 
        
    .. cs:property:: public double DonorHydrogenBondEnergy { get; set; }
        
        The lowest energy hydrogen bond formed by this residue's amine.
        
        

        :returns System.Double: 
        
    .. cs:property:: public SecondaryStructureResidueData AcceptorHydrogenBondResidue { get; set; }
        
        The residue to which this carbonyl is bonded to.
        
        

        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureResidueData: 
        
    .. cs:property:: public SecondaryStructureResidueData DonorHydrogenBondResidue { get; set; }
        
        The residue to which this amine is bonded to.
        
        

        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureResidueData: 
        
    .. cs:property:: public int ordinal { get; set; }
        
        The ordinal of this residue in the sequence.
        
        

        :returns System.Int32: 
        
    .. cs:property:: public int ResidueIndex { get; set; }
        
        The index of this residue in the system.
        
        

        :returns System.Int32: 
        
