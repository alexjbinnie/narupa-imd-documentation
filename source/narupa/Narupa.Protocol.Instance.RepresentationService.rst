RepresentationService
=====================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public static class RepresentationService : Object
    
    
    .. cs:method:: public static ServerServiceDefinition BindService(RepresentationService.RepresentationServiceBase serviceImpl)
        
        :param Narupa.Protocol.Instance.RepresentationService.RepresentationServiceBase serviceImpl: 
        :returns Grpc.Core.ServerServiceDefinition: 
        
    .. cs:method:: public static void BindService(ServiceBinderBase serviceBinder, RepresentationService.RepresentationServiceBase serviceImpl)
        
        :param Grpc.Core.ServiceBinderBase serviceBinder: 
        :param Narupa.Protocol.Instance.RepresentationService.RepresentationServiceBase serviceImpl: 
        
    .. cs:property:: public static ServiceDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.ServiceDescriptor: 
        
