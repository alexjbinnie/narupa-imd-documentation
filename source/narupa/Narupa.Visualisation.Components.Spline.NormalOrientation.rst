NormalOrientation
=================
.. cs:setscope:: Narupa.Visualisation.Components.Spline

.. cs:class:: public class NormalOrientation : VisualisationComponent<NormalOrientationNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<NormalOrientationNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Spline.NormalOrientationNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Spline.NormalOrientationNode}`
    
