RadialLayoutGroup
=================
.. cs:setscope:: Narupa.Frontend.UI

.. cs:class:: public class RadialLayoutGroup : LayoutGroup, ILayoutElement, ILayoutGroup, ILayoutController
    
    Layout group for arranging UI elements radially around a point
    
    

    :inherits: :cs:any:`~UnityEngine.UI.LayoutGroup`
    :implements: :cs:any:`~UnityEngine.UI.ILayoutElement`
    :implements: :cs:any:`~UnityEngine.UI.ILayoutGroup`
    :implements: :cs:any:`~UnityEngine.UI.ILayoutController`
    
    .. cs:method:: public override void CalculateLayoutInputVertical()
        
        
    .. cs:method:: public override void SetLayoutHorizontal()
        
        
    .. cs:method:: public override void SetLayoutVertical()
        
        
