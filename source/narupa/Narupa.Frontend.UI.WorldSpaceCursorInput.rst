WorldSpaceCursorInput
=====================
.. cs:setscope:: Narupa.Frontend.UI

.. cs:class:: public class WorldSpaceCursorInput : BaseInput
    
    Implementation of :cs:any:`~UnityEngine.EventSystems.BaseInput` that uses a physical object's near a
    canvas as a mouse pointer.
    
    

    :inherits: :cs:any:`~UnityEngine.EventSystems.BaseInput`
    
    .. cs:method:: protected override void Awake()
        
        
    .. cs:method:: protected override void Start()
        
        
    .. cs:method:: public static void ClearSelection()
        
        
    .. cs:property:: public bool IsClickPressed { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: public static void SetCanvasAndCursor(Canvas canvas, IPosedObject cursor, IButton click = null)
        
        Sets the canvas with an :cs:any:`~Narupa.Frontend.Input.IPosedObject` to provide the location
        of the physical cursor and an :cs:any:`~Narupa.Frontend.Input.IButton` to provide information on
        if a click is occuring.
        
        

        :param UnityEngine.Canvas canvas: 
        :param Narupa.Frontend.Input.IPosedObject cursor: 
        :param Narupa.Frontend.Input.IButton click: 
        
    .. cs:property:: public override Vector2 mousePosition { get; }
        
        

        :returns UnityEngine.Vector2: 
        
    .. cs:property:: public override bool mousePresent { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: public override bool GetMouseButtonDown(int button)
        
        

        :param System.Int32 button: 
        :returns System.Boolean: 
        
    .. cs:method:: public override bool GetMouseButton(int button)
        
        

        :param System.Int32 button: 
        :returns System.Boolean: 
        
    .. cs:method:: public override bool GetMouseButtonUp(int button)
        
        

        :param System.Int32 button: 
        :returns System.Boolean: 
        
    .. cs:method:: public static void ReleaseCanvas(Canvas canvas)
        
        :param UnityEngine.Canvas canvas: 
        
    .. cs:method:: public static void TriggerClick()
        
        
