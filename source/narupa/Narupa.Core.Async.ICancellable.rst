ICancellable
============
.. cs:setscope:: Narupa.Core.Async

.. cs:interface:: public interface ICancellable : ICancellationTokenSource
    
    An object which may be cancelled, providing access to its cancellation
    state (using :cs:any:`~Narupa.Core.Async.ICancellable.IsCancelled`), the ability to cancel it (using
    :cs:any:`~Narupa.Core.Async.ICancellable.Cancel`) and the ability to get a
    :cs:any:`~System.Threading.CancellationToken` using
    :cs:any:`~Narupa.Core.Async.ICancellationTokenSource.GetCancellationToken`, which allows
    related things to be cancelled when this object is.
    
    

    
    .. cs:property:: bool IsCancelled { get; }
        
        Check if this object has been cancelled.
        
        

        :returns System.Boolean: 
        
    .. cs:method:: void Cancel()
        
        Cancel this object as soon as possible, ending any tasks prematurely.
        
        

        
