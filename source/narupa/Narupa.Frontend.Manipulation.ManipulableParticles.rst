ManipulableParticles
====================
.. cs:setscope:: Narupa.Frontend.Manipulation

.. cs:class:: public class ManipulableParticles
    
    Situates and IMD simulation in a Unity transform and allows grab
    manipulations to begin and end particle interactions.
    
    

    
    .. cs:property:: public float ForceScale { get; set; }
        
        The force multiplier.
        
        

        :returns System.Single: 
        
    .. cs:property:: public IInteractableParticles InteractableParticles { get; set; }
        
        :returns Narupa.Frontend.Manipulation.IInteractableParticles: 
        
    .. cs:constructor:: public ManipulableParticles(Transform transform, ImdSession imdSession, IInteractableParticles interactableParticles)
        
        :param UnityEngine.Transform transform: 
        :param Narupa.Session.ImdSession imdSession: 
        :param Narupa.Frontend.Manipulation.IInteractableParticles interactableParticles: 
        
    .. cs:method:: public IActiveManipulation StartParticleGrab(UnitScaleTransformation grabberPose)
        
        Start a particle grab on whatever particle falls close to the position
        of the given grabber. Return either the manipulation or null if there was
        nothing grabbable.
        
        

        :param Narupa.Core.Math.UnitScaleTransformation grabberPose: 
        :returns Narupa.Frontend.Manipulation.IActiveManipulation: 
        
