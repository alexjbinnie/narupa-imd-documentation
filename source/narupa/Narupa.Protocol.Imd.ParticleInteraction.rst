ParticleInteraction
===================
.. cs:setscope:: Narupa.Protocol.Imd

.. cs:class:: public sealed class ParticleInteraction : Object, IMessage<ParticleInteraction>, IMessage, IEquatable<ParticleInteraction>, IDeepCloneable<ParticleInteraction>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Imd.ParticleInteraction}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Imd.ParticleInteraction}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Imd.ParticleInteraction}`
    
    .. cs:field:: public const string TypeKey = "type"
        
        :returns System.String: 
        
    .. cs:field:: public const string ScaleKey = "scale"
        
        :returns System.String: 
        
    .. cs:field:: public const string MassWeightedKey = "mass_weighted"
        
        :returns System.String: 
        
    .. cs:field:: public const string ResetVelocitiesKey = "reset_velocities"
        
        :returns System.String: 
        
    .. cs:field:: public const int PlayerIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int InteractionIdFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:field:: public const int PositionFieldNumber = 3
        
        :returns System.Int32: 
        
    .. cs:field:: public const int ParticlesFieldNumber = 4
        
        :returns System.Int32: 
        
    .. cs:field:: public const int PropertiesFieldNumber = 5
        
        :returns System.Int32: 
        
    .. cs:constructor:: public ParticleInteraction(string playerId, string interactionId = "0", string interactionType = "gaussian", float scale = 1F, bool massWeighted = true, bool resetVelocities = false)
        
        :param System.String playerId: 
        :param System.String interactionId: 
        :param System.String interactionType: 
        :param System.Single scale: 
        :param System.Boolean massWeighted: 
        :param System.Boolean resetVelocities: 
        
    .. cs:constructor:: public ParticleInteraction()
        
        
    .. cs:constructor:: public ParticleInteraction(ParticleInteraction other)
        
        :param Narupa.Protocol.Imd.ParticleInteraction other: 
        
    .. cs:method:: public ParticleInteraction Clone()
        
        :returns Narupa.Protocol.Imd.ParticleInteraction: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(ParticleInteraction other)
        
        :param Narupa.Protocol.Imd.ParticleInteraction other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(ParticleInteraction other)
        
        :param Narupa.Protocol.Imd.ParticleInteraction other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public string Type { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public float Scale { get; set; }
        
        :returns System.Single: 
        
    .. cs:property:: public bool MassWeighted { get; set; }
        
        :returns System.Boolean: 
        
    .. cs:property:: public bool ResetVelocities { get; set; }
        
        :returns System.Boolean: 
        
    .. cs:property:: public static MessageParser<ParticleInteraction> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Imd.ParticleInteraction}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string PlayerId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public string InteractionId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public RepeatedField<float> Position { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{System.Single}: 
        
    .. cs:property:: public RepeatedField<uint> Particles { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{System.UInt32}: 
        
    .. cs:property:: public Struct Properties { get; set; }
        
        :returns Google.Protobuf.WellKnownTypes.Struct: 
        
