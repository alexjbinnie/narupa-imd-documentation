ArrayProperty<TValue>
=====================
.. cs:setscope:: Narupa.Visualisation.Properties.Collections

.. cs:class:: [Serializable] public class ArrayProperty<TValue> : SerializableProperty<TValue[]>, IProperty<TValue[]>, IReadOnlyProperty<TValue[]>, IProperty, IReadOnlyProperty, IEnumerable<TValue>, IEnumerable
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an array of :code:`TValue`
    values;
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.SerializableProperty{{TValue}[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{{TValue}[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{{TValue}[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{{TValue}}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
    .. cs:method:: public TValue[] Resize(int size)
        
        Resize the array in this property, creating an array if not possible.
        
        

        :param System.Int32 size: 
        :returns {TValue}[]: 
        
    .. cs:method:: public IEnumerator<TValue> GetEnumerator()
        
        :implements: :cs:any:`~System.Collections.Generic.IEnumerable{{TValue}}.GetEnumerator`
        :returns System.Collections.Generic.IEnumerator{{TValue}}: 
        
    .. cs:method:: IEnumerator IEnumerable.GetEnumerator()
        
        :implements: :cs:any:`~System.Collections.IEnumerable.GetEnumerator`
        :returns System.Collections.IEnumerator: 
        
