ValueArray.ValuesOneofCase
==========================
.. cs:setscope:: Narupa.Protocol

.. cs:class:: public sealed class ValuesOneofCase : Enum
    
    
    .. cs:field:: public int value__
        
        :returns System.Int32: 
        
    .. cs:field:: public const ValueArray.ValuesOneofCase None
        
        :returns Narupa.Protocol.ValueArray.ValuesOneofCase: 
        
    .. cs:field:: public const ValueArray.ValuesOneofCase FloatValues
        
        :returns Narupa.Protocol.ValueArray.ValuesOneofCase: 
        
    .. cs:field:: public const ValueArray.ValuesOneofCase IndexValues
        
        :returns Narupa.Protocol.ValueArray.ValuesOneofCase: 
        
    .. cs:field:: public const ValueArray.ValuesOneofCase StringValues
        
        :returns Narupa.Protocol.ValueArray.ValuesOneofCase: 
        
