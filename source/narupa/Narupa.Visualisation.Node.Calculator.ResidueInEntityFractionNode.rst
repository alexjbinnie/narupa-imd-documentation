ResidueInEntityFractionNode
===========================
.. cs:setscope:: Narupa.Visualisation.Node.Calculator

.. cs:class:: [Serializable] public class ResidueInEntityFractionNode : GenericFractionNode
    
    Calculates the relative (0-1) fraction of each residue in an entity.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Calculator.GenericFractionNode`
    
    .. cs:property:: protected override bool IsInputValid { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        
    .. cs:method:: protected override void GenerateArray(ref float[] array)
        
        :param System.Single[] array: 
        
