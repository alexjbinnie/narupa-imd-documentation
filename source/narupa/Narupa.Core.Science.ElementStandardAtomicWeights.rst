ElementStandardAtomicWeights
============================
.. cs:setscope:: Narupa.Core.Science

.. cs:class:: public static class ElementStandardAtomicWeights
    
    Standard atomic weights of the atomic elements.
    
    

    The standard atomic weights provided are the CIAAW standard, originally
    presented in 2013 and revised in 2015 and 2017.
    
    

    
    .. cs:method:: public static float? GetStandardAtomicWeight(this Element element)
        
        Get the standard atomic weight of the element in u, returning null if that data
        does not exist.
        
        

        :param Narupa.Core.Science.Element element: 
        :returns System.Nullable{System.Single}: 
        
