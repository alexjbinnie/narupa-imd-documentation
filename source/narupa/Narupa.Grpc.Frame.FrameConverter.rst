FrameConverter
==============
.. cs:setscope:: Narupa.Grpc.Frame

.. cs:class:: public static class FrameConverter
    
    Conversion methods for converting :cs:any:`~Narupa.Protocol.Trajectory.FrameData`, the standard
    protocol for transmitting frame information, into a :cs:any:`~Narupa.Grpc.Frame`
    representation used by the frontend.
    
    

    
    .. cs:method:: public static (Frame Frame, FrameChanges Update) ConvertFrame([NotNull] FrameData data, [CanBeNull] Frame previousFrame = null)
        
        Convert data into a :cs:any:`~Narupa.Grpc.Frame`.
        
        

        :param Narupa.Protocol.Trajectory.FrameData data: 
        :param Narupa.Frame.Frame previousFrame: A previous frame, from which to copy existing arrays if they exist.
        :returns System.ValueTuple{Narupa.Frame.Frame,Narupa.Frame.Event.FrameChanges}: 
        
