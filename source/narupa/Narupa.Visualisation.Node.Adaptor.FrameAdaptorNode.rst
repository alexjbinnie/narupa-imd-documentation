FrameAdaptorNode
================
.. cs:setscope:: Narupa.Visualisation.Node.Adaptor

.. cs:class:: [Serializable] public class FrameAdaptorNode : BaseAdaptorNode, IDynamicPropertyProvider, IPropertyProvider, IFrameConsumer
    
    Visualisation node which reads frames using :cs:any:`~Narupa.Frame.IFrameConsumer` and
    dynamically provides the frame's data as properties for the visualisation
    system.
    
    

    This visualisation node acts as the bridge between the underlying trajectory
    and the visualisation system.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Adaptor.BaseAdaptorNode`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Frame.IFrameConsumer`
    
    .. cs:method:: public IProperty<TValue> AddOverrideProperty<TValue>(string name)
        
        Add a property with the given type and name to this adaptor that is not
        affected by the frame.
        
        

        :param System.String name: 
        :returns Narupa.Visualisation.Property.IProperty{{TValue}}: 
        
    .. cs:method:: public void RemoveOverrideProperty<TValue>(string name)
        
        Remove a property with the given type and name from this adaptor that is not
        affected by the frame.
        
        

        :param System.String name: 
        
    .. cs:method:: protected override void OnCreateProperty<T>(string key, IProperty<T> property)
        
        

        :param System.String key: 
        :param Narupa.Visualisation.Property.IProperty{{T}} property: 
        
    .. cs:property:: public ITrajectorySnapshot FrameSource { get; set; }
        
        

        :implements: :cs:any:`~Narupa.Frame.IFrameConsumer.FrameSource`
        :returns Narupa.Frame.ITrajectorySnapshot: 
        
