Element
=======
.. cs:setscope:: Narupa.Core.Science

.. cs:enum:: [Serializable] public enum Element
    
    Enum containing all atomic elements, with atomic number corresponding to the
    ordinal of the enum.
    
    

    To convert an atomic number i to an Element and back, direct conversion can be
    used
    
    .. code-block:: csharp
    
                Element e = (Element) i;
                int i = (int) e;
    
    
    
    .. cs:enummember:: Virtual = 0
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Hydrogen = 1
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Helium = 2
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Lithium = 3
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Beryllium = 4
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Boron = 5
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Carbon = 6
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Nitrogen = 7
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Oxygen = 8
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Fluorine = 9
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Neon = 10
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Sodium = 11
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Magnesium = 12
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Aluminum = 13
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Silicon = 14
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Phosphorus = 15
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Sulfur = 16
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Chlorine = 17
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Argon = 18
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Potassium = 19
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Calcium = 20
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Scandium = 21
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Titanium = 22
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Vanadium = 23
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Chromium = 24
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Manganese = 25
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Iron = 26
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Cobalt = 27
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Nickel = 28
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Copper = 29
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Zinc = 30
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Gallium = 31
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Germanium = 32
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Arsenic = 33
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Selenium = 34
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Bromine = 35
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Krypton = 36
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Rubidium = 37
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Strontium = 38
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Yttrium = 39
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Zirconium = 40
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Niobium = 41
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Molybdenum = 42
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Technetium = 43
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Ruthenium = 44
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Rhodium = 45
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Palladium = 46
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Silver = 47
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Cadmium = 48
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Indium = 49
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Tin = 50
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Antimony = 51
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Tellurium = 52
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Iodine = 53
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Xenon = 54
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Cesium = 55
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Barium = 56
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Lanthanum = 57
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Cerium = 58
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Praseodymium = 59
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Neodymium = 60
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Promethium = 61
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Samarium = 62
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Europium = 63
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Gadolinium = 64
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Terbium = 65
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Dysprosium = 66
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Holmium = 67
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Erbium = 68
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Thulium = 69
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Ytterbium = 70
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Lutetium = 71
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Hafnium = 72
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Tantalum = 73
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Tungsten = 74
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Rhenium = 75
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Osmium = 76
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Iridium = 77
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Platinum = 78
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Gold = 79
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Mercury = 80
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Thallium = 81
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Lead = 82
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Bismuth = 83
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Polonium = 84
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Astatine = 85
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Radon = 86
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Francium = 87
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Radium = 88
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Actinium = 89
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Thorium = 90
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Protactinium = 91
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Uranium = 92
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Neptunium = 93
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Plutonium = 94
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Americium = 95
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Curium = 96
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Berkelium = 97
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Californium = 98
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Einsteinium = 99
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Fermium = 100
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Mendelevium = 101
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Nobelium = 102
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Lawrencium = 103
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Rutherfordium = 104
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Dubnium = 105
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Seaborgium = 106
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Bohrium = 107
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Hassium = 108
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Meitnerium = 109
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Darmstadtium = 110
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Roentgenium = 111
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Copernicium = 112
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Nihonium = 113
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Flerovium = 114
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Moscovium = 115
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Livermorium = 116
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Tennessine = 117
        
        :returns Narupa.Core.Science.Element: 
        
    .. cs:enummember:: Oganesson = 118
        
        :returns Narupa.Core.Science.Element: 
        
