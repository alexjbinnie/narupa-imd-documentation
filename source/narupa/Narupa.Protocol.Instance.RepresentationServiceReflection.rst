RepresentationServiceReflection
===============================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public static class RepresentationServiceReflection : Object
    
    
    .. cs:property:: public static FileDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.FileDescriptor: 
        
