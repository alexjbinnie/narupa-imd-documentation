SplineRendererNode
==================
.. cs:setscope:: Narupa.Visualisation.Node.Renderer

.. cs:class:: [Serializable] public class SplineRendererNode : IDisposable
    
    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:property:: public IProperty<SplineSegment[]> SplineSegments { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{Narupa.Visualisation.Node.Spline.SplineSegment[]}: 
        
    .. cs:method:: public void Render(Camera camera)
        
        :param UnityEngine.Camera camera: 
        
    .. cs:property:: public Transform Transform { get; set; }
        
        :returns UnityEngine.Transform: 
        
    .. cs:method:: public void Dispose()
        
        :implements: :cs:any:`~System.IDisposable.Dispose`
        
