SecondaryStructureArrayProperty
===============================
.. cs:setscope:: Narupa.Visualisation.Properties.Collections

.. cs:class:: [Serializable] public class SecondaryStructureArrayProperty : ArrayProperty<SecondaryStructureAssignment>, IProperty<SecondaryStructureAssignment[]>, IReadOnlyProperty<SecondaryStructureAssignment[]>, IProperty, IReadOnlyProperty, IEnumerable<SecondaryStructureAssignment>, IEnumerable
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an array of :cs:any:`~Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment` values.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Properties.Collections.ArrayProperty{Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{Narupa.Visualisation.Node.Protein.SecondaryStructureAssignment}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
