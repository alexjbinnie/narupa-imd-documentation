SteamVRExtensions
=================
.. cs:setscope:: Narupa.Frontend.XR

.. cs:class:: public static class SteamVRExtensions
    
    Extension methods for SteamVR types.
    
    

    
    .. cs:method:: public static Transformation? GetPose(this SteamVR_Action_Pose pose, SteamVR_Input_Sources source)
        
        Return the pose matrix for the given input source, if available.
        
        

        :param Valve.VR.SteamVR_Action_Pose pose: 
        :param Valve.VR.SteamVR_Input_Sources source: 
        :returns System.Nullable{Narupa.Core.Math.Transformation}: 
        
    .. cs:method:: public static IPosedObject WrapAsPosedObject(this SteamVR_Action_Pose poseAction, SteamVR_Input_Sources poseSource)
        
        Wrap a SteamVR pose action and a SteamVR input source into a single posed
        object that can be used to track a single pose state.
        
        

        :param Valve.VR.SteamVR_Action_Pose poseAction: 
        :param Valve.VR.SteamVR_Input_Sources poseSource: 
        :returns Narupa.Frontend.Input.IPosedObject: 
        
    .. cs:method:: public static IButton WrapAsButton(this SteamVR_Action_Boolean buttonAction, SteamVR_Input_Sources buttonSource)
        
        Wrap a SteamVR boolean action and a SteamVR input source into a single
        button object that can be used to track a single button state.
        
        

        :param Valve.VR.SteamVR_Action_Boolean buttonAction: 
        :param Valve.VR.SteamVR_Input_Sources buttonSource: 
        :returns Narupa.Frontend.Input.IButton: 
        
