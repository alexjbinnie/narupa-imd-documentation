IInteractableParticles
======================
.. cs:setscope:: Narupa.Frontend.Manipulation

.. cs:interface:: public interface IInteractableParticles
    
    
    .. cs:method:: ActiveParticleGrab GetParticleGrab(Transformation grabber)
        
        :param Narupa.Core.Math.Transformation grabber: 
        :returns Narupa.Frontend.Manipulation.ActiveParticleGrab: 
        
