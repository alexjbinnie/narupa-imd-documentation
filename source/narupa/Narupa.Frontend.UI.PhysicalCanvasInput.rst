PhysicalCanvasInput
===================
.. cs:setscope:: Narupa.Frontend.UI

.. cs:class:: [RequireComponent(typeof(Canvas))] public class PhysicalCanvasInput : MonoBehaviour
    
    Component required to register a Unity canvas such that the given controller
    can interact with it.
    
    

    All canvases that would like to be interacted with by physical controllers
    should have a script that derives from :cs:any:`~Narupa.Frontend.UI.PhysicalCanvasInput`. This should
    provide the controller and the action which is counted as a 'click'. The
    :cs:any:`~Narupa.Frontend.UI.PhysicalCanvasInput.RegisterCanvas` method can be overriden to provide a custom
    :cs:any:`~Narupa.Frontend.Input.IPosedObject` and :cs:any:`~Narupa.Frontend.Input.IButton` to provide the cursor
    location and click button.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: protected Canvas Canvas { get; }
        
        :returns UnityEngine.Canvas: 
        
    .. cs:property:: public VrController Controller { get; }
        
        :returns Narupa.Frontend.Controllers.VrController: 
        
    .. cs:method:: protected virtual void RegisterCanvas()
        
        Register the canvas with the cursor input system.
        
        

        
