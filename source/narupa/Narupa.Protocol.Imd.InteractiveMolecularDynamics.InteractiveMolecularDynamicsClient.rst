InteractiveMolecularDynamics.InteractiveMolecularDynamicsClient
===============================================================
.. cs:setscope:: Narupa.Protocol.Imd

.. cs:class:: public class InteractiveMolecularDynamicsClient : ClientBase<InteractiveMolecularDynamics.InteractiveMolecularDynamicsClient>
    
    
    .. cs:constructor:: public InteractiveMolecularDynamicsClient(Channel channel)
        
        :param Grpc.Core.Channel channel: 
        
    .. cs:constructor:: public InteractiveMolecularDynamicsClient(CallInvoker callInvoker)
        
        :param Grpc.Core.CallInvoker callInvoker: 
        
    .. cs:constructor:: protected InteractiveMolecularDynamicsClient()
        
        
    .. cs:constructor:: protected InteractiveMolecularDynamicsClient(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        
    .. cs:method:: public virtual AsyncClientStreamingCall<ParticleInteraction, InteractionEndReply> PublishInteraction(Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncClientStreamingCall{Narupa.Protocol.Imd.ParticleInteraction,Narupa.Protocol.Imd.InteractionEndReply}: 
        
    .. cs:method:: public virtual AsyncClientStreamingCall<ParticleInteraction, InteractionEndReply> PublishInteraction(CallOptions options)
        
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncClientStreamingCall{Narupa.Protocol.Imd.ParticleInteraction,Narupa.Protocol.Imd.InteractionEndReply}: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<InteractionsUpdate> SubscribeInteractions(SubscribeInteractionsRequest request, Metadata headers = null, Nullable<DateTime> deadline = null, CancellationToken cancellationToken = null)
        
        :param Narupa.Protocol.Imd.SubscribeInteractionsRequest request: 
        :param Grpc.Core.Metadata headers: 
        :param System.Nullable{System.DateTime} deadline: 
        :param System.Threading.CancellationToken cancellationToken: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Imd.InteractionsUpdate}: 
        
    .. cs:method:: public virtual AsyncServerStreamingCall<InteractionsUpdate> SubscribeInteractions(SubscribeInteractionsRequest request, CallOptions options)
        
        :param Narupa.Protocol.Imd.SubscribeInteractionsRequest request: 
        :param Grpc.Core.CallOptions options: 
        :returns Grpc.Core.AsyncServerStreamingCall{Narupa.Protocol.Imd.InteractionsUpdate}: 
        
    .. cs:method:: protected override InteractiveMolecularDynamics.InteractiveMolecularDynamicsClient NewInstance(ClientBase.ClientBaseConfiguration configuration)
        
        :param Grpc.Core.ClientBase.ClientBaseConfiguration configuration: 
        :returns Narupa.Protocol.Imd.InteractiveMolecularDynamics.InteractiveMolecularDynamicsClient: 
        
