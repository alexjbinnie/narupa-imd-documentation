ElementColorMappingInputNode
============================
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class ElementColorMappingInputNode : InputNode<ElementColorMappingProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~Narupa.Core.IMapping`2` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.ElementColorMappingProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
