AminoAcid
=========
.. cs:setscope:: Narupa.Core.Science

.. cs:class:: public sealed class AminoAcid
    
    Definition of an amino acid.
    
    

    
    .. cs:field:: public static readonly AminoAcid Alanine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Arginine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Asparagine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid AsparticAcid
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Cysteine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Glutamine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid GlutamicAcid
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Glycine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Histidine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Isoleucine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Leucine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Lysine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Methionine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Phenylalanine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Proline
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Serine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Threonine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Tryptophan
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Tyrosine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid Valine
        
        :returns Narupa.Core.Science.AminoAcid: 
        
    .. cs:field:: public static readonly AminoAcid[] StandardAminoAcids
        
        The 20 standard amino acids.
        
        

        :returns Narupa.Core.Science.AminoAcid[]: 
        
    .. cs:property:: public string Name { get; }
        
        Common name of the amino acid.
        
        

        :returns System.String: 
        
    .. cs:property:: public string ThreeLetterCode { get; }
        
        Three letter code of the amino acid.
        
        

        :returns System.String: 
        
    .. cs:property:: public char SingleLetterCode { get; }
        
        Single letter code of the amino acid.
        
        

        :returns System.Char: 
        
    .. cs:method:: public static bool IsStandardAminoAcid(string residueName)
        
        Is the provided residue name recognized as a standard amino acid?
        
        

        :param System.String residueName: 
        :returns System.Boolean: 
        
    .. cs:method:: [CanBeNull] public static AminoAcid GetAminoAcidFromResidue(string residueName)
        
        Get the :cs:any:`~Narupa.Core.Science.AminoAcid` for the provided residue name, returning null
        if it is not a valid amino acid.
        
        

        :param System.String residueName: 
        :returns Narupa.Core.Science.AminoAcid: 
        
