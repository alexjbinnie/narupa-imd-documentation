DsspAlgorithm
=============
.. cs:setscope:: Narupa.Visualisation.Node.Protein

.. cs:class:: public class DsspAlgorithm
    
    
    .. cs:method:: public static SecondaryStructureResidueData[] GetResidueData(IReadOnlyCollection<int> aminoAcidResidues, int[] atomResidues, string[] atomNames)
        
        Get ResidueData for amino acid residues, storing locations of alpha carbons etc.
        
        

        :param System.Collections.Generic.IReadOnlyCollection{System.Int32} aminoAcidResidues: 
        :param System.Int32[] atomResidues: 
        :param System.String[] atomNames: 
        :returns Narupa.Visualisation.Node.Protein.SecondaryStructureResidueData[]: 
        
    .. cs:method:: public static void UpdateResidueAtomPositions(Vector3[] atomPositions, IList<SecondaryStructureResidueData> residues)
        
        :param UnityEngine.Vector3[] atomPositions: 
        :param System.Collections.Generic.IList{Narupa.Visualisation.Node.Protein.SecondaryStructureResidueData} residues: 
        
    .. cs:method:: public static void CalculateSecondaryStructure(IReadOnlyList<SecondaryStructureResidueData> residues, DsspOptions options)
        
        :param System.Collections.Generic.IReadOnlyList{Narupa.Visualisation.Node.Protein.SecondaryStructureResidueData} residues: 
        :param Narupa.Visualisation.Node.Protein.DsspOptions options: 
        
