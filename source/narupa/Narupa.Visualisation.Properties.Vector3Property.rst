Vector3Property
===============
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class Vector3Property : SerializableProperty<Vector3>, IProperty<Vector3>, IReadOnlyProperty<Vector3>, IProperty, IReadOnlyProperty
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for a :cs:any:`~UnityEngine.Vector3` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.SerializableProperty{UnityEngine.Vector3}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{UnityEngine.Vector3}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{UnityEngine.Vector3}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
