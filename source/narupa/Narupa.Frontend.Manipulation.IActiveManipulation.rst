IActiveManipulation
===================
.. cs:setscope:: Narupa.Frontend.Manipulation

.. cs:interface:: public interface IActiveManipulation
    
    A handle for communicating updates from a manipulator (e.g XR controller)
    to the manipulation it is actively engaging in (e.g grabbing an object)
    
    

    
    .. cs:event:: event Action ManipulationEnded
        
        Raised once when the manipulation ends.
        
        

        :returns System.Action: 
        
    .. cs:method:: void UpdateManipulatorPose(UnitScaleTransformation manipulatorPose)
        
        Provide an updated pose transformation for the manipulator.
        
        

        :param Narupa.Core.Math.UnitScaleTransformation manipulatorPose: 
        
    .. cs:method:: void EndManipulation()
        
        End the manipulation, and expect no further relationship between the
        manipulator and this manipulation.
        
        

        
