IProperty<TValue>
=================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:interface:: public interface IProperty<TValue> : IReadOnlyProperty<TValue>, IProperty, IReadOnlyProperty
    
    

    
    .. cs:property:: TValue Value { get; set; }
        
        

        :returns {TValue}: 
        
    .. cs:property:: IReadOnlyProperty<TValue> LinkedProperty { set; }
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{{TValue}}: 
        
