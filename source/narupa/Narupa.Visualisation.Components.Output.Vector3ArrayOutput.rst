Vector3ArrayOutput
==================
.. cs:setscope:: Narupa.Visualisation.Components.Output

.. cs:class:: public class Vector3ArrayOutput : VisualisationComponent<Vector3ArrayOutputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<Vector3ArrayOutputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Output.Vector3ArrayOutputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Output.Vector3ArrayOutputNode}`
    
