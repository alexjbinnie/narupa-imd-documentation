CommandReply
============
.. cs:setscope:: Narupa.Protocol.Command

.. cs:class:: public sealed class CommandReply : Object, IMessage<CommandReply>, IMessage, IEquatable<CommandReply>, IDeepCloneable<CommandReply>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Command.CommandReply}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Command.CommandReply}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Command.CommandReply}`
    
    .. cs:field:: public const int ResultFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public CommandReply()
        
        
    .. cs:constructor:: public CommandReply(CommandReply other)
        
        :param Narupa.Protocol.Command.CommandReply other: 
        
    .. cs:method:: public CommandReply Clone()
        
        :returns Narupa.Protocol.Command.CommandReply: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(CommandReply other)
        
        :param Narupa.Protocol.Command.CommandReply other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(CommandReply other)
        
        :param Narupa.Protocol.Command.CommandReply other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<CommandReply> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Command.CommandReply}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public Struct Result { get; set; }
        
        :returns Google.Protobuf.WellKnownTypes.Struct: 
        
