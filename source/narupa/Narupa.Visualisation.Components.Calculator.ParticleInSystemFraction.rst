ParticleInSystemFraction
========================
.. cs:setscope:: Narupa.Visualisation.Components.Calculator

.. cs:class:: public class ParticleInSystemFraction : VisualisationComponent<ParticleInSystemFractionNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ParticleInSystemFractionNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Calculator.ParticleInSystemFractionNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Calculator.ParticleInSystemFractionNode}`
    
