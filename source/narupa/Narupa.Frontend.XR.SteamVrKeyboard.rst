SteamVrKeyboard
===============
.. cs:setscope:: Narupa.Frontend.XR

.. cs:class:: public class SteamVrKeyboard : MonoBehaviour
    
    Interface with SteamVR that allows keyboard input to be done using the SteamVR overlay.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: public static SteamVrKeyboard Instance { get; }
        
        :returns Narupa.Frontend.XR.SteamVrKeyboard: 
        
    .. cs:property:: public static bool IsKeyboardAvailable { get; }
        
        :returns System.Boolean: 
        
    .. cs:field:: public bool minimalMode
        
        :returns System.Boolean: 
        
    .. cs:method:: public static void ShowKeyboard(string initialText, Action<string> onChanged, Action<string> onFinished)
        
        :param System.String initialText: 
        :param System.Action{System.String} onChanged: 
        :param System.Action{System.String} onFinished: 
        
