StringColorMapping
==================
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [CreateAssetMenu(menuName = "Definition/String-Color Mapping")] public class StringColorMapping : ScriptableObject
    
    Stores key-value pair of atomic number and color, for defining CPK style
    coloring.
    
    

    :inherits: :cs:any:`~UnityEngine.ScriptableObject`
    
    .. cs:method:: public Color GetColor(string name)
        
        Get the color for the given atomic element, returning a default color if the
        element is not defined.
        
        

        :param System.String name: 
        :returns UnityEngine.Color: 
        
