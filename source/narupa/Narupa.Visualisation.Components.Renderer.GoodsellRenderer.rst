GoodsellRenderer
================
.. cs:setscope:: Narupa.Visualisation.Components.Renderer

.. cs:class:: public class GoodsellRenderer : VisualisationComponentRenderer<GoodsellRendererNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<GoodsellRendererNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.Renderer.VisualisationComponentRenderer{Narupa.Visualisation.Node.Renderer.GoodsellRendererNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Renderer.GoodsellRendererNode}`
    
    .. cs:method:: protected override void OnEnable()
        
        
    .. cs:method:: protected override void Render(Camera camera)
        
        :param UnityEngine.Camera camera: 
        
    .. cs:method:: protected override void UpdateInEditor()
        
        
