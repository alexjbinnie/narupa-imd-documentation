TetrahedralSpline
=================
.. cs:setscope:: Narupa.Visualisation.Components.Spline

.. cs:class:: public class TetrahedralSpline : VisualisationComponent<TetrahedralSplineNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<TetrahedralSplineNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Spline.TetrahedralSplineNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Spline.TetrahedralSplineNode}`
    
