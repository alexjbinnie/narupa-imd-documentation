SetResourceValueRequest
=======================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class SetResourceValueRequest : Object, IMessage<SetResourceValueRequest>, IMessage, IEquatable<SetResourceValueRequest>, IDeepCloneable<SetResourceValueRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.SetResourceValueRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.SetResourceValueRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.SetResourceValueRequest}`
    
    .. cs:field:: public const int PlayerIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int ResourceIdFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:field:: public const int ResourceValueFieldNumber = 3
        
        :returns System.Int32: 
        
    .. cs:constructor:: public SetResourceValueRequest()
        
        
    .. cs:constructor:: public SetResourceValueRequest(SetResourceValueRequest other)
        
        :param Narupa.Protocol.Multiplayer.SetResourceValueRequest other: 
        
    .. cs:method:: public SetResourceValueRequest Clone()
        
        :returns Narupa.Protocol.Multiplayer.SetResourceValueRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(SetResourceValueRequest other)
        
        :param Narupa.Protocol.Multiplayer.SetResourceValueRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(SetResourceValueRequest other)
        
        :param Narupa.Protocol.Multiplayer.SetResourceValueRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<SetResourceValueRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.SetResourceValueRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string PlayerId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public string ResourceId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public Value ResourceValue { get; set; }
        
        :returns Google.Protobuf.WellKnownTypes.Value: 
        
