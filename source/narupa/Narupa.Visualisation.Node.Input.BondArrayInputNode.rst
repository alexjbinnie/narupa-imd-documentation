BondArrayInputNode
==================
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class BondArrayInputNode : InputNode<BondArrayProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~Narupa.Frame.BondPair` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.Collections.BondArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
