ResourceRequestResponse
=======================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class ResourceRequestResponse : Object, IMessage<ResourceRequestResponse>, IMessage, IEquatable<ResourceRequestResponse>, IDeepCloneable<ResourceRequestResponse>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.ResourceRequestResponse}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.ResourceRequestResponse}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.ResourceRequestResponse}`
    
    .. cs:field:: public const int SuccessFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public ResourceRequestResponse()
        
        
    .. cs:constructor:: public ResourceRequestResponse(ResourceRequestResponse other)
        
        :param Narupa.Protocol.Multiplayer.ResourceRequestResponse other: 
        
    .. cs:method:: public ResourceRequestResponse Clone()
        
        :returns Narupa.Protocol.Multiplayer.ResourceRequestResponse: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(ResourceRequestResponse other)
        
        :param Narupa.Protocol.Multiplayer.ResourceRequestResponse other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(ResourceRequestResponse other)
        
        :param Narupa.Protocol.Multiplayer.ResourceRequestResponse other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<ResourceRequestResponse> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.ResourceRequestResponse}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public bool Success { get; set; }
        
        :returns System.Boolean: 
        
