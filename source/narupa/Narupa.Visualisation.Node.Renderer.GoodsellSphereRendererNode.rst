GoodsellSphereRendererNode
==========================
.. cs:setscope:: Narupa.Visualisation.Node.Renderer

.. cs:class:: [Serializable] public class GoodsellSphereRendererNode : ParticleSphereRendererNode, IDisposable
    
    Extends the normal sphere rendering by providing residue IDs in a separate buffer.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Renderer.ParticleSphereRendererNode`
    :implements: :cs:any:`~System.IDisposable`
    
    .. cs:property:: public IntArrayProperty ParticleResidues { get; }
        
        :returns Narupa.Visualisation.Properties.IntArrayProperty: 
        
    .. cs:method:: protected override void UpdateBuffers()
        
        
    .. cs:method:: public override void ResetBuffers()
        
        
