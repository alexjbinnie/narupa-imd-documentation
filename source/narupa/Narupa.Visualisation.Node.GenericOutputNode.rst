GenericOutputNode
=================
.. cs:setscope:: Narupa.Visualisation.Node

.. cs:class:: public abstract class GenericOutputNode
    
    A generic structure for a visualisation node.
    
    

    
    .. cs:property:: protected abstract bool IsInputValid { get; }
        
        Is the current input valid and can it create a valid output?
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected abstract bool IsInputDirty { get; }
        
        Are any of the inputs dirty?
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected abstract void ClearDirty()
        
        Clear the dirty state of all inputs.
        
        

        
    .. cs:method:: protected abstract void UpdateOutput()
        
        Update the output based upon the input.
        
        

        
    .. cs:method:: protected abstract void ClearOutput()
        
        Clear the output to a default when the input is invalid.
        
        

        
    .. cs:method:: public void Refresh()
        
        Refresh the node.
        
        

        
