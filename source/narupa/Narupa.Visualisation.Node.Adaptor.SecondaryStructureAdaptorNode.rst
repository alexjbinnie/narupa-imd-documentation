SecondaryStructureAdaptorNode
=============================
.. cs:setscope:: Narupa.Visualisation.Node.Adaptor

.. cs:class:: [Serializable] public class SecondaryStructureAdaptorNode : ParentedAdaptorNode, IDynamicPropertyProvider, IPropertyProvider
    
    An :cs:any:`~Narupa.Visualisation.Node.Adaptor.BaseAdaptorNode` which injects a 'residue.secondarystructures' key which provides per residue secondary structure based on the DSSP algorithm.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Adaptor.ParentedAdaptorNode`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    
    .. cs:constructor:: public SecondaryStructureAdaptorNode()
        
        
    .. cs:method:: public override void Refresh()
        
        

        
    .. cs:method:: public override IReadOnlyProperty GetProperty(string key)
        
        

        :param System.String key: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
    .. cs:method:: public override IEnumerable<(string name, IReadOnlyProperty property)> GetProperties()
        
        

        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,Narupa.Visualisation.Property.IReadOnlyProperty}}: 
        
    .. cs:method:: public override IReadOnlyProperty<T> GetOrCreateProperty<T>(string name)
        
        

        :param System.String name: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{{T}}: 
        
