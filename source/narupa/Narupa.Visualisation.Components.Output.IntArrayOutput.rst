IntArrayOutput
==============
.. cs:setscope:: Narupa.Visualisation.Components.Output

.. cs:class:: public class IntArrayOutput : VisualisationComponent<IntArrayOutputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<IntArrayOutputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Output.IntArrayOutputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Output.IntArrayOutputNode}`
    
