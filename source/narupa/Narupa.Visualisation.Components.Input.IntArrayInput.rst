IntArrayInput
=============
.. cs:setscope:: Narupa.Visualisation.Components.Input

.. cs:class:: public class IntArrayInput : VisualisationComponent<IntArrayInputNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<IntArrayInputNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Input.IntArrayInputNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Input.IntArrayInputNode}`
    
