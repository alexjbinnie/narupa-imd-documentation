FloatArrayOutputNode
====================
.. cs:setscope:: Narupa.Visualisation.Node.Output

.. cs:class:: [Serializable] public class FloatArrayOutputNode : OutputNode<FloatArrayProperty>, IOutputNode
    
    :inherits: :cs:any:`~Narupa.Visualisation.Node.Output.OutputNode{Narupa.Visualisation.Properties.Collections.FloatArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Output.IOutputNode`
    
