LoadTrajectoryResponse
======================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public sealed class LoadTrajectoryResponse : Object, IMessage<LoadTrajectoryResponse>, IMessage, IEquatable<LoadTrajectoryResponse>, IDeepCloneable<LoadTrajectoryResponse>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Instance.LoadTrajectoryResponse}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Instance.LoadTrajectoryResponse}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Instance.LoadTrajectoryResponse}`
    
    .. cs:field:: public const int InstanceIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:constructor:: public LoadTrajectoryResponse()
        
        
    .. cs:constructor:: public LoadTrajectoryResponse(LoadTrajectoryResponse other)
        
        :param Narupa.Protocol.Instance.LoadTrajectoryResponse other: 
        
    .. cs:method:: public LoadTrajectoryResponse Clone()
        
        :returns Narupa.Protocol.Instance.LoadTrajectoryResponse: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(LoadTrajectoryResponse other)
        
        :param Narupa.Protocol.Instance.LoadTrajectoryResponse other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(LoadTrajectoryResponse other)
        
        :param Narupa.Protocol.Instance.LoadTrajectoryResponse other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<LoadTrajectoryResponse> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Instance.LoadTrajectoryResponse}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string InstanceId { get; set; }
        
        :returns System.String: 
        
