ColorArrayProperty
==================
.. cs:setscope:: Narupa.Visualisation.Properties.Collections

.. cs:class:: [Serializable] public class ColorArrayProperty : ArrayProperty<Color>, IProperty<Color[]>, IReadOnlyProperty<Color[]>, IProperty, IReadOnlyProperty, IEnumerable<Color>, IEnumerable
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an array of :cs:any:`~UnityEngine.Color`
    values.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Properties.Collections.ArrayProperty{UnityEngine.Color}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{UnityEngine.Color[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{UnityEngine.Color[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{UnityEngine.Color}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
