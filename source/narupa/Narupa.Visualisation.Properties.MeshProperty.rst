MeshProperty
============
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class MeshProperty : SerializableProperty<Mesh>, IProperty<Mesh>, IReadOnlyProperty<Mesh>, IProperty, IReadOnlyProperty
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for a :cs:any:`~UnityEngine.Mesh` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.SerializableProperty{UnityEngine.Mesh}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{UnityEngine.Mesh}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{UnityEngine.Mesh}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
