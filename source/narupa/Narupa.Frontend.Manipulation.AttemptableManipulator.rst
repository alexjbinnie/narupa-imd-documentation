AttemptableManipulator
======================
.. cs:setscope:: Narupa.Frontend.Manipulation

.. cs:class:: public class AttemptableManipulator : Manipulator, IPosedObject
    
    :inherits: :cs:any:`~Narupa.Frontend.Manipulation.Manipulator`
    :implements: :cs:any:`~Narupa.Frontend.Input.IPosedObject`
    
    .. cs:constructor:: public AttemptableManipulator(IPosedObject posedObject, ManipulationAttemptHandler manipulationAttempt)
        
        :param Narupa.Frontend.Input.IPosedObject posedObject: 
        :param Narupa.Frontend.Manipulation.ManipulationAttemptHandler manipulationAttempt: 
        
    .. cs:method:: public void AttemptManipulation()
        
        
