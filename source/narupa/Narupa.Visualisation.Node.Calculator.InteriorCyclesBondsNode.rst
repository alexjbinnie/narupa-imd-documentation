InteriorCyclesBondsNode
=======================
.. cs:setscope:: Narupa.Visualisation.Node.Calculator

.. cs:class:: [Serializable] public class InteriorCyclesBondsNode
    
    Generates all internal bonds requires for a cycle.
    
    

    
    .. cs:method:: public void Refresh()
        
        
