PropertyProviderExtensions
==========================
.. cs:setscope:: Narupa.Visualisation.Components

.. cs:class:: public static class PropertyProviderExtensions
    
    Extension methods for :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`.
    
    

    
    .. cs:method:: public static IReadOnlyProperty<T> GetOrCreateProperty<T>(this IPropertyProvider provider, string key)
        
        

        :param Narupa.Visualisation.Property.IPropertyProvider provider: 
        :param System.String key: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{{T}}: 
        
    .. cs:method:: public static IEnumerable<(string name, Type type)> GetPotentialProperties(this IPropertyProvider provider)
        
        

        :param Narupa.Visualisation.Property.IPropertyProvider provider: 
        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{System.String,System.Type}}: 
        
    .. cs:method:: public static IReadOnlyProperty<T> GetProperty<T>(this IPropertyProvider provider, string key)
        
        

        :param Narupa.Visualisation.Property.IPropertyProvider provider: 
        :param System.String key: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty{{T}}: 
        
    .. cs:method:: public static bool CanDynamicallyProvideProperty<T>(this IPropertyProvider provider, string name)
        
        

        :param Narupa.Visualisation.Property.IPropertyProvider provider: 
        :param System.String name: 
        :returns System.Boolean: 
        
    .. cs:method:: public static bool CanDynamicallyProvideProperty(this IPropertyProvider provider, string name, Type type)
        
        

        :param Narupa.Visualisation.Property.IPropertyProvider provider: 
        :param System.String name: 
        :param System.Type type: 
        :returns System.Boolean: 
        
    .. cs:method:: public static bool CanDynamicallyProvideProperty(this IDynamicPropertyProvider provider, string name, Type type)
        
        

        :param Narupa.Visualisation.Components.IDynamicPropertyProvider provider: 
        :param System.String name: 
        :param System.Type type: 
        :returns System.Boolean: 
        
    .. cs:method:: public static IReadOnlyProperty GetOrCreateProperty(this IDynamicPropertyProvider provider, string name, Type type)
        
        

        :param Narupa.Visualisation.Components.IDynamicPropertyProvider provider: 
        :param System.String name: 
        :param System.Type type: 
        :returns Narupa.Visualisation.Property.IReadOnlyProperty: 
        
