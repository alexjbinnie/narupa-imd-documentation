PerElementScaleNode
===================
.. cs:setscope:: Narupa.Visualisation.Node.Scale

.. cs:class:: [Serializable] public abstract class PerElementScaleNode : VisualiserScaleNode
    
    Base code for visualiser node which generates scales based upon atomic
    elements.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Scale.VisualiserScaleNode`
    
    .. cs:property:: public IProperty<Element[]> Elements { get; }
        
        Atomic element array input.
        
        

        :returns Narupa.Visualisation.Property.IProperty{Narupa.Core.Science.Element[]}: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:method:: protected override void UpdateOutput()
        
        

        
    .. cs:method:: protected override void ClearOutput()
        
        

        
    .. cs:method:: protected abstract float GetScale(Element element)
        
        Get the scale for the given atomic element.
        
        

        :param Narupa.Core.Science.Element element: 
        :returns System.Single: 
        
