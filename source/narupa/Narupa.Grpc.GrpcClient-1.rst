GrpcClient<TClient>
===================
.. cs:setscope:: Narupa.Grpc

.. cs:class:: public abstract class GrpcClient<TClient> : Cancellable, ICancellable, ICancellationTokenSource, IDisposable, IAsyncClosable where TClient : ClientBase<TClient>
    
    Base implementation of a C# wrapper around a gRPC client to a specific service.
    
    

    :inherits: :cs:any:`~Narupa.Core.Async.Cancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellable`
    :implements: :cs:any:`~Narupa.Core.Async.ICancellationTokenSource`
    :implements: :cs:any:`~System.IDisposable`
    :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable`
    
    .. cs:property:: protected TClient Client { get; }
        
        gRPC Client that provides access to RPC calls.
        
        

        :returns {TClient}: 
        
    .. cs:property:: protected Command.CommandClient CommandClient { get; }
        
        The client used to access the Command service on the same port as this client.
        
        

        :returns Narupa.Protocol.Command.Command.CommandClient: 
        
    .. cs:constructor:: public GrpcClient([NotNull] GrpcConnection connection)
        
        Create a client to a server described by the provided
        :cs:any:`~Narupa.Grpc.GrpcConnection`.
        
        

        :param Narupa.Grpc.GrpcConnection connection: 
        
    .. cs:method:: public Task<Dictionary<string, object>> RunCommandAsync(string command, Dictionary<string, object> arguments = null)
        
        Run a command on a gRPC service that uses the command service.
        
        

        :param System.String command: The name of the command to run, which must be registered on the server.
        :param System.Collections.Generic.Dictionary{System.String,System.Object} arguments: Name/value arguments to provide to the command.
        :returns System.Threading.Tasks.Task{System.Collections.Generic.Dictionary{System.String,System.Object}}: Dictionary of results produced by the command.
        
    .. cs:method:: protected IncomingStream<TResponse> GetIncomingStream<TRequest, TResponse>(ServerStreamingCall<TRequest, TResponse> call, TRequest request, CancellationToken externalToken = default(CancellationToken))
        
        Create an incoming stream from the definition of a gRPC call.
        
        

        :param Narupa.Grpc.Stream.ServerStreamingCall{{TRequest},{TResponse}} call: 
        :param {TRequest} request: 
        :param System.Threading.CancellationToken externalToken: 
        :returns Narupa.Grpc.Stream.IncomingStream{{TResponse}}: 
        
    .. cs:method:: protected OutgoingStream<TOutgoing, TResponse> GetOutgoingStream<TOutgoing, TResponse>(ClientStreamingCall<TOutgoing, TResponse> call, CancellationToken externalToken = default(CancellationToken))
        
        Create an outgoing stream from the definition of a gRPC call.
        
        

        :param Narupa.Grpc.Stream.ClientStreamingCall{{TOutgoing},{TResponse}} call: 
        :param System.Threading.CancellationToken externalToken: 
        :returns Narupa.Grpc.Stream.OutgoingStream{{TOutgoing},{TResponse}}: 
        
    .. cs:method:: public Task CloseAsync()
        
        

        :implements: :cs:any:`~Narupa.Core.Async.IAsyncClosable.CloseAsync`
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public void CloseAndCancelAllSubscriptions()
        
        
