AffineTransformation
====================
.. cs:setscope:: Narupa.Core.Math

.. cs:struct:: [Serializable] public struct AffineTransformation : ITransformation
    
    An transformation between two 3D spaces, defined by where it maps the
    three axes of cartesian space and an offset. This can represent any combination
    of rotations,
    reflections, translations, scaling and shears.
    
    

    Every affine transformation can be represented as an augmented 4x4 matrix, with
    the three axes (with the 4th component 0) and the origin (with the 4th
    component 1) representing the three columns of the matrix.
    
    

    :implements: :cs:any:`~Narupa.Core.Math.ITransformation`
    
    .. cs:property:: ITransformation ITransformation.inverse { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.inverse`
        :returns Narupa.Core.Math.ITransformation: 
        
    .. cs:field:: public Vector3 xAxis
        
        The vector to which this transformation maps the direction (1, 0, 0).
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Vector3 yAxis
        
        The vector to which this transformation maps the direction (0, 1, 0).
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Vector3 zAxis
        
        The vector to which this transformation maps the direction (0, 0, 1).
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:field:: public Vector3 origin
        
        The translation that this transformation applies.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:constructor:: public AffineTransformation(Vector3 xAxis, Vector3 yAxis, Vector3 zAxis, Vector3 position)
        
        Create an affine transformation which maps the x, y and z directions to new
        vectors, and translates to a new origin.
        
        

        :param UnityEngine.Vector3 xAxis: 
        :param UnityEngine.Vector3 yAxis: 
        :param UnityEngine.Vector3 zAxis: 
        :param UnityEngine.Vector3 position: 
        
    .. cs:constructor:: public AffineTransformation(Matrix4x4 matrix)
        
        Create an affine transformation from the upper 3x4 matrix.
        
        

        :param UnityEngine.Matrix4x4 matrix: 
        
    .. cs:property:: public static AffineTransformation identity { get; }
        
        The identity transformation.
        
        

        :returns Narupa.Core.Math.AffineTransformation: 
        
    .. cs:property:: public Vector3 axesMagnitudes { get; }
        
        The magnitudes of the three axes that define this linear transformation.
        
        

        :returns UnityEngine.Vector3: 
        
    .. cs:property:: public AffineTransformation inverse { get; }
        
        

        :returns Narupa.Core.Math.AffineTransformation: 
        
    .. cs:property:: public Matrix4x4 matrix { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.matrix`
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:property:: public Matrix4x4 inverseMatrix { get; }
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.inverseMatrix`
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:conversion:: public static implicit operator Matrix4x4(AffineTransformation transformation)
        
        :param Narupa.Core.Math.AffineTransformation transformation: 
        :returns UnityEngine.Matrix4x4: 
        
    .. cs:operator:: public static AffineTransformation operator *(AffineTransformation a, AffineTransformation b)
        
        :param Narupa.Core.Math.AffineTransformation a: 
        :param Narupa.Core.Math.AffineTransformation b: 
        :returns Narupa.Core.Math.AffineTransformation: 
        
    .. cs:method:: public Vector3 TransformPoint(Vector3 point)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.TransformPoint(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 InverseTransformPoint(Vector3 point)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.InverseTransformPoint(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 point: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 TransformDirection(Vector3 direction)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.TransformDirection(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 direction: 
        :returns UnityEngine.Vector3: 
        
    .. cs:method:: public Vector3 InverseTransformDirection(Vector3 direction)
        
        

        :implements: :cs:any:`~Narupa.Core.Math.ITransformation.InverseTransformDirection(UnityEngine.Vector3)`
        :param UnityEngine.Vector3 direction: 
        :returns UnityEngine.Vector3: 
        
