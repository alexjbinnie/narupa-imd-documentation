ProteinFilterNode
=================
.. cs:setscope:: Narupa.Visualisation.Node.Filter

.. cs:class:: [Serializable] public class ProteinFilterNode : VisualiserFilterNode
    
    Filters particles by if they're in a standard amino acid, optionally excluding hydrogens.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Filter.VisualiserFilterNode`
    
    .. cs:property:: public IProperty<bool> IncludeHydrogens { get; }
        
        Should hydrogens be included?
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Boolean}: 
        
    .. cs:property:: public IProperty<bool> IncludeWater { get; }
        
        Should water be included?
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Boolean}: 
        
    .. cs:property:: public IProperty<bool> IncludeNonStandardResidues { get; }
        
        Should non standard residues be included?
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Boolean}: 
        
    .. cs:property:: public IProperty<int[]> ParticleResidues { get; }
        
        The indices of the residue for each particle.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.Int32[]}: 
        
    .. cs:property:: public IProperty<string[]> ResidueNames { get; }
        
        The residue names.
        
        

        :returns Narupa.Visualisation.Property.IProperty{System.String[]}: 
        
    .. cs:property:: public IProperty<Element[]> ParticleElements { get; }
        
        The particle elements.
        
        

        :returns Narupa.Visualisation.Property.IProperty{Narupa.Core.Science.Element[]}: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:property:: protected override int MaximumFilterCount { get; }
        
        

        :returns System.Int32: 
        
    .. cs:method:: protected override IEnumerable<int> GetFilteredIndices()
        
        

        :returns System.Collections.Generic.IEnumerable{System.Int32}: 
        
