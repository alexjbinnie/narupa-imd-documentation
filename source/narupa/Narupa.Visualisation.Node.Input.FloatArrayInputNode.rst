FloatArrayInputNode
===================
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public class FloatArrayInputNode : InputNode<FloatArrayProperty>, IInputNode
    
    Input for the visualisation system that provides a :cs:any:`~float[]` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Input.InputNode{Narupa.Visualisation.Properties.Collections.FloatArrayProperty}`
    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
