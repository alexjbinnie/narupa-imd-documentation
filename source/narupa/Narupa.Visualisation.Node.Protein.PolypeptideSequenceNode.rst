PolypeptideSequenceNode
=======================
.. cs:setscope:: Narupa.Visualisation.Node.Protein

.. cs:class:: [Serializable] public class PolypeptideSequenceNode : GenericOutputNode
    
    Calculates sequences of subsequent residues in entities which are standard
    amino acids, hence forming polypeptide chains.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:property:: public IProperty<string[]> AtomNames { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{System.String[]}: 
        
    .. cs:property:: public IProperty<int[]> AtomResidues { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{System.Int32[]}: 
        
    .. cs:property:: public IProperty<string[]> ResidueNames { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{System.String[]}: 
        
    .. cs:property:: public IProperty<int[]> ResidueEntities { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{System.Int32[]}: 
        
    .. cs:property:: public IProperty<IReadOnlyList<int>[]> ResidueSequences { get; }
        
        :returns Narupa.Visualisation.Property.IProperty{System.Collections.Generic.IReadOnlyList{System.Int32}[]}: 
        
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:method:: protected override void UpdateOutput()
        
        

        
    .. cs:method:: protected override void ClearOutput()
        
        

        
