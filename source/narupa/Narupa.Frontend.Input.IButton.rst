IButton
=======
.. cs:setscope:: Narupa.Frontend.Input

.. cs:interface:: public interface IButton
    
    Represents a button input that can be pressed, held, then released. Allows
    polling of the current state and listening to press and release events.
    
    

    
    .. cs:property:: bool IsPressed { get; }
        
        Is the button in a pressed state?
        
        

        :returns System.Boolean: 
        
    .. cs:event:: event Action Pressed
        
        Callback when the button is pressed.
        
        

        :returns System.Action: 
        
    .. cs:event:: event Action Released
        
        Callback when the button is released.
        
        

        :returns System.Action: 
        
