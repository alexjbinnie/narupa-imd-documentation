IndexedPool<TInstance>
======================
.. cs:setscope:: Narupa.Frontend.Utility

.. cs:class:: public sealed class IndexedPool<TInstance>
    
    Utility class for creating and managing a pool of instances of some 
    object.
    
    

    The purpose of pooling is to avoid creating new instances from scratch 
    when the number of instances may be fluctuating frequently and instance
    creation has some non-neglible cost.
    
    

    
    .. cs:property:: public int ActiveInstanceCount { get; }
        
        The number of active instances.
        
        

        :returns System.Int32: 
        
    .. cs:indexer:: public TInstance this[int index] { get; }
        
        Return the instance as the given index. There are always at least
        `ActiveInstanceCount` instances.
        
        

        :param System.Int32 index: 
        :returns {TInstance}: 
        
    .. cs:event:: public event Action<TInstance> InstanceActivated
        
        Raised when an instance is about to be used.
        
        

        :returns System.Action{{TInstance}}: 
        
    .. cs:event:: public event Action<TInstance> InstanceDeactivated
        
        Raised when a previously used instance becomes unused.
        
        

        :returns System.Action{{TInstance}}: 
        
    .. cs:constructor:: public IndexedPool(Func<TInstance> createInstanceCallback, Action<TInstance> activateInstanceCallback = null, Action<TInstance> deactivateInstanceCallback = null)
        
        Create a pool with a given callback for creating new instances,
        and optionally callbacks for activating and deactivating instances
        when them switch in and out of use.
        
        

        :param System.Func{{TInstance}} createInstanceCallback: 
        :param System.Action{{TInstance}} activateInstanceCallback: 
        :param System.Action{{TInstance}} deactivateInstanceCallback: 
        
    .. cs:method:: public void SetActiveInstanceCount(int count)
        
        Ensure an exact number of instances are active, creating new 
        instances if necessary, otherwise reusing existing instances and 
        deactivating surplus instances.
        
        

        :param System.Int32 count: 
        
    .. cs:method:: public void MapConfig<TConfig>(IEnumerable<TConfig> configs, Action<TConfig, TInstance> mapConfigToInstance)
        
        Make sure that there are exactly as many instances as configs given
        then run a mapping function pairing every config to its own 
        instance.
        
        

        :param System.Collections.Generic.IEnumerable{{TConfig}} configs: 
        :param System.Action{{TConfig},{TInstance}} mapConfigToInstance: 
        
    .. cs:method:: public void MapIndex(int count, Action<int, TInstance> mapIndexToInstance)
        
        Set the count and then run a mapping function over every active
        instance.
        
        

        :param System.Int32 count: 
        :param System.Action{System.Int32,{TInstance}} mapIndexToInstance: 
        
