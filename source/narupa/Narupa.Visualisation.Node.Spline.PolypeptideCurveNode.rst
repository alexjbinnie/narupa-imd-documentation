PolypeptideCurveNode
====================
.. cs:setscope:: Narupa.Visualisation.Node.Spline

.. cs:class:: [Serializable] public class PolypeptideCurveNode : GenericOutputNode
    
    Calculates normals and tangents for splines going through a set of points, using the chemical structure for the normals.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:property:: protected override bool IsInputValid { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:property:: protected override bool IsInputDirty { get; }
        
        

        :returns System.Boolean: 
        
    .. cs:method:: protected override void ClearDirty()
        
        

        
    .. cs:method:: protected override void UpdateOutput()
        
        

        
    .. cs:method:: protected override void ClearOutput()
        
        

        
