ElementColorMappingProperty
===========================
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class ElementColorMappingProperty : InterfaceProperty<IMapping<Element, Color>>, IProperty<IMapping<Element, Color>>, IReadOnlyProperty<IMapping<Element, Color>>, IProperty, IReadOnlyProperty, ISerializationCallbackReceiver
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for a :cs:any:`~Narupa.Visualisation.Node.Color.ElementColorMapping`
    value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.InterfaceProperty{Narupa.Core.IMapping{Narupa.Core.Science.Element,UnityEngine.Color}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{Narupa.Core.IMapping{Narupa.Core.Science.Element,UnityEngine.Color}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Core.IMapping{Narupa.Core.Science.Element,UnityEngine.Color}}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    
