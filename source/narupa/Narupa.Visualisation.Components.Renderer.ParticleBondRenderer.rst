ParticleBondRenderer
====================
.. cs:setscope:: Narupa.Visualisation.Components.Renderer

.. cs:class:: public class ParticleBondRenderer : VisualisationComponentRenderer<ParticleBondRendererNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ParticleBondRendererNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.Renderer.VisualisationComponentRenderer{Narupa.Visualisation.Node.Renderer.ParticleBondRendererNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Renderer.ParticleBondRendererNode}`
    
    .. cs:method:: protected override void Render(Camera camera)
        
        :param UnityEngine.Camera camera: 
        
    .. cs:method:: protected override void UpdateInEditor()
        
        
