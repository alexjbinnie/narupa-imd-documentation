EnumerableExtensions
====================
.. cs:setscope:: Narupa.Core.Collections

.. cs:class:: public static class EnumerableExtensions
    
    
    .. cs:method:: public static IEnumerable<(TElement First, TElement Second)> GetPairs<TElement>(this IEnumerable<TElement> enumerable)
        
        Enumerate over all pairs of adjacent items in a list, such that the set [A, B,
        C, D] yields the pairs (A, B), (B, C) and (C, D).
        
        

        :param System.Collections.Generic.IEnumerable{{TElement}} enumerable: 
        :returns System.Collections.Generic.IEnumerable{System.ValueTuple{{TElement},{TElement}}}: 
        
    .. cs:method:: public static int IndexOf<TElement>(this IEnumerable<TElement> list, TElement item)
        
        Find the index of an item in an enumerable, returning -1 if the item is not
        present.
        
        

        :param System.Collections.Generic.IEnumerable{{TElement}} list: 
        :param {TElement} item: 
        :returns System.Int32: 
        
    .. cs:method:: public static IEnumerable<T> WithoutIndex<T>(this IEnumerable<T> list, int index)
        
        Yield an enumerable with the provided index skipped over.
        
        

        :param System.Collections.Generic.IEnumerable{{T}} list: 
        :param System.Int32 index: 
        :returns System.Collections.Generic.IEnumerable{{T}}: 
        
    .. cs:method:: public static IEnumerable<T> AsEnumerable<T>(this T value)
        
        Treat a single item as an :cs:any:`~System.Collections.Generic.IEnumerable`1` of one item.
        
        

        :param {T} value: 
        :returns System.Collections.Generic.IEnumerable{{T}}: 
        
    .. cs:method:: public static IEnumerable<IEnumerable<T>> GetPermutations<T>(this IEnumerable<T> set)
        
        Get all permutations of a set.
        
        

        :param System.Collections.Generic.IEnumerable{{T}} set: 
        :returns System.Collections.Generic.IEnumerable{System.Collections.Generic.IEnumerable{{T}}}: 
        
    .. cs:method:: public static IEnumerable<T> AsPretty<T>(this IEnumerable<T> enumerable)
        
        Wraps an :cs:any:`~System.Collections.Generic.IEnumerable`1` so the :cs:any:`~System.Object.ToString`
        method prints a list of the elements separated by commas.
        
        

        :param System.Collections.Generic.IEnumerable{{T}} enumerable: 
        :returns System.Collections.Generic.IEnumerable{{T}}: 
        
    .. cs:method:: public static IEnumerable<T> AsPretty<T>(this IEnumerable<T> enumerable, Func<T, string> toString)
        
        Wraps an :cs:any:`~System.Collections.Generic.IEnumerable`1` so the :cs:any:`~System.Object.ToString`
        method prints a list of the function :code:`toString` applied to
        each element, separated by commas.
        
        

        :param System.Collections.Generic.IEnumerable{{T}} enumerable: 
        :param System.Func{{T},System.String} toString: 
        :returns System.Collections.Generic.IEnumerable{{T}}: 
        
