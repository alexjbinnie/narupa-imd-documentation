ParentedAdaptorNode
===================
.. cs:setscope:: Narupa.Visualisation.Node.Adaptor

.. cs:class:: [Serializable] public class ParentedAdaptorNode : BaseAdaptorNode, IDynamicPropertyProvider, IPropertyProvider
    
    An :cs:any:`~Narupa.Visualisation.Node.Adaptor.BaseAdaptorNode` which is linked to another adaptor. This adaptor contains its own properties, but links them to the parent. This means that the parent can be changed without listeners to this adaptor needing to change their links.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.Adaptor.BaseAdaptorNode`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IDynamicPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    
    .. cs:property:: public IProperty<IDynamicPropertyProvider> ParentAdaptor { get; }
        
        The adaptor that this adaptor inherits from.
        
        

        :returns Narupa.Visualisation.Property.IProperty{Narupa.Visualisation.Components.IDynamicPropertyProvider}: 
        
    .. cs:method:: protected override void OnCreateProperty<T>(string key, IProperty<T> property)
        
        

        :param System.String key: 
        :param Narupa.Visualisation.Property.IProperty{{T}} property: 
        
    .. cs:method:: public override void Refresh()
        
        

        
