FrameReceivedCallback
=====================
.. cs:setscope:: Narupa.Grpc.Trajectory

.. cs:delegate:: public delegate void FrameReceivedCallback(uint frameIndex, [CanBeNull] FrameData frameData);
    
    Callback when a new frame is received.
    
    

    :param System.UInt32 frameIndex: 
    :param Narupa.Protocol.Trajectory.FrameData frameData: 
    
