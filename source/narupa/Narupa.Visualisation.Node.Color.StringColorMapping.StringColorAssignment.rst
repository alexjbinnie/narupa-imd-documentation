StringColorMapping.StringColorAssignment
========================================
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [Serializable] public class StringColorAssignment
    
    Key-value pair for atomic element to color mappings for serialisation in Unity
    
    

    
    .. cs:field:: public string value
        
        :returns System.String: 
        
    .. cs:field:: public Color color
        
        :returns UnityEngine.Color: 
        
