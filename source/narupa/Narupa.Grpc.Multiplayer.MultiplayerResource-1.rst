MultiplayerResource<TValue>
===========================
.. cs:setscope:: Narupa.Grpc.Multiplayer

.. cs:class:: public class MultiplayerResource<TValue>
    
    Represents a multiplayer resource that is shared across the multiplayer
    service.
    
    

    
    .. cs:constructor:: public MultiplayerResource(MultiplayerSession session, string key, Converter<object, TValue> objectToValue = null, Converter<TValue, object> valueToObject = null)
        
        Create a multiplayer resource.
        
        

        :param Narupa.Session.MultiplayerSession session: The multiplayer session that will provide this value.
        :param System.String key: The key that identifies this resource in the dictionary.
        :param System.Converter{System.Object,{TValue}} objectToValue: An optional converter for converting the value in the dictionary to an appropriate value.
        :param System.Converter{{TValue},System.Object} valueToObject: An optional converter for converting the value provided to one suitable for serialisation to protobuf.
        
    .. cs:event:: public event Action RemoteValueChanged
        
        Callback for when the shared value is changed.
        
        

        :returns System.Action: 
        
    .. cs:event:: public event Action ValueChanged
        
        Callback for when the value is changed, either remotely or locally.
        
        

        :returns System.Action: 
        
    .. cs:event:: public event Action LockAccepted
        
        Callback for when a lock request is accepted.
        
        

        :returns System.Action: 
        
    .. cs:event:: public event Action LockRejected
        
        Callback for when a lock request is rejected.
        
        

        :returns System.Action: 
        
    .. cs:event:: public event Action LockReleased
        
        Callback for when a lock is released.
        
        

        :returns System.Action: 
        
    .. cs:field:: public readonly string ResourceKey
        
        The key which identifies this resource.
        
        

        :returns System.String: 
        
    .. cs:property:: public MultiplayerResourceLockState LockState { get; }
        
        The current state of the lock on this resource.
        
        

        :returns Narupa.Grpc.Multiplayer.MultiplayerResourceLockState: 
        
    .. cs:property:: public TValue Value { get; }
        
        Value of this resource. Mirrors the value in the remote dictionary, unless a
        local change is in progress.
        
        

        :returns {TValue}: 
        
    .. cs:method:: public void ObtainLock()
        
        Obtain a lock on this resource.
        
        

        
    .. cs:method:: public void ReleaseLock()
        
        Release the lock on this resource.
        
        

        
    .. cs:method:: public void UpdateValueWithLock(TValue value)
        
        Set the local value of this key, and try to lock this resource to send it to
        everyone.
        If it is rejected, the value will revert to the default.
        
        

        :param {TValue} value: 
        
