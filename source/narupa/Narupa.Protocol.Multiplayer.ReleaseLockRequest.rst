ReleaseLockRequest
==================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class ReleaseLockRequest : Object, IMessage<ReleaseLockRequest>, IMessage, IEquatable<ReleaseLockRequest>, IDeepCloneable<ReleaseLockRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.ReleaseLockRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.ReleaseLockRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.ReleaseLockRequest}`
    
    .. cs:field:: public const int PlayerIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int ResourceIdFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public ReleaseLockRequest()
        
        
    .. cs:constructor:: public ReleaseLockRequest(ReleaseLockRequest other)
        
        :param Narupa.Protocol.Multiplayer.ReleaseLockRequest other: 
        
    .. cs:method:: public ReleaseLockRequest Clone()
        
        :returns Narupa.Protocol.Multiplayer.ReleaseLockRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(ReleaseLockRequest other)
        
        :param Narupa.Protocol.Multiplayer.ReleaseLockRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(ReleaseLockRequest other)
        
        :param Narupa.Protocol.Multiplayer.ReleaseLockRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<ReleaseLockRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.ReleaseLockRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string PlayerId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public string ResourceId { get; set; }
        
        :returns System.String: 
        
