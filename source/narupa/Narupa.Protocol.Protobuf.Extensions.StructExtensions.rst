StructExtensions
================
.. cs:setscope:: Narupa.Protocol.Protobuf.Extensions

.. cs:class:: public static class StructExtensions : Object
    
    
    .. cs:method:: public static Nullable<float> GetFloatValue(this Struct structure, string key)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :returns System.Nullable{System.Single}: 
        
    .. cs:method:: public static Nullable<double> GetDoubleValue(this Struct structure, string key)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :returns System.Nullable{System.Double}: 
        
    .. cs:method:: public static Nullable<int> GetIntValue(this Struct structure, string key)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :returns System.Nullable{System.Int32}: 
        
    .. cs:method:: public static Nullable<uint> GetUIntValue(this Struct structure, string key)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :returns System.Nullable{System.UInt32}: 
        
    .. cs:method:: public static string GetStringValue(this Struct structure, string key)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :returns System.String: 
        
    .. cs:method:: public static Nullable<bool> GetBoolValue(this Struct structure, string key)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :returns System.Nullable{System.Boolean}: 
        
    .. cs:method:: public static Struct GetStructValue(this Struct structure, string key)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :returns Google.Protobuf.WellKnownTypes.Struct: 
        
    .. cs:method:: public static ListValue GetListValue(this Struct structure, string key)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :returns Google.Protobuf.WellKnownTypes.ListValue: 
        
    .. cs:method:: public static Value GetValue(this Struct structure, string key)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :returns Google.Protobuf.WellKnownTypes.Value: 
        
    .. cs:method:: public static void SetBoolValue(this Struct structure, string key, bool value)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :param System.Boolean value: 
        
    .. cs:method:: public static void SetStringValue(this Struct structure, string key, string value)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :param System.String value: 
        
    .. cs:method:: public static void SetStructValue(this Struct structure, string key, Struct value)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :param Google.Protobuf.WellKnownTypes.Struct value: 
        
    .. cs:method:: public static void SetFloatValue(this Struct structure, string key, float value)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :param System.Single value: 
        
    .. cs:method:: public static void SetIntValue(this Struct structure, string key, int value)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :param System.Int32 value: 
        
    .. cs:method:: public static void SetUIntvalue(this Struct structure, string key, uint value)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :param System.UInt32 value: 
        
    .. cs:method:: public static void SetDoubleValue(this Struct structure, string key, double value)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :param System.Double value: 
        
    .. cs:method:: public static void SetListValue(this Struct structure, string key, Value[] value)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :param Google.Protobuf.WellKnownTypes.Value[] value: 
        
    .. cs:method:: public static void SetListValue(this Struct structure, string key, ListValue value)
        
        :param Google.Protobuf.WellKnownTypes.Struct structure: 
        :param System.String key: 
        :param Google.Protobuf.WellKnownTypes.ListValue value: 
        
