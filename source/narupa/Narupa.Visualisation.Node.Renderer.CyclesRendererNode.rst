CyclesRendererNode
==================
.. cs:setscope:: Narupa.Visualisation.Node.Renderer

.. cs:class:: [Serializable] public class CyclesRendererNode
    
    
    .. cs:property:: public Transform Transform { get; set; }
        
        :returns UnityEngine.Transform: 
        
    .. cs:method:: public void Render(Camera camera)
        
        :param UnityEngine.Camera camera: 
        
