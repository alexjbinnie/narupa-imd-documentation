VisualisationComponent.InputPropertyLink
========================================
.. cs:setscope:: Narupa.Visualisation.Components

.. cs:class:: [Serializable] public class InputPropertyLink
    
    Describes a link from one visualisation node to another
    
    

    
    .. cs:field:: [SerializeField] public Object sourceComponent
        
        The visualisation component that contains the node with the output field
        
        

        :returns UnityEngine.Object: 
        
    .. cs:field:: [SerializeField] public string sourceFieldName
        
        The name of the output field
        
        

        :returns System.String: 
        
    .. cs:field:: [SerializeField] public string destinationFieldName
        
        The name of the input field
        
        

        :returns System.String: 
        
