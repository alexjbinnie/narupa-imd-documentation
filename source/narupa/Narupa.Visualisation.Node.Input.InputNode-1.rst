InputNode<TProperty>
====================
.. cs:setscope:: Narupa.Visualisation.Node.Input

.. cs:class:: [Serializable] public abstract class InputNode<TProperty> : IInputNode where TProperty : Property, new()
    
    Generic input for the visualisation system that provides some value with a
    given key.
    
    

    :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode`
    
    .. cs:property:: IProperty IInputNode.Input { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode.Input`
        :returns Narupa.Visualisation.Property.IProperty: 
        
    .. cs:property:: public Type InputType { get; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode.InputType`
        :returns System.Type: 
        
    .. cs:property:: public TProperty Input { get; }
        
        :returns {TProperty}: 
        
    .. cs:property:: public string Name { get; set; }
        
        :implements: :cs:any:`~Narupa.Visualisation.Node.Input.IInputNode.Name`
        :returns System.String: 
        
