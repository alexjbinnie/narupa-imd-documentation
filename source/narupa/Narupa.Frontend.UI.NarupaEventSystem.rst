NarupaEventSystem
=================
.. cs:setscope:: Narupa.Frontend.UI

.. cs:class:: public class NarupaEventSystem : EventSystem
    
    Override for :cs:any:`~UnityEngine.EventSystems.EventSystem` so that losing application focus
    does not affect the UI.
    
    

    :inherits: :cs:any:`~UnityEngine.EventSystems.EventSystem`
    
    .. cs:method:: protected override void OnApplicationFocus(bool hasFocus)
        
        :param System.Boolean hasFocus: 
        
