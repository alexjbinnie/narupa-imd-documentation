InteractionsUpdate
==================
.. cs:setscope:: Narupa.Protocol.Imd

.. cs:class:: public sealed class InteractionsUpdate : Object, IMessage<InteractionsUpdate>, IMessage, IEquatable<InteractionsUpdate>, IDeepCloneable<InteractionsUpdate>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Imd.InteractionsUpdate}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Imd.InteractionsUpdate}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Imd.InteractionsUpdate}`
    
    .. cs:field:: public const int UpdatedInteractionsFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int RemovalsFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public InteractionsUpdate()
        
        
    .. cs:constructor:: public InteractionsUpdate(InteractionsUpdate other)
        
        :param Narupa.Protocol.Imd.InteractionsUpdate other: 
        
    .. cs:method:: public InteractionsUpdate Clone()
        
        :returns Narupa.Protocol.Imd.InteractionsUpdate: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(InteractionsUpdate other)
        
        :param Narupa.Protocol.Imd.InteractionsUpdate other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(InteractionsUpdate other)
        
        :param Narupa.Protocol.Imd.InteractionsUpdate other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<InteractionsUpdate> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Imd.InteractionsUpdate}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public RepeatedField<ParticleInteraction> UpdatedInteractions { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{Narupa.Protocol.Imd.ParticleInteraction}: 
        
    .. cs:property:: public RepeatedField<string> Removals { get; }
        
        :returns Google.Protobuf.Collections.RepeatedField{System.String}: 
        
