ColorPulserNode
===============
.. cs:setscope:: Narupa.Visualisation.Node.Color

.. cs:class:: [Serializable] public class ColorPulserNode
    
    Applies an oscillating color change to a subset of particles.
    
    

    
    .. cs:property:: public bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: public void Refresh()
        
        
