InstanceServiceReflection
=========================
.. cs:setscope:: Narupa.Protocol.Instance

.. cs:class:: public static class InstanceServiceReflection : Object
    
    
    .. cs:property:: public static FileDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.FileDescriptor: 
        
