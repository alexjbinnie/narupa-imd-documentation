AcquireLockRequest
==================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class AcquireLockRequest : Object, IMessage<AcquireLockRequest>, IMessage, IEquatable<AcquireLockRequest>, IDeepCloneable<AcquireLockRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.AcquireLockRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.AcquireLockRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.AcquireLockRequest}`
    
    .. cs:field:: public const int PlayerIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int ResourceIdFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:field:: public const int TimeoutDurationFieldNumber = 3
        
        :returns System.Int32: 
        
    .. cs:constructor:: public AcquireLockRequest()
        
        
    .. cs:constructor:: public AcquireLockRequest(AcquireLockRequest other)
        
        :param Narupa.Protocol.Multiplayer.AcquireLockRequest other: 
        
    .. cs:method:: public AcquireLockRequest Clone()
        
        :returns Narupa.Protocol.Multiplayer.AcquireLockRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(AcquireLockRequest other)
        
        :param Narupa.Protocol.Multiplayer.AcquireLockRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(AcquireLockRequest other)
        
        :param Narupa.Protocol.Multiplayer.AcquireLockRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<AcquireLockRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.AcquireLockRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string PlayerId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public string ResourceId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public float TimeoutDuration { get; set; }
        
        :returns System.Single: 
        
