RemoveResourceKeyRequest
========================
.. cs:setscope:: Narupa.Protocol.Multiplayer

.. cs:class:: public sealed class RemoveResourceKeyRequest : Object, IMessage<RemoveResourceKeyRequest>, IMessage, IEquatable<RemoveResourceKeyRequest>, IDeepCloneable<RemoveResourceKeyRequest>
    
    :implements: :cs:any:`~Google.Protobuf.IMessage{Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest}`
    :implements: :cs:any:`~Google.Protobuf.IMessage`
    :implements: :cs:any:`~System.IEquatable{Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest}`
    :implements: :cs:any:`~Google.Protobuf.IDeepCloneable{Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest}`
    
    .. cs:field:: public const int PlayerIdFieldNumber = 1
        
        :returns System.Int32: 
        
    .. cs:field:: public const int ResourceIdFieldNumber = 2
        
        :returns System.Int32: 
        
    .. cs:constructor:: public RemoveResourceKeyRequest()
        
        
    .. cs:constructor:: public RemoveResourceKeyRequest(RemoveResourceKeyRequest other)
        
        :param Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest other: 
        
    .. cs:method:: public RemoveResourceKeyRequest Clone()
        
        :returns Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest: 
        
    .. cs:method:: public override bool Equals(object other)
        
        :param System.Object other: 
        :returns System.Boolean: 
        
    .. cs:method:: public bool Equals(RemoveResourceKeyRequest other)
        
        :param Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest other: 
        :returns System.Boolean: 
        
    .. cs:method:: public override int GetHashCode()
        
        :returns System.Int32: 
        
    .. cs:method:: public override string ToString()
        
        :returns System.String: 
        
    .. cs:method:: public void WriteTo(CodedOutputStream output)
        
        :param Google.Protobuf.CodedOutputStream output: 
        
    .. cs:method:: public int CalculateSize()
        
        :returns System.Int32: 
        
    .. cs:method:: public void MergeFrom(RemoveResourceKeyRequest other)
        
        :param Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest other: 
        
    .. cs:method:: public void MergeFrom(CodedInputStream input)
        
        :param Google.Protobuf.CodedInputStream input: 
        
    .. cs:property:: public static MessageParser<RemoveResourceKeyRequest> Parser { get; }
        
        :returns Google.Protobuf.MessageParser{Narupa.Protocol.Multiplayer.RemoveResourceKeyRequest}: 
        
    .. cs:property:: public static MessageDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.MessageDescriptor: 
        
    .. cs:property:: public string PlayerId { get; set; }
        
        :returns System.String: 
        
    .. cs:property:: public string ResourceId { get; set; }
        
        :returns System.String: 
        
