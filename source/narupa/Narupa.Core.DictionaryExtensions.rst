DictionaryExtensions
====================
.. cs:setscope:: Narupa.Core

.. cs:class:: public static class DictionaryExtensions
    
    Useful extensions for using dictionaries.
    
    

    
    .. cs:method:: public static void Deconstruct<TKey, TValue>(this KeyValuePair<TKey, TValue> kvp, out TKey key, out TValue value)
        
        Deconstruct method for :cs:any:`~System.Collections.Generic.KeyValuePair`2`, so
        dictionaries can be used with tuples.
        
        

        :param System.Collections.Generic.KeyValuePair{{TKey},{TValue}} kvp: 
        :param {TKey} key: 
        :param {TValue} value: 
        
    .. cs:method:: public static bool TryGetValue<T>(this IDictionary<string, object> dictionary, string key, out T value)
        
        Try to get a value out of a dictionary of type T, returning false if the key is not present or the present value is of the wrong type.
        
        

        :param System.Collections.Generic.IDictionary{System.String,System.Object} dictionary: 
        :param System.String key: 
        :param {T} value: 
        :returns System.Boolean: 
        
    .. cs:method:: public static T GetValueOrDefault<T>(this IDictionary<string, object> dictionary, string id, T defaultValue = null)
        
        Get an item of the dictionary of the given type :code:`T`,
        returning the default if the key is not present and throwing an exception if
        there is a value for the given key, but the type is incompatible.
        
        

        :param System.Collections.Generic.IDictionary{System.String,System.Object} dictionary: 
        :param System.String id: 
        :param {T} defaultValue: 
        :returns {T}: 
        
    .. cs:method:: public static T[] GetArrayOrEmpty<T>(this IDictionary<string, object> dictionary, string id)
        
        Get an array in the dictionary of the given type :code:`T`
        returning an empty array if the key is not present and throwing an exception if
        there is an array for the given key, but the type is incompatible.
        
        

        :param System.Collections.Generic.IDictionary{System.String,System.Object} dictionary: 
        :param System.String id: 
        :returns {T}[]: 
        
