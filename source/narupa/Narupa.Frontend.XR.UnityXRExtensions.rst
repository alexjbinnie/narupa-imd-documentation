UnityXRExtensions
=================
.. cs:setscope:: Narupa.Frontend.XR

.. cs:class:: public static class UnityXRExtensions
    
    Extensions for Unity's XR system, in which you make queries about
    XRNode types (e.g LeftHand, TrackingReference, etc) and receive
    XRNodeState objects containing identifier and tracking information
    for that XR node.
    
    

    
    .. cs:method:: public static IEnumerable<XRNodeState> GetNodeStates(this XRNode nodeType)
        
        Get all XRNodeState for a given XRNode type.
        
        

        :param UnityEngine.XR.XRNode nodeType: 
        :returns System.Collections.Generic.IEnumerable{UnityEngine.XR.XRNodeState}: 
        
    .. cs:method:: public static XRNodeState? GetSingleNodeState(this XRNode nodeType)
        
        Get the XRNodeState for a given XRNode type, if available.
        
        

        :param UnityEngine.XR.XRNode nodeType: 
        :returns System.Nullable{UnityEngine.XR.XRNodeState}: 
        :throws System.InvalidOperationException: Thrown when there are multiple nodes of this type.
        
    .. cs:method:: public static Transformation? GetPose(this XRNodeState node)
        
        Return the node state's pose matrix, if available.
        
        

        :param UnityEngine.XR.XRNodeState node: 
        :returns System.Nullable{Narupa.Core.Math.Transformation}: 
        
    .. cs:method:: public static Transformation? GetSinglePose(this XRNode nodeType)
        
        Return the pose matrix for a given XRNode type, if available.
        
        

        :param UnityEngine.XR.XRNode nodeType: 
        :returns System.Nullable{Narupa.Core.Math.Transformation}: 
        :throws System.InvalidOperationException: Thrown when there are multiple nodes of this type
        
    .. cs:method:: public static IPosedObject WrapAsPosedObject(this XRNode nodeType)
        
        :param UnityEngine.XR.XRNode nodeType: 
        :returns Narupa.Frontend.Input.IPosedObject: 
        
    .. cs:property:: public static IReadOnlyList<XRNodeState> NodeStates { get; }
        
        Get all the states for tracked XR objects from Unity's XR system.
        
        

        :returns System.Collections.Generic.IReadOnlyList{UnityEngine.XR.XRNodeState}: 
        
