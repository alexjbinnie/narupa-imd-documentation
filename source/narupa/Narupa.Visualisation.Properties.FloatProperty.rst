FloatProperty
=============
.. cs:setscope:: Narupa.Visualisation.Properties

.. cs:class:: [Serializable] public class FloatProperty : SerializableProperty<float>, IProperty<float>, IReadOnlyProperty<float>, IProperty, IReadOnlyProperty
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for a :cs:any:`~System.Single` value.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Property.SerializableProperty{System.Single}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{System.Single}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{System.Single}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    
