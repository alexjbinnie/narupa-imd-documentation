BondArrayProperty
=================
.. cs:setscope:: Narupa.Visualisation.Properties.Collections

.. cs:class:: [Serializable] public class BondArrayProperty : ArrayProperty<BondPair>, IProperty<BondPair[]>, IReadOnlyProperty<BondPair[]>, IProperty, IReadOnlyProperty, IEnumerable<BondPair>, IEnumerable
    
    Serializable :cs:any:`~Narupa.Visualisation.Property` for an array of :cs:any:`~Narupa.Frame.BondPair`
    values.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Properties.Collections.ArrayProperty{Narupa.Frame.BondPair}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty{Narupa.Frame.BondPair[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty{Narupa.Frame.BondPair[]}`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IProperty`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IReadOnlyProperty`
    :implements: :cs:any:`~System.Collections.Generic.IEnumerable{Narupa.Frame.BondPair}`
    :implements: :cs:any:`~System.Collections.IEnumerable`
    
