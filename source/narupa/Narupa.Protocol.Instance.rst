Narupa.Protocol.Instance
========================
.. cs:namespace:: Narupa.Protocol.Instance

.. toctree::
    :maxdepth: 4
    
    Narupa.Protocol.Instance.CloseInstanceRequest
    Narupa.Protocol.Instance.CloseInstanceResponse
    Narupa.Protocol.Instance.GetInstanceInfoRequest
    Narupa.Protocol.Instance.GetInstanceInfoResponse
    Narupa.Protocol.Instance.InstanceService
    Narupa.Protocol.Instance.InstanceService.InstanceServiceBase
    Narupa.Protocol.Instance.InstanceService.InstanceServiceClient
    Narupa.Protocol.Instance.InstanceServiceReflection
    Narupa.Protocol.Instance.LoadTrajectoryRequest
    Narupa.Protocol.Instance.LoadTrajectoryResponse
    Narupa.Protocol.Instance.RepresentationService
    Narupa.Protocol.Instance.RepresentationService.RepresentationServiceBase
    Narupa.Protocol.Instance.RepresentationService.RepresentationServiceClient
    Narupa.Protocol.Instance.RepresentationServiceReflection
    Narupa.Protocol.Instance.SetRepresentationRequest
    Narupa.Protocol.Instance.SetRepresentationResponse
    
    
