FrameConversions
================
.. cs:setscope:: Narupa.Grpc.Frame

.. cs:class:: public static class FrameConversions
    
    Conversion methods for changing between Frame-specific GRPC values and C#
    objects.
    
    

    
    .. cs:method:: public static BondPair[] ToBondPairArray(this ValueArray valueArray)
        
        Convert a protobuf :cs:any:`~Narupa.Protocol.ValueArray` to an array of
        :cs:any:`~Narupa.Frame.BondPair`.
        
        

        :param Narupa.Protocol.ValueArray valueArray: 
        :returns Narupa.Frame.BondPair[]: 
        
    .. cs:method:: public static Element[] ToElementArray(this ValueArray valueArray)
        
        Convert a protobuf :cs:any:`~Narupa.Protocol.ValueArray` to an array of
        :cs:any:`~Narupa.Core.Science.Element`.
        
        

        :param Narupa.Protocol.ValueArray valueArray: 
        :returns Narupa.Core.Science.Element[]: 
        
    .. cs:method:: public static LinearTransformation ToLinearTransformation(this ValueArray valueArray)
        
        Convert a protobuf :cs:any:`~Narupa.Protocol.ValueArray` to a :cs:any:`~Narupa.Core.Math.LinearTransformation`.
        
        

        :param Narupa.Protocol.ValueArray valueArray: 
        :returns Narupa.Core.Math.LinearTransformation: 
        
