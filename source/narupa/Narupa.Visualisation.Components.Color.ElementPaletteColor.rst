ElementPaletteColor
===================
.. cs:setscope:: Narupa.Visualisation.Components.Color

.. cs:class:: public sealed class ElementPaletteColor : VisualisationComponent<ElementColorMappingNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<ElementColorMappingNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Color.ElementColorMappingNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Color.ElementColorMappingNode}`
    
