Command
=======
.. cs:setscope:: Narupa.Protocol.Command

.. cs:class:: public static class Command : Object
    
    
    .. cs:method:: public static ServerServiceDefinition BindService(Command.CommandBase serviceImpl)
        
        :param Narupa.Protocol.Command.Command.CommandBase serviceImpl: 
        :returns Grpc.Core.ServerServiceDefinition: 
        
    .. cs:method:: public static void BindService(ServiceBinderBase serviceBinder, Command.CommandBase serviceImpl)
        
        :param Grpc.Core.ServiceBinderBase serviceBinder: 
        :param Narupa.Protocol.Command.Command.CommandBase serviceImpl: 
        
    .. cs:property:: public static ServiceDescriptor Descriptor { get; }
        
        :returns Google.Protobuf.Reflection.ServiceDescriptor: 
        
