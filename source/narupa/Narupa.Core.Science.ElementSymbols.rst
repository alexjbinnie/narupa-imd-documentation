ElementSymbols
==============
.. cs:setscope:: Narupa.Core.Science

.. cs:class:: public static class ElementSymbols
    
    Mapping between atomic elements and their symbols.
    
    

    
    .. cs:method:: public static string GetSymbol(this Element element)
        
        Get the atomic symbol of the element.
        
        

        :param Narupa.Core.Science.Element element: 
        :returns System.String: 
        
    .. cs:method:: public static Element? GetFromSymbol(string symbol)
        
        Get an atomic element based on its atomic symbol.
        
        

        :param System.String symbol: 
        :returns System.Nullable{Narupa.Core.Science.Element}: 
        
