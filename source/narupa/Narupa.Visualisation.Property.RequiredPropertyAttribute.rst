RequiredPropertyAttribute
=========================
.. cs:setscope:: Narupa.Visualisation.Property

.. cs:class:: public class RequiredPropertyAttribute : Attribute
    
    Indicates a :cs:any:`~Narupa.Visualisation.Property.Property` is required. This does not enforce
    this, merely indicates in the Editor using an error box.
    
    

    :inherits: :cs:any:`~System.Attribute`
    
