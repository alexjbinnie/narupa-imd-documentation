ReflectionUtility
=================
.. cs:setscope:: Narupa.Core

.. cs:class:: public static class ReflectionUtility
    
    Utility methods for reflection not included in C#.
    
    

    
    .. cs:method:: public static FieldInfo GetFieldInSelfOrParents(this Type type, string name, BindingFlags flags)
        
        Get the field in a given type or its ancestors of the given name, with the
        given :cs:any:`~System.Reflection.BindingFlags`.
        
        

        :param System.Type type: 
        :param System.String name: 
        :param System.Reflection.BindingFlags flags: 
        :returns System.Reflection.FieldInfo: 
        
    .. cs:method:: public static IEnumerable<FieldInfo> GetFieldsInSelfOrParents(this Type type, BindingFlags flags)
        
        Get all fields in a given type and its ancestors, with the given
        :cs:any:`~System.Reflection.BindingFlags`.
        
        

        :param System.Type type: 
        :param System.Reflection.BindingFlags flags: 
        :returns System.Collections.Generic.IEnumerable{System.Reflection.FieldInfo}: 
        
    .. cs:method:: public static PropertyInfo GetPropertyInSelfOrParents(this Type type, string name, BindingFlags flags)
        
        Get the property in a given type or its ancestors of the given name, with the
        given :cs:any:`~System.Reflection.BindingFlags`.
        
        

        :param System.Type type: 
        :param System.String name: 
        :param System.Reflection.BindingFlags flags: 
        :returns System.Reflection.PropertyInfo: 
        
