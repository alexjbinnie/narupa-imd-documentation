SecondaryStructureScale
=======================
.. cs:setscope:: Narupa.Visualisation.Components.Scale

.. cs:class:: public sealed class SecondaryStructureScale : VisualisationComponent<SecondaryStructureScaleNode>, ISerializationCallbackReceiver, IPropertyProvider, IVisualisationComponent<SecondaryStructureScaleNode>
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Components.VisualisationComponent{Narupa.Visualisation.Node.Scale.SecondaryStructureScaleNode}`
    :implements: :cs:any:`~UnityEngine.ISerializationCallbackReceiver`
    :implements: :cs:any:`~Narupa.Visualisation.Property.IPropertyProvider`
    :implements: :cs:any:`~Narupa.Visualisation.Components.IVisualisationComponent{Narupa.Visualisation.Node.Scale.SecondaryStructureScaleNode}`
    
