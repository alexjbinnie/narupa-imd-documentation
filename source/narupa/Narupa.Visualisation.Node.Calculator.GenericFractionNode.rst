GenericFractionNode
===================
.. cs:setscope:: Narupa.Visualisation.Node.Calculator

.. cs:class:: [Serializable] public abstract class GenericFractionNode : GenericOutputNode
    
    Calculates some 0-1 value based on indices.
    
    

    :inherits: :cs:any:`~Narupa.Visualisation.Node.GenericOutputNode`
    
    .. cs:property:: protected abstract override bool IsInputValid { get; }
        
        :returns System.Boolean: 
        
    .. cs:property:: protected abstract override bool IsInputDirty { get; }
        
        :returns System.Boolean: 
        
    .. cs:method:: protected abstract override void ClearDirty()
        
        
    .. cs:method:: protected abstract void GenerateArray(ref float[] array)
        
        :param System.Single[] array: 
        
    .. cs:method:: protected override void UpdateOutput()
        
        
    .. cs:method:: protected override void ClearOutput()
        
        
