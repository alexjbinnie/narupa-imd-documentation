DynamicMenu
===========
.. cs:setscope:: Narupa.Frontend.UI

.. cs:class:: public class DynamicMenu : MonoBehaviour
    
    A manager for a element which can display a set of options as buttons, such as a radial menu.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:method:: public void AddItem(string name, Sprite icon, Action callback, string subtext = null)
        
        :param System.String name: 
        :param UnityEngine.Sprite icon: 
        :param System.Action callback: 
        :param System.String subtext: 
        
    .. cs:method:: public void ClearChildren()
        
        
