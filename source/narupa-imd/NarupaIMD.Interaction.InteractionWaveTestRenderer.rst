InteractionWaveTestRenderer
===========================
.. cs:setscope:: NarupaIMD.Interaction

.. cs:class:: public class InteractionWaveTestRenderer : MonoBehaviour
    
    Manage instances of InteractionWaveRenderer so that all known 
    interactions are rendered using Mike's pretty sine wave method from 
    Narupa 1
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
