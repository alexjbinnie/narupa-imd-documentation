VisualiserFactory
=================
.. cs:setscope:: NarupaIMD.Selection

.. cs:class:: public static class VisualiserFactory
    
    Construction methods for creating visualisers from nested data structures.
    
    

    
    .. cs:method:: public static bool TryParseElement(object value, out Element element)
        
        Parse an element from either a string symbol or an integer atomic number.
        
        

        :param System.Object value: 
        :param Narupa.Core.Science.Element element: 
        :returns System.Boolean: 
        
    .. cs:method:: public static bool TryParseColor(object value, out Color color)
        
        Attempt to parse a color, from a name, hex code or array of rgba values.
        
        

        :param System.Object value: 
        :param UnityEngine.Color color: 
        :returns System.Boolean: 
        
    .. cs:method:: public static GameObject GetPredefinedVisualiser(string name)
        
        Get a prefab of a predefined visualiser with the given name.
        
        

        :param System.String name: 
        :returns UnityEngine.GameObject: 
        
    .. cs:method:: public static GameObject GetRenderSubgraph(string name)
        
        Get a visualisation subgraph which is responsible for rendering information.
        
        

        :param System.String name: 
        :returns UnityEngine.GameObject: 
        
    .. cs:method:: public static GameObject GetColorSubgraph(string name)
        
        Get a visualisation subgraph which is responsible for coloring particles.
        
        

        :param System.String name: 
        :returns UnityEngine.GameObject: 
        
    .. cs:method:: public static GameObject GetScaleSubgraph(string name)
        
        Get a visualisation subgraph which is responsible for the scale of particles.
        
        

        :param System.String name: 
        :returns UnityEngine.GameObject: 
        
    .. cs:method:: public static GameObject GetWidthSubgraph(string name)
        
        Get a visualisation subgraph which is responsible for the width of particles in splines.
        
        

        :param System.String name: 
        :returns UnityEngine.GameObject: 
        
    .. cs:method:: public static GameObject GetSequenceSubgraph(string name)
        
        Get a visualisation subgraph which is responsible for calculating sequences.
        
        

        :param System.String name: 
        :returns UnityEngine.GameObject: 
        
    .. cs:method:: public static (GameObject visualiser, bool isPrefab) ConstructVisualiser(object data)
        
        Construct a visualiser from the provided arbitrary C# data.
        
        

        :param System.Object data: 
        :returns System.ValueTuple{UnityEngine.GameObject,System.Boolean}: 
        
