InteractableScene.InteractionTarget
===================================
.. cs:setscope:: NarupaIMD.Interaction

.. cs:enum:: public enum InteractionTarget
    
    
    .. cs:enummember:: Single = 0
        
        :returns NarupaIMD.Interaction.InteractableScene.InteractionTarget: 
        
    .. cs:enummember:: Residue = 1
        
        :returns NarupaIMD.Interaction.InteractableScene.InteractionTarget: 
        
