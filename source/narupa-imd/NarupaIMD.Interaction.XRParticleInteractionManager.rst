XRParticleInteractionManager
============================
.. cs:setscope:: NarupaIMD.Interaction

.. cs:class:: public class XRParticleInteractionManager : MonoBehaviour
    
    Translates XR input into interactions with particles in Narupa iMD.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
