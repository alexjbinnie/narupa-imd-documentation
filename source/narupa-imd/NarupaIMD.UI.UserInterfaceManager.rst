UserInterfaceManager
====================
.. cs:setscope:: NarupaIMD.UI

.. cs:class:: public class UserInterfaceManager : MonoBehaviour
    
    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: public GameObject SceneUI { get; }
        
        :returns UnityEngine.GameObject: 
        
    .. cs:method:: public void GotoScene(GameObject scene)
        
        :param UnityEngine.GameObject scene: 
        
    .. cs:method:: public void GotoSceneAndAddToStack(GameObject newScene)
        
        :param UnityEngine.GameObject newScene: 
        
    .. cs:method:: public void GoBack()
        
        
    .. cs:method:: public void CloseScene()
        
        
