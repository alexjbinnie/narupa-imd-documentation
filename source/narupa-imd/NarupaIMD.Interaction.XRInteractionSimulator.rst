XRInteractionSimulator
======================
.. cs:setscope:: NarupaIMD.Interaction

.. cs:class:: public class XRInteractionSimulator : MonoBehaviour
    
    Simulates two randomly moving controllers grabbing and manipulating the
    simulation space, and one randomly moving controller grabbing atoms.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
