InteractionWaveRenderer
=======================
.. cs:setscope:: NarupaIMD.Interaction

.. cs:class:: public class InteractionWaveRenderer : MonoBehaviour
    
    Renders Mike's pretty sine wave between two points. (From Narupa 1)
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:method:: public void SetPositionAndForce(Vector3 startPoint, Vector3 endPoint, float force)
        
        :param UnityEngine.Vector3 startPoint: 
        :param UnityEngine.Vector3 endPoint: 
        :param System.Single force: 
        
