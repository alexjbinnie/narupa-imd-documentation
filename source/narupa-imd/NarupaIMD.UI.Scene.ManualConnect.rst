ManualConnect
=============
.. cs:setscope:: NarupaIMD.UI.Scene

.. cs:class:: public class ManualConnect : MonoBehaviour
    
    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:method:: public void ConnectToServer()
        
        Called from the UI button to tell the application to connect
        to remote services.
        
        

        
