VisualisationLayer
==================
.. cs:setscope:: NarupaIMD.Selection

.. cs:class:: public class VisualisationLayer : MonoBehaviour
    
    A group of :cs:any:`~NarupaIMD.Selection.VisualisationSelection`s which are mutually exclusive.
    
    

    Selections within a visualisation layer display an atom precisely once,
    with the selection settings overwriting one another in the order that they are
    established.
    For example, a :cs:any:`~NarupaIMD.Selection.VisualisationLayer` with selections [1,2,3] and
    [3,4,5] will display atoms [1,2] in the first (lower) selection and atoms
    [3,4,5] in the second (upper) selection.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: public VisualisationScene Scene { get; set; }
        
        The :cs:any:`~NarupaIMD.Selection.VisualisationScene` to which this layer belongs.
        
        

        :returns NarupaIMD.Selection.VisualisationScene: 
        
    .. cs:property:: public IReadOnlyList<VisualisationSelection> Selections { get; }
        
        The set of selections that form this layer.
        
        

        :returns System.Collections.Generic.IReadOnlyList{NarupaIMD.Selection.VisualisationSelection}: 
        
    .. cs:method:: public VisualisationSelection AddSelection(ParticleSelection selection)
        
        Add a selection to this visualisation, based upon a
        :cs:any:`~NarupaIMD.Selection.ParticleSelection`.
        
        

        :param NarupaIMD.Selection.ParticleSelection selection: 
        :returns NarupaIMD.Selection.VisualisationSelection: 
        
    .. cs:method:: public void UpdateOrCreateSelection(string key, object value)
        
        Either update an existing selection with the given key or add a new selection
        with the given key.
        
        

        :param System.String key: 
        :param System.Object value: 
        
    .. cs:method:: public void RemoveSelection(string key)
        
        Remove the selection with the given key.
        
        

        :param System.String key: 
        
    .. cs:method:: public VisualisationSelection GetSelectionForParticle(int particleIndex)
        
        Find the selection on this layer which contains the particle of the given
        index.
        
        

        :param System.Int32 particleIndex: 
        :returns NarupaIMD.Selection.VisualisationSelection: 
        
