CurrentFrameBox
===============
.. cs:setscope:: NarupaIMD

.. cs:class:: public class CurrentFrameBox : MonoBehaviour
    
    Updates a :cs:any:`~Narupa.Visualisation.BoxVisualiser` with the simulation box of a
    :cs:any:`~Narupa.Frame.Frame`.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
