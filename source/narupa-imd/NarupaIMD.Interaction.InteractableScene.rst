InteractableScene
=================
.. cs:setscope:: NarupaIMD.Interaction

.. cs:class:: public class InteractableScene : MonoBehaviour, IInteractableParticles
    
    Exposes a :cs:any:`~Narupa.Visualisation.SynchronisedFrameSource` that allows particles to be grabbed, accounting for the interaction method of the selections.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    :implements: :cs:any:`~Narupa.Frontend.Manipulation.IInteractableParticles`
    
    .. cs:method:: public void SetInteractionTarget(InteractableScene.InteractionTarget target)
        
        :param NarupaIMD.Interaction.InteractableScene.InteractionTarget target: 
        
    .. cs:method:: public void SetInteractionTargetSingle()
        
        
    .. cs:method:: public void SetInteractionTargetResidue()
        
        
    .. cs:property:: public IReadOnlyProperty<int[]> InteractedParticles { get; }
        
        The set of particles which are currently being interacted with.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}: 
        
    .. cs:method:: public ActiveParticleGrab GetParticleGrab(Transformation grabberPose)
        
        Attempt to grab the nearest particle, returning null if no interaction is possible.
        
        

        :implements: :cs:any:`~Narupa.Frontend.Manipulation.IInteractableParticles.GetParticleGrab(Narupa.Core.Math.Transformation)`
        :param Narupa.Core.Math.Transformation grabberPose: The transformation of the grabbing pivot.
        :returns Narupa.Frontend.Manipulation.ActiveParticleGrab: 
        
