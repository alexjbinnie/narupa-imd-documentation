ControllerSnackBar
==================
.. cs:setscope:: NarupaIMD.UI

.. cs:class:: public class ControllerSnackBar : MonoBehaviour
    
    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:method:: public void PushNotification(string text)
        
        :param System.String text: 
        
