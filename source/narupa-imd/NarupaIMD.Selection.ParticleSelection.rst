ParticleSelection
=================
.. cs:setscope:: NarupaIMD.Selection

.. cs:class:: public class ParticleSelection
    
    A selection containing a group of particles.
    
    

    
    .. cs:property:: public string ID { get; }
        
        The unique identifier for this selection.
        
        

        :returns System.String: 
        
    .. cs:property:: public IReadOnlyList<int> Selection { get; }
        
        The set of indices that are contained in this selection. When null, this
        represents a selection containing everything.
        
        

        :returns System.Collections.Generic.IReadOnlyList{System.Int32}: 
        
    .. cs:property:: public string Name { get; }
        
        The user-facing name of this selection.
        
        

        :returns System.String: 
        
    .. cs:property:: public IDictionary<string, object> Properties { get; }
        
        A set of arbitrary properties associated with this selection.
        
        

        :returns System.Collections.Generic.IDictionary{System.String,System.Object}: 
        
    .. cs:field:: public const string KeyName = "name"
        
        :returns System.String: 
        
    .. cs:field:: public const string KeyProperties = "properties"
        
        :returns System.String: 
        
    .. cs:field:: public const string KeyId = "id"
        
        :returns System.String: 
        
    .. cs:field:: public const string KeySelected = "selected"
        
        :returns System.String: 
        
    .. cs:field:: public const string KeyParticleIds = "particle_ids"
        
        :returns System.String: 
        
    .. cs:field:: public const string KeyHideProperty = "narupa.rendering.hide"
        
        :returns System.String: 
        
    .. cs:field:: public const string KeyRendererProperty = "narupa.rendering.renderer"
        
        :returns System.String: 
        
    .. cs:field:: public const string KeyInteractionMethod = "narupa.interaction.method"
        
        :returns System.String: 
        
    .. cs:field:: public const string KeyResetVelocities = "narupa.interaction.velocity_reset"
        
        :returns System.String: 
        
    .. cs:field:: public const string InteractionMethodSingle = "single"
        
        :returns System.String: 
        
    .. cs:field:: public const string InteractionMethodGroup = "group"
        
        :returns System.String: 
        
    .. cs:field:: public const string InteractionMethodNone = "none"
        
        :returns System.String: 
        
    .. cs:field:: public const string RootSelectionId = "selection.root"
        
        :returns System.String: 
        
    .. cs:field:: public const string RootSelectionName = "Base"
        
        :returns System.String: 
        
    .. cs:field:: public const string SelectionIdPrefix = "selection."
        
        :returns System.String: 
        
    .. cs:event:: public event Action SelectionUpdated
        
        Callback for when the selection is altered.
        
        

        :returns System.Action: 
        
    .. cs:constructor:: public ParticleSelection(string id)
        
        Create a selection with the given ID, that contains all atoms.
        
        

        :param System.String id: 
        
    .. cs:constructor:: public ParticleSelection(Dictionary<string, object> obj)
        
        Create a selection from a dictionary representation of the selection.
        
        

        :param System.Collections.Generic.Dictionary{System.String,System.Object} obj: 
        
    .. cs:method:: public void UpdateFromObject(Dictionary<string, object> obj)
        
        Update this selection based upon a dictionary representation.
        
        

        :param System.Collections.Generic.Dictionary{System.String,System.Object} obj: 
        
    .. cs:method:: public static ParticleSelection CreateRootSelection()
        
        Create a selection representing the shared root selection.
        
        

        The root selection, containing all atoms, is denoted with a :code:`null` selection.
        
        

        :returns NarupaIMD.Selection.ParticleSelection: 
        
    .. cs:property:: public bool HideRenderer { get; }
        
        Should this selection not have a visualiser?
        
        

        :returns System.Boolean: 
        
    .. cs:property:: public object Renderer { get; }
        
        A string or dictionary that describes the visualiser for this selection.
        
        

        :returns System.Object: 
        
    .. cs:property:: public string InteractionMethod { get; }
        
        The type of interaction that should occur using this selection.
        
        

        :returns System.String: 
        
    .. cs:property:: public bool ResetVelocities { get; }
        
        Should the velocities be reset after this selection is interacted with.
        
        

        :returns System.Boolean: 
        
