NarupaAvatarManager
===================
.. cs:setscope:: NarupaIMD

.. cs:class:: public class NarupaAvatarManager : MonoBehaviour
    
    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:method:: public Transformation? TransformPoseCalibratedToWorld(Transformation? pose)
        
        :param System.Nullable{Narupa.Core.Math.Transformation} pose: 
        :returns System.Nullable{Narupa.Core.Math.Transformation}: 
        
    .. cs:method:: public Transformation? TransformPoseWorldToCalibrated(Transformation? pose)
        
        :param System.Nullable{Narupa.Core.Math.Transformation} pose: 
        :returns System.Nullable{Narupa.Core.Math.Transformation}: 
        
