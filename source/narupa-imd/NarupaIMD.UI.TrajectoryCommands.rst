TrajectoryCommands
==================
.. cs:setscope:: NarupaIMD.UI

.. cs:class:: public sealed class TrajectoryCommands : MonoBehaviour
    
    Component that exposes the trajectory playback commands to Unity UI
    components.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:method:: public void SendPlayCommand()
        
        
    .. cs:method:: public void SendPauseCommand()
        
        
    .. cs:method:: public void SendStepCommand()
        
        
    .. cs:method:: public void SendResetCommand()
        
        
