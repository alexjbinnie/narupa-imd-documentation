VisualisationSelection
======================
.. cs:setscope:: NarupaIMD.Selection

.. cs:class:: public class VisualisationSelection : MonoBehaviour
    
    Scene representation of a selection, which will render the selection using a
    given visualiser.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:event:: public event Action SelectionUpdated
        
        Callback for when the underlying selection has changed.
        
        

        :returns System.Action: 
        
    .. cs:property:: public IntArrayProperty FilteredIndices { get; }
        
        The indices of particles that should be rendered in this selection.
        
        

        :returns Narupa.Visualisation.Properties.IntArrayProperty: 
        
    .. cs:property:: public IntArrayProperty UnfilteredIndices { get; }
        
        The indices of particles not rendered by this or any higher selections.
        
        

        :returns Narupa.Visualisation.Properties.IntArrayProperty: 
        
    .. cs:property:: public ParticleSelection Selection { get; set; }
        
        The underlying selection that is reflected by this visualisation.
        
        

        :returns NarupaIMD.Selection.ParticleSelection: 
        
    .. cs:method:: public void CalculateFilteredIndices(VisualisationSelection upperSelection, int maxCount)
        
        Given a selection that is at a higher level in the layer, which will have drawn
        some particles, work out which particles that have not been drawn should be
        drawn by this selection and which should be left for another selection further
        down the stack.
        
        

        :param NarupaIMD.Selection.VisualisationSelection upperSelection: 
        :param System.Int32 maxCount: 
        
    .. cs:method:: public static void FilterIndices([CanBeNull] IReadOnlyCollection<int> indices, [CanBeNull] IReadOnlyList<int> filter, int maxCount, ref int[] filteredIndices, ref int[] unfilteredIndices)
        
        :param System.Collections.Generic.IReadOnlyCollection{System.Int32} indices: 
        :param System.Collections.Generic.IReadOnlyList{System.Int32} filter: 
        :param System.Int32 maxCount: 
        :param System.Int32[] filteredIndices: 
        :param System.Int32[] unfilteredIndices: 
        
    .. cs:method:: public void UpdateVisualiser()
        
        Update the visualiser based upon the data stored in the selection.
        
        

        
    .. cs:method:: public void SetVisualiser(GameObject newVisualiser, bool isPrefab = true)
        
        Set the visualiser of this selection
        
        

        :param UnityEngine.GameObject newVisualiser: 
        :param System.Boolean isPrefab: Is the argument a prefab, and hence needs instantiating?
        
