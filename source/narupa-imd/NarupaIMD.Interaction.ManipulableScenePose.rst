ManipulableScenePose
====================
.. cs:setscope:: NarupaIMD.Interaction

.. cs:class:: public class ManipulableScenePose
    
    Provides the ability to move the simulation scene, but preventing this
    if multiplayer is active and the user does not have a lock on the
    scene.
    
    

    
    .. cs:property:: public bool CurrentlyEditingScene { get; }
        
        :returns System.Boolean: 
        
    .. cs:constructor:: public ManipulableScenePose(Transform sceneTransform, MultiplayerSession multiplayer, NarupaIMDPrototype prototype)
        
        :param UnityEngine.Transform sceneTransform: 
        :param Narupa.Session.MultiplayerSession multiplayer: 
        :param NarupaIMD.NarupaIMDPrototype prototype: 
        
    .. cs:method:: public IActiveManipulation StartGrabManipulation(UnitScaleTransformation manipulatorPose)
        
        Attempt to start a grab manipulation on this box, with a 
        manipulator at the current pose.
        
        

        :param Narupa.Core.Math.UnitScaleTransformation manipulatorPose: 
        :returns Narupa.Frontend.Manipulation.IActiveManipulation: 
        
