DiscoverServers
===============
.. cs:setscope:: NarupaIMD.UI.Scene

.. cs:class:: public class DiscoverServers : MonoBehaviour
    
    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:method:: public Task SearchAsync()
        
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public void Refresh()
        
        
    .. cs:method:: public void RefreshHubs()
        
        
