PopupUserInterfaceManager
=========================
.. cs:setscope:: NarupaIMD.UI

.. cs:class:: public class PopupUserInterfaceManager : UserInterfaceManager
    
    A :cs:any:`~NarupaIMD.UI.UserInterfaceManager` that only shows the UI while a cursor is held down.
    
    

    :inherits: :cs:any:`~NarupaIMD.UI.UserInterfaceManager`
    
