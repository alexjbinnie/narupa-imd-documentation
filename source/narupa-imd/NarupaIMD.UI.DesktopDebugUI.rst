DesktopDebugUI
==============
.. cs:setscope:: NarupaIMD.UI

.. cs:class:: public class DesktopDebugUI : MonoBehaviour
    
    Unity Immediate Mode GUI for connecting, configuring, etc from the
    desktop (without needing VR).
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:field:: public float interactionForceMultiplier
        
        :returns System.Single: 
        
