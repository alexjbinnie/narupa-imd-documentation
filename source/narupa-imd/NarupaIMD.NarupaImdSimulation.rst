NarupaImdSimulation
===================
.. cs:setscope:: NarupaIMD

.. cs:class:: public class NarupaImdSimulation : MonoBehaviour
    
    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: public TrajectorySession Trajectory { get; }
        
        :returns Narupa.Grpc.Trajectory.TrajectorySession: 
        
    .. cs:property:: public ImdSession Imd { get; }
        
        :returns Narupa.Session.ImdSession: 
        
    .. cs:property:: public MultiplayerSession Multiplayer { get; }
        
        :returns Narupa.Session.MultiplayerSession: 
        
    .. cs:property:: public ManipulableScenePose ManipulableSimulationSpace { get; }
        
        The route through which simulation space can be manipulated with
        gestures to perform translation, rotation, and scaling.
        
        

        :returns NarupaIMD.Interaction.ManipulableScenePose: 
        
    .. cs:property:: public ManipulableParticles ManipulableParticles { get; }
        
        The route through which simulated particles can be manipulated with
        grabs.
        
        

        :returns Narupa.Frontend.Manipulation.ManipulableParticles: 
        
    .. cs:property:: public SynchronisedFrameSource FrameSynchronizer { get; }
        
        :returns Narupa.Visualisation.SynchronisedFrameSource: 
        
    .. cs:event:: public event Action ConnectionEstablished
        
        :returns System.Action: 
        
    .. cs:method:: public Task Connect(string address, int? trajectoryPort, int? imdPort = default(int? ), int? multiplayerPort = default(int? ))
        
        Connect to the host address and attempt to open clients for the
        trajectory and IMD services.
        
        

        :param System.String address: 
        :param System.Nullable{System.Int32} trajectoryPort: 
        :param System.Nullable{System.Int32} imdPort: 
        :param System.Nullable{System.Int32} multiplayerPort: 
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public Task Connect(ServiceHub hub)
        
        Connect to services as advertised by an ESSD service hub.
        
        

        :param Essd.ServiceHub hub: 
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public Task AutoConnect(int millisecondsTimeout = 1000)
        
        Run an ESSD search and connect to the first service found, or none
        if the timeout elapses without finding a service.
        
        

        :param System.Int32 millisecondsTimeout: 
        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public Task CloseAsync()
        
        Close all sessions.
        
        

        :returns System.Threading.Tasks.Task: 
        
    .. cs:method:: public void Disconnect()
        
        
    .. cs:method:: public void PlayTrajectory()
        
        
    .. cs:method:: public void PauseTrajectory()
        
        
    .. cs:method:: public void ResetTrajectory()
        
        
    .. cs:method:: public void ResetBox()
        
        Reset the box to the unit position.
        
        

        
