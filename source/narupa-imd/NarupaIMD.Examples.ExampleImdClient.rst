ExampleImdClient
================
.. cs:setscope:: NarupaIMD.Examples

.. cs:class:: public sealed class ExampleImdClient : MonoBehaviour
    
    Example Interactive Molecular Dynamics client. Connects to a remote
    trajectory and imd service and visualises the particles.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: public TrajectorySession TrajectorySession { get; }
        
        Current :cs:any:`~NarupaIMD.Examples.ExampleImdClient.TrajectorySession`.
        
        

        :returns Narupa.Grpc.Trajectory.TrajectorySession: 
        
    .. cs:property:: public ImdSession InteractiveSession { get; }
        
        Current :cs:any:`~NarupaIMD.Examples.ExampleImdClient.TrajectorySession`.
        
        

        :returns Narupa.Session.ImdSession: 
        
