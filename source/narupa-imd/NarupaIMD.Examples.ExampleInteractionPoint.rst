ExampleInteractionPoint
=======================
.. cs:setscope:: NarupaIMD.Examples

.. cs:class:: public sealed class ExampleInteractionPoint : MonoBehaviour
    
    Example component that applies interaction forces to an IMD simulation
    via the :cs:any:`~NarupaIMD.Examples.ExampleImdClient`.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
