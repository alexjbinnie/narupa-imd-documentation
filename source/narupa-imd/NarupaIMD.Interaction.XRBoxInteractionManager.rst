XRBoxInteractionManager
=======================
.. cs:setscope:: NarupaIMD.Interaction

.. cs:class:: public class XRBoxInteractionManager : MonoBehaviour
    
    Translates XR input into interactions the box in Narupa iMD.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
