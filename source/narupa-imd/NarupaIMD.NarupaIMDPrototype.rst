NarupaIMDPrototype
==================
.. cs:setscope:: NarupaIMD

.. cs:class:: [DisallowMultipleComponent] public sealed class NarupaIMDPrototype : MonoBehaviour
    
    The entry point to the application, and central location for accessing
    shared resources.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: public NarupaImdSimulation Simulation { get; }
        
        :returns NarupaIMD.NarupaImdSimulation: 
        
    .. cs:property:: public PhysicallyCalibratedSpace CalibratedSpace { get; }
        
        :returns Narupa.Frontend.XR.PhysicallyCalibratedSpace: 
        
    .. cs:method:: public void Connect(string address, int? trajectoryPort = default(int? ), int? imdPort = default(int? ), int? multiplayerPort = default(int? ))
        
        Connect to remote Narupa services.
        
        

        :param System.String address: 
        :param System.Nullable{System.Int32} trajectoryPort: 
        :param System.Nullable{System.Int32} imdPort: 
        :param System.Nullable{System.Int32} multiplayerPort: 
        
    .. cs:method:: public void Connect(ServiceHub hub)
        
        Connect to the Narupa services described in a given ServiceHub.
        
        

        :param Essd.ServiceHub hub: 
        
    .. cs:method:: public void AutoConnect()
        
        Connect to the first set of Narupa services found via ESSD.
        
        

        
    .. cs:method:: public void Disconnect()
        
        Disconnect from all Narupa services.
        
        

        
    .. cs:method:: public void Quit()
        
        Called from UI to quit the application.
        
        

        
