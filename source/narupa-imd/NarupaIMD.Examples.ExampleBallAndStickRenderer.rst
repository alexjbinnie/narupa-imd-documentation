ExampleBallAndStickRenderer
===========================
.. cs:setscope:: NarupaIMD.Examples

.. cs:class:: public sealed class ExampleBallAndStickRenderer : MonoBehaviour
    
    Example of how to use the IndirectMeshDrawCommand for rendering particles.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
