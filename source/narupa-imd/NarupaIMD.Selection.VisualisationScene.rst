VisualisationScene
==================
.. cs:setscope:: NarupaIMD.Selection

.. cs:class:: public class VisualisationScene : MonoBehaviour
    
    A set of layers and selections that are used to render a frame using multiple
    visualisers.
    
    

    It contains a :cs:any:`~NarupaIMD.Selection.VisualisationScene.FrameAdaptor` to which each visualiser will be
    linked.
    
    

    :inherits: :cs:any:`~UnityEngine.MonoBehaviour`
    
    .. cs:property:: public IReadOnlyProperty<int[]> InteractedParticles { get; }
        
        The set of particles which are currently being interacted with.
        
        

        :returns Narupa.Visualisation.Property.IReadOnlyProperty{System.Int32[]}: 
        
    .. cs:property:: public FrameAdaptor FrameAdaptor { get; }
        
        The :cs:any:`~NarupaIMD.Selection.VisualisationScene.FrameAdaptor` that exposes all the data present in the frame
        in a way that is compatible with the visualisation system.
        
        

        :returns Narupa.Visualisation.Components.Adaptor.FrameAdaptor: 
        
    .. cs:property:: public int ParticleCount { get; }
        
        The number of particles in the current frame, or 0 if no frame is present.
        
        

        :returns System.Int32: 
        
    .. cs:method:: public VisualisationLayer AddLayer(string name)
        
        Create a visualisation layer with the given name.
        
        

        :param System.String name: 
        :returns NarupaIMD.Selection.VisualisationLayer: 
        
    .. cs:method:: public VisualisationSelection GetSelectionForParticle(int particleIndex)
        
        Get the selection in the base layer which contains the particle.
        
        

        :param System.Int32 particleIndex: 
        :returns NarupaIMD.Selection.VisualisationSelection: 
        
